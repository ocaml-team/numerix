(* file exemples/ocaml/shanks.ml: square root modulo an odd prime
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |            Racine carr�e modulo p, algorithme de Shanks               |
 |                                                                       |
 +-----------------------------------------------------------------------*)

open Numerix
open Printf
module Main(E:Int_type) = struct
  module I = Infixes(E)
  open E
  open I
  let ( %% ) = gmod Nearest_up

                         (* +------------------------+
                            |  Symbole de Kronecker  |
                            +------------------------+ *)
  
  let rec kronecker =
  
    (* d�compose a = 2^i*impair *)
    let rec decompose a i =
      if nth_bit a i then (i,a>>i) else decompose a (i+1) in
  
    (*
      Algorithme d'Euclide + r�ciprocit� quadratique
      s <=> le nb de changements de signes pair
      a est de parit� ind�termin�e, b est impair
    *)
    let rec loop a b s =
  
      (* (0/1) = 1, (0/b) = 0 si b > 0 *)
      if a =. 0 then if (b <>. 1) then 0 else match s with
      | true  ->  1
      | false -> -1
  
      else
  
        (* i <- valuation 2-adique de a, a <- a/2^i *)
        let (i,a) = decompose a 0 in
  
        (* (2^i/b) = -1 ssi i est impair et b = 3 ou 5 mod 8 *)
        let s = s <> ((i land 1 = 1) & ((((nth_word b 0) + 2) land 7) > 4)) in
  
        (* (a/b) = (b/a) si a ou b = 1 mod 4, -(b/a) sinon *)
        let s = s <> ((nth_bit a 1) & (nth_bit b 1)) in
  
        (* c <- b mod a, division centr�e *)
        let c = b %% a in
  
        (* (-1/a) = 1 ssi a = 1 mod 4 *)
        if sgn(c) >= 0 then loop c a s
                       else loop (neg c) a (s <> (nth_bit a 1))
  
    in
  
    (* a = naturel quelconque, b = naturel impair, retourne (a/b) *)
    fun a b -> loop a b true
  
  
  
  
                        (* +---------------------------+
                           |  Racine carr�e modulaire  |
                           +---------------------------+ *)
  
  let sqrtmod =
  
    (* d�compose p = 1 + 2^k*q *)
    let rec decompose p k =
      if (nth_bit p k) then k,(p>>k) else decompose p (k+1)
    in
  
    (* cherche un �l�ment d'ordre 2^k modulo p *)
    let rec generator p q =
      let x = nrandom (nbits p) in
      if kronecker x p < 0 then powmod x q p else generator p q
    in
  
    (* d�termine l'ordre de x modulo p, il doit �tre inf�rieur � 2^k *)
    let rec ordre x p k l =
      if l > k then begin
        printf "internal error, l > k\n";
        flush stdout;
        exit 1
      end
      else if x =. 1 then l
      else ordre (sqr(x) %% p) p k (l+1)
    in
  
    (* calcule x^(2^i) mod p *)
    let rec ith_square x p i =
      if i = 0 then x else ith_square (sqr(x) %% p) p (i-1)
    in
  
    (* x est d'ordre 2^l (� d�terminer) avec 1 <= l < k
       y est d'ordre 2^k
       retourne b * (racine carr�e de x) modulo p *)
    let rec complete_sqrt b x y p k =
  
      (* ordre(x) = 2^l *)
      let l = ordre x p k 0 in
  
      (* b <- b*y^(2^(k-l-1))
         x <- x*y^(2^(k-l)), 
         y <- y^(2^(k-l))
         donc b^2 = a*x mod p, ordre(y) = 2^l et ordre(x) < 2^l *)
  
      let y = ith_square y p (k-l-1) in
      let b = (b ** y) %% p in
      let y = sqr(y)   %% p in
      let x = (x ** y) %% p in
  
      if x <>. 1 then complete_sqrt b x y p l else b
    in
  
    (* p = premier impair, a = b^2 mod p, retourne b (compris entre 0 et p/2) *)
    fun a p ->
  
      let k,q = decompose p 1 in
  
      (* b <- a^(q+1)/2 mod p, x <- a^q mod p (donc b^2 = a*x mod p) *)
      let x = powmod a (q >> 1) p in
      let b = (a ** x) %% p in
      let x = (x ** b) %% p in
  
      (* compl�te la racine carr�e si x > 1 *)
      let b = if x =. 1 then b else complete_sqrt b x (generator p q) p k in
  
      (* r�duit � l'intervalle [0,p/2] *)
      abs(b %% p)
  
  
                          (* +-----------------------+
                             |  Fonction principale  |
                             +-----------------------+ *)
  
  let main arglist =
  
      let help = ref false
      and test = ref false
      and p_ok = ref false
      and a_ok = ref false
      and n    = ref 200
      and p    = make_ref(zero)
      and a    = make_ref(zero)
  
      and test_p = "100000000000000000000000000000000000133"
      and test_a = "123456"
      and test_b = "36705609512061164177523976477230978260"
      in
  
      (* d�code les arguments *)
      let rec parse = function
        | "-p"::x::s    -> of_string_in p x; p_ok := true; parse s
        | "-a"::x::s    -> of_string_in a x; a_ok := true; parse s
        | "-bits"::x::s -> n := int_of_string(x);          parse s
        | "-test"::s    -> of_string_in p test_p; p_ok := true;
                           of_string_in a test_a; a_ok := true;
                           test := true;
                           parse s
        | []            -> ()
        | _             -> help := true
      in parse (List.tl arglist);
  
      if !help
      then printf "usage: %s [-p <odd prime>] [-a <quadres mod p>] [-test] [-bits n]\n" (List.hd arglist)
      else begin

        random_init(0);
  
        (* si p est d�fini, v�rifie qu'il est premier impair.
           sinon tire un nombre premier impair au hasard *)
        if begin
          if !p_ok then begin
            if not(nth_bit (look p) 0) or (isprime (look p) = False)
            then begin
              printf("p is not an odd prime\n");
              false
            end
            else true
          end
          else begin
            nrandom1_in p !n;
            if not(nth_bit (look p) 0) then p +=. 1;
            while (p +=. 2; isprime (look p) = False) do () done;
            printf "p = %s\n" (string_of (look p));
            true
          end
        end
  
        (* si a est d�fini, v�rifie que c'est un carr� non nul modulo p.
           sinon tire un r�sidu quadratique au hasard.*)
        & begin
          if !a_ok then begin
            if kronecker (look a) (look p) < 1 then begin
              printf("kronecker(a,p) != 1\n");
              false
            end
            else true
          end
          else begin
            nrandom_in a !n;
            while (a +=. 1; kronecker (look a) (look p) < 1) do () done;
            printf "a = %s\n" (string_of (look a));
            true
          end
        end
  
        (* calcule la racine carr�e *)
        then begin
          let b = sqrtmod (look a) (look p) in
          let s = string_of(b) in
          if !test then
              if s = test_b then printf "%s\t%s\ttest ok\n" (List.hd arglist) (name())
              else               printf "error in the %s %s test\n" (List.hd arglist) (name())
          else                   printf "b = %s\n" s;
        end
  
      end;
      flush stdout
  
end
let _ = let module S = Start(Main) in S.start()

