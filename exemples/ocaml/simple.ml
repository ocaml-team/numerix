(* file exemples/ocaml/simple.ml: simple demo of Numerix
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                    Exemple d'utilisation de Numerix                   |
 |                                                                       |
 +-----------------------------------------------------------------------*)

(* compute  (sqrt(3) + sqrt(2))/(sqrt(3)-sqrt(2)) with n digits *)

open Numerix
open Printf
module Main(E:Int_type) = struct
  module I = Infixes(E)
  open E
  open I


  (* compute the quotient with n digits *)
  let compute(n) =
  
    (* d <- 10^n, d2 <- 10^(2n) *)
    let d  = (5 ^. n) << n  in
    let d2 = sqr d          in
  
    (* a <- round(sqrt(2*10^(2n+2))), b <- round(sqrt(3*10^(2n+2)))  *)
    let a = gsqrt Nearest_up (d2 *. 200) in
    let b = gsqrt Nearest_up (d2 *. 300) in
  
    (* return round(10^n*(b+a)/(b-a)) *)
    gquo Nearest_up (d**(b++a)) (b--a)

  
  (* user interface *)
  let main arglist =
  
    let test_n = 30
    and test_r = "9898979485566356196394568149411" in
  
    let rec parse (n,test) = function
    | []         -> (n,test)
    | "-test"::s -> (test_n,true)
    | "-n"::x::s -> parse (int_of_string x, test) s
    | _          -> printf "usage: %s [-test] [-n <digits>]\n" (List.hd arglist);
                    flush stdout;
                     exit(1)
    in
    let (n,test) = parse (test_n,false) (List.tl arglist) in
  
    let s = string_of (compute n) in
    if test then
        if s = test_r then printf "%s\t%s\ttest ok\n"  (List.hd arglist) (name())
        else               printf "error in the %s %s test\n"  (List.hd arglist) (name())
    else printf "r=%s\n" s;
    
    (*
      better way : (sqrt(3) + sqrt(2))/(sqrt(3)-sqrt(2)) = 5 + sqrt(24)
      so we compute sqrt(24) with n digits, transform into a string
      and add 5 to the first digit (is this cheating ?)
    *)
  
    if false then begin
      let r = gsqrt Nearest_up (((5 ^. (2*n)) << (2*n)) *. 24) in
      let s = string_of(r) in
      s.[0] <- char_of_int( int_of_char(s.[0]) + 5);
      printf "r=%s\n" s
    end;
  
    flush stdout

end
let _ = let module S = Start(Main) in S.start()
