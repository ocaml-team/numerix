(* file exemples/ocaml/rcheck.ml: check Rfuns functions against MuPAD
 *-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Contr�le des fonctions de Rfuns                   |
 |                                                                       |
 +-----------------------------------------------------------------------*)

open Numerix
open Printf

module Main(E:Int_type) = struct
  module I = Infixes(E)
  module R = Rfuns(E)
  open E
  open I
  open R

  (* bornes pour les intervalles de d�finition *)
  type borne = Minf | Mun | Zero | Un | Inf

  (* premier/second r�sultat d'un couple *)
  let fst   f a b n = Pervasives.fst(f a b n)
  let snd   f a b n = Pervasives.snd(f a b n)

  let r_fst f r a b c = Pervasives.fst(f r a b c)
  let r_snd f r a b c = Pervasives.snd(f r a b c)

  (* Liste des fonctions � tester
     Le domaine de arccot est limit� � ]0,+oo[ car la d�finition de arccot(x)
     pour x < 0 est diff�rente dans Rfuns et dans MuPAD.
 *)
  let checks = [|
    arccos,     r_arccos,       "arccos",  Mun,  Un;
    arccosh,    r_arccosh,      "arccosh", Un,   Inf;
    arccot,     r_arccot,       "arccot",  Zero, Inf;
    arccoth,    r_arccoth,      "arccoth", Minf, Mun;
    arccoth,    r_arccoth,      "arccoth", Un,   Inf;
    arcsin,     r_arcsin,       "arcsin",  Mun,  Un;
    arcsinh,    r_arcsinh,      "arcsinh", Minf, Inf;
    arctan,     r_arctan,       "arctan",  Minf, Inf;
    arctanh,    r_arctanh,      "arctanh", Mun,  Un;
    arg,        r_arg,          "arg",     Minf, Inf;
    cos,        r_cos,          "cos",     Minf, Inf;
    fst cosin,  r_fst r_cosin,  "cos",     Minf, Inf;
    cosh,       r_cosh,         "cosh",    Minf, Inf;
    fst cosinh, r_fst r_cosinh, "cosh",    Minf, Inf;
    cot,        r_cot,          "cot",     Minf, Zero;
    cot,        r_cot,          "cot",     Zero, Inf;
    coth,       r_coth,         "coth",    Minf, Zero;
    coth,       r_coth,         "coth",    Zero, Inf;
    exp,        r_exp,          "exp",     Minf, Inf;
    ln,         r_ln,           "ln",      Zero, Inf;
    sin,        r_sin,          "sin",     Minf, Inf;
    snd cosin,  r_snd r_cosin,  "sin",     Minf, Inf;
    sinh,       r_sinh,         "sinh",    Minf, Inf;
    snd cosinh, r_snd r_cosinh, "sinh",    Minf, Inf;
    tan,        r_tan,          "tan",     Minf, Inf;
    tanh,       r_tanh,         "tanh",    Minf, Inf
  |]

  (* tire un rationnel au hasard entre a et b *)
  let rec random_rat a b bits =

    let u = zrandom bits
    and v = zrandom bits in
    if (u =. 0) or (v =. 0) or (eq (abs u) (abs v)) then random_rat a b bits

    (* v�rifie que u/v > a et sinon, arrange la fraction *)
    else let u,v = match a with
      | Mun  when sgn(u++v) <> sgn(v) -> v,u
      | Zero when sgn(u)    <> sgn(v) -> (neg u), v
      | Un   when sgn(u--v) <> sgn(v) ->
               if sgn(u) = sgn(v)     then v,u
          else if sgn(u++v) <> sgn(v) then (neg u), v
          else                             (neg v), u
      | _ -> u,v
    in

    (* v�rifie que u/v < b et sinon, arrange la fraction *)
    match b with
      | Mun when sgn(u++v) = sgn(v) ->
                 if sgn(u) <> sgn(v)    then v,u
            else if sgn(u--v) <> sgn(v) then (neg v), u
            else                             (neg u), v
      | Zero when sgn(u) <> sgn(v) -> (neg u), v
      | Un   when sgn(u--v) = sgn(v) -> v,u
      | _ -> u,v

  (*
    f     = fonction � tester
    r_f   = fonction � tester
    fn    = nom MuPAD pour f
    a,b   = limites du domaine de f
    n     = nombre de bits de pr�cision pour f
    c     = coefficient pour r_f
    niter = nombre d'essais � effectuer
    bits  = nombre de bits pour les arguments

    rmq : MuPAD calcule incorrectement certaines parties enti�res pour les
    fonctions hyperboliques directes avec un argument de grande valeur absolue.
    En cas de d�saccord entre une valeur calcul�e par Rfuns et la valeur
    correspondante calcul�e par MuPAD, relancer le calcul suspect sous MUPAD
    avec DIGITS suffisament grand (t�tonner) pour �tre s�r que c'est Rfuns qui
    a tort.
  *)
  let check (f,r_f,fn,a,b) bits n c niter = for i=1 to niter do

      let u,v = random_rat a b bits in
      let x = f u v n  in
      printf "x := %s: u := %s: v := %s:\n"
        (string_of x) (string_of u) (string_of v);
      if fn = "arg" then printf "y := %s(u,v):\n" fn
                    else printf "y := %s(u/v):\n" fn;
      let d = match fn with
        | "tanh" | "coth" -> 8*bits
        | _               -> nbits(x)/3
      in
      printf "d := DIGITS: if DIGITS < %d then DIGITS := %d end_if:\n" d d;
      printf "if x > ceil(2^n*y) or x < floor(2^n*y) then ";
      printf "print(hold(n)=n);";
      printf "print(hold(x)=x);";
      printf "print(hold(u)=u);";
      printf "print(hold(v)=v);";
      printf "print(f=hold(%s));" fn;
      printf "end_if:\nDIGITS := d:\n";

      let mode = [| Floor; Ceil; Nearest_up |] in
      for j = 0 to 2 do
        let r = mode.(j) in
        let rn = match r with
          | Floor -> "floor"
          | Ceil  -> "ceil"
          | _     -> "round"
        in
        let u,v = random_rat a b bits in
        let x = r_f r u v c in
        printf "x := %s: u := %s: v := %s:\n"
          (string_of x) (string_of u) (string_of v);
        if fn = "arg" then printf "y := %s(u,v):\n" fn
                      else printf "y := %s(u/v):\n" fn;
        let d = match fn with
          | "tanh" | "coth" -> 8*bits
          | _               -> nbits(x)/3
        in
        printf "d := DIGITS: if DIGITS < %d then DIGITS := %d end_if:\n" d d;
        printf "if x <> %s(c*y) then " rn;
        printf "print(hold(c)=c);";
        printf "print(hold(x)=x);";
        printf "print(hold(u)=u);";
        printf "print(hold(v)=v);";
        printf "print(f=hold(%s));" fn;
        printf "print(r=hold(%s));" rn;
        printf "end_if:\nDIGITS := d:\n";
      done

  done

  let main arglist =
    let bits   = ref 100
    and n      = ref 200
    and c      = make_ref(10^.40)
    and niter  = ref 1000
    and seed   = ref 0
    and help   = ref false
    in

    let rec parse = function
    | "-bits" ::x::suite -> bits := int_of_string(x); parse suite
    | "-n"    ::x::suite -> n    := int_of_string(x); parse suite
    | "-niter"::x::suite -> niter:= int_of_string(x); parse suite
    | "-seed" ::x::suite -> seed := int_of_string(x); parse suite
    | "-c"    ::x::suite -> of_string_in c x;         parse suite
    | [] -> ()
    |  _ -> help := true
    in
    parse (List.tl arglist);

    if !help then printf "usage: %s [-bits b] [-n n] [-c c] [-niter i] [-seed s]\n" (List.hd arglist)
    else begin

      random_init !seed;
      printf "n := %d: c := %s:\n" !n (string_of ~~c);
      Array.iter (fun x -> check x !bits !n ~~c !niter) checks;
      printf "quit;\n"

    end;
    flush stdout

end
let _ = let module S = Start(Main) in S.start()
