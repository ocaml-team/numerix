(* file exemples/ocaml/digits.ml: find a power of 2 with known leading digits
 *-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |       Recherche une puissance de 2 � partir des chiffres de t�te      |
 |                                                                       |
 +-----------------------------------------------------------------------*)

open Numerix
open Printf
module Main(E:Int_type) = struct
  module I = Infixes(E)
  module R = Rfuns(E)
  open E
  open I
  open R

         (* +----------------------------------------------------+
            |  Cherche x,y positifs minimaux tq c <= ax-by <= d  |
            +----------------------------------------------------+ *)

(* Algorithme : 0 <= b <= a et c <= d.

   1. Si c <= 0 <= d, retourner (0,0).

   D�sormais c,d sont non nuls de m�me signe.

   2. Si a = 0 :
      Si b = 0 ou c > 0, il n'y a pas de solution.
      Si ceil(-d/b) > floor(-c/b), il n'y a pas de solution.
      Sinon, x=0, y=ceil(-d/b).

   D�sormais a > 0.

   3. Si c > 0 et ceil(c/a) <= floor(d/a), alors x=ceil(c/a), y=0.
      Sinon si b = 0, il n'y a pas de solution.

   D�sormais, 0 < b <= a et toute solution v�rifie x >= max(0,d/a).

   4. Si c < 0 et ceil(-d/b) <= floor(-c/b), alors x=0, y=ceil(-d/b).

   Soit a = qb + r la division euclidienne de a par b. On effectue le
   changement de variable y = qx + t, ce qui donne les conditions sur (x,t) :

        -d <= bt - rx <= -c,   x >= max(0,d/a),   qx + t >= 0.

   5. Les deux premi�res relations impliquent t >= -qd/a donc la troisi�me
      est satisfaite. Dans ce cas, poser :

          x0 = ceil(max(0, d)/a),   x = x0 + u,
          t0 = ceil(-qd/a),         t = t0 + v,

      puis chercher u,v positifs minimaux tels que

          rx0 - bt0 -d <= bv - ru <= rx0 - bt0 - c.

*)
  let rec cherche a b c d =

    if (c <=. 0) & (d >=. 0) then (zero,zero)

    else if a =. 0 then begin
      if (b =. 0) or (c >. 0) then raise Not_found;
      let y0,z = gquomod Ceil (neg d) b in
      if inf z (c -- d) then raise Not_found;
      (zero,y0)
    end

    else let ok,x0,y0 =
      if c >. 0 then begin
        let x0,z = gquomod Ceil c a in
        if supeq z (d -- c) then true,x0,zero
        else if b =. 0 then raise Not_found
        else false,x0,zero
      end
      else if b =. 0 then raise Not_found
      else begin
        let y0,z = gquomod Ceil (neg d) b in
        (supeq z (c -- d)),zero,y0
      end
    in if ok then (x0,y0)

    else begin
      let q,r = quomod a b                 in
      let t0  = gquo Ceil (neg(q**d)) a    in
      let s   = r**x0 -- b**t0             in
      let v,u = cherche b r (s--d) (s--c)  in
      let x   = x0++u                      in
      let t   = t0++v                      in
      (x,q**x++t)
    end

         (* +---------------------------------------------------+
            |  Cherche x,y positifs tels que c < a^x/b^y < c+1  |
            +---------------------------------------------------+ *)

(* Algorithme : a,b,c > 1, ln(a)/ln(b) irrationnel

   On veut ln(c) <= x*ln(a) - y*ln(b) <= ln(c+1). Soit n � choisir, on note
   la,lb,lc,ld des entiers tels que

       la-1 < 2^n*ln(a)   < la+1
       lb-1 < 2^n*ln(b)   < lb+1
       lc-1 < 2^n*ln(c)   < lc+1
       ld-1 < 2^n*ln(c+1) < ld+1

   Comme la suite des parties fractionnaires de k*ln(a)/ln(b)) est �quir�partie
   modulo 1, on peut esp�rer trouver une solution avec x,y de l'ordre de
   ln(b)/ln(1+1/c) ~ c*ln(b). Soit m >= 20*c*ln(b), m entier.

   On cherche un couple (x,y) positif minimal tel que :

       lc + m <= x*la - y*lb <= ld - m

   puis on v�rifie que x*(la-1) - y*(lb+1) >= lc+1
                       x*(la+1) - y*(lb-1) <= ld-1.

   Si le test est positif alors (x,y) convient, sinon ou si on n'a pas trouv�
   de solution alors on on r�essaye en doublant la pr�cision.

   Minimalit� : soit (x0,y0) la solution trouv�e pour le probl�me approch�.
   Toute solution (x,y) du probl�me exact telle que x <= x0 et y <= y0 v�rifie

       lc - 1 -x0 -y0 <= x*la - y*lb <= ld + 1 + x0 + y0

   donc on peut prouver que (x0,y0) est solution minimale du probl�me exact
   en r�solvant ce deuxi�me probl�me approch� et en v�rifiant que sa solution
   minimale est (x0,y0).

   Si (x0,y0) n'est pas minimale alors on recommence en doublant la pr�cision.

*)

  let leading_digits a b c p debug = (* p = nb maximal d'essais � effectuer *)

    let v  = of_int(20*nbits(b))   in
    let m  = c**v                  in

    let essai(n) =
      let la = ln a      one n 
      and lb = ln b      one n 
      and lc = ln c      one n 
      and ld = ln (c+.1) one n     in
      let c1 = lc ++ m
      and d1 = ld -- m              in

      (* ceci ne doit pas arriver *)
      if sup c1 d1 then failwith "unexpected error, c1 > d1";

      (* solution minimale du premier probl�me approch� *)
      let x0,y0 = if supeq la lb
        then cherche la lb c1 d1
        else let y,x = cherche lb la (neg d1) (neg c1) in x,y
      in

      (* v�rifie que c'est une solution du probl�me exact *)
      let u = x0**la -- y0**lb       in
      let v = x0 ++ y0               in
      if (infeq (u -- v) lc) or (supeq (u ++ v) ld) then raise Not_found;

      (* solution minimale du deuxi�me probl�me approch� *)
      let c2 = lc -- x0 -- y0 -. 1
      and d2 = ld ++ x0 ++ y0 +. 1 in
      let x,y = if supeq la lb
        then cherche la lb c2 d2
        else let y,x = cherche lb la (neg d2) (neg c2) in x,y
      in ((eq x0 x) & (eq y0 y)), x0, y0
    in

    (* essaye au maximum p fois *)
    let rec loop n p =
      if p = 0 then raise Not_found
      else begin
        try
          let minimal,x,y = essai(n) in
          if debug then chrono (sprintf "n=%d" n);
          if not(minimal) & (p > 1) then raise Not_found else minimal,x,y
        with Not_found when p > 1 -> loop (2*n) (p-1)
      end
    in

    (* Choix initial de de n:
       il faut 2m <= ld-lc ~ 2^n/c, soit n >= log_2(40*c^2*ln(b)) *)
    loop (2*nbits(c)+nbits(v)+1) p

  
                         (* +-------------------------+
                            |  Interface utilisateur  |
                            +-------------------------+ *)

  let main arglist =

    let a_test = 2
    and b_test = 10
    and c_test = 577215
    and p_test = 1
    and m_test = true
    and x_test = 4244058
    and y_test = 1277583 in

    let a = make_ref(of_int a_test) and a_ok = ref false
    and b = make_ref(of_int b_test)
    and c = make_ref(of_int c_test) and c_ok = ref false
    and p = ref p_test              and p_ok = ref false
    and test = ref false
    and help = ref false
    and debug= ref false in

    let rec parse = function
      | "-h"::_         -> help := true
      | "-b"::x::s      -> of_string_in b x; parse s
      | "-d"::s         -> debug := true;    parse s
      | "-test"::_      -> test := true;
                           of_int_in a a_test;
                           of_int_in b b_test;
                           of_int_in c c_test;
                           p := p_test
      | x::_ when !p_ok -> help := true
      | x::s when !c_ok -> p := int_of_string x; p_ok := true; parse s
      | x::s when !a_ok -> of_string_in c x;     c_ok := true; parse s
      | x::s            -> of_string_in a x;     a_ok := true; parse s
      | [] -> ()
    in parse (List.tl arglist);

    if !help
    then printf "usage: %s [a] [c] [p] [-b base] [-d]\n" (List.hd arglist)
    else begin
      if !debug then chrono ("E = "^name());
      let minimal,x,y = leading_digits (~~a) (~~b) (~~c) !p !debug in
      if !test then begin
        if (minimal = m_test) & (x =. x_test) & (y =. y_test)
        then printf "%s\t%s\ttest ok\n" (List.hd arglist) (name())
        else printf "error in the %s %s test\n" (List.hd arglist) (name())
      end
      else begin
        printf "%s %s%s\n" (string_of x) (string_of y)
                           (if minimal then " (minimal)" else "");
      end;
      flush stdout
    end

end
let _ = let module S = Start(Main) in S.start()
