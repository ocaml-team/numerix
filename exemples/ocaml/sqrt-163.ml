(* file exemples/ocaml/sqrt-163.ml: compute exp(pi*sqrt(163))
 *-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                       Calcul de exp( pi*sqrt(163) )                   |
 |                                                                       |
 +-----------------------------------------------------------------------*)

open Numerix
open Printf
module Main(E:Int_type) = struct
  module I = Infixes(E)
  module R = Rfuns(E)
  open E
  open I
  open R

  (* calcule exp(pi*sqrt(163)) � 1/2^n pr�s

     soit       x tq x-1 <= 2^(n+p)*pi        <= x+1
                y tq y-1 <= 2^(n+q)*sqrt(163) <= y+1
                z = approx(x*y/2^(n+p+q-r))

     on a alors

                z-1 <= 2^(n+r)*pi*sqrt(163)   <= z+1

     si n+p+q-r >= 4, q >= r+4, p >= r+6.
     Mettons p = r+6 et q = r+4, il reste la condition n+r+6 >= 0.

     pi*sqrt(163) est de l'ordre de 40, donc l'erreur sur l'exponentielle
     est major�e par e^41/2^(n+r) ~ 1/2^(n+r-60). Cette erreur doit �tre
     inf�rieure ou �gale � 1/2^(n+1) pour qu'on puisse arrondir � n bits.

     Il faut donc r >= 61.
  *)
  let compute n =

    let r = 61  in
    let p = r+6 in
    let q = r+4 in

    let x = arctan one one (n+p+2)                       in
    let y = gsqrt Nearest_up ((of_int 163) << (2*n+2*q)) in
    let z = (((x**y) >> (n+p+q-r-1)) +. 1) >> 1          in

    let t = exp z (one << (n+r)) (n+2) in (t +. 2) >> 2


  (* interface utilisateur *)
  let main arglist =
    
    let n    = ref 0
    and help = ref false
    and test = ref false
    and x_test = of_string "262537412640768743" in

    let rec parse = function
      | "-h"::_    -> help := true
      | "-test"::_ -> test := true; n := 0
      | x::s       -> n := int_of_string x; parse s
      | []         -> ()
    in parse (List.tl arglist);

    if !help then printf "usage: %s [n]\n" (List.hd arglist)
    else begin
      let x = round compute Floor (10 ^. !n) in
      if !test then begin
        if eq x x_test
        then printf "%s\t%s\ttest ok\n"          (List.hd arglist) (name())
        else printf "error in the %s %s test\n" (List.hd arglist) (name())
      end
      else begin
        let s = string_of x     in
        let l = String.length s in
        if !n <= 0
        then printf "%s\n" s
        else printf "%s.%s\n" (String.sub s 0 (l - !n)) (String.sub s (l - !n) !n)
      end
    end;
    flush stdout

end
let _ = let module S = Start(Main) in S.start()



