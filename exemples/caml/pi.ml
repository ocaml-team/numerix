(* file exemples/caml/pi.ml: compute some digits of pi
 *-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                    Calcul de Pi, formule de Ramanujan                 |
 |                                                                       |
 +-----------------------------------------------------------------------*)

(* cf. "The Caml Numbers Reference Manual", Inria, RT-0141 *)
(* annexe A, pp. 115 et suivantes.                         *)

#open "_name_";;
#open "printf";;

(* les 1000 premiers chiffres de pi pour contr�le *)
let true_pi  = "\
31415926535897932384626433832795028841971693993751\
05820974944592307816406286208998628034825342117067\
98214808651328230664709384460955058223172535940812\
84811174502841027019385211055596446229489549303819\
64428810975665933446128475648233786783165271201909\
14564856692346034861045432664821339360726024914127\
37245870066063155881748815209209628292540917153643\
67892590360011330530548820466521384146951941511609\
43305727036575959195309218611738193261179310511854\
80744623799627495673518857527248912279381830119491\
29833673362440656643086021394946395224737190702179\
86094370277053921717629317675238467481846766940513\
20005681271452635608277857713427577896091736371787\
21468440901224953430146549585371050792279689258923\
54201995611212902196086403441815981362977477130996\
05187072113499999983729780499510597317328160963185\
95024459455346908302642522308253344685035261931188\
17101000313783875288658753320838142061717766914730\
35982534904287554687311595628638823537875937519577\
81857780532171226806613001927876611195909216420198";;

(* Affichage par tranches de 5 chiffres, lignes de 10 tranches *)
let pretty_print s i skip =
  let j = ref i in
  while !j < string_length(s) do
    print_char s.[!j];
    j := !j + 1;
         if (!j-i) mod 250 = 0 then print_string "\n\n"
    else if (!j-i) mod  50 = 0 then print_string "\n"
    else if (!j-i) mod  10 = 0 then print_string "  "
    else if (!j-i) mod   5 = 0 then print_string " ";
    if skip && ((!j-i) mod 50 = 0) then begin
      let k = (string_length(s) - !j)/50 - 1 in
      if k > 0 then begin
        printf "... (%d lines omitted)\n" k;
        j := !j + 50*k
      end;
    end;
  done;
  if (string_length(s)-1-i) mod 50 <> 49 then print_string "\n";;


                    (* +----------------------+
                       |  somme dichotomique  |
                       +----------------------+ *)

#open "inf_name_";;
let somme prec =

  (* constantes *)
  let cinq  = of_int 5
  and a     = of_int 13591409
  and b     = of_int 545140134
  and c     = ((of_int 320160) ^^ 3) /. 3
  and c0    = of_int  53360
  in

  (* �tat du calcul :
     p     = index s�rie
     alpha = 2p+1
     beta  = 6p+1
     gamma = 6p+5
     delta = c*p^3
     eps   = a + b*p
  *)

  (* calcule et retranche les termes de rangs p et p+1 *)
  let calc2 (p,alpha,beta,gamma,delta,eps) =

    let  ma    = (alpha ** beta) ** gamma      in
    let  mb    = delta                         in
    let  ms    = eps                           in
      
    let  p     = p      +. 1                   in
    let  alpha = alpha  +. 2                   in
    let  beta  = beta   +. 6                   in
    let  gamma = gamma  +. 6                   in
    let  delta = sqr(p) ** p ** c              in
    let  eps   = eps    ++ b                   in
      
    let  ms    = delta ** ms -- ma ** eps      in
    let  ma    = (alpha ** beta ** gamma)** ma in
    let  mb    = mb ** delta                   in
      
    let  p     = p      +. 1                   in
    let  alpha = alpha  +. 2                   in
    let  beta  = beta   +. 6                   in
    let  gamma = gamma  +. 6                   in
    let  delta = sqr(p) ** p ** c              in
    let  eps   = eps    ++ b                   in

    (ma,mb,ms), (p,alpha,beta,gamma,delta,eps) in

  (* calcule et additionne 2*k termes *)
  let rec calc k etat =
    if k = 1 then calc2 etat
    else let (a0, b0, s0), etat = calc (k/2)     etat  in
         let (a1, b1, s1), etat = calc (k - k/2) etat  in
         (a0**a1, b0**b1, a0**s1 ++ b1**s0), etat
  in

  (* lance les calculs *)
  let k    = (prec + 197)/94               in
  let etat = (zero, one, one, cinq, c0, a) in
  let (a,b,s),_ = calc k etat              in (b,s)

;;

            (* +--------------------------------------+
               |  calcule pi avec digits+2 d�cimales  |
               +--------------------------------------+ *)

let pi digits pgcd prt skip debug test cmd =

  if debug then chrono("module = " ^ name());

  (* p5 <- 5^(digits+2) *)
  let p5 = 5 ^. (digits+2) in
  if debug then chrono "puiss-5";

  (* rac <- floor( sqrt(640320) * 10^(digits+2) ) *)
  let rac = sqrt((sqr(p5) *. 640320) << (2*digits + 4)) in
  if debug then chrono "sqrt";
  
  (* num/den <- somme de la s�rie � env. 10^(-digits-2) pr�s *)
  let prec = nbits(p5) + digits in
  let (num,den) = somme prec    in
  if debug then chrono(sprintf "s�rie lb=%d" (nbits num));

  (* simplifie la fraction si demand� (ceci ne vaut pas le coup, le
     temps de calcul du pgcd est tr�s sup�rieur au temps de calcul
     de la division sans simplification) *)
  let num,den = if pgcd then begin
    let _,_,_,num,den = cfrac num den in
    if debug then chrono(sprintf "pgcd  lb=%d" (nbits num));
    num,den
  end else num,den in

  (* pi <- sqrt(640320)*num/den * 10^digits+2) *)
  let pi = (rac ** num) // den in
  if debug then chrono "quotient";

  (* affiche le r�sultat *)
  if prt then begin
    let s = string_of pi in
    if debug then chrono "conversion";
    print_char s.[0];
    print_string ".\n";
    pretty_print s 1 skip;
  end

  (* contr�le *)
  else if test then begin
    let s  = string_of pi           in
    let l1 = string_length s
    and l2 = string_length true_pi  in
    let l  = min l1 l2              in
    let s1 = sub_string s 0 l
    and s2 = sub_string true_pi 0 l in
    if s1 = s2 then printf "%s\ttest ok\n" cmd
    else begin
      printf "error in the %s test, value computed:\n" cmd;
      for i=0 to l-1 do
        printf "%c" s.[i];
        if (i mod 50 = 49) then printf "\n"
      done;
      if l mod 50 > 0 then printf "\n"
    end
  end

;;

                       (* +-------------------------+
                          |  Interface utilisateur  |
                          +-------------------------+ *)
let main arglist =

  let digits = ref 100
  and pgcd   = ref false
  and print  = ref true
  and skip   = ref false
  and debug  = ref false
  and test   = ref false
  and help   = ref false in

  let rec parse = function
    | "-h"::s       -> help   := true
    | "-d"::s       -> debug  := true;  parse s
    | "-noprint"::s -> print  := false; parse s
    | "-skip"::s    -> skip   := true;  parse s
    | "-gcd"::s     -> pgcd   := true;  parse s;
    | "-test"::s    -> digits := 1000;
                       print  := false;
                       skip   := false;
                       debug  := false;
                       pgcd   := false;
                       test   := true
    | d::s          -> digits := int_of_string d; parse s
    | []            -> ()
  in parse (tl arglist);

  if !help
  then printf "usage: %s <digits> [-d] [-noprint] [-skip] [-gcd]\n" (hd arglist)
  else pi (!digits-2) !pgcd !print !skip !debug !test (hd arglist);
  flush stdout

;;

main (list_of_vect sys__command_line);;
