// file kernel/exemples/c/prime-test.c: check the validity of Numerix primality test
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |               Validit� du test de primalit� de Numerix                |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "numerix.h"

                         /* +----------------------+
                            |  Variables globales  |
                            +----------------------+ */

long sp;       /* les nombres premiers triviaux sont ceux <= 2^sp    */
long xp;       /* on v�rifie la validit� du test jusqu'� 4^xp        */
long kp;       /* nombre de discriminants � consid�rer               */
long kq;       /* kp/sizeof(long)                                    */
long ns;       /* nombre de nombres premiers <= 2^sp                 */
long np;       /* nombre de nombres premiers entre 2^sp et 2^xp      */
long sk;       /* somme des kp premiers nombres premiers             */

unsigned long *sprimes; /* n.p. <= 2^sp, par ordre croisant, termin� par 0    */
unsigned long *jlist;   /* table des symboles de Jacobi pour les kp 1ers n.p. */
unsigned long *plist;   /* n.p. entre 2^sp et 2^xp + symboles de Jacobi       
 	          une entr�e de la liste plist est constitu�e :
		    du nombre premier (un long)
		    des kp premiers symboles de Jacobi
		    de kp bits indiquant si un symbole sert          */
long dp;       /* taille d'une entr�e en mots                        */

                        /* +-------------------------+
                           |  Allocation de m�moire  |
                           +-------------------------+ */

/* alloue un tableau de n long et contr�le qu'il n'y a pas d'erreur */
static inline unsigned long *alloc(long n) {
  unsigned long *r = (unsigned long *)malloc(n*sizeof(long));
  if (r == NULL) {printf("out of memory\n"); exit(1);}
  return(r);
}

                 /* +---------------------------------------+
                    |  Op�rations sur les tableaux de bits  |
                    +---------------------------------------+ */

#define LQ(n)  ((n) / (8*sizeof(long)))
#define LR(n)  ((n) & (8*sizeof(long)-1))
#define LS(n)  (((n)+8*sizeof(long)-1)/(8*sizeof(long)))

static inline long tstbit(unsigned long *t, long n) {return((t[LQ(n)] >> LR(n)) & 1);}
static inline void setbit(unsigned long *t, long n) {t[LQ(n)] |= 1L << LR(n);}
static inline void clrbit(unsigned long *t, long n) {t[LQ(n)] &= (-1L << LR(n)) - 1;}

                   /* +----------------------------------+
                      |  Initialise sprimes,plist,jlist  |
                      +----------------------------------+ */

void init() {
  unsigned long *c,*p;
  long i,j,l;
  char msg[80];

  /* crible les nombres premiers <= 2^xp */
  c = alloc(LS(1<<(xp-1)));
  memset(c,-1L,sizeof(long)*LS(1<<(xp-1)));
  for (i = 3; ((i*i) >> xp) == 0; i += 2) if (tstbit(c,i>>1))
    for (j = (i*i)/2; (j>>(xp-1)) == 0; j += i) clrbit(c,j);

  /* liste des nombres premiers triviaux */
  for (i=1, ns=0; (i >> (sp-1)) == 0; i++) ns += tstbit(c,i);
  sprimes = alloc(ns+1);
  for (i=3, p=sprimes; (i >> sp) == 0; i+=2) if (tstbit(c,i>>1)) *(p++) = i;
  *p = 0;

  /* liste des symboles de Jacobi */
  kq = LS(kp); kp = 8*sizeof(long)*kq;
  if (ns < kp) {
    printf("error, ns=%ld < kp=%ld\n",ns,kp);
    exit(1);
  }
  for (p=sprimes + kp-1, sk=0; p >= sprimes; p--) sk += p[0];
  jlist = alloc(LS(sk));
  memset(jlist,-1L,sizeof(long)*LS(sk));
  for (p=sprimes + kp-1, l=0; p >= sprimes; l += p[0], p--)
    for (j=0; j <= p[0]/2; j++) clrbit(jlist, l + (j*j)%p[0]);

  /* liste des nombres premiers entre 2^sp et 2^xp */
  dp = 1 + 2*kq;
  for (np=0, j=1<<(sp-1); (j>>(xp-1)) == 0; j++) np += tstbit(c,j);
  plist = alloc(np*dp);
  for (p=plist, i=(1<<sp)+1; (i>>xp) == 0; i+=2) if (tstbit(c,i>>1)) {
    p[0] = i;
    for (j=kp-1, l=0; j>=0; l += sprimes[j], j--)
      if (tstbit(jlist, l + i%sprimes[j])) setbit(p+1,j);
    p += dp;
  }

  /* termin� */
  sprintf(msg,"%ld primes <= 2^%ld, %ld primes between 2^%ld and 2^%ld",ns,sp,np,sp,xp); chrono(msg);
  free(c);

}

                         /* +-----------------------+
                            |  Trie la liste plist  |
                            +-----------------------+ */

/*
  entr�e :
    p,q = pointeurs sur des entr�es de plist
    k,kmin >= 0

  sortie :
    trie les �l�ments de [p,q] selon les bits k,k-1,..,kmin
 */
void sort(unsigned long *p, unsigned long *q, long k, long kmin) {
  unsigned long *i,*j,x[dp];

  if ((p >= q) || (k < kmin)) return;

  /* quicksort */
  for (i=p, j=q; i <= j; ) {
    if     (!tstbit(i,k)) i += dp;
    else if (tstbit(j,k)) j -= dp;
    else {
      memcpy(x,i,sizeof(x));
      memcpy(i,j,sizeof(x));
      memcpy(j,x,sizeof(x));
      i += dp;
      j -= dp;
    }
  }

  /* trier le petit sous-tableau en premier */
  if (j-p < q-i) {sort(p,j,k-1,kmin); sort(i,q,k-1,kmin);}
  else           {sort(i,q,k-1,kmin); sort(p,j,k-1,kmin);}
}


              /* +--------------------------------------------+
                 |  Crible les entiers sans diviseur trivial  |
                 +--------------------------------------------+ */

long lc;       	   /* nb de bits dans une tranche */
unsigned long *cr; /* tranche de lc bits          */
long *st;      	   /* bits de d�part              */
xint k0;       	   /* indice en d�but de mot      */
long *ra;  	   /* r�sidus de a                */
long *r0;      	   /* r�sidus en d�but de mot     */
long tr;       	   /* nb de tranches restant      */
long ip, jp;   	   /* position dans la tranche    */
unsigned long mp;  /* cr[ip] >> jp                */
unsigned long *jl; /* symboles de Jacobi          */

/* allocation des variables de criblage */
void cr_alloc() {
  st = (long *)alloc(LQ(lc) + ns + sk + 2*kp);
  ra = st + ns;
  r0 = ra + kp;
  jl = (unsigned long *)r0 + kp;
  cr = jl + sk;
  k0 = xx(new)();
}


/*
  entr�e :
    a pair
    b premier � a
    kmax >= 0

  action :
    initialise la recherche des entiers k < kmax tq k*a+b n'a pas
    de diviseur trivial
*/
void cr_init(xint a, xint b, xint kmax) {
  long i,j,k,l,m,*r,*s,u;
  unsigned long *p;
  xint x,y;

  x = xx(new)();
  y = xx(new)();

  /* nombre de tranches et indice de d�part */
  k  = -xx(gquomod_1)(&x,kmax,lc,2);
  tr = xx(int_of)(x);
  ip = LQ(k); jp = LR(k);
  xx(copy_int)(&k0,-jp);

  /* r�sidus et symboles de Jacobi */
  xx(mul_1)(&y,a,jp); xx(sub)(&y,b,y);
  for (p=sprimes+kp-1, r=ra+kp-1, s=r0+kp-1, l=0;
       p>=sprimes;
       l += p[0], p--, r--, s--) {
    u    = xx(mod_1)(a, p[0]);
    r[0] = (8*sizeof(long)*u) % p[0];
    s[0] = xx(mod_1)(y, p[0]);
    for (j=8*sizeof(long)-1; j>=0; j--)
      for (m=0; m<p[0]; m++)
	jl[l+m] = (jl[l+m]<<1) + tstbit(jlist, l+(u*j+m)%p[0]);
  }

  /* crible la premi�re tranche */
  memset(cr+ip, -1L, lc/8 - ip*sizeof(long));
  for (i=0; sprimes[i]; i++) {
    xx(copy_int)(&x, sprimes[i]);
    xx(gcd_ex)(&x, NULL, &y, x, a);
    if (xx(neq_1)(x,1)) st[i] = -1;
    else {
      xx(mul)(&y, y, b);
      for (j = xx(mod_1)(y,sprimes[i]) + k; j < lc; j += sprimes[i])
	clrbit(cr,j);
      st[i] = j - lc;
    }
  }

  /* termin� */
  mp = cr[ip] >> jp;
  xx(free)(&y);
  xx(free)(&x);

}

/*
  cherche le prochain k tel que a*k + b n'a pas de diviseur trivial
  et calcule les symboles de Jacobi associ�s dans js (kp bits)

  retourne 1 si k a �t� trouv�, 0 sinon
*/
long cr_next(xint *k, unsigned long *js) {
  long i,l,*s,y;
  unsigned long *p,x;

  /* s'il n'y a plus de bits non nuls dans mp charge le mot suivant */
  while (!mp) {
    jp = 0;
    ip++;
    xx(add_1)(&k0,k0,8*sizeof(long));

    /* mise � jour de r0 */
    for (i=0; i<kp; i++) {
      x = r0[i] + ra[i];
      y = x - sprimes[i];
      r0[i] = (y < 0) ? x : y;
    }

    if (ip*8*sizeof(long) < lc) mp = cr[ip];
    else {

      /* crible une nouvelle tranche */
      tr--;
      if (!tr) return(0);
      memset(cr,-1L, lc/8);
      for (p=sprimes, s=st; *p; p++,s++) if (s[0] >= 0) {
	for (i=s[0]; i<lc; i+=p[0]) clrbit(cr,i);
	s[0] = i - lc;
      }
      ip = 0; mp = cr[0];
    }
  }

  /* avance au bit non nul suivant */
  while ((mp&1) == 0) {mp >>= 1; jp++;}
  xx(add_1)(k,k0,jp);

  /* symboles de Jacobi */
  for (i=kp-1, l=0, x=0; i>=0; l += sprimes[i], i--) {
    x = (x << 1) | ((jl[l+r0[i]] >> jp) & 1);
    if (LR(i) == 0) js[LQ(i)] = x;
  }

  /* pr�pare l'appel suivant */
  mp >>= 1; jp++;
  return(1);

}

                  /* +------------------------------------+
                     |  Cherche les discriminants utiles  |
                     +------------------------------------+ */

/*
  pour chaque n.p. de plist marque les discriminants succeptibles
  d'�tre utilis�s sur un multiple de p <= 4^xp sans diviseur trivial
*/
void disc_util(char *fn) {
  long i,j,m,n;
  unsigned long *p,**pi,x[kq],y;
  xint c,u,v,w;
  char msg[80], *s;
  FILE *f;

  c = xx(new)();
  u = xx(new)();
  v = xx(new)();
  w = xx(new)();

  /* trie et indexe plist sur les log2(np) premiers symboles de Jacobi */
  for (n=0; np>>n; n++);
  m = (1<<n) - 1;
  sort(plist,plist+(np-1)*dp,8*sizeof(long)+n-1,8*sizeof(long));
  pi = (unsigned long **)alloc(1<<n);
  for (i=0, p=plist; i<np; i++, p += dp) {
    pi[p[1]&m] = p;
    p[1+kq] = m;
  }
  sprintf(msg,"index plist on %ld first Jacobi symbols",n); chrono(msg);

  /* cherche les entiers impairs <= 2^(2xp-sp) sans diviseur trivial */
  xx(copy_int)(&c,0);
  xx(copy_int)(&u,2);
  xx(copy_int)(&v,3);
  xx(copy_int)(&w,1); xx(shl)(&w,w,2*xp-sp-1); xx(sub_1)(&w,w,2);
  cr_init(u,v,w);

  for(; cr_next(&u,x); xx(add_1)(&c,c,1)) {

    /* examine les �l�ments de plist ayant les m�mes n premiers symboles */
    for (p=pi[x[0]&m]; (p>=plist) && !((p[1]^x[0])&m); p -= dp) {

      /* marque le rang du premier symbole diff�rant avec celui de p */
      for (j=1, y=x[0]^p[1]; (y==0) && (j<kq); j++, y=x[j-1]^p[j]);
      if (y) p[kq+j] |= y ^ (y&(y-1));

      /* pas de diff�rence, v�rifie que 2*u+3 = p[0]*(un carr�) */
      else {
	xx(mul_1)(&u,u,2); xx(add_1)(&u,u,3);
	if (xx(quomod_1)(&v,u,p[0]) == 0) {
	  xx(sqrt)(&w,v); xx(sqr)(&w,w);
	  if (xx(eq)(v,w)) continue;
	}
	s = xx(string_of)(u);
	printf("collision: p=%ld q=%s\n",p[0],s);
	exit(1);
      }
    }
  }
  s = xx(string_of)(c);
  sprintf(msg,"%s numbers <= 2^%ld without small divisors",s,2*xp-sp);
  chrono(msg); free(s);

  sort(plist,plist+(np-1)*dp,xp-1,0);
  chrono("unsort prime list");

  /* enregistre les r�sultats */
  if (fn) {
    f = fopen(fn,"w");
    if (!f) {printf("open error on file %s\n",fn); exit(1);}
    for (i=0, p=plist; i<np; i++, p += dp) {
      fprintf(f,"%ld",p[0]);
      for (j=0; j<kq; j++) fprintf(f," 0x%lx",p[1+kq+j]);
      fprintf(f,"\n");
    }
    fflush(f);
    fclose(f);
  }

  /* termin� */
  free(pi); xx(free)(&w); xx(free)(&v); xx(free)(&u); xx(free)(&c);

}


                /* +-----------------------------------------+
                   |  Relecture des r�sultats de la phase 1  |
                   +-----------------------------------------+ */

void read_phase_1(char *fn) {
  long i,j;
  unsigned long *p;
  char msg[80];
  FILE *f;

   f = fopen(fn,"r");
   if (!f) {printf("open error on file %s\n",fn); exit(1);}

   for (i=0, p=plist; i<np; i++) {
     fscanf(f,"%ld",&j);
     if (j != p[0]) {
       printf("error reading %s: got %ld instead of %ld\n",fn,j,p[0]);
       exit(1);
     }
     for (j=0, p += 1+kq; j<kq; j++, p++) fscanf(f," 0x%lx",p);
   }

   fclose(f);

   strcpy(msg,"read phase-1 result from file ");
   i = strlen(msg);
   j = strlen(fn); if (i+j > sizeof(msg)-1) j = sizeof(msg)-1-i;
   strncpy(msg+i,fn,j); msg[i+j] = 0;
   chrono(msg);

}

                       /* +-----------------+
                          |  factorisation  |
                          +-----------------+ */

/*
  entr�e :
    a >= 1
    r = tableau de taille > 2*(nb de facteurs premiers de a)

  sortie :
    r <- liste des facteurs premiers de a et des exposants, termin�e par 0
    place en premier les facteurs premiers congrus � 3 modulo 4, par ordre
    croissant
 */
void factor(long a, long *r) {
  long l,*s;
  unsigned long *p;

  s = r;

  /* extrait la partie impaire */
  for (l=0; (a&1) == 0; l++, a >>= 1);
  if (l) {r[0] = 2; r[1] = l; r+=2;}

  /* diviseurs premiers impairs */
  for (p=sprimes; a > 1; ) {
    for (l=0; (a%p[0]) == 0; l++, a /= p[0]);
    if (l) {
      if (p[0]&2) {
	if (r > s) memmove(s+2,s,(r-s)*sizeof(long));
	s[0] = p[0];
	s[1] = l;
	s += 2;
      } else {
	r[0] = p[0];
	r[1] = l;
      }
      r += 2;
    }
    else if (a/p[0] < p[0]) {
      if (a&2) {
	if (r > s) memmove(s+2,s,(r-s)*sizeof(long));
	s[0] = a;
	s[1] = 1;
      } else {
	r[0] = a;
	r[1] = 1;
      } r += 2;
      break;
    }
    if ((p[0] >> sp) == 0) {p++; if (p[0] == 0) p = plist;}
    else p += dp;
  }

  /* fin de liste */
  r[0] = 0;

}

                    /* +---------------------------------+
                       |  calcule (a+b*sqrt(d))^k mod p  |
                       +---------------------------------+ */

/*
  entr�e :
    m[0] = a, m[1] = b
    k > 0
    p != 0
    d quelconque

  sortie :
    m[0],m[1] <- coefficients de (a+b*sqrt(d))^k mod p
 */
void powmod(xint *m, xint k, xint p, long d) {
  xint a,b,x,y;
  long i,u;

  /* cas b = 0 */
  if (xx(eq_1)(m[1],0)) {xx(gpowmod)(m,m[0],k,p,1); return;}

  /* cas g�n�ral */
  a = xx(f_copy)(m[0]);
  b = xx(f_copy)(m[1]);
  x = xx(new)();
  y = xx(new)();
  u = xx(eq_1)(a,1) & xx(eq_1)(b,1);

  for (i = xx(nbits)(k)-2; i>=0; i--) {

    xx(sqr)(&x,m[1]);       xx(mul_1)(&x,x,d);        /* x    <- d*m(1]^2    */
    xx(mul)(m+1,m[0],m[1]); xx(add)(m+1,m[1],m[1]);   /* m[1] <- 2*m[0]*m[1] */
    xx(sqr)(m,m[0]);        xx(add)(m,m[0],x);        /* m[0] <- m[0]^2 + x  */

    if (xx(nth_bit)(k,i)) {
      if (u) {
	xx(mul_1)(&x,m[1],d);       		      /* x    <- d*m[1]      */
	xx(add)(m+1,m[0],m[1]);     		      /* m[1] <- m[0] + m[1] */
	xx(add)(m,  m[0],x);        		      /* m[0] <- m[0] + x    */
      } else {
	xx(mul)(&x, b,m[1]); xx(mul_1)(&x,x,d);       /* x    <- d*b*m[1]    */
	xx(mul)(&y, a,m[1]);                          /* y    <- a*m[1]      */
	xx(mul)(m+1,b,m[0]); xx(add)(m+1,m[1],y);     /* m[1] <- b*m[0] + y  */
	xx(mul)(m,  a,m[0]); xx(add)(m,  m[0],x);     /* m[0] <- a*m[0] + x  */
      }
    }

    xx(gmod)(m,m[0],p,1);  xx(gmod)(m+1,m[1],p,1);
  }

  xx(free)(&y); xx(free)(&x); xx(free)(&b); xx(free)(&a);
}

                /* +-----------------------------------------+
                   |  Ordre de 1+sqrt(d) modulo p, (d/p)=-1  |
                   +-----------------------------------------+ */

/*
  entr�e :
    p  = nombre premier
    f0 = factorisation de p-1
    f1 = factorisation de p+1
    d  = discriminant tq (d/p) = -1

  sortie :
    ord <- ordre de 1+sqrt(d) dans F(p^2)
    s'il existe un discriminant d' divisant ord tq |d'| < |d| et (d'/p) = -1
    alors a <- 0 sinon a <- ord.
*/
void ordre_1(long p, long *f0, long *f1, long d, xint *a) {
  xint f,m[2],pp,u,v;
  long d1,d2,l;

  pp   = xx(of_int)(p);
  m[0] = xx(new)();
  m[1] = xx(new)();
  f    = xx(new)();
  u    = xx(new)();
  v    = xx(of_int)(0);
  d1 = (d < 0) ? -d : d;

  /* cherche a minimal > 0 tq (1+sqrt(d))^a = v est dans Fp */
  for (xx(copy_int)(a,p+1); *f1; f1 += 2) {
    for (l = f1[1]; l; l--) xx(quo_1)(a,*a,f1[0]);
    xx(copy_int)(m,  1);
    xx(copy_int)(m+1,1);
    powmod(m,*a,pp,d);

    /* si f1[0] = 3 mod 4 et |f1[0]| < |d| il faut m[1] = 0 */
    d2 = (f1[0] == 3) ? 15 : f1[0];
    if (((d2&3) == 3) && (d2 < d1)) {
      if (xx(eq_1)(m[1],0)) continue;
      xx(free)(&v);
      xx(free)(&u);
      xx(free)(&f);
      xx(free)(m+1);
      xx(free)(m);
      xx(free)(&pp);
      xx(copy_int)(a,0);
      return;
    }

    /* sinon, �l�ve � la puissance f1[0] jusqu'� avoir un �l�ment de Fp */
    xx(copy_int)(&f,f1[0]);
    for (l=1; (l < f1[1]) && (xx(neq_1)(m[1],0)); l++) {
      xx(mul_1)(a,*a,f1[0]);
      powmod(m,f,pp,d);

    }
    if (xx(neq_1)(m[1],0)) xx(mul_1)(a,*a,f1[0]); else xx(copy)(&v,m[0]);
  }

  /* si v = 0, on n'a jamais calcul� (1+sqrt(d)^a, le fait maintenant */
  if (xx(eq_1)(v,0)) {
    xx(copy_int)(m,  1);
    xx(copy_int)(m+1,1);
    powmod(m,*a,pp,d);
    xx(copy)(&v,m[0]);
  }

  /* cherche u minimal tq v^u = 1 mod p */
  for (xx(copy_int)(&u,p-1); *f0; f0 += 2) {
    for(l = f0[1]; l; l--) xx(quo_1)(&u,u,f0[0]);
    xx(copy_int)(&f,f0[0]);
    xx(gpowmod)(m,v,u,pp,1);

    for (l=1; (l < f0[1]) && (xx(neq_1)(m[0],1)); l++) {
      xx(mul_1)(&u,u,f0[0]);
      xx(gpowmod)(m,m[0],f,pp,1);
    }
    if (xx(neq_1)(m[0],1)) xx(mul_1)(&u,u,f0[0]);
  }

  /* r�sultat = a*u */
  xx(mul)(a,*a,u);
  xx(free)(&v);
  xx(free)(&u);
  xx(free)(&f);
  xx(free)(m+1);
  xx(free)(m);
  xx(free)(&pp);

}

        /* +--------------------------------------------------------+
           |  r�soud (1+sqrt(d))^k = 1-sqrt(d) modulo p, (d/p) = 1  |
           +--------------------------------------------------------+ */

/*
  entr�e :
    p  = nombre premier
    f0 = factorisation de p-1
    d  = discriminant tq (d/p) = 1

  sortie :
    ord <- ordre de 1+sqrt(d) modulo p
    cherche s'il existe k >= 0 tq (1+sqrt(d))^k = 1-sqrt(d) modulo p et s'il
    n'existe pas de discriminant d' divisant ord tq |d'| < |d| et (d'/k) = -1
  si k existe et d' n'existe pas :
     a[0] <- ord
     a[1] <- valeur minimale pour k
   sinon :
     a[0] <- 0
     a[1] <- ind.
*/
void ordre_0(long p, long *f0, long d, xint *a) {
  xint f,m[2],pp,x,y;
  long d1,d2,k,l,u;

  pp   = xx(of_int)(p);
  m[0] = xx(new)();
  m[1] = xx(new)();
  f    = xx(new)();
  x    = xx(new)();
  y    = xx(new)();
  xx(copy_int)(a,1);
  xx(copy_int)(a+1,0);
  d1 = (d < 0) ? -d : d;

  for (; *f0; f0+=2) {

    for(u=p-1, l=f0[1]; l; l--, u /= f0[0]);
    xx(copy_int)(&f, u);
    xx(copy_int)(m,  1);
    xx(copy_int)(m+1,1);
    powmod(m,f,pp,d);

    /* d�termine la classe de congruence de k modulo f0[0]^l :

       M^u scalaire => k =  1
       det(M^u) = 1 => k = -1

       et pour f0[0] = 2 :

       M^u antidiagonale => k =  1 + 2^(l-1) (cod� 3)
       det(M^u) = -1     => k = -1 - 2^(l-1) (cod� -3)

       si aucune de ces conditions n'est satisfaite, k n'existe pas
    */
    if (xx(eq_1)(m[1],0)) k = 1;
    else if ((f0[0] == 2) && (xx(eq_1)(m[0],0))) k = 3;
    else {
      xx(sqr)(&x,m[0]); xx(sqr)(&y,m[1]); xx(mul_1)(&y,y,d); xx(sub)(&y,y,x);
      k = xx(gmod_1)(y,p,1);
      if (k == -1) {
	d2 = (f0[0] == 3) ? 15 : f0[0];
	if (((d2&3) == 3) && (d2 < d1)) {xx(copy_int)(a,0); break;}
      }
      else if ((k == 1) && (f0[0] == 2)) k = -3;
      else {xx(copy_int)(a,0); break;}
    }

    /* d�termine l'exposant de f0[0] dans l'ordre de (1+sqrt(d)) modulo p */
    xx(copy_int)(&f,f0[0]);
    for (u=1, l=1;
	 (l < f0[1]) && ((xx(neq_1)(m[0],1)) || (xx(neq_1)(m[1],0)));
	 l++, u *= f0[0]) powmod(m,f,pp,d);
    if ((xx(neq_1)(m[0],1)) || (xx(neq_1)(m[1],0))) u *= f0[0];

    /* nouvelle congruence pour k */
    if (u > 1) {
      if (k == 3) k = 1 + u/2; else if(k == -3) k = -1 + u/2;
      if (!(k&1)) {xx(copy_int)(a,0); break;}
      if (k < 0) k += u;

      xx(copy_int)(&x,u);
      xx(gcd_ex)(NULL,&x,&y,x,a[0]);            /* x*u - y*a[0] = 1     */
      xx(mul_1)(&x,x,u); xx(mul)(&x,x,a[1]);    /* x    <- x*u*a[1]     */
      xx(mul_1)(&y,y,k); xx(mul)(&y,y,a[0]);    /* y    <- y*k*a[0]     */
      xx(mul_1)(a,a[0],u);                      /* a[0] <- a[0]*u       */
      xx(sub)(&x,x,y);   xx(mod)(a+1,x,a[0]);   /* a[1] <- x-y mod a[0] */
    }
  }

  /* termin� */
  xx(free)(&y);
  xx(free)(&x);
  xx(free)(&f);
  xx(free)(m+1);
  xx(free)(m);
  xx(free)(&pp);
}

             /* +-------------------------------------+
                |  Examine tous les nombres premiers  |
                +-------------------------------------+ */

void look(char *fn) {
  long d,i,j,k,*pi,f0[2*xp+1],f1[2*xp+1];
  unsigned long *p,js[kq];
  xint *b,c,g,n,q,t[2];
  char *s,msg[80];
  FILE *f;

  /* structures de donn�es pour les suites arithm�tiques � explorer  */
  xint m[2*kp+1];  /* coefficients a,b pour la suite (n*a+b)         */
  long mi[kp];     /* mi[k] = index sur m pour le k-�me discriminant */
  long mp[2*kp];   /* indexs sur m pour le tri topologique           */
  long i0,i1,i2;   /* indexs de remplissage pour m                   */

  /* initialisations */
  if (fn) {
    f = fopen(fn,"w");
    if (!f) {printf("open error on file %s\n",fn); exit(1);}
  } else f = NULL;
  g = xx(new)();
  n = xx(new)();
  q = xx(new)();
  c = xx(of_int)(0);
  t[0] = xx(new)(); t[1] = xx(new)();
  for (i=0; i<=2*kp; i++) m[i] = xx(new)();

  /* boucle sur p */
  for (p = plist; p - plist < np*dp; p += dp) {

    /* initialise le tableau m */
    for (i=0; i<kp; i++) mi[i] = -1;
    i0 = 2*kp; i1 = 0; i2 = -1;

    /* factorise p-1 et p+1 */
    factor(p[0]-1,f0);
    factor(p[0]+1,f1);

    /* on de doit pas d�passer 4^xp/p[0] */
    xx(copy_int)(&g,1);
    xx(shl)(  &g, g, 2*xp);
    xx(quo_1)(&g, g, *p  );

    /* examine les discriminants possibles */
    for (k=0; k < kp; k++) if (tstbit(p+1+kq,k)) {

      /* discriminant et symbole de Jacobi */
      if (k) {d = sprimes[k]; j = tstbit(p+1,k);}
      else   {d = 15;         j = tstbit(p+1,0)^tstbit(p+1,1);}
      if (d&2) d = -d;

      /* d�termine les param�tres de congruence pour le cofacteur de p */
      if (j) {

	/* cas (d/p) = -1 : a = ppcm(2, ordre(1+sqrt(d))), b = 1 */
	ordre_1(*p,f0,f1,d,m+i1);
	if (xx(eq_1)(m[i1],0)) continue;

	if (xx(nth_bit)(m[i1],0)) xx(add)(m+i1,m[i1],m[i1]);

	/* v�rifie que a+1 <= 4^xp/p[0] */
	if (xx(supeq)(m[i1],g)) continue;

	/* on ins�re a dans la liste croissante des nombres d�j� trouv�s.
	   Le tableau mp indique pour chaque �l�ment de m l'indice de
	   l'�l�ment suivant, le premier de la liste est celui d'indice i2
	   et la fin de la liste est rep�r�e par un indice �gal = -1 */

	for (pi = &i2; *pi >= 0; pi = mp+(*pi)) {
	  j = xx(cmp)(m[i1],m[*pi]);
	  if (j <= 0) break;
	}
	if (j) {mp[i1] = *pi; *pi = i1; i1++;}
	mi[k] = *pi;
      }

      else {

	/* cas (d/p) = 1 : ordre_0 d�termine les param�tres a,b �ventuels
	   avec a = 0 s'il n'y a pas de solution. Lorsqu'il y a une solution
	   on a b^2 = 1 mod a donc a,b ne sont pas tous deux pairs. Si a
	   est impair alors on peut remplacer (a,b) par (2a,a+b) ou (2a,b)
	   selon que b est pair ou impair. */

	ordre_0(*p,f0,d,m+i0-2);
	if (xx(eq_1)(m[i0-2],0)) continue;

	i0 -= 2;
	mi[k] = i0; mp[i0] = i0+2;
	if (xx(nth_bit)(m[i0],0)) {
	  if (!xx(nth_bit)(m[i0+1],0)) xx(add)(m+i0+1,m[i0],m[i0+1]);
	  xx(add)(m+i0,m[i0],m[i0]);
	}
      }
    } /* for k */

    /* regroupe les suites obtenues quand (d/p) = -1 par ordre d'inclusion.
       Si m[i] d�signe une suite maximale, pour chaque j tel que m[j] est
       strictement inclus dans m[i] on remplace le lien de cha�nage mp[j] par
       la valeur -2-i et le coefficient m[j] associ� par le quotient m[j]/m[i] */

    for (i=i2; i>=0; i=mp[i]) 
      for (pi = mp+i; *pi >= 0; ) {
	j = *pi;
	xx(quomod)(m+i1,m+i1+1,m[j],m[i]);
	if (xx(neq_1)(m[i1+1],0)) pi = mp+j;
	else {
	  xx(copy)(m+j,m[i1]);
	  *pi = mp[j];
	  mp[j] = -2-i;
	}
      }

    /* ins�re les suites correspondant � (d/p) = 1 en t�te de liste */
    if (i0 < 2*kp) {mp[2*kp-2] = i2; i2 = i0;}

    /* examine les suites obtenues */
    for (i = i2; i >= 0; i=mp[i]) {

      /* pour (d/p) = -1 on commence la suite (k*a+1) � k=1 car on veut
	 un cofacteur non trivial. Pour (d/p) = 1 on a b != 1 donc on
	 doit commencer � k=0 */

      if (i < i1) {b = m+2*kp; xx(add_1)(b,m[i],1);} else  {b = m+i+1;}

      if (fn) {
	fprintf(f,"%ld",p[0]);
	s = xx(string_of)(m[i]); fprintf(f," %s",s); free(s);
	if (i < i1) fprintf(f," 1");
	else {s = xx(string_of)(*b);   fprintf(f," %s",s); free(s);}
	for (j=0; j<kp; j++) {
	  d = (j) ? sprimes[j] : 15; if (d&2) d = -d;
	  if (mi[j] == i) fprintf(f," %ld",d);
	  else if ((mi[j] >= 0) && (mp[mi[j]] == -i-2)) fprintf(f," (%ld)",d);
	}
	fprintf(f,"\n");
      }

      /* lance le criblage */
      xx(sub)(  &q, g, *b  );
      xx(quo)(  &q, q, m[i]);
      xx(add_1)(&q, q, 1   ); if (xx(infeq_1)(q,0)) continue;
      cr_init(m[i],*b,q);
      while (cr_next(&q,js)) {

	/* cherche le discriminant associ� � q */
	for (j=0; (j<kq) && ((k = js[j]^p[j+1]) == 0); j++);
	if (k == 0) continue; /* collision => q = p*(un carr�) */
	for (j *= 8*sizeof(long); (k&1) == 0; j++, k>>=1);
	if ((j==0) && (k&30)) do {j++; k>>=1;} while ((k&1) == 0);
	d = (j) ? sprimes[j] : 15; if (d&2) d = -d;

	/* v�rifie que q est dans la suite associ�e � ce discriminant */
	if (mi[j] != i) {
	  if ((mi[j] < 0) || (mp[mi[j]] != -i-2)) continue;
	  xx(gmod)(t,q,m[mi[j]],2);
	  if (xx(neq_1)(t[0],-1)) continue;
	}
	xx(add_1)(&c,c,1);

	/* v�rifie que (1-d)^(pq-1) = 1 mod q */
	xx(mul)(&q,q,m[i]); xx(add)(&q,q,*b);
	xx(mul_1)(&n,q,*p); xx(sub_1)(&n,n,1);
	xx(copy_int)(t,1-d);
	xx(powmod)(t,t[0],n,q);
	if (xx(neq_1)(t[0],1)) continue;

	/* v�rifie que (1+sqrt(d))^(pq) = 1-sqrt(d) mod pq */
	xx(add_1)(&n,n,1);
	xx(copy_int)(t,1); xx(copy_int)(t+1,1);
	if (xx(mod_1)(q,p[0])) powmod(t,n,q,d); else powmod(t,n,n,d);
	if (xx(eq_1)(t[0],1) & xx(eq_1)(t[1],-1)) {
	  s = xx(string_of)(q);
	  printf("p=%ld q=%s d=%ld",p[0],s,d);
	  free(s);
	}
      } /* while(cr_next()) */
    } /* for i */
  } /* for p */

  /* termin� */
  if (fn) {fflush(f); fclose(f);}
  s = xx(string_of)(c); sprintf(msg,"%s composites tested",s); chrono(msg);
  free(s);
  xx(free)(t);
  xx(free)(t+1);
  xx(free)(&c);
  xx(free)(&q);
  xx(free)(&n);
  xx(free)(&g);
  for (i=2*kp; i>=0; i--) xx(free)(m+i);
  
}


                         /* +-----------------------+
                            |  Programme principal  |
                            +-----------------------+ */

#define default_sp 10
#define default_xp 20
#define default_kp 64
#define default_cp 20

int main(int argc, char **argv) {
  long i,ph;
  char *f1, *f2;

  /* analyse la ligne de commande */
  sp = default_sp;
  xp = default_xp;
  kp = default_kp;
  lc = 1 << default_cp;
  ph = 3;
  f1 = NULL;
  f2 = NULL;

  for (argc--, argv++; argc; argc -= i, argv += i) {

    /* options � un argument */
    i=2; if (argc >= i) {
      if (!strcmp(*argv,"-s"))  {sp = atol(argv[1]);      continue;}
      if (!strcmp(*argv,"-k"))  {kp = atol(argv[1]);      continue;}
      if (!strcmp(*argv,"-x"))  {xp = atol(argv[1]);      continue;}
      if (!strcmp(*argv,"-c"))  {lc = 1 << atol(argv[1]); continue;}
      if (!strcmp(*argv,"-p"))  {ph = atol(argv[1]);      continue;}
      if (!strcmp(*argv,"-f1")) {f1 = argv[1];            continue;}
      if (!strcmp(*argv,"-f2")) {f2 = argv[1];            continue;}
    }

    /* options non reconnues */ 
    printf("usage: prime-test [-s s] [-x x] [-k k] [-c c] [-p p] [-f1 f1] [-f2 f2]\n");
    printf("  s:  small primes are those <= 2^s,  default = %d\n",default_sp);
    printf("  x:  search composites <= 4^x,       default = %d\n",default_xp);
    printf("  k:  compute k first Jacobi symbols, default = %d\n",default_kp);
    printf("  c:  use sieves of 2^c bits,         default = %d\n",default_cp);
    printf("  p:  do phase p only                 default = do phases 1 & 2\n");
    printf("  f1: phase-1 output file             default = no output\n");
    printf("  f2: phase-2 output file             default = no output\n");
    return(0);
  }

  /* lance les calculs */
  init();
  cr_alloc();
  if (ph&1) disc_util(f1); else if (ph&2) read_phase_1(f1);
  if (ph&2) look(f2);

  return(0);

}
