// file exemples/c/simple.c: simple demo of Numerix
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                    Exemple d'utilisation de Numerix                   |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* compute  (sqrt(3) + sqrt(2))/(sqrt(3)-sqrt(2)) with n digits */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "numerix.h"


/*
  compute into res the quotient with n digits
  res must be initialized by the caller
*/
void compute(xint *res, long n) {
    xint a,b,d,d2,x,y;

   /* d <- 10^n, d2 <- 10^(2n) */
   d  = xx(f_pow_1)(5,n); xx(shl)(&d,d,n);
   d2 = xx(f_sqr)(d);

   /* a <- round(sqrt(2*10^(2n+2))), b <- round(sqrt(3*10^(2n+2)))  */
   a = xx(f_mul_1)(d2,200); xx(gsqrt)(&a,a,1);
   b = xx(f_mul_1)(d2,300); xx(gsqrt)(&b,b,1);

   /* res <- round(10^n*(b+a)/(b-a)) */
   x = xx(f_add)(b,a); xx(mul)(&x,x,d);
   y = xx(f_sub)(b,a);
   xx(gquo)(res,x,y,1);

   /* free temporary memory */
   xx(free)(&d); xx(free)(&d2);
   xx(free)(&a); xx(free)(&b);
   xx(free)(&x); xx(free)(&y);

}

/* user interface */
int main(int argc, char **argv) {

#define test_n 30
#define test_r "9898979485566356196394568149411"

  long n = test_n;
  xint r = xx(new)();
  char *s, *cmd = argv[0];
  int  test = 0;

  for(argc--, argv++; (argc); argc--, argv++) {
    if (strcmp(*argv,"-test") == 0) {
        test = 1;
        n = test_n;
        break;
    }
    else if ((strcmp(*argv,"-n") == 0) && (argc > 0)) {
      argc--, argv++;
      n = atol(*argv);
    }
    else {
      printf("usage: %s [-test] [-n <digits>]\n",cmd);
      fflush(stdout);
      exit(1);
    }
  }

  compute(&r,n);
  s = xx(string_of)(r);
  if (test) {
      if ((strcmp)(s,test_r)) printf("error in the %s test\n",cmd);
      else                    printf("%s\ttest ok\n",cmd);
  }
  else printf("r=%s\n",s);
  free(s);
  
#if 0
  /*
    better way : (sqrt(3) + sqrt(2))/(sqrt(3)-sqrt(2)) = 5 + sqrt(24)
    so we compute sqrt(24) with n digits, transform into a string
    and add 5 to the first digit (is this cheating ?)
  */

  xx(copy_int)(&r,5); xx(pow)(&r,r,2*n); xx(shl)(&r,r,2*n);
  xx(mul_1)(&r,r,24);
  xx(gsqrt)(&r,r,1);
  s = xx(string_of)(r); s[0] += 5; printf("r=%s\n",s); free(s);
#endif

  /* done */
  xx(free)(&r);
  fflush(stdout);
  return(0);
}

               
