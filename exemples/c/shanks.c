// file exemples/c/shanks.c: square root modulo an odd prime
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |            Racine carr�e modulo p, algorithme de Shanks               |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "numerix.h"

                       /* +------------------------+
                          |  Symbole de Kronecker  |
                          +------------------------+ */

/* a = naturel quelconque, b = naturel impair */
long kronecker(xint a, xint b) {
    xint x = xx(f_copy)(a);
    xint y = xx(f_copy)(b);
    xint z;
    long i,k;

    /* Algorithme d'Euclide + r�ciprocit� quadratique */
    k = 1;
    while (xx(neq_1)(x,0)) {

        /* i <- valuation 2-adique de x, x <- x/2^i */
        for (i=0; xx(nth_bit)(x,i) == 0; i++);
        xx(shr)(&x, x, i);

        /* (2^i/y) = -1 ssi i est impair et y = 3 ou 5 mod 8 */
        if ((i&1) && (((xx(nth_word)(y,0) + 2) & 7) > 4)) k = -k;

        /* (x/y) = (y/x) si x ou y = 1 mod 4, -(y/x) sinon */
        if ((xx(nth_bit)(x,1)) && (xx(nth_bit)(y,1))) k = -k;

        /* y <- x, x <- y mod x */
        z = x; x = y; y = z; xx(gmod)(&x, x, y, 1);

        /* (-1/y) = 1 ssi y = 1 mod 4 */
        if (xx(sgn)(x) < 0) {xx(abs)(&x,x); if (xx(nth_bit)(y,1)) k = -k;}
    }

    /* si pgcd(x,y) != 1 alors (x/y) = 0 */
    if (xx(neq_1)(y,1)) k = 0;

    xx(free)(&x);
    xx(free)(&y);
    return(k);
}



                      /* +---------------------------+
                         |  Racine carr�e modulaire  |
                         +---------------------------+ */

/* p = premier impair, a = b^2 mod p, retourne b (compris entre 0 et p/2) */
void sqrtmod(xint *_b, xint a, xint p) {
#define b *_b
    xint q = xx(new)();
    xint x = xx(new)();
    xint y = xx(new)();
    xint z = xx(new)();
    long k,l;

    /* d�compose p = 1 + 2^k*(2*q+1) */
    for (k=1; xx(nth_bit)(p,k) == 0; k++); xx(shr)(&q, p, k+1);

    /* b <- a^(q+1) mod p, x <- a^(2*q+1) mod p (donc b^2 = a*x mod p) */
    xx(powmod)(&x, a, q, p);
    xx(mul)(_b, x, a); xx(mod)(_b, b, p);
    xx(mul)(&x, x, b); xx(mod)(&x, x, p);

    if (xx(sup_1)(x,1)) {

        /* y <- �l�ment d'ordre 2^k */
        do xx(nrandom)(&y, xx(nbits)(p)); while (kronecker(y,p) != -1);
        xx(shl)(&q, q, 1); xx(add_1)(&q, q, 1);
        xx(powmod)(&y, y, q, p);

        do {
            /* ordre de x = 2^l */
            xx(sqr)(&z, x); xx(mod)(&z, z, p);
            for (l=1; (l < k) && (xx(neq_1)(z,1)); l++) {
                xx(sqr)(&z, z); xx(mod)(&z, z, p);
            }
            if (l >= k) {
                printf("internal error: l >= k\n");
                fflush(stdout);
                exit(1);
            }

            /* b <- b*y^(2^(k-l-1))
               x <- x*y^(2^(k-l)), 
               y <- y^(2^(k-l))
               donc b^2 = a*x mod p, ordre(y) = 2^l et ordre(x) < 2^l */
            for (; l < k-1; k--) {xx(sqr)(&y, y); xx(mod)(&y, y, p);}
            xx(mul)(_b, b, y); xx(mod)(_b, b, p);
            xx(sqr)(&y, y);    xx(mod)(&y, y, p); k--;
            xx(mul)(&x, x, y); xx(mod)(&x, x, p);

        } while(xx(neq_1)(x,1));
    }

    /* r�duit b � l'intervalle [0,p/2] */
    xx(gmod)(_b, b, p, 1); xx(abs)(_b, b);

    /* termin� */
    xx(free)(&x);
    xx(free)(&y);
    xx(free)(&z);
#undef b
}

                        /* +-----------------------+
                           |  Fonction principale  |
                           +-----------------------+ */

int main(int argc, char **argv) {
    int help=0, test=0, p_ok=0, a_ok=0, n=200;
    xint p = xx(new)();
    xint a = xx(new)();
    xint b = xx(new)();
    char *s, *cmd = argv[0];
    char *test_p = "100000000000000000000000000000000000133",
         *test_a = "123456",
         *test_b = "36705609512061164177523976477230978260";

    /* d�code les arguments */
    argv++;
    while ((!help) && (*argv)) {
        if (strcmp(*argv,"-p") == 0) {
            argv++;
            if (*argv) {xx(copy_string)(&p,*argv); p_ok = 1;}
            else help = 1;
        }
        else if (strcmp(*argv,"-a") == 0) {
            argv++;
            if (*argv) {xx(copy_string)(&a,*argv); a_ok = 1;}
            else help = 1;
        }
        else if (strcmp(*argv,"-bits") == 0) {
            argv++;
            if (*argv) n = atoi(*argv); else help = 1;
        }
        else if (strcmp(*argv,"-test") == 0) {
            xx(copy_string)(&p,test_p);
            xx(copy_string)(&a,test_a);
            test = p_ok = a_ok = 1;
            break;
        }
        else help=1;
        argv++;
    }
    if (help) printf("usage: %s [-p <odd prime>] [-a <quadres mod p>] [-test] [-bits n]\n",cmd);
    else do {

        xx(random_init)(0);

        /* si p est d�fini, v�rifie qu'il est premier impair.
           sinon tire un nombre premier impair au hasard */
        if (p_ok) {
            if ((xx(nth_bit)(p,0) == 0) || (!xx(isprime)(p))) {
                printf("p is not an odd prime\n");
                break;
            }
        } else {
            xx(nrandom1)(&p, n); if (xx(nth_bit)(p,0) == 0) xx(add_1)(&p,p,1);
            do xx(add_1)(&p, p, 2); while (!xx(isprime)(p));
            s = xx(string_of)(p); printf("p = %s\n",s); free(s);
        }

        /* si a est d�fini, v�rifie que c'est un carr� non nul modulo p.
           sinon tire un r�sidu quadratique au hasard.*/
        if (a_ok) {
            if (kronecker(a,p) != 1) {
                printf("kronecker(a,p) != 1\n");
                break;
            }
        } else {
            xx(nrandom)(&a,n);
            do xx(add_1)(&a, a, 1); while (kronecker(a,p) != 1);
            s = xx(string_of)(a); printf("a = %s\n",s); free(s);
        }

        /* calcule la racine carr�e */
        sqrtmod(&b, a, p);
        s = xx(string_of)(b);
        if (test) {
            if (strcmp(s,test_b)) printf("error in the %s test\n",cmd);
            else                  printf("%s\ttest ok\n",cmd);
        } else                    printf("b = %s\n",s);
        free(s);

    } while(0);

    xx(free)(&p);
    xx(free)(&a);
    xx(free)(&b);
    fflush(stdout);
    return 0;
}
