// file exemples/c/pi-gmp.c: compute some digits of pi
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                    Calcul de Pi, formule de Ramanujan                 |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* cf. "The Caml Numbers Reference Manual", Inria, RT-0141 */
/* annexe A, pp. 115 et suivantes.                         */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <gmp.h>

/* les 1000 premiers chiffres de pi pour contr�le */
char *true_pi  =
"31415926535897932384626433832795028841971693993751"
"05820974944592307816406286208998628034825342117067"
"98214808651328230664709384460955058223172535940812"
"84811174502841027019385211055596446229489549303819"
"64428810975665933446128475648233786783165271201909"
"14564856692346034861045432664821339360726024914127"
"37245870066063155881748815209209628292540917153643"
"67892590360011330530548820466521384146951941511609"
"43305727036575959195309218611738193261179310511854"
"80744623799627495673518857527248912279381830119491"
"29833673362440656643086021394946395224737190702179"
"86094370277053921717629317675238467481846766940513"
"20005681271452635608277857713427577896091736371787"
"21468440901224953430146549585371050792279689258923"
"54201995611212902196086403441815981362977477130996"
"05187072113499999983729780499510597317328160963185"
"95024459455346908302642522308253344685035261931188"
"17101000313783875288658753320838142061717766914730"
"35982534904287554687311595628638823537875937519577"
"81857780532171226806613001927876611195909216420198";

/* chronom�trage */
extern void chrono(char *msg);


                       /* +--------------------------+
                          |  Sommation dichotomique  |
                          +--------------------------+ */

#define maxprof 32 /* profondeur de r�cursion maximale */
void somme(long prec, mpz_t *num, mpz_t *den) {

  long  etapes = (prec+197)/94; /* nombre de termes � calculer */
  mpz_t pile[3*maxprof];        /* pile de r�cursion */
  mpz_t *sp = pile;             /* pointeur de pile  */

/* constantes  */
#define a  13591409
#define b 545140134

  mpz_t c,p,alpha,beta,gamma,delta,eps,t,u; 
  long i,j;

  mpz_init_set_str(c,"10939058860032000",10);
  mpz_init_set_ui(p,0);                  /* index s�rie */
  mpz_init_set_ui(alpha,1);              /* 2p + 1      */
  mpz_init_set_ui(beta,1);               /* 6p + 1      */
  mpz_init_set_ui(gamma,5);              /* 6p + 5      */
  mpz_init_set_ui(delta,53360);          /* c*p^3       */
  mpz_init_set_ui(eps,a);                /* a + bp      */
  mpz_init(t);                           /* scratch     */
  mpz_init(u);                           /* scratch     */

  /* initialise la pile */
  for (i=0; i < 3*maxprof; i++) mpz_init(pile[i]);

  for (i=1; i <= etapes; i++) {

    /* calcule et retranche les termes de rangs p et p+1 */
    mpz_mul  (t,      alpha,  beta);
    mpz_mul  (sp[0],  t,      gamma);
    mpz_set  (sp[1],  delta); 
    mpz_set  (sp[2],  eps);   
                                
    mpz_add_ui(p,     p,      1);
    mpz_add_ui(alpha, alpha,  2);
    mpz_add_ui(beta,  beta,   6);
    mpz_add_ui(gamma, gamma,  6);
    mpz_mul  (t,      p,      p);
    mpz_mul  (u,      c,      p);
    mpz_mul  (delta,  t,      u);
    mpz_add_ui(eps,   eps,    b);
                                
    mpz_mul  (t,      delta,  sp[2]);
    mpz_mul  (u,      sp[0],  eps);
    mpz_sub  (sp[2],  t,      u);
    mpz_mul  (t,      alpha,  beta);
    mpz_mul  (u,      sp[0],  gamma);
    mpz_mul  (sp[0],  t,      u);
    mpz_mul  (sp[1],  sp[1],  delta);
                                
    mpz_add_ui(p,     p,      1);
    mpz_add_ui(alpha, alpha,  2);
    mpz_add_ui(beta,  beta,   6);
    mpz_add_ui(gamma, gamma,  6);
    mpz_mul  (t,      p,      p);
    mpz_mul  (u,      c,      p);
    mpz_mul  (delta,  t,      u);
    mpz_add_ui(eps,   eps,    b);

    sp += 3;
    
    /* combine avec les calculs pr�c�dents */
    for (j=1; (j&i) == 0; j <<= 1) {
      sp -= 3;

      mpz_mul(t,      sp[1],  sp[-1]);
      mpz_mul(sp[-1], sp[-3], sp[2]);
      mpz_add(sp[-1], sp[-1], t);
      mpz_mul(sp[-3], sp[-3], sp[0]);
      mpz_mul(sp[-2], sp[-2], sp[1]);

    }
  }

  /* termine les calculs en instance */
  sp -= 3;
  while (sp != pile) {
    sp -= 3;

    mpz_mul(t,     sp[4],  sp[2]);
    mpz_mul(sp[2], sp[0],  sp[5]);
    mpz_add(sp[2], sp[2],  t);
    mpz_mul(sp[1], sp[1],  sp[4]);
  }

  /* nettoie les variables locales et retourne la fraction */
  mpz_clear(pile[0]);
  memmove(num,pile+1,sizeof(mpz_t));
  memmove(den,pile+2,sizeof(mpz_t));
  for (i=3; i<3*maxprof; i++) mpz_clear(pile[i]);
  mpz_clear(c);
  mpz_clear(p);
  mpz_clear(alpha);
  mpz_clear(beta);
  mpz_clear(gamma);
  mpz_clear(delta);
  mpz_clear(eps);
  mpz_clear(t);
  mpz_clear(u);
}


                 /* +--------------------------------------+
                    |  Calcule pi avec digits+2 d�cimales  |
                    +--------------------------------------+ */

void pi(long digits, int pgcd, int print, int skip, int debug, int test, char *cmd) {

  long  prec, i,j;
  mpz_t num;
  mpz_t den;
  mpz_t t;
  char *s;
  char ss[80];
  
  if (debug) chrono("start");

  /* t <- 5^(digits+2) */
  mpz_init(t);
  mpz_ui_pow_ui(t, 5, digits+2);
  if (debug) chrono("puiss-5");

  /* t <- floor( sqrt(640320) * 10^(digits+2) ) */
  prec = mpz_sizeinbase(t,2) + digits;
  mpz_mul     (t,  t,  t);
  mpz_mul_ui  (t,  t,  640320);
  mpz_mul_2exp(t,  t,  2*digits+4);
  mpz_sqrt    (t,  t);
  if (debug) chrono("sqrt");

  /* num/den <- somme de la s�rie � env. 10^(-digits-2) pr�s */
  somme(prec,&num,&den);
  if (debug) {sprintf(ss,"series lb=%ld",(long)mpz_sizeinbase(num,2)); chrono(ss);}

  /* simplifie la fraction si demand� (ceci ne vaut pas le coup, le
     temps de calcul du pgcd est tr�s sup�rieur au temps de calcul
     de la division sans simplification) */
  if (pgcd) {
      mpz_t d;
      mpz_init(d);
      mpz_gcd(d,num,den);
      mpz_divexact(num,num,d);
      mpz_divexact(den,den,d);
      if (debug) {sprintf(ss,"gcd  lb=%ld",(long)mpz_sizeinbase(num,2)); chrono(ss);}
  }

  /* t <- sqrt(640320)*num/den * 10^digits+2) */
  mpz_mul   (t, num, t);
  mpz_fdiv_q(t, t, den);
  if (debug) chrono("quotient");

  /* on n'a plus besoin de num,den */
  mpz_clear(num);
  mpz_clear(den);

  /* conversion en d�cimal */
  if (print) {
    s = mpz_get_str(NULL,10,t);
    if (debug) chrono("conversion");

    printf("%c.\n",s[0]);
    for (i=1; (s[i]); i++) {
      printf("%c",s[i]);
      if      ((i%250) == 0) printf("\n\n");
      else if ((i%50)  == 0) printf("\n");
      else if ((i%10)  == 0) printf("  ");
      else if ((i%5)   == 0) printf(" ");
      if ((skip) && ((i%50) == 0)) {
        j = strlen(s+i)/50 - 1;
        if (j > 0) {printf("... (%ld lines omitted)\n",j); i += 50*j;}
      }
    }
    if ((i%50) != 1) printf("\n");
    free(s);
  }

  else if (test) {
      long l1,l2,l;

      s = mpz_get_str(NULL,10,t);
      l1 = strlen(s);
      l2 = strlen(true_pi);
      l = (l1 < l2) ? l1 : l2;
      if (strncmp(s,true_pi, l)) {
          printf("error in the %s test, value computed:\n",cmd);
          for (i=0; i<l; i++) {
              printf("%c",s[i]);
              if (i%50 == 49) printf("\n");
          }
          if (l%50) printf("\n");
      } else {
          printf("%s\t\ttest ok\n",cmd);
      }
      free(s);
  }

  /* termin� */
  mpz_clear(t);

}


int main(int argc, char **argv) {
  long digits=100;
  int  pgcd=0, print=1, skip=0, debug=0, help=0, test=0;
  char *cmd = argv[0];

  argv++;
  while (*argv) {
         if (strcmp(*argv,"-h") == 0)       {help = 1; break;}
    else if (strcmp(*argv,"-d") == 0)       debug = 1;
    else if (strcmp(*argv,"-noprint") == 0) print = 0;
    else if (strcmp(*argv,"-skip") == 0)    skip  = 1;
    else if (strcmp(*argv,"-gcd") == 0)     pgcd  = 1;
    else if (strcmp(*argv,"-test") == 0)    {digits=1000;
                                             print=skip=debug=pgcd=0;
                                             test=1;
                                             break;}
    else digits = strtol(argv[0],NULL,0);
    argv++;
  }

  if (help) printf("usage: %s [digits] [-d] [-noprint] [-skip] [-gcd]\n",cmd);
  else      pi(digits-2,pgcd,print,skip,debug,test,cmd);

  fflush(stdout);
  return(0);
}
