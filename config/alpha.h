// file config/alpha.h: configuration options for DEC alpha processors
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |               Configuration pour processeurs DEC alpha                |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* Machine word size */
#define bits_64

/* Memory allocation strategy  */
@use_alloca@

/* Double-long available */
@have_long_long@

                          /* +---------------------+
                             |  Debugging options  |
                             +---------------------+ */

/*
   When a "debug_xxx" symbol is defined, every call to "xxx" will be
   monitored by a special control function that checks arguments
   and results. Except when debugging Numerix, leave all the following
   symbols undefined !
*/
   
/* memory allocation */
#undef debug_alloc

/* multiplication-square */
#undef debug_mul_n2
#undef debug_karamul
#undef debug_toommul
#undef debug_mmul
#undef debug_butterfly
#undef debug_smul
#undef debug_sjoin
#undef debug_fftmul

/* division */
#undef debug_div_n2
#undef debug_burnidiv
#undef debug_moddiv
#undef debug_karpdiv

/* square root */
#undef debug_sqrt_n2
#undef debug_zimsqrt
#undef debug_modsqrt

/* pth root */
#undef debug_root

/* modular exponentiation */
#undef debug_powmod

/* gcd */
#undef debug_gcd_n2
#undef debug_lehmer

/* primality */
#undef debug_isprime

                /* +-----------------------------------------+
                   |  Function written in assembly language  |
                   +-----------------------------------------+ */

#ifdef use_slong

/* comparison */
#define assembly_sn_cmp

/* addition/subtraction */
#define assembly_sn_add
#define assembly_sn_sub
#define assembly_sn_inc
#define assembly_sn_dec

/* multiplication and square */
#define assembly_sn_mul_1
#define assembly_sn_mul_n2
#define assembly_sn_sqr_n2
#define assembly_sn_karamul
#define assembly_sn_karasqr
#define assembly_sn_toommul
#define assembly_sn_toomsqr

/* operations modulo BASE^n + 1 */
#define assembly_sn_mmul
#define assembly_sn_msqr
#define assembly_sn_butterfly

/* operations modulo BASE^n - 1 */
#define assembly_sn_ssub
#define assembly_sn_smul
#define assembly_sn_ssqr
#define assembly_sn_sjoin3

/* shifts */
#define assembly_sn_shift_down
#define assembly_sn_shift_up

/* division */
#define assembly_sn_div_1
#define assembly_sn_mod_1
#define assembly_sn_div_n2
#define assembly_sn_burnidiv

/* square root */
#define assembly_sn_sqrt_n2

/* modular exponentiation */
#define assembly_sn_mgdiv_n2

/* gcd */
#define assembly_sn_gcd_2
#define assembly_sn_hgcd_2

#endif /* use_slong */

                             /* +--------------+
                                |  Thresholds  |
                                +--------------+ */

/*
  The constants below tell when to switch between various algorithms
  for an operation. They have been setup for an Alpha-ev6
*/


/*
   -------------------- mul(a,la,b,lb,c)

   if 2*lb > fftmul_lim :
     if la+lb  > fftmul_lim_8 -> fft(13)
     if la+lb  > fftmul_lim_7 -> fft(12)
     if la+lb  > fftmul_lim_6 -> fft(11)
     if la+lb  > fftmul_lim_5 -> fft(10)
     if la+lb  > fftmul_lim_4 -> fft(9)
     if la+lb  > fftmul_lim_3 -> fft(8)
     if la+lb  > fftmul_lim_2 -> fft(7)
     if la+lb  > fftmul_lim_1 -> smul(24)
     else                     -> smul(12)

   if 2*lb <= fftmul_lim :
     if lb > toommul_lim      -> toommul
     if lb > karamul_lim      -> karamul
     else                     -> mul_n2

   -------------------- remdiv(a,lc,b,lb,c)

   if lc > remdiv_lim :
     if lb+1 > fftmul_lim_8 -> fft(13)
     if lb+1 > fftmul_lim_7 -> fft(12)
     if lb+1 > fftmul_lim_6 -> fft(11)
     if lb+1 > fftmul_lim_5 -> fft(10)
     if lb+1 > fftmul_lim_4 -> fft(9)
     if lb+1 > fftmul_lim_3 -> fft(8)
     if lb+1 > fftmul_lim_2 -> fft(7)
     if lb+1 > fftmul_lim_1 -> smul(24)
     else                   -> smul(12)

   if lc <= remdiv_lim :    -> toommul

   -------------------- smul(a,la,b,lb,c,n)

   if n is even and n > smul_lim_even -> smul
   if n is odd  and n > smul_lim_odd  -> smul    (assembly version)
   else                               -> toommul

   -------------------- mmul(a,b,n)
   if n > mmul_lim -> mmul
   else            -> toommul

*/

#if defined(use_clong)

#define karamul_lim         31
#define toommul_lim        100
#define fftmul_lim         348
#define fftmul_lim_1      1600
#define fftmul_lim_2      7200
#define fftmul_lim_3     16000
#define fftmul_lim_4     40000
#define fftmul_lim_5    120000
#define fftmul_lim_6    360000
#define fftmul_lim_7    450000
#define fftmul_lim_8   3600000
#define remdiv_lim          72
#define smul_lim_even       12
#define mmul_lim            17

#elif defined(use_dlong)

#define karamul_lim         15
#define toommul_lim         46
#define fftmul_lim         214
#define fftmul_lim_1      2000
#define fftmul_lim_2      4000
#define fftmul_lim_3     12000
#define fftmul_lim_4     21000
#define fftmul_lim_5     60000
#define fftmul_lim_6    120000
#define fftmul_lim_7    440000
#define fftmul_lim_8   1800000
#define remdiv_lim          72
#define smul_lim_even        8
#define mmul_lim             8

#elif defined(use_slong)

#define karamul_lim         20
#define toommul_lim        100
#define fftmul_lim         294
#define fftmul_lim_1      6000
#define fftmul_lim_2      6000
#define fftmul_lim_3     16000
#define fftmul_lim_4     30000
#define fftmul_lim_5     80000
#define fftmul_lim_6    180000
#define fftmul_lim_7    800000
#define fftmul_lim_8   2000000
#define remdiv_lim          72
#define smul_lim_even       15
#define smul_lim_odd        14
#define mmul_lim            17

#endif

/*
   -------------------- sqr(a,la,b)

   if 2*la > fftsqr_lim :
     if 2*la > fftsqr_lim_8 -> fft(13)
     if 2*la > fftsqr_lim_7 -> fft(12)
     if 2*la > fftsqr_lim_6 -> fft(11)
     if 2*la > fftsqr_lim_5 -> fft(10)
     if 2*la > fftsqr_lim_4 -> fft(9)
     if 2*la > fftsqr_lim_3 -> fft(8)
     if 2*la > fftsqr_lim_2 -> fft(7)
     if 2*la > fftsqr_lim_1 -> ssqr(24)
     else                   -> ssqr(12)

   if 2*la <= fftsqr_lim :
     if la > toomsqr_lim    -> toomsqr
     if la > karasqr_lim    -> karasqr
     else                   -> sqr_n2

   -------------------- remsqrt(a,la,b)

   if lc > remsqrt_lim :
     if lb+1 > fftsqr_lim_8 -> fft(13)
     if lb+1 > fftsqr_lim_7 -> fft(12)
     if lb+1 > fftsqr_lim_6 -> fft(11)
     if lb+1 > fftsqr_lim_5 -> fft(10)
     if lb+1 > fftsqr_lim_4 -> fft(9)
     if lb+1 > fftsqr_lim_3 -> fft(8)
     if lb+1 > fftsqr_lim_2 -> fft(7)
     if lb+1 > fftsqr_lim_1 -> ssqr(24)
     else                   -> ssqr(12)

   if lc <= remsqrt_lim :   -> toomsqr

   -------------------- ssqr(a,la,b,n)

   if n is even and n > ssqr_lim_even -> ssqr
   if n is odd  and n > ssqr_lim_odd  -> ssqr    (assembly version)
   else                               -> toomsqr

   -------------------- msqr(a,n)
   if n > msqr_lim -> msqr
   else            -> toomsqr

*/

#if defined(use_clong)

#define karasqr_lim         43
#define toomsqr_lim        100
#define fftsqr_lim         362
#define fftsqr_lim_1      1600
#define fftsqr_lim_2      6200
#define fftsqr_lim_3     16000
#define fftsqr_lim_4     40000
#define fftsqr_lim_5    120000
#define fftsqr_lim_6    360000
#define fftsqr_lim_7    450000
#define fftsqr_lim_8   3600000
#define remsqrt_lim        144
#define ssqr_lim_even       16
#define msqr_lim            26

#elif defined(use_dlong)

#define karasqr_lim         23
#define toomsqr_lim         64
#define fftsqr_lim         214
#define fftsqr_lim_1      2000
#define fftsqr_lim_2      4000
#define fftsqr_lim_3     12000
#define fftsqr_lim_4     21000
#define fftsqr_lim_5     60000
#define fftsqr_lim_6    120000
#define fftsqr_lim_7    440000
#define fftsqr_lim_8   1800000
#define remsqrt_lim        144
#define ssqr_lim_even        8
#define msqr_lim            11

#elif defined(use_slong)

#define karasqr_lim         37
#define toomsqr_lim        100
#define fftsqr_lim         372
#define fftsqr_lim_1      6000
#define fftsqr_lim_2      6000
#define fftsqr_lim_3     16000
#define fftsqr_lim_4     30000
#define fftsqr_lim_5     80000
#define fftsqr_lim_6    180000
#define fftsqr_lim_7    800000
#define fftsqr_lim_8   2000000
#define remsqrt_lim        144
#define ssqr_lim_even       15
#define ssqr_lim_odd        18
#define msqr_lim            23

#endif

/*
   -------------------- div(a,lc,b,lb,c)

   if lb > karpdiv_lim_1 and lc > 1.50*lb         -> karpdiv
   if lb > karpdiv_lim_2 and lc > karpdiv_lim_2   -> karpdiv
   if lb > moddiv_lim    and lc > div_small_c_lim -> moddiv
   if lb > burnidiv_lim  and lc > div_small_c_lim -> burnidiv
   else                                           -> div_n2

   -------------------- inv(a,la,b)

   if la > karpinv_lim -> karpinv
   else                -> moddiv

*/


#if defined(use_clong)

#define div_small_c_lim     15
#define burnidiv_lim        30
#define moddiv_lim         261
#define karpinv_lim        144
#define karpdiv_lim_1      200
#define karpdiv_lim_2      500

#elif defined(use_dlong)

#define div_small_c_lim     17
#define burnidiv_lim        35
#define moddiv_lim         145
#define karpinv_lim        144
#define karpdiv_lim_1       35
#define karpdiv_lim_2      150

#elif defined(use_slong)

#define div_small_c_lim     20
#define burnidiv_lim        40
#define moddiv_lim         225
#define karpinv_lim        144
#define karpdiv_lim_1      214
#define karpdiv_lim_2      600

#endif

/* -------------------- sqrt(a,la,b)

  if la > modsqrt_lim -> modsqrt
  if la > zimsqrt_lim -> zimsqrt
  else                -> sqrt_n2

*/

#if defined(use_clong)

#define zimsqrt_lim         24
#define modsqrt_lim        600

#elif defined(use_dlong)

#define zimsqrt_lim        180
#define modsqrt_lim        600

#elif defined(use_slong)

#define zimsqrt_lim         40
#define modsqrt_lim        600

#endif

/* -------------------- powmod(a,la,b,lb,c,lc,d)

  if c mod 2 = 0 -> powmod
  if lc = 1      -> powmod
  if c mod 2 = 1 -> powmod_mg with ...
    if lc <= montgomery_lim_1 -> mgdiv_n2
    if lc <= montgomery_lim_2 -> mgdiv_i, toommul, smul
    else                      -> mgdiv_i, fftmul
*/
  
#if defined(use_clong)

#define montgomery_lim_1 105
#define montgomery_lim_2 10000 /* > 1000, et trop long pour �tre d�termin� */

#elif defined(use_dlong)

#define montgomery_lim_1 65
#define montgomery_lim_2 10000 /* > 1000, et trop long pour �tre d�termin� */

#elif defined(use_slong)

#define montgomery_lim_1 100
#define montgomery_lim_2 10000 /* > 1000, et trop long pour �tre d�termin� */

#endif

/* -------------------- gcd(a,b,mode)

  mode = 0
    if min(la,lb) <= lehmer_lim_0 -> gcd_n2
    else                          -> lehmer

  mode = 1 or 2
    if min(la,lb) <= lehmer_lim_1 -> gcd_n2
    else                          -> lehmer
*/

#if defined (use_clong)

#define lehmer_lim_0    700
#define lehmer_lim_1    250

#elif defined (use_dlong)

#define lehmer_lim_0    800
#define lehmer_lim_1    280

#elif defined (use_slong)

#define lehmer_lim_0   1200
#define lehmer_lim_1    280

#endif

  

/* -------------------- End of config file -------------------- */
