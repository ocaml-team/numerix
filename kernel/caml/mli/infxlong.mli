(* file kernel/caml/mli/infxlong.mli: Infix bindings
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                           Opérateurs infixes                          |
 |                                                                       |
 +-----------------------------------------------------------------------*)

#open "_name_";;

  value prefix  ++   :   t -> t   -> t;;    (* add      *)
  value prefix  --   :   t -> t   -> t;;    (* sub      *)
  value prefix  **   :   t -> t   -> t;;    (* mul      *)
  value prefix  //   :   t -> t   -> t;;    (* quo      *)
  value prefix  %%   :   t -> t   -> t;;    (* modulo   *)
  value prefix  /%   :   t -> t   -> t*t;;  (* quomod   *)
  value prefix  <<   :   t -> int -> t;;    (* shl      *)
  value prefix  >>   :   t -> int -> t;;    (* shr      *)
  value prefix  ^^   :   t -> int -> t;;    (* pow      *)
                   
  value prefix  +=   : tref -> t -> unit;;  (* add_in   *)
  value prefix  -=   : tref -> t -> unit;;  (* sub_in   *)
  value prefix  *=   : tref -> t -> unit;;  (* mul_in   *)
  value prefix  /=   : tref -> t -> unit;;  (* quo_in   *)
  value prefix  %=   : tref -> t -> unit;;  (* mod_in  *)
                
  value prefix  +.   :   t -> int -> t;;    (* add_1    *)
  value prefix  -.   :   t -> int -> t;;    (* sub_1    *)
  value prefix  *.   :   t -> int -> t;;    (* mul_1    *)
  value prefix  /.   :   t -> int -> t;;    (* quo_1    *)
  value prefix  %.   :   t -> int -> int;;  (* mod_1    *)
  value prefix  /%.  :   t -> int -> t*int;;(* quomod_1 *)
  value prefix  ^.   : int -> int -> t;;    (* pow_1    *)
                   
  value prefix  +=.  : tref -> int -> unit;;(* add_1_in *)
  value prefix  -=.  : tref -> int -> unit;;(* sub_1_in *)
  value prefix  *=.  : tref -> int -> unit;;(* mul_1_in *)
  value prefix  /=.  : tref -> int -> unit;;(* quo_1_in *)

  value prefix  =.   :   t -> int -> bool;; (* eq_1     *)
  value prefix  <>.  :   t -> int -> bool;; (* neq_1    *)
  value prefix  <.   :   t -> int -> bool;; (* inf_1    *)
  value prefix  <=.  :   t -> int -> bool;; (* infeq_1  *)
  value prefix  >.   :   t -> int -> bool;; (* sup_1    *)
  value prefix  >=.  :   t -> int -> bool;; (* supeq_1  *)
                   
  value prefix  ?    : tref -> t;;          (* look     *)
