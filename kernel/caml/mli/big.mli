(* file kernel/caml/mli/big.mli: Caml-light bignum interface
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                       D�finition du module Big                        |
 |                                                                       |
 +-----------------------------------------------------------------------*)

type t;;                      (* entier         *)
type tref;;                   (* entier mutable *)

value name : unit -> string;; (* nom du module  *)
value zero : t;;              (* the number 0   *)
value one  : t;;              (* the number 1   *)

(* mode d'arrondi *)
type round_mode = Floor | Nearest_up | Ceil | Nearest_down;;

(* r�sultat ternaire *)
type tristate = False | Unknown | True;;

(* r�f�rence        ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value make_ref    :                               t ->               tref   ;;
value copy_in     :               tref ->         t ->               unit   ;;
value copy_out    :               tref ->                            t      ;;
value look        :               tref ->                            t      ;;

(* addition         ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value add         :                               t -> t   ->        t      ;;
value add_1       :                               t -> int ->        t      ;;
value add_in      :               tref ->         t -> t   ->        unit   ;;
value add_1_in    :               tref ->         t -> int ->        unit   ;;

(* soustraction     ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value sub         :                               t -> t   ->        t      ;;
value sub_1       :                               t -> int ->        t      ;;
value sub_in      :               tref ->         t -> t   ->        unit   ;;
value sub_1_in    :               tref ->         t -> int ->        unit   ;;

(* multiplication   ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value mul         :                               t -> t   ->        t      ;;
value mul_1       :                               t -> int ->        t      ;;
value mul_in      :               tref ->         t -> t   ->        unit   ;;
value mul_1_in    :               tref ->         t -> int ->        unit   ;;

(* division         ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value quomod      :                               t -> t   ->        t*t    ;;
value div         :                               t -> t   ->        t      ;;
value modulo      :                               t -> t   ->        t      ;;
value gquomod     : round_mode ->                 t -> t   ->        t*t    ;;
value gquo        : round_mode ->                 t -> t   ->        t      ;;
value gmod        : round_mode ->                 t -> t   ->        t      ;;

value quomod_in   :               tref -> tref -> t -> t   ->        unit   ;;
value quo_in      :               tref ->         t -> t   ->        unit   ;;
value mod_in      :                       tref -> t -> t   ->        unit   ;;
value gquomod_in  : round_mode -> tref -> tref -> t -> t   ->        unit   ;;
value gquo_in     : round_mode -> tref ->         t -> t   ->        unit   ;;
value gmod_in     : round_mode ->         tref -> t -> t   ->        unit   ;;

value quomod_1    :                               t -> int ->        t*int  ;;
value quo_1       :                               t -> int ->        t      ;;
value mod_1       :                               t -> int ->        int    ;;
value gquomod_1   : round_mode ->                 t -> int ->        t*int  ;;
value gquo_1      : round_mode ->                 t -> int ->        t      ;;
value gmod_1      : round_mode ->                 t -> int ->        int    ;;

value quomod_1_in :               tref ->         t -> int ->        int    ;;
value quo_1_in    :               tref ->         t -> int ->        unit   ;;
value gquomod_1_in: round_mode -> tref ->         t -> int ->        int    ;;
value gquo_1_in   : round_mode -> tref ->         t -> int ->        unit   ;;

(* valeur absolue   ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value abs         :                               t ->               t      ;;
value abs_in      :               tref ->         t ->               unit   ;;

(* oppos�           ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value neg         :                               t ->               t      ;;
value neg_in      :               tref ->         t ->               unit   ;;

(* puissance p-�me  ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value sqr         :                               t ->               t      ;;
value pow         :                               t -> int ->        t      ;;
value pow_1       :                             int -> int ->        t      ;;
value powmod      :                               t -> t   -> t ->   t      ;;
value gpowmod     : round_mode ->                 t -> t   -> t ->   t      ;;
value sqr_in      :               tref ->         t ->               unit   ;;
value pow_in      :               tref ->         t -> int ->        unit   ;;
value pow_1_in    :               tref ->       int -> int ->        unit   ;;
value powmod_in   :               tref ->         t -> t   -> t ->   unit   ;;
value gpowmod_in  : round_mode -> tref ->         t -> t   -> t ->   unit   ;;

(* racine p-�me     ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value sqrt        :                               t ->               t      ;;
value root        :                               t -> int ->        t      ;;
value gsqrt       : round_mode ->                 t ->               t      ;;
value groot       : round_mode ->                 t -> int ->        t      ;;
value sqrt_in     :               tref ->         t ->               unit   ;;
value root_in     :               tref ->         t -> int ->        unit   ;;
value groot_in    : round_mode -> tref ->         t -> int ->        unit   ;;
value gsqrt_in    : round_mode -> tref ->         t ->               unit   ;;

(* factorielle      ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value fact        :                             int ->               t      ;;
value fact_in     :               tref ->       int ->               unit   ;;

(* pgcd                -------------------------------------------------------- *)
(*                    d     u     v     p    q    a    b      c      res        *)
value gcd         :                               t -> t   ->        t      ;;
value gcd_ex      :                               t -> t   ->        t*t*t  ;;
value cfrac       :                               t -> t   ->     t*t*t*t*t ;;
value gcd_in      : tref->                        t -> t   ->        unit   ;;
value gcd_ex_in   : tref->tref->tref->            t -> t   ->        unit   ;;
value cfrac_in    : tref->tref->tref->tref->tref->t -> t   ->        unit   ;;
  
(* primalit�      ----------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res  *)
value isprime     :                               t ->               tristate;;
value isprime_1   :                             int ->               tristate;;

(* comparaison      ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value sgn         :                               t ->               int    ;;
value cmp         :                               t -> t   ->        int    ;;
value cmp_1       :                               t -> int ->        int    ;;
value eq          :                               t -> t   ->        bool   ;;
value eq_1        :                               t -> int ->        bool   ;;
value neq         :                               t -> t   ->        bool   ;;
value neq_1       :                               t -> int ->        bool   ;;
value inf         :                               t -> t   ->        bool   ;;
value inf_1       :                               t -> int ->        bool   ;;
value infeq       :                               t -> t   ->        bool   ;;
value infeq_1     :                               t -> int ->        bool   ;;
value sup         :                               t -> t   ->        bool   ;;
value sup_1       :                               t -> int ->        bool   ;;
value supeq       :                               t -> t   ->        bool   ;;
value supeq_1     :                               t -> int ->        bool   ;;

(* conversion       ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value of_int      :                             int ->               t      ;;
value of_string   :                          string ->               t      ;;
value of_int_in   :               tref ->       int ->               unit   ;;
value of_string_in:               tref ->    string ->               unit   ;;
value int_of      :                               t ->               int    ;;
value string_of   :                               t ->               string ;;
value bstring_of  :                               t ->               string ;;
value hstring_of  :                               t ->               string ;;
value ostring_of  :                               t ->               string ;;

(* nombre al�atoire ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value nrandom     :                             int->                t      ;;
value zrandom     :                             int->                t      ;;
value nrandom1    :                             int->                t      ;;
value zrandom1    :                             int->                t      ;;
value nrandom_in  :               tref ->       int->                unit   ;;
value zrandom_in  :               tref ->       int->                unit   ;;
value nrandom1_in :               tref ->       int->                unit   ;;
value zrandom1_in :               tref ->       int->                unit   ;;
value random_init :                             int->                unit;;

(* repr�sentation binaire  ---------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value nbits       :                               t ->               int    ;;
value lowbits     :                               t ->               int    ;;
value highbits    :                               t ->               int    ;;
value nth_word    :                               t -> int ->        int    ;;
value nth_bit     :                               t -> int ->        bool   ;;

(* d�calage         ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value shl         :                               t -> int ->        t      ;;
value shr         :                               t -> int ->        t      ;;
value split       :                               t -> int ->        t*t    ;;
value join        :                               t -> t   -> int -> t      ;;
value shl_in      :               tref ->         t -> int ->        unit   ;;
value shr_in      :               tref ->         t -> int ->        unit   ;;
value split_in    :               tref -> tref -> t -> int ->        unit   ;;
value join_in     :               tref ->         t -> t   -> int -> unit   ;;

(* affichage        ----------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res        *)
value chrono      :                               string ->          unit   = 1 "chrono";;
value toplevel_print   :                          t ->               unit   ;;
value toplevel_print_tref:        tref ->                            unit   ;;
