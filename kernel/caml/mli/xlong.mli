(* file kernel/caml/mli/xlong.mli: Caml-Light extensible integer definitions
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                           D�finitions                                 |
 |                                                                       |
 +-----------------------------------------------------------------------*)

type t;;                      (* entier         *)
type tref;;                   (* entier mutable *)

value name : unit -> string;; (* nom du module  *)
value zero : t;;              (* the number 0   *)
value one  : t;;              (* the number 1   *)

(* mode d'arrondi *)
type round_mode = Floor | Nearest_up | Ceil | Nearest_down;;

(* r�sultat ternaire *)
type tristate = False | Unknown | True;;

(* r�f�rence        ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value make_ref    :                               t ->               tref   = 1 "xx(make_ref)";;
value copy_in     :               tref ->         t ->               unit   = 2 "xx(copy)";;
value copy_out    :               tref ->                            t      = 1 "xx(copy_out)";;
value look        :               tref ->                            t      = 1 "xx(look)";;

(* addition         ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value add         :                               t -> t   ->        t      = 2 "xx(f_add)";;
value add_1       :                               t -> int ->        t      = 2 "xx(f_add_1)";;
value add_in      :               tref ->         t -> t   ->        unit   = 3 "xx(add)";;
value add_1_in    :               tref ->         t -> int ->        unit   = 3 "xx(add_1)";;

(* soustraction     ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value sub         :                               t -> t   ->        t      = 2 "xx(f_sub)";;
value sub_1       :                               t -> int ->        t      = 2 "xx(f_sub_1)";;
value sub_in      :               tref ->         t -> t   ->        unit   = 3 "xx(sub)";;
value sub_1_in    :               tref ->         t -> int ->        unit   = 3 "xx(sub_1)";;

(* multiplication   ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value mul         :                               t -> t   ->        t      = 2 "xx(f_mul)";;
value mul_1       :                               t -> int ->        t      = 2 "xx(f_mul_1)";;
value mul_in      :               tref ->         t -> t   ->        unit   = 3 "xx(mul)";;
value mul_1_in    :               tref ->         t -> int ->        unit   = 3 "xx(mul_1)";;

(* division         ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value quomod      :                               t -> t   ->        t*t    = 2 "xx(f_quomod)";;
value div         :                               t -> t   ->        t      = 2 "xx(f_quo)";;
value modulo      :                               t -> t   ->        t      = 2 "xx(f_mod)";;
value gquomod     : round_mode ->                 t -> t   ->        t*t    = 3 "xx(f_gquomod)";;
value gquo        : round_mode ->                 t -> t   ->        t      = 3 "xx(f_gquo)";;
value gmod        : round_mode ->                 t -> t   ->        t      = 3 "xx(f_gmod)";;

value quomod_in   :               tref -> tref -> t -> t   ->        unit   = 4 "xx(quomod)";;
value quo_in      :               tref ->         t -> t   ->        unit   = 3 "xx(quo)";;
value mod_in      :                       tref -> t -> t   ->        unit   = 3 "xx(mod)";;
value gquomod_in  : round_mode -> tref -> tref -> t -> t   ->        unit   = 5 "xx(gquomod)";;
value gquo_in     : round_mode -> tref ->         t -> t   ->        unit   = 4 "xx(gquo)";;
value gmod_in     : round_mode ->         tref -> t -> t   ->        unit   = 4 "xx(gmod)";;

value quomod_1    :                               t -> int ->        t*int  = 2 "xx(f_quomod_1)";;
value quo_1       :                               t -> int ->        t      = 2 "xx(f_quo_1)";;
value mod_1       :                               t -> int ->        int    = 2 "xx(f_mod_1)";;
value gquomod_1   : round_mode ->                 t -> int ->        t*int  = 3 "xx(f_gquomod_1)";;
value gquo_1      : round_mode ->                 t -> int ->        t      = 3 "xx(f_gquo_1)";;
value gmod_1      : round_mode ->                 t -> int ->        int    = 3 "xx(f_gmod_1)";;

value quomod_1_in :               tref ->         t -> int ->        int    = 3 "xx(quomod_1)";;
value quo_1_in    :               tref ->         t -> int ->        unit   = 3 "xx(quo_1)";;
value gquomod_1_in: round_mode -> tref ->         t -> int ->        int    = 4 "xx(gquomod_1)";;
value gquo_1_in   : round_mode -> tref ->         t -> int ->        unit   = 4 "xx(gquo_1)";;

(* valeur absolue   ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value abs         :                               t ->               t      = 1 "xx(f_abs)";;
value abs_in      :               tref ->         t ->               unit   = 2 "xx(abs)";;

(* oppos�           ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value neg         :                               t ->               t      = 1 "xx(f_neg)";;
value neg_in      :               tref ->         t ->               unit   = 2 "xx(neg)";;

(* puissance p-�me  ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value sqr         :                               t ->               t      = 1 "xx(f_sqr)";;
value pow         :                               t -> int ->        t      = 2 "xx(f_pow)";;
value pow_1       :                             int -> int ->        t      = 2 "xx(f_pow_1)";;
value powmod      :                               t -> t   -> t ->   t      = 3 "xx(f_powmod)";;
value gpowmod     : round_mode ->                 t -> t   -> t ->   t      = 4 "xx(f_gpowmod)";;
value sqr_in      :               tref ->         t ->               unit   = 2 "xx(sqr)";;
value pow_in      :               tref ->         t -> int ->        unit   = 3 "xx(pow)";;
value pow_1_in    :               tref ->       int -> int ->        unit   = 3 "xx(pow_1)";;
value powmod_in   :               tref ->         t -> t   -> t ->   unit   = 4 "xx(powmod)";;
value gpowmod_in  : round_mode -> tref ->         t -> t   -> t ->   unit   = 5 "xx(gpowmod)";;

(* racine p-�me     ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value sqrt        :                               t ->               t      = 1 "xx(f_sqrt)";;
value root        :                               t -> int ->        t      = 2 "xx(f_root)";;
value gsqrt       : round_mode ->                 t ->               t      = 2 "xx(f_gsqrt)";;
value groot       : round_mode ->                 t -> int ->        t      = 3 "xx(f_groot)";;
value sqrt_in     :               tref ->         t ->               unit   = 2 "xx(sqrt)";;
value root_in     :               tref ->         t -> int ->        unit   = 3 "xx(root)";;
value groot_in    : round_mode -> tref ->         t -> int ->        unit   = 4 "xx(groot)";;
value gsqrt_in    : round_mode -> tref ->         t ->               unit   = 3 "xx(gsqrt)";;

(* factorielle      ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value fact        :                             int ->               t      = 1 "xx(f_fact)";;
value fact_in     :               tref ->       int ->               unit   = 2 "xx(fact)";;

(* pgcd                ----------------------------------------------------------------- *)
(*                    d     u     v     p    q    a    b      c      res       code C *)
value gcd         :                               t -> t   ->        t      = 2 "xx(f_gcd)";;
value gcd_ex      :                               t -> t   ->        t*t*t  = 2 "xx(f_gcd_ex)";;
value cfrac       :                               t -> t   ->     t*t*t*t*t = 2 "xx(f_cfrac)";;
value gcd_in      : tref->                        t -> t   ->        unit   = 3 "xx(gcd)";;
value gcd_ex_in   : tref->tref->tref->            t -> t   ->        unit   = 5 "xx(gcd_ex)";;
value cfrac_in    : tref->tref->tref->tref->tref->t -> t   ->        unit   = 7 "xx(cfrac_bytecode)";;
  
(* primalit�      ----------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res  *)
value isprime     :                               t ->               tristate= 1 "xx(isprime)";;
value isprime_1   :                             int ->               tristate= 1 "xx(isprime_1)";;

(* comparaison      ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value sgn         :                               t ->               int    = 1 "xx(sgn)";;
value cmp         :                               t -> t   ->        int    = 2 "xx(cmp)";;
value cmp_1       :                               t -> int ->        int    = 2 "xx(cmp_1)";;
value eq          :                               t -> t   ->        bool   = 2 "xx(eq)";;
value eq_1        :                               t -> int ->        bool   = 2 "xx(eq_1)";;
value neq         :                               t -> t   ->        bool   = 2 "xx(neq)";;
value neq_1       :                               t -> int ->        bool   = 2 "xx(neq_1)";;
value inf         :                               t -> t   ->        bool   = 2 "xx(inf)";;
value inf_1       :                               t -> int ->        bool   = 2 "xx(inf_1)";;
value infeq       :                               t -> t   ->        bool   = 2 "xx(infeq)";;
value infeq_1     :                               t -> int ->        bool   = 2 "xx(infeq_1)";;
value sup         :                               t -> t   ->        bool   = 2 "xx(sup)";;
value sup_1       :                               t -> int ->        bool   = 2 "xx(sup_1)";;
value supeq       :                               t -> t   ->        bool   = 2 "xx(supeq)";;
value supeq_1     :                               t -> int ->        bool   = 2 "xx(supeq_1)";;

(* conversion       ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value of_int      :                             int ->               t      = 1 "xx(of_int)";;
value of_string   :                          string ->               t      = 1 "xx(of_string)";;
value of_int_in   :               tref ->       int ->               unit   = 2 "xx(copy_int)";;
value of_string_in:               tref ->    string ->               unit   = 2 "xx(copy_string)";;
value int_of      :                               t ->               int    = 1 "xx(int_of)";;
value string_of   :                               t ->               string = 1 "xx(string_of)";;
value bstring_of  :                               t ->               string = 1 "xx(bstring_of)";;
value hstring_of  :                               t ->               string = 1 "xx(hstring_of)";;
value ostring_of  :                               t ->               string = 1 "xx(ostring_of)";;

(* nombre al�atoire ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value nrandom     :                             int->                t      = 1 "xx(f_nrandom)";;
value zrandom     :                             int->                t      = 1 "xx(f_zrandom)";;
value nrandom1    :                             int->                t      = 1 "xx(f_nrandom1)";;
value zrandom1    :                             int->                t      = 1 "xx(f_zrandom1)";;
value nrandom_in  :               tref ->       int->                unit   = 2 "xx(nrandom)";;
value zrandom_in  :               tref ->       int->                unit   = 2 "xx(zrandom)";;
value nrandom1_in :               tref ->       int->                unit   = 2 "xx(nrandom1)";;
value zrandom1_in :               tref ->       int->                unit   = 2 "xx(zrandom1)";;
value xrandom_init:                             int->                unit   = 1 "xx(random_init)";;
value random_init :                             int->                unit;;

(* repr�sentation binaire  ---------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value nbits       :                               t ->               int    = 1 "xx(nbits)";;
value lowbits     :                               t ->               int    = 1 "xx(lowbits)";;
value highbits    :                               t ->               int    = 1 "xx(highbits)";;
value nth_word    :                               t -> int ->        int    = 2 "xx(nth_word)";;
value nth_bit     :                               t -> int ->        bool   = 2 "xx(nth_bit)";;

(* d�calage         ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value shl         :                               t -> int ->        t      = 2 "xx(f_shl)";;
value shr         :                               t -> int ->        t      = 2 "xx(f_shr)";;
value split       :                               t -> int ->        t*t    = 2 "xx(f_split)";;
value join        :                               t -> t   -> int -> t      = 3 "xx(f_join)";;
value shl_in      :               tref ->         t -> int ->        unit   = 3 "xx(shl)";;
value shr_in      :               tref ->         t -> int ->        unit   = 3 "xx(shr)";;
value split_in    :               tref -> tref -> t -> int ->        unit   = 4 "xx(split)";;
value join_in     :               tref ->         t -> t   -> int -> unit   = 4 "xx(join)";;

(* affichage        ----------------------------------------------------------------- *)
(*                  mode          r       s       a    b      c      res       code C *)
value dump        :                               t ->               unit   = 1 "xx(dump)";;
value chrono      :                               string ->          unit   = 1 "chrono";;
value toplevel_print   :                          t ->               unit;;
value toplevel_print_tref:        tref ->                            unit;;
