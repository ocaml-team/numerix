(* file kernel/caml/ml/toplevelcfg.ml: Caml-Light toplevel configuration
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                        Configuration du toplevel                      |
 |                                                                       |
 +-----------------------------------------------------------------------*)

#open "printf";;

let _ =
  toplevel__directory "_directory_";
  let modules_list = _mode_list_ [] in
  printf "camlnumx : Caml toplevel with big integer libraries\nNumerix submodules :";
  do_list (fun x -> printf " %s" x) modules_list;
  printf "\nNumerix version    : _numerix_version_\n\n";
  let tlip x = toplevel__install_printer (sprintf "%s__toplevel_print" x);
               toplevel__install_printer (sprintf "%s__toplevel_print_tref" x)
  in do_list tlip modules_list
;;
