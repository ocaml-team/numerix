(* file kernel/caml/ml/infxlong.ml: Infix bindings
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                           Opérateurs infixes                          |
 |                                                                       |
 +-----------------------------------------------------------------------*)

#open "_name_";;

  let prefix  ++   = add;;      
  let prefix  --   = sub;;      
  let prefix  **   = mul;;      
  let prefix  //   = div;;      
  let prefix  %%   = modulo;;   
  let prefix  /%   = quomod;;   
  let prefix  <<   = shl;;      
  let prefix  >>   = shr;;      
  let prefix  ^^   = pow;;      
                 
  let prefix  +=  r x = add_in r (look r) x;;
  let prefix  -=  r x = sub_in r (look r) x;;
  let prefix  *=  r x = mul_in r (look r) x;;
  let prefix  /=  r x = quo_in r (look r) x;;
  let prefix  %=  r x = mod_in r (look r) x;;
                
  let prefix  +.   = add_1;;    
  let prefix  -.   = sub_1;;    
  let prefix  *.   = mul_1;;    
  let prefix  /.   = quo_1;;    
  let prefix  %.   = mod_1;;    
  let prefix  /%.  = quomod_1;;
  let prefix  ^.   = pow_1;;      
                 
  let prefix  +=.  r x = add_1_in r (look r) x;;
  let prefix  -=.  r x = sub_1_in r (look r) x;;
  let prefix  *=.  r x = mul_1_in r (look r) x;;
  let prefix  /=.  r x = quo_1_in r (look r) x;;

  let prefix   =.  = eq_1;;     
  let prefix  <>.  = neq_1;;    
  let prefix   <.  = inf_1;;    
  let prefix  <=.  = infeq_1;;  
  let prefix   >.  = sup_1;;    
  let prefix  >=.  = supeq_1;;  

  let prefix ?     = look;;     
