(* file kernel/caml/ml/xlong.ml: Caml-Light extensible integer definitions
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                           D�finitions                                 |
 |                                                                       |
 +-----------------------------------------------------------------------*)

let name() = "" ^ "_Name_";; (* nom du module  *)
let zero = of_int 0;;
let one  = of_int 1;;

let random_init x = xrandom_init(x); random__init(x);;

(* affichage tronqu� � 1000 caract�res *)
let toplevel_print(a) =
  let s = string_of a      in
  let l = string_length(s) in
  if l < 1000 then format__print_string s
  else begin
    format__print_string (sub_string s 0 100);
    format__print_string " ... (";
    format__print_int    (l-200);
    format__print_string " digits) ... ";
    format__print_string (sub_string s (l-100) 100)
  end
;;

let toplevel_print_tref(a) =
  format__print_string "tref(";
  toplevel_print(look a);
  format__print_char `)`
;;
