// file kernel/caml/h/numerix.h: Caml extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                  D�finitions pour l'interface Caml                    |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include "../../n/h/numerix.h"
#include <mlvalues.h>
#include <memory.h>
#include <alloc.h>

/*
  Un entier extensible est repr�sent� par un "long" codant la longueur
  effective en nombre de chiffres et le signe, suivi du tableau des chiffres
  codant la valeur absolue (encadr� par deux marques si debug_alloc
  est d�fini). Le mot d'en-t�te du nombre x est interpr�t� comme suit :

    -- le bit de poids fort vaut 0 si x >= 0, 1 si x < 0
    -- les autres bits codent la longueur l avec x = 0 si l = 0
       et BASE^(l-1) <= |x| < BASE^l sinon.
    -- les chiffres d'indice >= l sont ignor�s
  La longueur maximale d'un entier extensible est cod�e (en nombre de mots)
  dans le mot d'en-t�te propre � Caml qui pr�c�de l'entier extensible.
*/

typedef struct {    /* entier extensible          */
#ifdef debug_alloc
  void (*finalize)(value v); /* lib�ration        */
#endif
  long    hd;       /* longueur r�elle et signe   */
#ifdef debug_alloc
  long    m1;       /* marque de d�but            */
#endif
  chiffre val[1];   /* 1 pour faire plaisir � gcc */
} * xint;

/* acc�s aux champs */
#ifdef debug_alloc
#define xx_capacity(a) (((a)==Val_null) ? -1 : \
  (Wosize_val((a)) - 4)*chiffres_per_long)
#else
#define xx_capacity(a) (((a)==Val_null) ? -1 : \
  (Wosize_val((a)) - 1)*chiffres_per_long)
#endif
#define xx_sgn(a) ((a)->hd & SIGN_m)
#define xx_lg(a)  ((a)->hd & LONG_m)

/* pointeur nul */
#define xx_null  (xint *)Val_unit
#define Val_null (xint)  Val_unit

/* Interface avec le GC */
#define xx_push_roots_1(a)              \
  struct {                              \
    typeof(a) a;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,1,c_roots_head};          \
  c_roots_head = &__lr.__nitems

#define xx_push_roots_2(a,b)            \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,2,c_roots_head};        \
  c_roots_head = &__lr.__nitems

#define xx_push_roots_3(a,b,c)          \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    typeof(c) c;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,c,3,c_roots_head};      \
  c_roots_head = &__lr.__nitems

#define xx_push_roots_4(a,b,c,d)        \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    typeof(c) c;                        \
    typeof(d) d;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,c,d,4,c_roots_head};    \
  c_roots_head = &__lr.__nitems

#define xx_push_roots_5(a,b,c,d,e)      \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    typeof(c) c;                        \
    typeof(d) d;                        \
    typeof(e) e;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,c,d,e,5,c_roots_head};  \
  c_roots_head = &__lr.__nitems

#define xx_push_roots_6(a,b,c,d,e,f)    \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    typeof(c) c;                        \
    typeof(d) d;                        \
    typeof(e) e;                        \
    typeof(f) f;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,c,d,e,f,6,c_roots_head};\
  c_roots_head = &__lr.__nitems

#define xx_push_roots_11(a,b)           \
  struct {                              \
    typeof(a) a;                        \
    xint      b;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,Val_null,2,c_roots_head}; \
  c_roots_head = &__lr.__nitems

#define xx_push_roots_21(a,b,c)         \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    xint      c;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,Val_null,3,c_roots_head};\
  c_roots_head = &__lr.__nitems

#define xx_push_roots_32(a,b,c,d,e)     \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    typeof(c) c;                        \
    xint      d;                        \
    xint      e;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,c,Val_null,Val_null,5,c_roots_head};\
  c_roots_head = &__lr.__nitems

#define xx_push_roots_42(a,b,c,d,e,f)   \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    typeof(c) c;                        \
    typeof(d) d;                        \
    xint      e;                        \
    xint      f;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,c,d,Val_null,Val_null,6,c_roots_head};\
  c_roots_head = &__lr.__nitems

#define xx_push_roots_75(a,b,c,d,e,f,g,h,i,j,k,l) \
  struct {                              \
    typeof(a) a;                        \
    typeof(b) b;                        \
    typeof(c) c;                        \
    typeof(d) d;                        \
    typeof(e) e;                        \
    typeof(f) f;                        \
    typeof(g) g;                        \
    xint      h;                        \
    xint      i;                        \
    xint      j;                        \
    xint      k;                        \
    xint      l;                        \
    value __nitems; value *__cr;}       \
    __lr = {a,b,c,d,e,f,g,              \
            Val_null,Val_null,Val_null, \
            Val_null,Val_null,          \
            12,c_roots_head};           \
  c_roots_head = &__lr.__nitems

#define xx_pop_roots() c_roots_head = __lr.__cr

/* mise � jour d'une r�f�rence */
extern inline void xx(update)(xint *_x, xint y) {
    if ((_x != xx_null) && (*_x != y)) modify((value *)_x,(value)y);
}

/* retour d'une valeur */
#define xx_update_and_return(_x,x) do {               \
    if (_x == xx_null) {                              \
        xx_pop_roots();                               \
        return(x);                                    \
    } else {                                          \
        if (*_x != x) modify((value *)_x,(value)x);   \
        xx_pop_roots();                               \
        return(Val_null);                             \
    }                                                 \
} while(0)

/* traitement des erreurs */
void failwith(char *msg) __attribute__((noreturn));
extern inline void xx(failwith)(char *msg) __attribute__((noreturn));
extern inline void xx(failwith)(char *msg) {failwith(msg);}

/* mode d'arrondi */
#define Round_val Tag_val

/* valeur 3 �tats */
#define Val_tri Atom

/* interface commune � toutes les api */
#define caml_api
#include "../../x/h/numerix.h"
