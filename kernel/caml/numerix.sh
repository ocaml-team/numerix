#!/bin/sh
# file kernel/caml/numerix.sh: output a suitable xlong.ml/mli file
#-----------------------------------------------------------------------+
#  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
#                                                                       |
#  This file is part of Numerix. Numerix is free software; you can      |
#  redistribute it and/or modify it under the terms of the GNU Lesser   |
#  General Public License as published by the Free Software Foundation; |
#  either version 2.1 of the License, or (at your option) any later     |
#  version.                                                             |
#                                                                       |
#  The Numerix Library is distributed in the hope that it will be       |
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
#  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
#  Lesser General Public License for more details.                      |
#                                                                       |
#  You should have received a copy of the GNU Lesser General Public     |
#  License along with the GNU MP Library; see the file COPYING. If not, |
#  write to the Free Software Foundation, Inc., 59 Temple Place -       |
#  Suite 330, Boston, MA 02111-1307, USA.                               |
#-----------------------------------------------------------------------+
#                                                                       |
#                        Cr�ation de xlong.ml/mli                       |
#                                                                       |
#-----------------------------------------------------------------------+

# argument = nom du fichier � cr�er
# d�termine le nom du fichier source et le mode associ�

filename=`basename $1`
case $filename in
*.ml)  ext=ml;;
*.mli) ext=mli;;
esac
case $filename in
  inf*)   source=kernel/caml/$ext/infxlong.$ext;;
  big*)   source=kernel/caml/$ext/big.$ext   ;;
  *long*) source=kernel/caml/$ext/xlong.$ext ;;
  *gmp*)  source=kernel/caml/$ext/xlong.$ext ;;
esac
case $filename in
 *clong*) mode=c; name=clong; Name=Clong;;
 *dlong*) mode=d; name=dlong; Name=Dlong;;
 *slong*) mode=s; name=slong; Name=Slong;;
 *big*)   mode=b; name=big;   Name=Big;;
 *gmp*)   mode=g; name=gmp;   Name=Gmp;;
esac

sed -e "s/xx(\([^)]*\))/${mode}x_\1/g" \
    -e "s/_Name_/$Name/g" \
    -e "s/_name_/$name/g" \
    $source >$1

