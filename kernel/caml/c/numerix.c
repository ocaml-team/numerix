// file kernel/caml/c/numerix.c: Caml extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                   Entiers extensibles, interface Caml                 |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include "../h/numerix.h"

#include "../../x/c/add.c"
#include "../../x/c/cmp.c"
#include "../../x/c/convert.c"
#include "../../x/c/copy.c"
#include "../../x/c/div.c"
#include "../../x/c/dump.c"
#include "../../x/c/fact.c"
#include "../../x/c/gcd.c"
#include "../../x/c/mul.c"
#include "../../x/c/pow.c"
#include "../../x/c/powmod.c"
#include "../../x/c/prime.c"
#include "../../x/c/random.c"
#include "../../x/c/root.c"
#include "../../x/c/shift.c"
#include "../../x/c/sqrt.c"
#include "../../x/c/string.c"

#ifdef debug_alloc

/* marques pour tester les débordements */
#if defined(bits_32)
#define magic_1 3141592653UL
#elif defined(bits_64)
#define magic_1 3141592653589793238UL
#endif
#if HW == 16
#define magic_2 27182
#elif HW == 32
#define magic_2 2718281828UL
#elif HW == 64
#define magic_2 2718281828459045235UL
#endif

/* compteur d'allocation */
static long xalloc = 0;
long xx(get_alloc_count)() {return(xalloc);}
#endif

/* libération */
#ifdef debug_alloc
void xx(finalize)(value v) {
    xint x = (xint)v;

    if (xalloc) xalloc--;
    else xx(internal_error)("unexpected call to xx(finalize)",0);

    if (   (xx_lg(x) <= xx_capacity(x))
           && (x->m1 == magic_1)
           && (x->val[xx_capacity(x)] == magic_2)) {
        return;
    }

    xx(internal_error)("buffer overflow (x layer)",0);
    
}
#endif

/* alloue max(2a,b) chiffres */
xint xx(alloc)(long a, long b) {
  long l;
  xint x;

  /* convertit 2a et b en nombre de long + taille header */
#if chiffres_per_long == 1
  a = 2*a + 1; b++;
#else
  a++; b = (b+3)/2;
#endif
#ifdef debug_alloc
  a += 3; b += 3;
#endif

  if (b > Max_wosize) failwith(NUMBER_TOO_BIG);
  if (a > Max_wosize) a = Max_wosize;
  l = (a > b) ? a : b;

#ifdef debug_alloc
  x = (xint)(alloc_shr(l,Final_tag));
  x->finalize = xx(finalize);
  x->m1 = magic_1;
  x->val[xx_capacity(x)] = magic_2;
  xalloc++;
#else
  x = (l <= Max_young_wosize) ? (xint)(alloc    (l,Abstract_tag)):
                                (xint)(alloc_shr(l,Abstract_tag));
#endif
  x->hd = 0;

  return(x);
}

