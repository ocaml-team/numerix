// file kernel/caml/c/gmp.c: Interface with Gmp
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                   Entiers extensibles, interface Gmp                  |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* headers standard */
#include <string.h>
#include <stdio.h>
#include <time.h>

/* header Gmp */
#include <gmp.h>

/* headers Caml */
#include <mlvalues.h>
#include <memory.h>
#include <alloc.h>

/* mode d'arrondi */
#define Round_val(x) (x) ? Tag_val(x) : 0

/* valeur 3 �tats */
#define Val_tri Atom

/* Un entier est vu depuis Caml comme une donn�e abstraite
   avec finalisation */
typedef struct {
  void *final_fun;
  mpz_t val;
} *gint;

void gx_finalize(value a){
  mpz_clear(((gint)a)->val);
}


                            /* +--------------+
                               |  allocation  |
                               +--------------+ */

/*
  gx_alloc_1_j(x,y1,..,yj) alloue x (gint) en pr�servant y1,..,yj.

  gx_alloc_n_i_j(r,n,x1,..,xi,y1,..,yj) alloue r (n-uplet) et x1,..,xi (gint)
  en pr�servant y1,..,yj.

  gx_alloc_string_1(s,n,y) alloue s (cha�ne de n caract�res) en pr�servant y

*/

#define gx_alloc_1_0(x) do {                                         \
    x = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(x->val);                                                \
} while(0)

#define gx_alloc_1_1(x,y1) do {                                      \
    struct {                                                         \
        typeof(y1) y1;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {y1,1,c_roots_head};                                      \
    c_roots_head = &__lr.__nitems;                                   \
    x = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(x->val);                                                \
    y1 = __lr.y1;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_1_2(x,y1,y2) do {                                   \
    struct {                                                         \
        typeof(y1) y1;                                               \
        typeof(y2) y2;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {y1,y2,2,c_roots_head};                                   \
    c_roots_head = &__lr.__nitems;                                   \
    x = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(x->val);                                                \
    y1 = __lr.y1;                                                    \
    y2 = __lr.y2;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_1_3(x,y1,y2,y3) do {                                \
    struct {                                                         \
        typeof(y1) y1;                                               \
        typeof(y2) y2;                                               \
        typeof(y3) y3;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {y1,y2,y3,3,c_roots_head};                                \
    c_roots_head = &__lr.__nitems;                                   \
    x = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(x->val);                                                \
    y1 = __lr.y1;                                                    \
    y2 = __lr.y2;                                                    \
    y3 = __lr.y3;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_n_1_1(r,n,x1,y1) do {                               \
    struct {                                                         \
        typeof(x1) x1;                                               \
        typeof(y1) y1;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {(gint)Val_unit,y1,2,c_roots_head};                       \
    c_roots_head = &__lr.__nitems;                                   \
    __lr.x1 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x1->val);                                          \
    r = alloc_tuple(n);                                              \
    x1 = __lr.x1;                                                    \
    y1 = __lr.y1;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_n_1_2(r,n,x1,y1,y2) do {                            \
    struct {                                                         \
        typeof(x1) x1;                                               \
        typeof(y1) y1;                                               \
        typeof(y2) y2;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {(gint)Val_unit,y1,y2,3,c_roots_head};                    \
    c_roots_head = &__lr.__nitems;                                   \
    __lr.x1 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x1->val);                                          \
    r = alloc_tuple(n);                                              \
    x1 = __lr.x1;                                                    \
    y1 = __lr.y1;                                                    \
    y2 = __lr.y2;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_n_2_1(r,n,x1,x2,y1) do {                            \
    struct {                                                         \
        typeof(x1) x1;                                               \
        typeof(x2) x2;                                               \
        typeof(y1) y1;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {(gint)Val_unit,(gint)Val_unit,y1,3,c_roots_head};        \
    c_roots_head = &__lr.__nitems;                                   \
    __lr.x1 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x1->val);                                          \
    __lr.x2 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x2->val);                                          \
    r = alloc_tuple(n);                                              \
    x1 = __lr.x1;                                                    \
    x2 = __lr.x2;                                                    \
    y1 = __lr.y1;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_n_2_2(r,n,x1,x2,y1,y2) do {                         \
    struct {                                                         \
        typeof(x1) x1;                                               \
        typeof(x2) x2;                                               \
        typeof(y1) y1;                                               \
        typeof(y2) y2;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {(gint)Val_unit,(gint)Val_unit,y1,y2,4,c_roots_head};     \
    c_roots_head = &__lr.__nitems;                                   \
    __lr.x1 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x1->val);                                          \
    __lr.x2 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x2->val);                                          \
    r = alloc_tuple(n);                                              \
    x1 = __lr.x1;                                                    \
    x2 = __lr.x2;                                                    \
    y1 = __lr.y1;                                                    \
    y2 = __lr.y2;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_n_3_2(r,n,x1,x2,x3,y1,y2) do {                      \
    struct {                                                         \
        typeof(x1) x1;                                               \
        typeof(x2) x2;                                               \
        typeof(x3) x3;                                               \
        typeof(y1) y1;                                               \
        typeof(y2) y2;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {(gint)Val_unit,(gint)Val_unit,(gint)Val_unit,y1,y2,5,c_roots_head};     \
    c_roots_head = &__lr.__nitems;                                   \
    __lr.x1 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x1->val);                                          \
    __lr.x2 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x2->val);                                          \
    __lr.x3 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x3->val);                                          \
    r = alloc_tuple(n);                                              \
    x1 = __lr.x1;                                                    \
    x2 = __lr.x2;                                                    \
    x3 = __lr.x3;                                                    \
    y1 = __lr.y1;                                                    \
    y2 = __lr.y2;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_n_5_2(r,n,x1,x2,x3,x4,x5,y1,y2) do {                \
    struct {                                                         \
        typeof(x1) x1;                                               \
        typeof(x2) x2;                                               \
        typeof(x3) x3;                                               \
        typeof(x4) x4;                                               \
        typeof(x5) x5;                                               \
        typeof(y1) y1;                                               \
        typeof(y2) y2;                                               \
        value __nitems; value *__cr;}                                \
    __lr = {(gint)Val_unit,(gint)Val_unit,(gint)Val_unit,(gint)Val_unit,(gint)Val_unit,y1,y2,7,c_roots_head};     \
    c_roots_head = &__lr.__nitems;                                   \
    __lr.x1 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x1->val);                                          \
    __lr.x2 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x2->val);                                          \
    __lr.x3 = (gint)alloc_final(1+sizeof(mpz_t)/sizeof(long),gx_finalize,0,1); \
    mpz_init(__lr.x3->val);                                          \
    r = alloc_tuple(n);                                              \
    x1 = __lr.x1;                                                    \
    x2 = __lr.x2;                                                    \
    x3 = __lr.x3;                                                    \
    x4 = __lr.x4;                                                    \
    x5 = __lr.x5;                                                    \
    y1 = __lr.y1;                                                    \
    y2 = __lr.y2;                                                    \
    c_roots_head = __lr.__cr;                                        \
} while(0)

#define gx_alloc_string_1(s,n,y) do {                                \
    struct {                                                         \
        typeof(y) y;                                                 \
        value __nitems; value *__cr;}                                \
    __lr = {y,1,c_roots_head};                                       \
    c_roots_head = &__lr.__nitems;                                   \
    s = (char *)alloc_string(n);                                     \
    y = __lr.y;                                                      \
    c_roots_head = __lr.__cr;                                        \
} while(0)

                            /* +--------------+
                               |  exceptions  |
                               +--------------+ */

void failwith(char *msg) __attribute__((noreturn));
#define gx_failwith failwith

/* code commun Caml-Ocaml */
#include "../../x/c/gmp.c"
