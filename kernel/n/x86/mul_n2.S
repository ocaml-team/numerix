// file kernel/n/x86/mul_n2.S: O(n^2) multiplication of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                        Multiplication quadratique                     |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                      # +------------------------------+
                      # |  Multiplication par un long  |
                      # +------------------------------+

# unsigned long xn(mul_1)(chiffre *a, long la, unsigned long b, chiffre *c)
#
# entr�e :
# a = naturel de longueur la
# b = long >= 0
# c = naturel de longueur la, peut �tre confondu avec a
#
# sortie :
# c <- a*b
# retourne la retenue

#ifdef assembly_sn_mul_1
ENTER(sn_mul_1)

#if defined(use_sse2)

        movl   arg1,    %esi            # esi <- &a
        movl   arg2,    %ecx            # ecx <- la
        movd   arg3,    %mm0            # mm0 <- b
        movl   arg4,    %edi            # edi <- &c
        leal   (%esi,%ecx,4), %esi      # esi <- &a[la]
        leal   (%edi,%ecx,4), %edi      # edi <- &c[la]
        negl   %ecx
        pxor   %mm2,    %mm2            # mm2 <- 0 (retenue)
        jecxz  2f

        ALIGN_4
1:
        movd   (%esi,%ecx,4), %mm1
        pmuludq %mm0,   %mm1            # mm1 <- b*a[i]
        incl    %ecx
        paddq   %mm1,   %mm2            # ajoute poids fort pr�c�dent
        movd    %mm2,-4(%edi,%ecx,4)    # c[i] <- poids faible
        psrlq   $32,    %mm2            # mm2 <- poids fort
        jne     1b

2:
        movd    %mm2,   %eax            # eax <- dernier poids fort
        emms                            # r�initialise le FPU
        RETURN_WITH_SP
        
#else /* use_sse2 */
        
        movl   arg1,    %esi            # esi <- &a
        movl   arg2,    %ecx            # ecx <- la
        movl   arg3,    %ebx            # ebx <- b
        movl   arg4,    %edi            # edi <- &c
        leal   (%esi,%ecx,4), %esi      # esi <- &a[la]
        leal   (%edi,%ecx,4), %edi      # edi <- &c[la]
        negl   %ecx
        xorl   %edx,    %edx            # init retenue
        xorl   %ebp,    %ebp
        jecxz  2f
        ALIGN_4

1:
        movl   (%esi,%ecx,4), %eax
        adcl   %edx,    %ebp
        mull   %ebx
        addl   %ebp,    %eax
        movl   $0,      %ebp
        movl   %eax,    (%edi,%ecx,4)
        incl   %ecx
        jne    1b
        adcl   %ebp,    %edx

2:
        movl   %edx,    %eax            # eax <- retenue
        RETURN_WITH_SP

#endif /* use_sse2 */

#endif /* assembly_sn_mul_1 */

                      # +------------------------------+
                      # |  Multiplication quadratique  |
                      # +------------------------------+

# entr�e :
#   a = naturel de longueur la     esi = &a, edx = la
#   b = naturel de longueur lb     ebx = &b, ecx = lb
#   c = naturel de longueur la+lb  edi = &c
# contraintes : 0 < lb <= la
#               si lb > 1, c ne doit pas recouvrir ni a ni b
#               si lb = 1, c peut recouvrir a ou b
#
# sortie :
#   c <- a * b
#
# registres modifi�s :
#   eax,ebx,ecx,edx,esi,edi,ebp <- ind.
        
#ifdef assembly_sn_mul_n2
#undef L
#define L(x) .Lsn_fmul_n2_##x
        ALIGN_32
#ifdef debug_mul_n2
.Lsn_fmul_n2_buggy:
#else
.Lsn_fmul_n2:
#endif

#if defined(use_sse2)
        
        # c <- a*b[0]
        leal   (%esi,%edx,4), %esi      # esi <- &a[la]
        leal   (%edi,%edx,4), %edi      # edi <- &c[la]
        negl   %edx                     # edx <- -la
        movd   (%ebx),  %mm0            # mm0 <- b[0]
        movd   (%esi,%edx,4), %mm2
        pmuludq %mm0,   %mm2            # mm2 <- b[0]*a[0]
        incl    %edx
        movl    %edx,   %eax            # eax <- 1-la
        movd    %mm2,-4(%edi,%edx,4)    # c[0] <- poids faible
        jz      2f
        
        ALIGN_4
1:
        movd   (%esi,%eax,4), %mm1
        psrlq   $32,    %mm2            # mm2 <- poids fort
        pmuludq %mm0,   %mm1            # mm1 <- b[0]*a[i]
        incl    %eax
        paddq   %mm1,   %mm2            # ajoute poids fort pr�c�dent
        movd    %mm2,-4(%edi,%eax,4)    # c[i] <- poids faible
        jne     1b
        jmp     2f

        # boucle sur les chiffres suivants de b
        ALIGN_4
L(loop):
        leal   4(%ebx), %ebx            # b++
        leal   4(%edi), %edi            # c++
        movl   %edx,    %eax            # eax <- 1-la
        movd   (%ebx),  %mm0            # mm0 <- b[j]
        
        movd -4(%esi,%edx,4), %mm2
        movd -4(%edi,%edx,4), %mm3
        pmuludq %mm0,   %mm2
        paddq   %mm3,   %mm2            # mm2 <- b[j]*a[0] + c[j]
        movd    %mm2, -4(%edi,%edx,4)   # c[j] <- poids faible
        
        ALIGN_4
1:
        movd   (%esi,%eax,4), %mm1
        movd   (%edi,%eax,4), %mm3
        psrlq   $32,    %mm2            # mm2 <- poids fort
        pmuludq %mm0,   %mm1            # mm1 <- b[j]*a[i]
        incl    %eax
        paddq   %mm3,   %mm2            # mm2 <- pfort pr�c. + c[i+j]
        paddq   %mm1,   %mm2
        movd    %mm2,-4(%edi,%eax,4)    # c[i+j] <- poids faible
        jne     1b
2:
        decl    %ecx
        psrlq   $32,    %mm2            # mm2 <- poids fort
        movd    %mm2,  (%edi)           # sauve le dernier poids fort
        jne     L(loop)
        emms                            # r�initialise le FPU
        ret

#else /* sse2 */

        # initialise les variables locales
        #undef  _b_
        #undef  _lb_
        #undef  _a0_
        #undef  _ctb_
        #undef  _br_
        #undef  _cta_
        #define _b_   20(%esp)
        #define _lb_  16(%esp)
        #define _a0_  12(%esp)
        #define _ctb_  8(%esp)
        #define _br_   4(%esp)
        #define _cta_   (%esp)
        
        pushl  %ebx                     # sauve &b
        pushl  %ecx                     # sauve lb
        pushl  (%esi)                   # sauve a[0]
        leal    (%esi,%edx,4), %esi     # esi <- &a[la]
        leal  -4(%edi,%edx,4), %edi     # edi <- &c[la-1]
        movl   %edx,    %ecx
        negl   %ecx                     # ecx <- -la

        # pr�calcule le d�roulement de la boucle interne sur a
        leal   1(%ecx), %edx            # edx <- 1 - la
        movl   %edx,    %eax
        andl   $31,     %eax            # eax <- (1-la) % 32
        sarl   $5,      %edx            # edx <- (1-la)/32 = -(nb de tours)
        pushl  %edx                     # le sauve dans ctb
        call   L(here)
L(here):
        leal   (,%eax,8), %edx          # multiplication par 17 = 8*2 + 1
        leal   L(loop_a)-L(here)(%eax,%edx,2), %eax
        addl   %eax,  (%esp)            # br <- adresse de saut dans la boucle
        leal   -4(%esp),%esp            # r�serve un mot pour le
                                        # compteur de boucle interne

        # c <- a*b[0]
        movl   (%ebx),  %ebp            # ebp <- b[0]
        xorl   %ebx,    %ebx            # init retenue
        sarl   $1,      %ecx
        jc     L(odd)
        ALIGN_4
L(init):
        movl   (%esi,%ecx,8), %eax      # eax <- a[2i]
        mull   %ebp                     # multiplie par b[0]
        addl   %ebx,    %eax            # ajoute pfort pr�c.
        movl   $0,      %ebx
        movl   %eax,   4(%edi,%ecx,8)   # c[2i] <- pfaible
        adcl   %edx,    %ebx            # propage la retenue
L(odd):
        movl   4(%esi,%ecx,8), %eax     # eax <- a[2i+1]
        mull   %ebp                     # multiplie par b[0]
        addl   %ebx,    %eax            # ajoute pfort pr�c.
        movl   $0,      %ebx
        movl   %eax,   8(%edi,%ecx,8)   # c[2i+1] <- pfaible
        adcl   %edx,    %ebx            # propage la retenue
        incl   %ecx
        jne    L(init)
        
        movl   %ebx,    4(%edi)         # sauve le dernier chiffre
        decl   _lb_                     # lb--
        jnz    L(loop_b)
        leal   24(%esp), %esp           # nettoie la pile
        ret

        # boucle sur les chiffres de b
        ALIGN_4
L(loop_b):
        movl   _ctb_,    %eax
        movl   %eax,    _cta_           # init compteur de boucle interne
        shll   $5,      %eax
        movl   _b_,      %ebp
        leal   (%esi,%eax,4), %esi      # esi <- &a[la-32*tours]
        leal  4(%edi,%eax,4), %edi      # edi <- &c[la-32*tours+j]
        leal   4(%ebp),  %ebp           # b++
        movl   _a0_,     %eax           # eax <- a[0]
        movl   %ebp,     _b_
        movl   (%ebp),   %ebp           # ebp <- b[j]
        xorl   %ebx,     %ebx           # init retenues
        mull   %ebp                     # multiplie par b[j]
        xorl   %ecx,     %ecx
        jmp    *_br_
   
        # corps de boucle � d�rouler. taille du code = 17 octets
        # entrer dans la boucle avec edx:eax = retenue, ebx = ecx = 0, CF = 0
        # code inspir� de GMP (k7/mul_basecase.asm)
#undef BODY
#define BODY(x,y) \
          adcl   %eax,    %ecx           /* ecx += pfaible courant  */;\
       /* movl   x(%esi), %eax              eax <- a[2i+1]          */;\
          .byte 0x8b, 0x46, x                                         ;\
          adcl   %edx,    %ebx           /* ebx <- pfort courant    */;\
          mull   %ebp                    /* multiplie par b[j]      */;\
       /* addl   %ecx,    x(%edi)           c[2i+j] <- pfaible pr�c.*/;\
          .byte 0x01, 0x4f, x                                         ;\
          movl   $0,      %ecx                                        ;\
          adcl   %eax,    %ebx           /* ebx += pfaible courant  */;\
          movl   y(%esi), %eax           /* eax <- a[2i+2]          */;\
          adcl   %edx,    %ecx           /* ecx <- pfort courant    */;\
          mull   %ebp                    /* multiplie par b[j]      */;\
          addl   %ebx,    y(%edi)        /* c[2i+j+1] <- pf. pr�c.  */;\
          movl   $0,      %ebx

        # boucle de multiplication d�roul�e pour 32 chiffres
        ALIGN_4
L(loop_a):
        BODY(0,4);    BODY(8,12);    BODY(16,20);   BODY(24,28)
        BODY(32,36);  BODY(40,44);   BODY(48,52);   BODY(56,60)
        BODY(64,68);  BODY(72,76);   BODY(80,84);   BODY(88,92)
        BODY(96,100); BODY(104,108); BODY(112,116); BODY(120,124)
        
        incl   _cta_
        leal   128(%esi), %esi          # a += 32
        leal   128(%edi), %edi          # c += 32
        jne    L(loop_a)

        # range les deux derniers chiffres de a*b[j]
        adcl   %eax,    %ecx
        adcl   %ebx,    %edx
        addl   %ecx,    (%edi)
        adcl   %ebx,    %edx
        movl   %edx,    4(%edi)

        # chiffre suivant pour b
        decl   _lb_                     # lb--
        jnz    L(loop_b)
        leal   24(%esp), %esp           # nettoie la pile
        ret

#endif /* sse2 */
                              # +---------------+
                              # |  interface C  |
                              # +---------------+

#  void xn(mul_n2)(chiffre *a, long la, chiffre *b, long lb, chiffre *c)
#
#  entr�e :
#  a = naturel de longueur la
#  b = naturel de longueur lb
#  c = naturel de longueur la+lb, non confondu avec a ou b
#  contraintes : 0 < lb <= la
#
#  sortie :
#  c <- a*b

#ifdef debug_mul_n2
ENTER(sn_mul_n2_buggy)
#else
ENTER(sn_mul_n2)
#endif

        movl   arg1,    %esi            # esi <- &a
        movl   arg2,    %edx            # edx <- la
        movl   arg3,    %ebx            # ebx <- &b
        movl   arg4,    %ecx            # ecx <- lb
        movl   arg5,    %edi            # edi <- &c
#ifdef debug_mul_n2
        call   .Lsn_fmul_n2_buggy       # effectue la multiplication
#else
        call   .Lsn_fmul_n2
#endif
        RETURN_WITH_SP
#endif /* assembly_sn_mul_n2 */

        # cas o� la version assembleur est d�sactiv�e ou d�bogu�e :
        # sn_fmul_n2 renvoie vers la version C
        
#if !defined(assembly_sn_mul_n2) || defined(debug_mul_n2)
        ALIGN_32
.Lsn_fmul_n2:

        pushl  %edi
        pushl  %ecx
        pushl  %ebx
        pushl  %edx
        pushl  %esi
        call   SUBR(sn_mul_n2)
        leal   20(%esp), %esp
        ret
        
#endif /* !defined(assembly_sn_mul_n2) || defined(debug_mul_n2) */

        
                                 # +---------+
                                 # |  Carr�  |
                                 # +---------+

# entr�e :
#   a = naturel de longueur la     esi = &a, edx = la
#   c = naturel de longueur 2*la   edi = &c
# contraintes : 0 < la
#               si la > 1, c ne doit pas recouvrir a
#               si la = 1, c peut recouvrir a
#
# sortie :
#   c <- a^2
#
# registres modifi�s :
#   eax,ebx,ecx,edx,esi,edi,ebp <- ind.

#ifdef assembly_sn_sqr_n2
#undef L
#define L(x) .Lsn_fsqr_n2_##x
        
        ALIGN_32
#ifdef debug_mul_n2
.Lsn_fsqr_n2_buggy:
#else
.Lsn_fsqr_n2:
#endif

#if defined(use_sse2)

        leal   (%esi,%edx,4), %esi      # esi <- &a[la]
        leal   (%edi,%edx,4), %edi      # edi <- &c[la]
        negl   %edx
        incl   %edx                     # edx <- -(la-1)
        jne    L(big)

        # cas la = 1 : c <- a[0]^2      
        movl   -4(%esi), %eax
        mull   %eax
        movl   %eax,    -4(%edi)
        movl   %edx,      (%edi)
        ret
        
        # cas la > 1 : c <- a[0]*a[1..la-1]
        ALIGN_4
L(big):
        movl    %edx,   %ecx
        incl    %ecx
        movd -4(%esi,%edx,4), %mm0      # mm0 <- a[0]
        movd   (%esi,%edx,4), %mm2
        pmuludq %mm0,   %mm2            # mm2 <- a[1]*a[0]
        movd    %mm2,  (%edi,%edx,4)    # c[1] <- poids faible
        jz      2f
        ALIGN_4
1:
        movd   (%esi,%ecx,4), %mm1
        pmuludq %mm0,   %mm1            # mm1 <- a[i]*a[0]
        psrlq   $32,    %mm2            # mm2 <- poids fort pr�c�dent
        incl    %ecx
        paddq   %mm1,   %mm2            # ajoute poids fort pr�c�dent
        movd    %mm2,-4(%edi,%ecx,4)    # c[i] <- poids faible
        jne     1b
2:
        incl    %edx                    # edx <- -(la-2)
        psrlq   $32,    %mm2
        movd    %mm2,  (%edi)           # sauve le dernier poids fort
        movl    %edx,   %ecx            # ecx <- -(la-2)
        jz      L(squares)

        # ajoute sum(a[i]*a[i+1..la-1], i=1..la-2)
L(loop):
        leal 4(%edi),   %edi            # c++
        movl   %ecx,    %eax            # eax <- -(la-2)
        movd -4(%esi,%ecx,4), %mm0      # mm0 <- a[i]
        movd   (%esi,%ecx,4), %mm2
        movd   (%edi,%ecx,4), %mm3
        incl    %eax
        pmuludq %mm0,   %mm2
        paddq   %mm3,   %mm2            # mm2 <- a[i+1]*a[i] + c[2i+1]
        movd    %mm2,  (%edi,%ecx,4)    # c[2i+i] <- poids faible
        jz      2f
        ALIGN_4
1:
        movd   (%esi,%eax,4), %mm1
        movd   (%edi,%eax,4), %mm3
        psrlq   $32,    %mm2            # mm2 <- poids fort
        pmuludq %mm0,   %mm1            # mm1 <- a[j]*a[i]
        incl    %eax
        paddq   %mm3,   %mm2            # mm2 <- pfort pr�c. + c[i+j]
        paddq   %mm1,   %mm2
        movd    %mm2,-4(%edi,%eax,4)    # c[i+j] <- poids faible
        jne     1b
2:      
        incl    %ecx
        psrlq   $32,    %mm2            # mm2 <- poids fort
        movd    %mm2,  (%edi)           # sauve le dernier poids fort
        jnz     L(loop)

        # multiplie par 2 et ajoute les carr�s
L(squares):
        movl    $0,   4(%edi)           # c[2*la-1] <- 0
        subl    $2,     %edx            # edx <- -la
        movl    $-1,    %eax
        movd    %eax,   %mm7            # mm7 <- 0..0:f..f (masque)
        movd   (%esi,%edx,4), %mm3
        movd 12(%edi,%edx,8), %mm2
        incl    %edx
        pmuludq %mm3,   %mm3            # mm3 <- a[0]^2
        paddq   %mm2,   %mm2            # mm2 <- 2*c[1]
        movd    %mm3,  (%edi,%edx,8)    # sauve c[0]
        psrlq   $32,    %mm3
        paddq   %mm2,   %mm3            # mm3 <- pfort(a[0]^2) + 2*c[1]
        movd    %mm3, 4(%edi,%edx,8)    # sauve dans c[1]
        jz      2f
        ALIGN_4
1:
        movd   (%esi,%edx,4), %mm0
        movd  8(%edi,%edx,8), %mm1
        movd 12(%edi,%edx,8), %mm2
        psrlq   $32,    %mm3            # mm3 <- retenue
        pmuludq %mm0,   %mm0            # mm0 <- a[i]^2
        paddq   %mm1,   %mm1            # mm1 <- 2*c[2i]
        paddq   %mm2,   %mm2            # mm2 <- 2*c[2i+1]
        incl    %edx
        paddq   %mm1,   %mm3            # mm3 <- 2*c[2i] + retenue
        movq    %mm3,   %mm4
        pand    %mm7,   %mm3            # mm3 <- poids faible
        psrlq   $32,    %mm4            # mm4 <- poids fort
        paddq   %mm0,   %mm3            # mm3 <- a[i]^2 + poids faible
        movd    %mm3,  (%edi,%edx,8)    # sauve c[2i]
        psrlq   $32,    %mm3
        paddq   %mm4,   %mm2
        paddq   %mm2,   %mm3            # mm3 <- somme des poids forts
        movd    %mm3, 4(%edi,%edx,8)    # sauve dans c[2i+1]
        jne     1b
2:
        emms                            # r�initialise le FPU
        ret
        
#else /* sse2 */

        # initialise c avec les a[i]^2
        movl   %edx,    %ebx            # sauve la
        movl   %edx,    %ecx
        bt     $0,      %ecx
        jc     L(odd)
        ALIGN_4
L(squares):
        movl   -4(%esi,%ecx,4), %eax
        mull   %eax
        decl   %ecx
        movl   %eax,    (%edi,%ecx,8)
        movl   %edx,   4(%edi,%ecx,8)
L(odd):
        movl   -4(%esi,%ecx,4), %eax
        mull   %eax
        decl   %ecx
        movl   %eax,    (%edi,%ecx,8)
        movl   %edx,   4(%edi,%ecx,8)
        jne    L(squares)
        decl   %ebx
        jne    L(doubles)
        ret
        
        # variables locales
        #undef _i_
        #undef _j_
        #undef _br_
        #define _i_   8(%esp)
        #define _j_   4(%esp)
        #define _br_   (%esp)

        # ajoute sum(2*a[i]*a[j], 0 <= i < j < la)
        ALIGN_4
L(doubles):
        negl   %ebx                     # ebx <- 1-la
        movl   %ebx,   %eax             # eax <- 1-la
        pushl  %eax                     # init compteur externe
        sarl   $5,     %ebx
        pushl  %ebx                     # init compteur interne
        call   L(here)
L(here):
        xorl   %ebx,   %ebx             # init retenue pour 2a[-1]
        movl   (%esi), %ebp             # ebp <- a[0]
        andl   $31,    %eax
        leal   (,%eax,8), %edx          # adresse de saut pour le 1�re it�ration
        leal   L(loop_j_begin)-L(here)(%eax,%edx,2), %edx
        addl   %edx, (%esp)
        negl   %eax
        leal   4(%esi,%eax,4), %esi     # cadre les pointeurs pour un
        leal    (%edi,%eax,4), %edi     # saut en milieu de boucle

        # boucle sur i
        ALIGN_4
L(loop_i):
        xorl   %eax,    %eax            # init retenues
        xorl   %ecx,    %ecx
        xorl   %edx,    %edx
        bt     $31,     %ebx
        jnc    1f
        movl   %ebp,    %eax
1:
        rcll   $1,      %ebp            # ebp <- 2a[i] + retenue de 2*a[i-1]
        xorl   %ebx,    %ebx
        jmp    *_br_
        
        # corps de boucle � d�rouler. taille du code = 17 octets
        # entrer dans la boucle avec edx:eax = retenue, ebx = ecx = 0, CF = 0
        # code inspir� de GMP (k7/mul_basecase.asm)
#undef BODY
#define BODY(x,y) \
          adcl   %eax,    %ebx           /* ebx += pfaible courant  */;\
       /* movl   x(%esi), %eax              eax <- a[2j]            */;\
	  .byte  0x8b, 0x46, x                                        ;\
          adcl   %edx,    %ecx           /* ecx <- pfort courant    */;\
          mull   %ebp                    /* multiplie par 2a[i]     */;\
       /* addl   %ebx,    x(%edi)           c[2j] <- pfaible pr�c.  */;\
	  .byte  0x01, 0x5f, x                                        ;\
          movl   $0,      %ebx                                        ;\
          adcl   %eax,    %ecx           /* ecx += pfaible courant  */;\
          movl   y(%esi), %eax           /* eax <- a[2j+1]          */;\
          adcl   %edx,    %ebx           /* ebx <- pfort courant    */;\
          mull   %ebp                    /* multiplie par 2a[i]     */;\
          addl   %ecx,    y(%edi)        /* c[2j+1] <- pf. pr�c.    */;\
          movl   $0,      %ecx

        # boucle interne sur j d�roul�e 32 fois
        ALIGN_4
L(loop_j_begin):
        BODY(0,4);    BODY(8,12);    BODY(16,20);   BODY(24,28)
        BODY(32,36);  BODY(40,44);   BODY(48,52);   BODY(56,60)
        BODY(64,68);  BODY(72,76);   BODY(80,84);   BODY(88,92)
        BODY(96,100); BODY(104,108); BODY(112,116); BODY(120,124)
L(loop_j_end):

        # fin de la boucle sur j        
        leal   128(%esi), %esi
        leal   128(%edi), %edi
        incl   _j_
        jne     L(loop_j_begin)
        
        # fin de la boucle sur i
        adcl   %eax,    %ebx            # sauve les derniers chiffres   
        movl   _i_,     %eax
        adcl   %ecx,    %edx
        incl   %eax                     # i++
        addl   %ebx,    (%edi)
        movl   %eax,    _i_
        adcl   %edx,   4(%edi)
        movl   %eax,    %edx
        adcl   %ecx,   8(%edi)
        andl   $31,     %eax
        movl   $17, %ebx                # maj adresse de saut
        jnz    1f
        movl   $-31*17, %ebx
1:
        subl   %eax,    %edx
        addl   %ebx,    _br_
        leal    (%esi,%edx,4), %esi     # remet les pointeurs au d�part
        leal   4(%edi,%edx,4), %edi
        movl  -8(%esi,%eax,4), %ebx     # ebp:ebx <- a[i+1]:a[i]
        movl  -4(%esi,%eax,4), %ebp
        sarl   $5,      %edx
        movl   %edx,     _j_            # compteur interne <- externe/32
        jnz    L(loop_i)

        # derni�re retenue
        bt     $31,     %ebx            # CF <- a[la-2][31]
        jnc    L(done)
        addl   %ebp,   (%edi)           # ajoute a[la-1] si besoin
        adcl   $0,    4(%edi)
L(done):
        leal  12(%esp), %esp            # nettoie la pile
        ret

#endif /* sse2 */
                              # +---------------+
                              # |  interface C  |
                              # +---------------+

#  void xn(sqr_n2)(chiffre *a, long la, chiffre *b)
#
#  entr�e :
#  a = naturel de longueur la
#  b = naturel de longueur 2*la, non confondu avec a
#  contraintes : 0 < la
#
#  sortie :
#  b <- a^2

#ifdef debug_mul_n2
ENTER(sn_sqr_n2_buggy)
#else
ENTER(sn_sqr_n2)
#endif

        movl   arg1,    %esi            # esi <- &a
        movl   arg2,    %edx            # edx <- la
        movl   arg3,    %edi            # edi <- &b
#ifdef debug_mul_n2
        call   .Lsn_fsqr_n2_buggy       # calcule le carr�
#else
        call   .Lsn_fsqr_n2      
#endif
        RETURN_WITH_SP
#endif /* assembly_sn_sqr_n2 */

        # cas o� la version assembleur est d�sactiv�e ou d�bogu�e :
        # sn_fsqr_n2 renvoie vers la version C
        
#if !defined(assembly_sn_sqr_n2) || defined(debug_mul_n2)
        ALIGN_32
.Lsn_fsqr_n2:

        pushl  %edi
        pushl  %edx
        pushl  %esi
        call   SUBR(sn_sqr_n2)
        leal   12(%esp), %esp
        ret
        
#endif /* !defined(assembly_sn_sqr_n2) || defined(debug_mul_n2) */


