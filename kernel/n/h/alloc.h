// file kernel/n/h/alloc.h: temporary storage allocator
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                          Allocation de m�moire                        |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#ifdef debug_alloc
chiffre *xn(alloc)(unsigned long n);
void xn(free)(chiffre *x);
long xn(get_alloc_count)();
#else

/* alloue un bloc de n chiffres */
extern inline chiffre *xn(alloc)(unsigned long n) {
  chiffre *p = (chiffre *)malloc(n*sizeof(chiffre));                
  if ((p) || (n==0)) return(p);
  else xn(internal_error)("out of memory",0);
}

/* lib�ration */
extern inline void xn(free)(chiffre *x) {free(x);}
#endif

/* Allocation temporaire

  xn(alloc_tmp) alloue la m�moire dans la pile si use_alloca est d�fini,
  et dans le tas sinon. L'allocation dans la pile est plus rapide, mais
  il n'y a pas de contr�le  de d�bordement. N'utiliser cette option que
  sur les syst�mes o� la pile n'est pas limit�e (Linux) et que pour des
  donn�es "petites".
*/

#ifdef use_alloca
#include <alloca.h>
#define cn_alloc_tmp(n) (chiffre *)alloca((n)*sizeof(chiffre))
#define dn_alloc_tmp(n) (chiffre *)alloca((n)*sizeof(chiffre))
#define sn_alloc_tmp(n) (chiffre *)alloca((n)*sizeof(chiffre))
#define cn_free_tmp(x)
#define dn_free_tmp(x)
#define sn_free_tmp(x)
#else
#define cn_alloc_tmp cn_alloc
#define dn_alloc_tmp dn_alloc
#define sn_alloc_tmp sn_alloc
#define cn_free_tmp  cn_free
#define dn_free_tmp  dn_free
#define sn_free_tmp  sn_free
#endif

/* v�rifie que l*2^k < LMAX */
extern inline void xn(check_lmax)(long l, long k) {
  if (l >= (LMAX >> k)) xn(internal_error)("number too big",0);
}


/* Initialisation

   clear: a <- 0
   fill : a <- BASE^la - 1
   move : b <- a
 */
extern inline void xn(clear)(chiffre *a, long la)             {memset( a,  0, (la)*(HW/8));}
extern inline void xn(fill) (chiffre *a, long la)             {memset( a,255, (la)*(HW/8));}
extern inline void xn(move) (chiffre *a, long la, chiffre *b) {memmove(b,  a, (la)*(HW/8));}

