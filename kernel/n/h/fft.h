// file kernel/n/h/fft.h: Fast Fourier transform, low level functions
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                       Transform�e de Fourier rapide                   |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- D�composition nombre -> polyn�me
  entr�e :
  a = naturel de longueur la >= 0
  b = 2^k naturels cons�cutifs de longueurs n+1, non confondus avec a
  f = entier naturel tel que 0 < f <= n

  sortie :
  x <- a mod (BASE^(2^k*f) - 1)
  pour i=0..2^k-1, b[i] <- x[i*f .. (i+1)*f-1]
*/

void xn(fft_split)(chiffre *a, long la, chiffre *b, long n, long k, long f);

/* ---------------------------------------- Recomposition polyn�me -> nombre
  entr�e :
  a = 2^k naturels cons�cutifs de longueurs n+1
  b = naturel de longueur f*2^k, peut �tre confondu avec a
  f = entier naturel tel que 0 < f <= n

  sortie :
  a <- ind.
  b <- sum((a[i]/2^k mod (BASE^n+1))*BASE^(i*f), i=0..2^k-1) mod BASE^(f*2^k)-1

  contrainte :
  les nombres a[i] doivent �tre congrus modulo BASE^n + 1 � des entiers
  divisibles par 2^k et compris entre 0 et BASE^n - 1
*/

void xn(fft_merge)(chiffre *a, chiffre *b, long n, long k, long f);

/* ---------------------------------------- Transform�e de Fourier
  entr�e :
  a = 2^k naturels cons�cutifs de longueurs n+1

  contrainte :
  n doit �tre pair, n >= 2
  n*HW doit �tre divisible par 2^(k-2) : n*HW = p*2^(k-2)

  sortie :
  pour i=0..2^k-1,
       a[i] <- sum(a[j]*2^(p*inv(i,k)*j/2), j=0..2^k-1) mod (BASE^n + 1)
  avec
       inv(i,k) = inversion binaire de i sur k bits
*/

void xn(fft)(chiffre *a, long n, long k);

/* ---------------------------------------- Transform�e de Fourier inverse
  entr�e :
  a = 2^k naturels cons�cutifs de longueurs n+1
  f = entier naturel tel que 0 < f <= n

  contrainte :
  n doit �tre pair, n >= 2
  n*HW doit �tre divisible par 2^(k-2) : n*HW = p*2^(k-2)

  sortie :
  pour i=0..2^k-1,
       a[i] <- sum(a[j]*2^(-p*i*inv(j,k)/2), j=0..2^k-1) mod (BASE^n + 1)
  avec inv(j,k) = inversion binaire de i sur k bits
*/

void xn(fft_inv)(chiffre *a, long n, long k);

/* ---------------------------------------- Heuristique pour acc�l�rer la FFT
   entr�e :
   n = taille des blocs (calculs modulo BASE^n + 1)
   p = diviseur impos� pour n

   sortie :
   retourne une nouvelle valeur de n sup�rieure ou �gale � celle fournie,
   pour laquelle les calculs sont exp�rimentalement plus rapides.
*/

long xn(fft_improve)(long n, long p);

