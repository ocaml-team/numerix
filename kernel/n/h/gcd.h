// file kernel/n/h/gcd.h: greatest common divisor
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                                 PGCD                                  |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  _v = tableau de pointeurs vers 6 naturels a,b,p,s,q,r
  _l = tableau de 6 longueurs la,lb,lp,lq,lr,ls
  mode = 0,1,2

  contraintes :
    la > 0, lb > 0, a[la-1] > 0, b[lb-1] > 0
    capacit�(p) >= la, capacit�(q) >= la,
    capacit�(r) >= lb, capacit�(s) >= lb.

  d�veloppe en fraction continue la fraction a/b (mode = 0 ou 1)
  ou les fractions a/(b+1) et (a+1)/b tant que les quotients sont
  �gaux et la pr�cision des nombres r�siduels est suffisante (mode = 2).

  dans les trois mode, calcule des entiers naturels p',q',r',s',a',b' tels que
    a = p'*a' + q'*b'
    b = r'*a' + s'*b'
    p'*s' - q'*r' = 1

  avec a' = 0 ou b' = 0 (mode 0 ou 1)
       a' >= q' et b' >= r' (mode 2)

  sortie si mode = 0 :
    a,b <- a',b', met � jour les longueurs
    p,q,r,s <- ind.

  sortie si mode = 1 ou 2 :
    a,b,p,s,q,r <- a',b',p',s',q',r', met � jour les longueurs

  remarque :
    en mode 2 on progresse d'au moins une �tape (= un chiffre dans la
    premi�re division) si et seulement si a <> b.

*/

void xn(gcd_n2)(chiffre **_v, long *_l, int mode);
void xn(lehmer)(chiffre **_v, long *_l, int mode);

