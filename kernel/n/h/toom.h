// file kernel/n/h/toom.h: Toom multiplication of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                          Multiplication de Toom                       |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Multiplication de Toom
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur la + lb non confondu avec a ou b

  contrainte : 0 < lb <= la

  sortie :
  c <- a*b
*/
void xn(toommul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur 2*la, non confondu avec a

  contraintes : 0 < la

  sortie :
  b <- a^2
*/
void xn(toomsqr)(chiffre *a, long la, chiffre *b);

