// file kernel/n/h/zimmermann.h: Zimmermann square root
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Racine carr�e de Zimmermann                   |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Racine carr�e
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la/2

  contraintes :
  la > 0, la pair, BASE/16 <= a[la-1] < BASE/4
  a,b non confondus

  sortie :
  b <- 2*floor(sqrt(a))
  a <- a - b^2/4
*/
void xn(zimsqrt)(chiffre *a, long la, chiffre *b);

