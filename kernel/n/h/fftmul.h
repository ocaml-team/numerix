// file kernel/n/h/fftmul.h: multiplication and remainder with FFT
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |            Multiplication par transform�e de Fourier rapide           |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Multiplication
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb <= la
  c = naturel de longuaur la+lb

  sortie :
  c <- a*b
*/
void xn(fftmul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);

/* ---------------------------------------- Reste d'une division
  entr�e :
  a = naturel de longueur lb+lc
  b = naturel de longueur lb
  c = naturel de longuaur lc

  contraintes : 
  0 < lc < lb, le bit de poids fort de b est non nul,
  -b <= a - b*c < b
  a,b,c non confondus

  sortie :
  c <- floor(a/b)
  a <- a mod b
*/
void xn(remdiv)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c);

/* ---------------------------------------- Carr�
  entr�e :
  a = naturel de longueur la
  b = naturel de longuaur 2*la, non confondu avec a

  sortie :
  b <- a^2
*/
void xn(fftsqr)(chiffre *a, long la, chiffre *b);

/* ---------------------------------------- Reste d'une racine carr�e
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la/2

  contraintes : 
  la > 0, pair.
  le bit de poids fort de b est non nul,
  -b <= a - b^2/4 < b
  a,b non confondus

  sortie :
  b <- 2*floor(sqrt(a))
  a <- a - b^2/4
*/
void xn(remsqrt)(chiffre *a, long la, chiffre *b);

