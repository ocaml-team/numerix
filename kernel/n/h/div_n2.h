// file kernel/n/h/div_n2.h: O(n^2) division of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Division quadratique                          |
 |                                                                       |
 +-----------------------------------------------------------------------*/


/* ---------------------------------------- division deux chiffres par un
  entr�e :
  a,b,c,*d,*e = chiffres

  contraintes : b < c, d,e non confondus et non confondus avec a,b,c

  sortie :
  *d <- floor((a + BASE*b)/c)
  *e <- (a + BASE*b) mod c

  remarque :
  code inefficace, � n'utiliser qu'en l'absence de ndoubles
*/

extern inline void xn(div_0)(chiffre a, chiffre b, chiffre c, chiffre *d, chiffre *e) {
  long i;
  int r;

  /* division bit � bit */
  for (*d = 0, *e = b, i = 0; i < HW; i++) {
    *d <<= 1;
    r = (*e >= BASE_2); 
    *e = 2*(*e) + (a >= BASE_2);
    if ((r) || (*e >= c)) {(*d)++; *e -= c;}
    a <<= 1;
  }
}


/* ---------------------------------------- Division par un long
  entr�e :
  a = naturel de longueur la >= 0
  b = long > 0
  c = naturel de longueur la, peut �tre confondu avec a

  sortie :
  c <- floor(a/b)
  retourne a mod b
*/
unsigned long xn(div_1)(chiffre *a, long la, unsigned long b, chiffre *c);

/*
  entr�e :
  a = naturel de longueur la >= 0
  b = long > 0

  sortie :
  retourne a mod b
*/
unsigned long xn(mod_1)(chiffre *a, long la, unsigned long b);

/* ---------------------------------------- Division quadratique
  entr�e :
  a = naturel de longueur lc+lb
  b = naturel de longueur lb
  c = naturel de longueur lc

  contraintes : 
  lb >= 2, lc > 0, le bit de poids fort de b est non nul,
  a < BASE^lc*b
  a,b,c non confondus

  sortie :
  a <- a mod b
  c <- floor(a/b)
*/

void xn(div_n2)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c);

