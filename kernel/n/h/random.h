// file kernel/n/h/random.h: random numbers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                            Nombres al�atoires                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* pas de random ni srandom sous mingw, utiliser rand et srand � la place */
#ifdef __MSVCRT__
#define random rand
#define srandom srand
#endif

/* ---------------------------------------- Naturel al�atoire
  entr�e :
  a = naturel de longueur la

  sortie :
  a <- chiffres al�atoires
*/

void xn(random)(chiffre *a, long la);

