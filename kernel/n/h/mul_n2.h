// file kernel/n/h/mul_n2.h: O(n^2) multiplication of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                        Multiplication quadratique                     |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Op�ration entre chiffres

  entr�e :
  a,b,*c,*d = chiffres

  sortie :
  *c + BASE*(*d) <- a*b ou a^2

  remarque :
  pour palier l'abscence du type ndouble ou pour d�boguage.
*/

extern inline void xn(mul_0)(chiffre a, chiffre b, chiffre *c, chiffre *d) {
  chiffre a0 = a & (((chiffre)1<<(HW/2)) - 1), a1 = a >> (HW/2);
  chiffre b0 = b & (((chiffre)1<<(HW/2)) - 1), b1 = b >> (HW/2);
  chiffre u,v,x;

  x = a0*b0;
  u = a0*b1;
  v = u + a1*b0;
  u = (chiffre)(v < u) << (HW/2);
  *c = x + (v << (HW/2));
  u |= (*c < x);
  *d = a1*b1 + u + (v >> (HW/2));

}

extern inline void xn(sqr_0)(chiffre a, chiffre *c, chiffre *d) {
  chiffre a0 = a & (((chiffre)1<<(HW/2)) - 1), a1 = a >> (HW/2);
  chiffre u,v,x;

  x = a0*a0;
  u = a0*a1;
  v = 2*u;
  u = (chiffre)(v < u) << (HW/2);
  *c = x + (v << (HW/2));
  u |= (*c < x);
  *d = a1*a1 + u + (v >> (HW/2));

}

/* ---------------------------------------- Multiplication par un long
  entr�e :
  a = naturel de longueur la
  b = long >= 0
  c = naturel de longueur la, peut �tre confondu avec a

  sortie :
  c <- a*b
  retourne la retenue
*/
unsigned long xn(mul_1)(chiffre *a, long la, unsigned long b, chiffre *c);

/* ---------------------------------------- Multiplication quadratique
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur la+lb, non confondu avec a ou b

  contrainte : 0 < lb <= la

  sortie :
  c <- a*b
*/
void xn(mul_n2)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur 2*la, non confondu avec a

  contrainte : la > 0

  sortie :
  b <- a^2
*/
void xn(sqr_n2)(chiffre *a, long la, chiffre *b);

/* ---------------------------------------- Contr�le
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur la+lb

  contrainte : 0 < lb <= la

  sortie :
  retourne 1 si c = a*b, 0 sinon
*/

#if defined(debug_mul_n2) || defined(debug_karamul)
int xn(ctrl_mul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);
#endif

