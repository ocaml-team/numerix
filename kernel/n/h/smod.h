// file kernel/n/h/smod.h: operations on residues modulo BASE^n - 1
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Arithm�tique modulo BASE^n - 1                    |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Soustraction modulo BASE^n - 1
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb > 0

  sortie :
  b <- (a - b) mod (BASE^lb - 1), non normalis�
*/
void xn(ssub)(chiffre *a, long la, chiffre *b, long lb);

/* ---------------------------------------- Multiplication modulo BASE^n - 1
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur n

  contraintes : n > 0, 0 < lb <= la

  sortie :
  c <- (a*b) mod (BASE^n - 1)
*/
void xn(smul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, long n);

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur n

  contraintes : n > 0, la > 0

  sortie :
  b <- a^2 mod (BASE^n - 1)
*/
void xn(ssqr)(chiffre *a, long la, chiffre *b, long n);

/* ---------------------------------------- Recombinaison de r�sidus
  entr�e :
  a = naturel de longueur n+p+q
  n = (2h+2)k, p = (2h+1)k, q = (2h)k

  contraintes : h >= 2, k >= 2

  sortie :
  a <- x mod ppcm(BASE^n - 1, BASE^p - 1, BASE^q - 1) normalis�
  avec
    a[0..n-1]       = x mod (BASE^n - 1),
    a[n..n+p-1]     = x mod (BASE^p - 1),
    a[n+p..n+p+q-1] = x mod (BASE^q - 1)

  remarque : ppcm = produit/(BASE^k - 1)/(BASE^(2k) - 1)
*/
void xn(sjoin3)(chiffre *a, long h, long k);

