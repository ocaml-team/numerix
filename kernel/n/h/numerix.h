// file kernel/n/h/numerix.h: natural integer definitions
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                    D�finitions pour la couche n                       |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* fichier de configuration sp�cifique � la machine cible */
#include "../../config.h"

/* options de compilation :

   bits_32     -> architecture 32 bits
   bits_64     -> architecture 64 bits

   use_clong   -> arithm�tique en C, un chiffre = 1/2 mot
   use_dlong   -> arithm�tique en C, un chiffre = 1 mot
   use_slong   -> arithm�tique en C et assembleur, un chiffre = 1 mot

   have_long_long -> utiliser le type long long pour les modes dlong/slong
*/

                        /* +-------------------------+
                           |  Contr�le de coh�rence  |
                           +-------------------------+ */

#if   defined(bits_32) + defined(bits_64) < 1
#error  "missing bits_xx definition"
#elif defined(bits_32) + defined(bits_64) > 1
#error  "multiple bits_xx definitions"
#endif

/* mode = clong ou bien dlong ou bien slong */
#if   defined(use_clong) + defined(use_dlong) + defined(use_slong) < 1
#error  "missing use_xlong definition"
#elif defined(use_clong) + defined(use_dlong) + defined(use_slong) > 1
#error  "multiple use_xlong definitions"
#endif /* use_xlong */

#ifdef  bits_32

                        /* +------------------------+
                           |  Architecture 32 bits  |
                           +------------------------+ */

#define SIGN_m   0x80000000L            /* masque bit de signe     */
#define LONG_m   0x7fffffffL            /* masque mot de longueur  */

#ifdef  use_clong
#define chiffres_per_long 2             /* conversion de longueur  */
#define HW       16                     /* bits par chiffre        */
#define hw       4                      /* log_2(HW)               */
#define BASE_2   0x8000L                /* base de num�ration/2    */
#define LMAX     0x20000000L            /* longueur maximale       */
#define chiffre  unsigned short         /* un chiffre              */

#else
#define chiffres_per_long 1             /* conversion de longueur  */
#define HW       32                     /* bits par chiffre        */
#define hw       5                      /* log_2(HW)               */
#define BASE_2   0x80000000L            /* base de num�ration/2    */
#define LMAX     0x10000000L            /* longueur maximale       */
#define chiffre  unsigned long          /* un chiffre              */
#endif  /* use_clong */
#endif  /* bits_32 */

#ifdef  bits_64
                        /* +------------------------+
                           |  Architecture 64 bits  |
                           +------------------------+ */

#define SIGN_m   0x8000000000000000L    /* masque bit de signe     */
#define LONG_m   0x7fffffffffffffffL    /* masque mot de longueur  */

#ifdef  use_clong
#define chiffres_per_long 2             /* conversion de longueur  */
#define HW       32                     /* bits par chiffre        */
#define hw       5                      /* log_2(HW)               */
#define BASE_2   0x80000000L            /* base de num�ration/2    */
#define LMAX     0x1000000000000000L    /* longueur maximale       */
#define chiffre  unsigned int           /* un chiffre              */

#else
#define chiffres_per_long 1             /* conversion de longueur  */
#define HW       64                     /* bits par chiffre        */
#define hw       6                      /* log_2(HW)               */
#define BASE_2   0x8000000000000000L    /* base de num�ration/2    */
#define LMAX     0x800000000000000L     /* longueur maximale       */
#define chiffre  unsigned long          /* un chiffre              */
#endif  /* use_clong */
#endif /* bits_64 */

                          /* +--------------------+
                             |  Double pr�cision  |
                             +--------------------+ */

#if defined(use_clong)
#define ndouble  unsigned long          /* entier double non sign� */
#define zdouble           long          /* entier double sign�     */

#elif defined(have_long_long)
#define ndouble  unsigned long long     /* entier double non sign� */
#define zdouble           long long     /* entier double sign�     */

#else
#undef ndouble                          /* arithm�tique double     */
#undef zdouble                          /* pr�cision indisponible  */
#endif

                   /* +-----------------------------------+
                      |  pr�fixe des fonctions publiques  |
                      +-----------------------------------+ */

#if defined(use_clong)
#define xn(nom)  cn_##nom
#define xx(nom)  cx_##nom
#elif defined(use_dlong)
#define xn(nom)  dn_##nom
#define xx(nom)  dx_##nom
#elif defined(use_slong)
#define xn(nom)  sn_##nom
#define xx(nom)  sx_##nom
#endif

/* include standards */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

                        /* +-----------------------+
                           |  Liste des fonctions  |
                           +-----------------------+ */


/*
  dump.h doit �tre lu en premier pour avoir le prototype de xn(internal_error)
  Les autres fichiers peuvent �tre charg�s dans un ordre quelconque, ici
  ils sont regroup�s par cat�gorie d'op�rations
*/
#include "dump.h"

/* allocation m�moire */
#include "alloc.h"

/* addition/soustraction */
#include "add.h"

/* multiplication/carr� */
#include "mul_n2.h"
#include "karatsuba.h"
#include "toom.h"

/* op�rations modulo BASE^n +/- 1 */
#include "mmod.h"
#include "smod.h"

/* transformation de Fourier */
#include "fft.h"
#include "fftmul.h"

/* division/racine carr�e */
#include "div_n2.h"
#include "sqrt_n2.h"
#include "burnikel.h"
#include "zimmermann.h"
#include "moddiv.h"
#include "karp.h"

/* exponentiation */
#include "pow.h"
#include "powmod.h"
#include "montgomery.h"

/* pgcd */
#include "gcd.h"

/* test de primalit� */
#include "prime.h"

/* divers */
#include "cmp.h"
#include "shift.h"
#include "random.h"


       /* +-----------------------------------------------------------+
          |  Aiguillage des op�rations selon la taille des op�randes  |
          +-----------------------------------------------------------+ */

extern inline void xn(mul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
    (lb <= karamul_lim) ? xn(mul_n2)(a,la,b,lb,c) : xn(fftmul)(a,la,b,lb,c);
}

extern inline void xn(sqr)(chiffre *a, long la, chiffre *c) {
    (la <= karasqr_lim) ?  xn(sqr_n2)(a,la,c) : xn(fftsqr)(a,la,c);
}

extern inline void xn(div)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
    (lb <= burnidiv_lim) ? xn(div_n2) (a,la,b,lb,c) : xn(karpdiv)(a,la,b,lb,c,1);
}

