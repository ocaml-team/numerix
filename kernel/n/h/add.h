// file kernel/n/h/add.h: addition/subtraction of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Addition/soustraction                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Addition
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb <= la
  c = naturel de longueur la

  sortie :
  c <- a + b
  retourne la retenue
*/
chiffre xn(add)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);

/* ---------------------------------------- Incr�mentation
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb avec lb <= la

  sortie :
  a <- a + b
  retourne la retenue
*/
chiffre xn(inc)(chiffre *a, long la, chiffre *b, long lb);

/* ---------------------------------------- Incr�mentation de 1
  entr�e :
  a = naturel de longueur la

  sortie :
  a <- a + 1
  retourne la retenue
*/
chiffre xn(inc1)(chiffre *a, long la);

/* ---------------------------------------- Soustraction
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb <= la
  c = naturel de longueur la

  sortie :
  c <- a - b
  retourne la retenue
*/
chiffre xn(sub)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);

/* ---------------------------------------- D�cr�mentation
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb avec lb <= la

  sortie :
  a <- a - b
  retourne la retenue
*/
chiffre xn(dec)(chiffre *a, long la, chiffre *b, long lb);

/* ---------------------------------------- D�cr�mentation de 1
  entr�e :
  a = naturel de longueur la

  sortie :
  a <- a - 1
  retourne la retenue
*/
chiffre xn(dec1)(chiffre *a, long la);

