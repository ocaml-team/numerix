// file kernel/n/h/mmod.h: operations on residues modulo BASE^n + 1
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                  Arithm�tique modulo BASE^n + 1                       |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Multiplication modulo BASE^n + 1
  entr�e :
  a = naturel de longueur n+1
  b = naturel de longueur n+1 non confondu avec a

  contrainte : n > 0

  sortie :
  a <- (a*b) mod (BASE^n + 1)
  b <- b mod (BASE^n + 1)
*/
void xn(mmul)(chiffre *a, chiffre *b, long n);

/*
  entr�e :
  a = naturel de longueur n+1

  contrainte : n > 0

  sortie :
  a <- a^2 mod (BASE^n + 1)
*/

void xn(msqr)(chiffre *a, long n);

/* ---------------------------------------- Papillon modulo BASE^n + 1
  entr�e :
  a = naturel de longueur n+1
  b = naturel de longueur n+1 non confondu avec a
  q = entier tel que 0 <= q <= 2*n*HW
  s = 0 ou 1

  contraintes : n >= 3 et si q est impair, n doir �tre pair

  sortie :
  a <- a + (-1)^s * b * 2^(q/2) mod (BASE^n + 1)
  b <- a - (-1)^s * b * 2^(q/2) mod (BASE^n + 1)

  remarque :
  2^(1/2) = BASE^(3n/4)*(BASE^(n/2) + 1) mod (BASE^n + 1)
*/
void xn(butterfly)(chiffre *a, chiffre *b, long n, long q, int s);

