// file kernel/n/h/moddiv.h: division/square root with modular remainder
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |            Division et racine carr�e avec reste modulaire             |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Division avec ou sans reste
  entr�e :
  a = naturel de longueur lc+lb
  b = naturel de longueur lb
  c = naturel de longueur lc
  rem = 0 ou 1 ou 2

  contraintes : 
  lb >= 2, lc > 0, le bit de poids fort de b est non nul,
  a < BASE^lc*b
  a,b,c non confondus

  sortie si rem = 0 :
    a <- ind.
    c <- approx(a/b) avec -ceil(log_2(lb))*BASE^(lb-1) < a - b*c < b
  sortie si rem = 1 :
    a <- a mod b
    c <- floor(a/b)
  sortie si rem = 2 :
    a <- ind.
    c <- approx(a/b) avec -ceil(log_2(lb))*BASE^(lb-1) < a - b*c < b
         et c = floor(a/b) si c[0] = 0
*/

void xn(moddiv)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem);

/* ---------------------------------------- Racine carr�e
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la/2

  contraintes :
  la > 0, la pair, BASE/16 <= a[la-1] < BASE/4
  a,b non confondus

  sortie :
  b <- 2*floor(sqrt(a))
  a <- a - b^2/4
*/

void xn(modsqrt)(chiffre *a, long la, chiffre *b);

