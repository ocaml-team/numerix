// file kernel/n/h/pow.h: exponentiation
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Exponentiation                           |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb

  contraintes :
  p > 0, la > 0, a[la-1] > 0, (lb-1)*HW >= p*nbits(a)
  b non confondu avec a

  sortie :
  b <- a^p
  retourne la longueur effective de b
*/

long xn(pow)(chiffre *a, long la, chiffre *b, long p);

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur ceil(la/p)

  contraintes :
  p >= 2, la > 0, a[la-1] > 0
  b non confondu avec a

  sortie :
  b <- floor(a^(1/p))
  retourne 1 si a^(1/p) est entier, 0 sinon
*/

long xn(root)(chiffre *a, long la, chiffre *b, long p);

