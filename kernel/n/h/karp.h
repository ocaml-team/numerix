// file kernel/n/h/karp.h: Karp-Markstein division
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Division de Karp et Markstein                     |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Inversion
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la+1

  contraintes :
  la >= 2, le bit de poids fort de a vaut 1

  sortie :
  d <- approx(BASE^(2*la)/a) avec a*(d-2) < BASE^(2la) <= a*d
*/

void xn(karpinv)(chiffre *a, long la, chiffre *b);

/* ---------------------------------------- Division avec ou sans reste
  entr�e :
  a = naturel de longueur lb+lc
  b = naturel de longueur lb
  c = naturel de longueur lc
  rem = 0 ou 1 ou 2

  contraintes :
  lc > 0, lb >= 2
  a < BASE^lc*b
  le bit de poids fort de b vaut 1

  sortie si rem = 0 :
    a <- ind.
    c <- approx(a/b) avec -b < a - b*c < b
  sortie si rem = 1 :
    a <- a mod b
    c <- floor(a/b)
  sortie si rem = 2 :
    a <- ind.
    c <- approx(a/b) avec -b < a - b*c < b et c = floor(a/b) si c[0] = 0
*/

void xn(karpdiv)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem);

