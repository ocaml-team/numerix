// file kernel/n/alpha/div_n2.S: O(n^2) division of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Division quadratique                          |
 |                                                                       |
 +-----------------------------------------------------------------------*/



                               # +-------------+
                               # |  Inversion  |
                               # +-------------+

   # entr�e :
   #    a = entier naturel tel que a >= BASE/2
   #    x,y = scratch
   #
   # sortie :
   #    u <- floor((BASE^2-1)/a), compris entre BASE et 2*BASE-1
   #    u <- u - BASE
   #
   # algorithme : it�ration de Newton pour la fonction
   #
   #               u -> u + u*(BASE - (a*u)/BASE)/BASE
   #
   # en tronquant les quotients par d�faut et en partant de u0 = BASE.
   # La suite (u_n) calcul�e est strictement croissante tant que
   #
   #            u_n < 2*BASE  et  a*u_n < BASE^2
   #
   # La condition u_n < 2*BASE sera toujours satisfaite si a > BASE/2,
   # dans ce cas on sort lorsque a*u_n >= BASE^2 et le quotient cherch�
   # est u_n - 1 ou u_n - 2.
   # Pour a = BASE/2, on court-circuite les calculs et on sort directement
   # le r�sultat : u = 2*BASE-1.

#define INVERSE(a,u,x,y) \
        bis    $31,  $31,  u    /* u <- BASE                           */ ;\
        bis    a,    a,    x    /* x <- a = floor(a*u/BASE)            */ ;\
        sll    a,    1,    y    /* si a = BASE/2, alors inv = 2*BASE-1 */ ;\
        beq    y,    3f                                                   ;\
        .align 5                                                          ;\
1:                                                                        ;\
        subq    $31, x,    x    /* x <- BASE - floor(a*u/BASE)         */ ;\
        umulh   x,   u,    y    /* r <- floor(u*x)                     */ ;\
        addq    x,   y,    x                                              ;\
        addq    x,   u,    u    /* u <- u + x                          */ ;\
        umulh   a,   u,    x                                              ;\
        addq    a,   x,    x    /* x <- floor(a*u/BASE)                */ ;\
        cmpult  x,   a,    y    /* si a*u >= BASE ...                  */ ;\
        beq     y,   1b                                                   ;\
2:                                                                        ;\
        subq    u,   1,    u    /* alors le r�sultat est u-1 ou u-2    */ ;\
        umulh   a,   u,    x    /* essaye avec u-1                     */ ;\
        addq    a,   x,    x                                              ;\
        cmpult  x,   a,    y                                              ;\
        beq     y,   4f                                                   ;\
3:                                                                        ;\
        subq    u,   1,    u    /* encore trop grand, on prend u-2     */ ;\
4:
        
                       # +----------------------------+
                       # |  Division 128 bits par 64  |
                       # +----------------------------+

   # entr�e :
   #    a = entier naturel tel que a >= BASE/2
   #    b = floor((BASE^2-1)/a) - BASE
   #    c:d = entier naturel tel que d <= a
   #    x,y = scratch
   #
   # sortie :
   #    q   <- max(floor(c:d/a), BASE-1)
   #    c:d <- c:d - q*a

#define DIV(c,d,a,b,q,x,y) \
        umulh  d,    b,    q   /* q <- quotient sous-estim� d au plus 3 */ ;\
        addq   d,    q,    q                                               ;\
        mulq   q,    a,    x   /* c:d <- c:d - q*a                      */ ;\
        cmpult c,    x,    y                                               ;\
        subq   c,    x,    c                                               ;\
        subq   d,    y,    d                                               ;\
        umulh  q,    a,    x                                               ;\
                                                                           ;\
        /* corrige la division tant que c:d >= a et q < BASE-1          */ ;\
1:                                                                         ;\
        subq   d,    x,    d                                               ;\
        cmpult c,    a,    x    /* x <- (c < a)                         */ ;\
        cmpule x,    d,    y    /* y <- (c:d >= a)                      */ ;\
        beq    y,    3f                                                    ;\
        addq   q,    1,    q    /* q++                                  */ ;\
        beq    q,    2f         /* si d�bordement, abandonne            */ ;\
        subq   c,    a,    c    /* c:d -= a                             */ ;\
        br     $31,  1b                                                    ;\
2:                                                                         ;\
        subq   q,    1,    q                                               ;\
3:

                       # +-----------------------------+
                       # |  Division 192 bits par 128  |
                       # +-----------------------------+

	# entr�e :
	#  a:b   = entier naturel tel que b >= BASE/2
	#  c     = floor((BASE^2-1)/b) - BASE
	#  d:e:f = entier naturel tel que e:f <= a:b
	#  x,y   = scratch
	#
	# sortie :
	#  q:r   <- max(floor(0:d:e:f/a:b), BASE^2-1)
	#  d:e:f <- d:e:f - q*b - r*a:b

#define DIV2(d,e,f,a,b,c,q,r,x,y) \
        /* r <- quotient sous-estim� d au plus 3 ou surestim� d au plus 2 */ ;\
        umulh  f,    c,    r                            ;\
        addq   f,    r,    r                            ;\
                                                        ;\
        /* d:e:f <- d:e:f - r*a:b */                    ;\
        mulq   r,    a,    x                            ;\
        cmpult d,    x,    y                            ;\
        subq   d,    x,    d                            ;\
        umulh  r,    a,    x                            ;\
        addq   x,    y,    x                            ;\
        cmpult e,    x,    y                            ;\
        subq   e,    x,    e                            ;\
        subq   f,    y,    f                            ;\
        mulq   r,    b,    x                            ;\
        cmpult e,    x,    y                            ;\
        subq   e,    x,    e                            ;\
        subq   f,    y,    f                            ;\
        umulh  r,    b,    x                            ;\
        subq   f,    x,    f                            ;\
                                                        ;\
        /* tant que f < 0, diminue r et ajoute a:b */   ;\
        bge    f,    3f                                 ;\
1:                                                      ;\
        subq   r,    1,    r                            ;\
        addq   d,    a,    d                            ;\
        cmpult d,    a,    x                            ;\
        addq   x,    b,    x                            ;\
        cmpult x,    b,    y                            ;\
        addq   f,    y,    f                            ;\
        addq   e,    x,    e                            ;\
        cmpult e,    x,    y                            ;\
        addq   f,    y,    f                            ;\
        bne    f,    1b                                 ;\
        br     $31,  5f                                 ;\
                                                        ;\
        /* tant que d:e:f >= a:b et r <= BASE-1, augmente r et retranche a:b */ ;\
2:                                                      ;\
        subq   d,    a,    d                            ;\
        subq   e,    x,    e                            ;\
3:                                                      ;\
        cmpult d,    a,    x                            ;\
        addq   x,    b,    x                            ;\
        cmpult x,    b,    q                            ;\
        cmpult e,    x,    y                            ;\
        addq   q,    y,    y                            ;\
        subq   f,    y,    f                            ;\
        blt    f,    4f                                 ;\
        addq   r,    1,    r                            ;\
        bne    r,    2b                                 ;\
        subq   r,    1,    r                            ;\
4:                                                      ;\
        addq   f,    y,    f                            ;\
5:                                                      ;\
	/* si e:f >= b, q <- BASE-1, sinon q <- approx(d:e/b) */ ;\
	cmpult e,    b,    x                            ;\
	cmpult f,    x,    q                            ;\
	subq   q,    1,    q                            ;\
	bne    q,    6f                                 ;\
        umulh  e,    c,    q                            ;\
        addq   e,    q,    q                            ;\
6:                                                      ;\
	/* d:e:f <- d:e:f - q*b */                      ;\
        mulq   q,    b,    x                            ;\
        cmpult d,    x,    y                            ;\
        subq   d,    x,    d                            ;\
        umulh  q,    b,    x                            ;\
        addq   x,    y,    x                            ;\
        cmpult e,    x,    y                            ;\
        subq   f,    y,    f                            ;\
                                                        ;\
        /* tant que d:e:f >= b et q < BASE-1, augmente q et retranche b */ ;\
7:                                                      ;\
        subq   e,    x,    e                            ;\
        cmpult d,    b,    x                            ;\
        cmpult e,    x,    y                            ;\
        subq   f,    y,    f                            ;\
        blt    f,    9f                                 ;\
        addq   q,    1,    q                            ;\
        beq    q,    8f                                 ;\
        subq   d,    b,    d                            ;\
	br     $31,  7b                                 ;\
8:	                                                ;\
        subq   q,    1,    q                            ;\
9:                                                      ;\
        addq   f,    y,    f

	
	
                         # +-------------------------+
                         # |  Division � un chiffre  |
                         # +-------------------------+


   # unsigned long xn(div_1)(chiffre *a, long la, unsigned long b, chiffre *c)
   #
   # entr�e :
   # a = naturel de longueur la >= 0
   # b = long > 0
   # c = naturel de longueur la, peut �tre confondu avec a
   #
   # sortie :
   # c <- floor(a/b)
   # retourne a mod b
        
#ifdef assembly_sn_div_1
#define L(x) .Lsn_div_1_##x

        # param�tres
#define _a_  $16
#define _b_  $18
#define _c_  $19
#define _la_ $17

        # variables locales
        # s     = d�calage pour avoir b >= BASE/2
        # t     = d�calage compl�mentaire
        # al:ah = chiffres de poids fort de a << s
        # bi    = floor((BASE^2-1)/b) - BASE
        # q     = chiffre  de poids fort de c
        # x,y   = scratch
#define _al_ $0
#define _ah_ $1
#define _bi_ $2
#define _s_  $3
#define _t_  $4
#define _q_  $5
#define _x_  $6
#define _y_  $7

        .align 5
        .globl sn_div_1
        .ent   sn_div_1
sn_div_1:
        .frame $30,0,$26,0
        .prologue 1
	ldgp   $gp,  0($27)

        s8addq _la_, _a_,  _a_  # a <- &a[la] 
        s8addq _la_, _c_,  _c_  # c <- &c[la]
        bis    $31,  $31,  _al_ # reste <- 0
        beq    _la_, L(done)
        
        # d�cale b pour avoir b >= BASE/2, puis l inverse
        bis    $31,  $31,  _s_
        blt    _b_,  2f
        .align 5
1:
        addq   _s_,  1,    _s_
        sll    _b_,  1,    _b_
        bge    _b_,  1b
2:
        lda    _t_,  64($31)
        subq   _t_,  _s_,  _t_
        INVERSE(_b_,_bi_,_x_,_y_)

        # r�cup�re les s bits de poids fort de a
        beq    _s_,  1f
        ldq    _al_,-8(_a_)
        srl    _al_, _t_,  _al_
1:

        # boucle sur les chiffres de a
        .align 5
L(loop):
        subq   _a_,  8,    _a_  # a--
        subq   _c_,  8,    _c_  # c--
        subq   _la_, 1,    _la_ # la--
        
        bis    _al_, _al_, _ah_ # al:ah <- chiffres de poids fort de a << s
        ldq    _al_, 0(_a_)
        beq    _s_,  1f
        sll    _al_, _s_,  _al_
        beq    _la_, 1f
        ldq    _x_, -8(_a_)
        srl    _x_,  _t_,  _x_
        bis    _x_,  _al_, _al_
1:
        DIV(_al_,_ah_,_b_,_bi_,_q_,_x_,_y_)
        stq    _q_,  0(_c_)
        bne    _la_, L(loop)

        # d�cale le dernier reste
        srl    _al_, _s_,  _al_
L(done):
        ret    $31,  ($26),1

	.end sn_div_1

#undef L
#undef _a_
#undef _b_
#undef _c_
#undef _la_
#undef _al_
#undef _ah_
#undef _bi_
#undef _s_
#undef _t_
#undef _q_
#undef _x_
#undef _y_
#undef _z_
#endif /* assembly_sn_div_1 */

   # unsigned long xn(mod_1)(chiffre *a, long la, unsigned long b)
   #
   # entr�e :
   # a = naturel de longueur la >= 0
   # b = long > 0
   #
   # sortie :
   # retourne a mod b
        
#ifdef assembly_sn_mod_1
#define L(x) .Lsn_mod_1_##x

        # param�tres
#define _a_  $16
#define _b_  $18
#define _la_ $17

        # variables locales
        # s     = d�calage pour avoir b >= BASE/2
        # t     = d�calage compl�mentaire
        # al:ah = chiffres de poids fort de a << s
        # bi    = floor((BASE^2-1)/b) - BASE
        # q     = chiffre  de poids fort de c
        # x,y   = scratch
#define _al_ $0
#define _ah_ $1
#define _bi_ $2
#define _s_  $3
#define _t_  $4
#define _q_  $5
#define _x_  $6
#define _y_  $7

        .align 5
        .globl sn_mod_1
        .ent   sn_mod_1
sn_mod_1:
        .frame $30,0,$26,0
        .prologue 1
	ldgp   $gp,  0($27)

        s8addq _la_, _a_,  _a_  # a <- &a[la] 
        bis    $31,  $31,  _al_ # reste <- 0
        beq    _la_, L(done)
        
        # d�cale b pour avoir b >= BASE/2, puis l inverse
        bis    $31,  $31,  _s_
        blt    _b_,  2f
        .align 5
1:
        addq   _s_,  1,    _s_
        sll    _b_,  1,    _b_
        bge    _b_,  1b
2:
        lda    _t_,  64($31)
        subq   _t_,  _s_,  _t_
        INVERSE(_b_,_bi_,_x_,_y_)

        # r�cup�re les s bits de poids fort de a
        beq    _s_,  1f
        ldq    _al_,-8(_a_)
        srl    _al_, _t_,  _al_
1:

        # boucle sur les chiffres de a
        .align 5
L(loop):
        subq   _a_,  8,    _a_  # a--
        subq   _la_, 1,    _la_ # la--
        
        bis    _al_, _al_, _ah_ # al:ah <- chiffres de poids fort de a << s
        ldq    _al_, 0(_a_)
        beq    _s_,  1f
        sll    _al_, _s_,  _al_
        beq    _la_, 1f
        ldq    _x_, -8(_a_)
        srl    _x_,  _t_,  _x_
        bis    _x_,  _al_, _al_
1:
        DIV(_al_,_ah_,_b_,_bi_,_q_,_x_,_y_)
        bne    _la_, L(loop)

        # d�cale le dernier reste
        srl    _al_, _s_,  _al_
L(done):
        ret    $31,  ($26),1

	.end sn_mod_1

#undef L
#undef _a_
#undef _b_
#undef _la_
#undef _al_
#undef _ah_
#undef _bi_
#undef _s_
#undef _t_
#undef _q_
#undef _x_
#undef _y_
#undef _z_
#endif /* assembly_sn_mod_1 */

                         # +------------------------+
                         # |  Division quadratique  |
                         # +------------------------+

   # void xn(div_n2)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c)
   #
   # entr�e :
   # a = naturel de longueur lc+lb
   # b = naturel de longueur lb
   # c = naturel de longueur lc
   #
   # contraintes : 
   # lb >= 2, lc > 0, le bit de poids fort de b est non nul,
   # a < BASE^lc*b
   # a,b,c non confondus
   #
   # sortie :
   # a <- a mod b
   # c <- floor(a/b)

#ifdef assembly_sn_div_n2
#define L(x) .Lsn_div_n2_##x

	# param�tres (apr�s relogement pour utiliser mulsubloop et addloop)
#define _a_  $20
#define _b_  $16
#define _c_  $21
#define _lb_ $19
#define _lc_ $17

        # variables locales
        # al:am:ah = chiffres de poids fort de a
        # bl:bh    = chiffres de poids fort de b
        # bi       = floor((BASE^2-1)/bh) - BASE
        # ql:qh    = chiffres  de poids fort de c
        # i        = indice pour b
        # x:y      = retenue pour mulsub, inc
	# add      = point d entr�e pour addloop
	# msub     = point d entr�e pour mulsubloop
	# msub2    = point d entr�e pour mulsubloop2
#define _al_ $4
#define _am_ $5
#define _ah_ $6
#define _bl_ $23
#define _bh_ $24
#define _bi_ $25
#define _qh_ $8
#define _ql_ $7
#define _i_  $2
#define _x_  $1
#define _y_  $0
#define _add_   $22
#define _msub_  $23
#define _msub2_ $28

        .align 5
#ifdef debug_div_n2
        .globl sn_div_n2_buggy
        .ent   sn_div_n2_buggy
sn_div_n2_buggy:
        .frame $30,0,$26,0
        .prologue 1
	ldgp   $gp,  0($27)
#else
        .globl sn_div_n2
        .ent   sn_div_n2
sn_div_n2:
        .frame $30,0,$26,0
        .prologue 1
	ldgp   $gp,  0($27)
L(nogp):
#endif

	# mise en place des param�tres
	subq   $17,  1,    _lc_
	s8addq _lc_, $20,  _c_
	s8addq _lc_, $16,  _a_
        s8addq _lb_, _a_,  _a_
        s8addq _lb_, $18,  _b_
	ldq    _bh_, -8(_b_)
        INVERSE(_bh_,_bi_,_x_,_y_)

	# pr�pare le d�roulement des boucles internes
	subq   $31,  _lb_, _lb_
	and    _lb_, 31,   _x_
	bic    _lb_, 31,   _lb_
	sll    _x_,  3,    _y_
	lda    _add_,   sn_addloop
	lda    _msub_,  sn_mulsubloop
	lda    _msub2_, sn_mulsubloop2
	s4addq _y_,  _add_,_add_
	s4addq _x_,  _y_,  _y_
	subq   _y_,  _x_,  _y_
	s4addq _y_,  _msub_,_msub_
	s8addq _x_,  _y_,  _y_
	s4addq _y_,  _msub2_,_msub2_

	# calcule le chiffre de t�te de c � part si lc est impair
	blbs   _lc_,  L(even)
	ldq    _ah_,  0(_a_)
	ldq    _am_, -8(_a_)
        DIV(_am_,_ah_,_bh_,_bi_,_qh_,_x_,_y_)
	s8addq _lb_, _a_,  _a_	# restaure a
	s8addq _lb_, _b_,  _b_	# restaure b
	bis    _lb_,  _lb_, _i_	# i <- compteur
	bis    $31,  $31,  _y_  # a <- a - q*b
	bis    $31,  $31,  _x_
	jsr    $27,  (_msub_)
	ldq    _ah_, 0(_a_)
	subq   _ah_, _y_,  _ah_
	
        # corrige le quotient et le reste si n�gatif
        beq    _ah_, L(qh_ok)
L(cor1):
        subq   _qh_, 1,    _qh_ # q--
	s8addq _lb_, _a_,  _a_	# restaure a
	s8addq _lb_, _b_,  _b_	# restaure b
	bis    _lb_,  _lb_, _i_	# i <- compteur
	bis    $31,  $31,  _y_  # a <- a+b
	bis    _a_,  _a_,  $18
	jsr    $27,  (_add_)
	addq   _ah_, _y_,  _ah_
        bne    _ah_, L(cor1)

L(qh_ok):
        stq    _ah_,  0(_a_)
        stq    _qh_,  0(_c_)    # c[lc-1] <- q
        lda    _lc_, -1(_lc_)
        lda    _c_,  -8(_c_)
        lda    _a_,  -8(_a_)
        bge    _lc_, L(even)
        ret    $31,  ($26),1

	.align 5
L(even):
        lda    _a_,  -8(_a_)
	ldq    _bl_, -16(_b_)
	
	# boucle sur les chiffres suivants de a
	.align 5
L(loop_a):
	ldq    _ah_,  8(_a_)
	ldq    _am_,  0(_a_)
	ldq    _al_, -8(_a_)
	DIV2(_al_,_am_,_ah_,_bl_,_bh_,_bi_,_ql_,_qh_,_x_,_y_)

	s8addq _lb_, _a_,  _a_	# restaure a
	s8addq _lb_, _b_,  _b_	# restaure b
	bis    _lb_,  _lb_, _i_	# i <- compteur
	bis    $31,  $31,  _y_  # a <- a - q*b
	bis    $31,  $31,  _x_
	jsr    $27,  (_msub2_)
	ldq    _am_, 0(_a_)
	subq   _am_, _y_,  _am_
	
        # corrige le quotient et le reste si n�gatif
        beq    _am_, L(q_ok)
L(cor2):
	cmpult _ql_, 1,    _x_  # q--
        subq   _ql_, 1,    _ql_
	subq   _qh_, _x_,  _qh_
	s8addq _lb_, _a_,  _a_	# restaure a
	s8addq _lb_, _b_,  _b_	# restaure b
	bis    _lb_,  _lb_, _i_	# i <- compteur
	bis    $31,  $31,  _y_  # a <- a+b
	bis    _a_,  _a_,  $18
	jsr    $27,  (_add_)
	addq   _am_, _y_,  _am_
        bne    _am_, L(cor2)

        .align 5
L(q_ok):
        stq    _am_,  8(_a_)    # am:ah <- 0
        stq    _am_,  0(_a_)
        stq    _qh_,  0(_c_)    # sauve ql:qh
        stq    _ql_, -8(_c_)
        lda    _lc_, -2(_lc_)
        lda    _c_, -16(_c_)
        lda    _a_, -16(_a_)
        bge    _lc_, L(loop_a)

        # termin�
        ret    $31,  ($26),1
	
#ifdef debug_div_n2
	.end sn_div_n2_buggy
#else
	.end sn_div_n2
#endif

#undef L
#undef _a_
#undef _b_
#undef _c_
#undef _lb_
#undef _lc_
#undef _al_
#undef _am_
#undef _ah_
#undef _bl_
#undef _bh_
#undef _bi_
#undef _ql_
#undef _qh_
#undef _i_
#undef _x_
#undef _y_
#undef _add_
#undef _msub_
#undef _msub2_
#endif /* assembly_sn_div_n2 */
#if !defined(assembly_sn_div_n2) || defined(debug_div_n2)
	REPLACE(sn_div_n2)
#endif
