// file kernel/n/alpha/numerix.S: assembly code for mode = SLONG
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                 Fonctions assembleur pour le mode SLONG               |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* fichier de configuration sp�cifique � la machine cible */
#include "../../config.h"

.set noat

/* code de remplacement pour une fonction assembleur d�sactiv�e */
#define REPLACE(nom) \
        .align 5              ;\
        .ent   .L##nom##_nogp ;\
.L##nom##_nogp:		      ;\
	lda    $30,  -16($30) ;\
	stq    $26,  0($30)   ;\
	stq    $gp,  8($30)   ;\
	jsr    $26,  nom      ;\
	ldq    $26,  0($30)   ;\
	ldq    $gp,  8($30)   ;\
	lda    $30,  16($30)  ;\
	ret    $31,  ($26),1  ;\
        .end   .L##nom##_nogp
	
/* d�boguage */ 
/* #include "dumpreg.S" */

/* addition/soustraction */
#include "add.S"

/* multiplication/carr� */
#include "mul_n2.S"
#include "karatsuba.S"
#include "toom.S"

/* division/racine carr�e */
#include "div_n2.S"
#include "sqrt_n2.S"
#include "burnikel.S"

/* op�rations modulo BASE^n +/- 1 */
#include "mmod.S"
#include "smod.S"
	
/* exponentiation modulaire */
#include "montgomery.S"
        
/* pgcd */
#include "gcd_n2.S"

/* divers */
#include "cmp.S"
#include "shift.S"
