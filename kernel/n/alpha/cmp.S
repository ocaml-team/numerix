// file kernel/n/cmp/cmp.S: comparison of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Comparaison                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/


   # long xn(cmp)(chiffre *a, long la, chiffre *b, long lb)
   #
   # entr�e :
   # a = naturel de longueur la
   # b = naturel de longueur lb
   #
   # sortie :
   ## 1 si a > b, 0 si a = b, -1 si a < b

#ifdef assembly_sn_cmp
#define L(x) .Lsn_cmp_##x
#define _a_  $16
#define _b_  $18
#define _la_ $17
#define _lb_ $19

        .align 5
        .globl sn_cmp
        .ent   sn_cmp
sn_cmp:
        .frame $30,0,$26,0
        .prologue 0
        
	s8addq _la_, _a_,  _a_	# a  <- &a[la]
	s8addq _lb_, _b_,  _b_	# b  <- &b[lb]
	subq   _la_, _lb_, $0   # r0 <- la-lb
	cmovgt $0,   _lb_, _la_ # la <- min(la,lb)
	blt    $0,   2f

	# si la >= lb, saute les z�ros de t�te de a
	.align 5
1:
	beq    $0,   3f
	subq   _a_,  8,    _a_
	subq   $0,   1,    $0
	ldq    $1,   0(_a_)
	beq    $1,   1b
	addq   $31,  1,    $0
	ret    $31,  ($26),1
	
	# si lb > la, saute les z�ros de t�te de b
	.align 5
2:
	beq    $0,   3f
	subq   _b_,  8,    _b_
	addq   $0,   1,    $0
	ldq    $1,   0(_b_)
	beq    $1,   2b
	subq   $31,  1,    $0
	ret    $31,  ($26),1
	
        # ici, la == lb, compare les chiffres
	.align 5
3:
        beq    _la_, 4f
	subq   _a_,  8,    _a_
	subq   _b_,  8,    _b_
	subq   _la_, 1,    _la_
	ldq    $0,   0(_a_)
	ldq    $1,   0(_b_)
	cmpult $0,   $1,   $2
	cmpult $1,   $0,   $0
	subq   $0,   $2,   $0
	beq    $0,   3b
4:
	ret    $31,  ($26),1

	.end sn_cmp

#undef L
#undef _a_
#undef _b_
#undef _la_
#undef _lb_
#endif /* assembly_sn_cmp */
