// file kernel/n/alpha/mmod.S: operations on residues modulo BASE^n + 1
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                  Arithm�tique modulo BASE^n + 1                       |
 |                                                                       |
 +-----------------------------------------------------------------------*/


        # +---------------------------------------------------------+
        # |  R�duction modulo BASE^p + 1 et BASE^(2p) - BASE^p + 1  |
        # +---------------------------------------------------------+

   # entr�e :
   #  r16 = naturel longueur 3p         
   #  r17 = naturel de longueur 3p+1
   #  r18 = p
   #  r26 = adresse de retour
   #
   # contraintes :
   #  p > 0
   #
   # sortie :
   #  r17[0..2p-1] <- (r16) mod BASE^(2p) - BASE^p + 1
   #  r17[2p..3p]  <- (r16) mod BASE^p + 1

#if defined(assembly_sn_mmul) || defined(assembly_sn_msqr)
#define L(x) .Lsn_mred_##x

        .align 5
        .globl sn_mred
        .ent   sn_mred
sn_mred:
        .frame $30,0,$26,0
        .prologue 1
        ldgp   $gp,  0($27)
L(nogp):

        s8addq $18, $16, $19    # r19 <- &a1
        s8addq $18, $19, $20    # r20 <- &a2
        s8addq $18, $17, $21    # r21 <- &b1
        s8addq $18, $21, $22    # r22 <- &b2
        bis    $31, $31, $0     # r0 <- 0 (retenue de a0-a2)
        bis    $31, $31, $1     # r1 <- 0 (retenue de a1+a2)
        bis    $31, $31, $2     # r2 <- 0 (retenue de a0-a1+a2)
        bis    $18, $18, $8     # r8 <- p

        # b0 <- a0 - a2, b1 <- a1 + a2, b2 <- a0 - a1 + a2
        .align 5
1:
        ldq    $3,  0($16)      # r3 <- a0[i]
        ldq    $4,  0($19)      # r4 <- a1[i]
        ldq    $5,  0($20)      # r5 <- a2[i]
        addq   $5,  $0,  $6     # r6 <- a2[i] + ret0
        cmpult $6,  $5,  $0
        subq   $3,  $6,  $6     # r6 <- a0[i] - a2[i] - ret0
        stq    $6,  0($17)      # sauve b0[i]
        cmpult $3,  $6,  $6
        addq   $6,  $0,  $0     # r0 <- retenue
        addq   $4,  $1,  $6     # r6 <- a1[i] + ret1
        cmpult $6,  $4,  $1
        addq   $5,  $6,  $6     # r6 <- a1[i] + a2[i] + ret1
        stq    $6,  0($21)      # sauve b1[i]
        cmpult $6,  $5,  $6
        addq   $6,  $1,  $1     # r1 <- retenue
        addq   $3,  $2,  $6     # r6 <- a0[i] + ret2
        sra    $2,  1,   $2
        cmpult $6,  $3,  $3
        addq   $3,  $2,  $2
        subq   $6,  $4,  $4     # r4 <- a0[i] - a1[i] + ret2
        cmpult $6,  $4,  $6
        subq   $2,  $6,  $2
        addq   $4,  $5,  $6     # r6 <- a0[i] - a1[i] + a2[i] + ret2
        stq    $6,  0($22)      # sauve b2[i]
        cmpult $6,  $4,  $6
        addq   $6,  $2,  $2     # r2 <- retenue
        lda    $8,  -1($8)      # avance les pointeurs
        lda    $16, 8($16)
        lda    $17, 8($17)
        lda    $19, 8($19)
        lda    $20, 8($20)
        lda    $21, 8($21)
        lda    $22, 8($22)
        bne    $8,  1b

        # propage la retenue sortant de b0
        beq    $0,  2f
        bis    $18, $18, $8     # r8 <- p
1:
        ldq    $3,  0($17)
        cmpult $3,  1,  $0
        subq   $3,  1,  $3
        stq    $3,  0($17)
        beq    $0,  2f
        lda    $8,  -1($8)
        lda    $17, 8($17)
        bne    $8,  1b          # si la retenue traverse b1 alors
        br     $31, 5f          # a1+a2 = BASE^p et il n y a plus rien � faire

        # recycle la retenue sortant de b1
        # si elle vaut 1, il faut ajouter BASE^p - 1 et comme
        # b1 <= BASE^p - 2, il ne peut pas y avoir de nouvelle retenue
        .align 5
2:
        beq    $1,  5f
        sll    $18, 3,   $8     # r8 <- 8p
        subq   $21, $8,  $19    # r19 <- &b1
        subq   $19, $8,  $16    # r16 <- &b0
3:
        ldq    $3,  0($16)
        cmpult $3,  1,   $0
        subq   $3,  1,   $3
        stq    $3,  0($16)
        beq    $0,  4f
        lda    $8,  -8($8)
        lda    $16, 8($16)
        bne    $8,  3b
        br     $31, 5f
        .align 5
4:
        ldq    $3,  0($19)
        addq   $3,  1,  $3
        stq    $3,  0($19)
        lda    $19, 8($19)
        beq    $3,  4b

        # recycle la retenue sortant de b2
        .align 5
5:
        bge    $2,  7f
        bis    $31, $31, $2
6:
        ldq    $3,  0($21)
        addq   $3,  1,  $3
        stq    $3,  0($21)
        bne    $3,  7f
        lda    $18, -1($18)
        lda    $21, 8($21)
        bne    $18, 6b
        addq   $2,  1,  $2
7:
        stq    $2,  0($22)

        # termin�
        ret    $31, ($26),1

        .end   sn_mred
#undef L
#endif /* defined(assembly_sn_mmul) || defined(assembly_sn_msqr) */


        
                   # +-----------------------------------
                   # |  Multiplication modulo BASE^n + 1  |
                   # +------------------------------------+

   # void xn(mmul)(chiffre *a, chiffre *b, long n)
   # 
   # entr�e :
   # a = naturel de longueur n+1
   # b = naturel de longueur n+1 non confondu avec a
   # 
   # contrainte : n > 0
   # 
   # sortie :
   # a <- (a*b) mod (BASE^n + 1) avec 0 <= a[n] <= 1
   # b <- b mod (BASE^n + 1)

#ifdef assembly_sn_mmul
#define L(x) .Lsn_mmul_##x

        .align 5
#ifdef debug_mmul
        .globl sn_mmul_buggy
        .ent   sn_mmul_buggy
sn_mmul_buggy:
        .frame $30,0,$26,0
        .prologue 1
        ldgp   $gp,  0($27)
#else
        .globl sn_mmul
        .ent   sn_mmul
sn_mmul:
        .frame $30,0,$26,0
        .prologue 1
        ldgp   $gp,  0($27)
L(nogp):
#endif
        
        #define _a_  $16
        #define _b_  $17
        #define _n_  $18

        # normalise a
        s8addq  _n_, _a_, $20
        ldq     $0,  0($20)
        bis     _a_, _a_, $20
        bis     _n_, _n_, $2
        bsr     $27, sn_decloop
        bis     _a_, _a_, $20
        bis     _n_, _n_, $2
        bsr     $27, sn_incloop
        s8addq  _n_, _a_, $20
        stq     $0,  0($20)
        bis     $0,  $0,  $3

        # normalise b
        s8addq  _n_, _b_, $20
        ldq     $0,  0($20)
        bis     _b_, _b_, $20
        bis     _n_, _n_, $2
        bsr     $27, sn_decloop
        bis     _b_, _b_, $20
        bis     _n_, _n_, $2
        bsr     $27, sn_incloop
        s8addq  _n_, _b_, $20
        stq     $0,  0($20)

        # cas o� l un des op�randes vaut BASE^n -> res = oppos�(l autre)
        bis     $0,  $3,  $0
        beq     $0,  L(norm)
        cmoveq  $3,  _a_, _b_      # b <- nombre � opposer
        bis     $31, $31, $2       # r2 <- 0 (retenue)
        .align 5
L(neg):
        lda     _n_, -1(_n_)
        ldq     $1,  0(_b_)
        addq    $2,  $1,  $1
        cmpult  $1,  $2,  $2
        subq    $0,  $1,  $1
        stq     $1,  0(_a_)
        cmpult  $0,  $1,  $1
        addq    $2,  $1,  $2
        bis     $31, $31, $0
        lda     _a_, 8(_a_)
        lda     _b_, 8(_b_)
        bne     _n_, L(neg)
        addq    $31, 1,   $1
        subq    $1,  $2,  $1
        stq     $1,  0(_a_)
        ret     $31, ($26),1

         # op�randes normalis�s, non �gaux � BASE^n
         # si n est petit ou non divisible par 3, multiplication ordinaire
         # puis r�duction :
         # a*b = x + y*BASE^n + z*BASE^(2n) = x - y + z mod (BASE^n + 1)
        .align 5
L(norm):
        cmpule _n_, mmul_lim, $0
        bne    $0, L(simple)
        lda    $0, 0x5555($31)
        sll    $0,  16,  $1
        or     $1,  $0,  $0
        sll    $0,  32,  $1
        or     $1,  $0,  $0     # r0 <- (BASE-1)/3
        umulh  $0,  _n_, $0
        addq   $0,  1,   $0     # r0 <- ceil(n/3) = p
        addq   $0,  _n_, $1     # r1 <- n+p, nul mod 4 ssi n = 3p
        and    $1,  3,   $1
        beq    $1,  L(decomp)
        
L(simple):
        s4addq _n_, 8,   $0     # r�serve 2n+4 chiffres dans la pile
        sll    $0,  2,   $0
        subq   $30, $0,  $30
        stq    _a_, 0($30)      # sauve a,n et l adresse de retour
        stq    _n_, 8($30)
        stq    $26, 16($30)
        lda    $20, 24($30)     # r20 <- adresse tampon
        bis    _n_, _n_, $19    # r19 <- n
        bis    _b_, _b_, $18    # r18 <- &b
        bis    $19, $19, $17    # r17 <- n
        bsr    $26, .Lsn_toommul_nogp # c <- a*b

        # point de chute pour msqr
L(simple_aux):
        ldq    $20, 0($30)      # r20 <- a
        ldq    $17, 8($30)      # r17 <- n
        bis    $17, $17, $19    # r19 <- n
        lda    $16, 24($30)     # r16 <- &c
        s8addq $17, $16, $18    # r18 <- &c[n]
        bsr    $26, .Lsn_sub_nogp     # a <- c0 - c1
        ldq    $26, 16($30)     # r�cup�re l adresse de retour
        ldq    $20, 0($30)      # r20 <- &a
        ldq    $2,  8($30)      # r2  <- n
        lda    $30, 8($18)      # nettoie la pile
        s8addq $2,  $20,  $16   # r16 <- &a[n]
        bsr    $27, sn_incloop  # r�injecte la retenue
        stq    $0,  0($16)
        ret    $31, ($26),1
    
        #undef _a_
        #undef _b_
        #undef _n_
        
        # si n est divisible par 3, on d�compose en une multiplication
        # modulo BASE^p + 1 et une modulo BASE^(2p) - BASE^p + 1
        .align 5
L(decomp):
        #define _a_   0($30)
        #define _b_   8($30)
        #define _c_  32($30)
        #define _p_  16($30)
        #define _ra_ 24($30)
        
        # r�serve 6p+4 chiffres dans la pile
        sll    $0,  1,   $1
        s4addq $0,  $1,  $1
        s8addq $1,  32,  $1
        subq   $30, $1,  $30
        stq    $16, _a_
        stq    $17, _b_
        stq    $0,  _p_
        stq    $26, _ra_

         # d�compose a et b modulo BASE^(2p) - BASE^p + 1 et BASE^p + 1
        bis    $16, $16, $17       # r17 <- &a
        bis    $0,  $0,  $18       # r18 <- p
        bsr    $26, .Lsn_mred_nogp # d�compose a sur place
        ldq    $16, _b_
        lda    $17, _c_
        ldq    $18, _p_
        bsr    $26, .Lsn_mred_nogp # d�compose b dans c[0..3p]

         # a[2p..3p] <- (a*b) mod BASE^p + 1
        ldq    $16, _a_
        lda    $17, _c_
        ldq    $18, _p_
        sll    $18, 1,   $0
        s8addq $0,  $16, $16    # r16 <- &a[2p]
        s8addq $0,  $17, $17    # r17 <- &c[2p]
        bsr    $26, .Lsn_mmul_nogp
        
        # c[2p..6p-1] <- (a*b) mod (BASE^(2p) - BASE^p + 1), non r�duit
        ldq    $16, _a_
        lda    $18, _c_
        ldq    $17, _p_
        sll    $17, 1,   $17    # r17 <- 2p
        bis    $17, $17, $19    # r19 <- 2p
        s8addq $17, $18, $20    # r20 <- &c[2p]
        bsr    $26, .Lsn_toommul_nogp

        # point de chute pour msqr
L(decomp_aux):

        # a[0..2p-1] <- (a*b) mod (BASE^(2p) - BASE^p + 1)
        # a[2p..3p]  <- a[0..2p-1] - (a*b) mod (BASE^p + 1)
        ldq    $23, _p_
        ldq    $16, _a_         # r16 <- &a0
        lda    $19, _c_         # r19 <- &c2
        s8addq $23, $19, $19
        s8addq $23, $19, $19
        s8addq $23, $16, $17    # r17 <- &a1
        s8addq $23, $17, $18    # r18 <- &a2
        s8addq $23, $19, $20    # r20 <- &c3
        s8addq $23, $20, $21    # r21 <- &c4
        s8addq $23, $21, $22    # r22 <- &c5
        bis    $31, $31, $0     # r0  <- 0 (retenue(c2-c4-c5))
        bis    $31, $31, $1     # r1  <- 0 (retenue(c3+c4))
        bis    $31, $31, $2     # r2  <- 0 (retenue(a0-a1-a2))
        bis    $2,  $2,  $24    # r24 <- a2[p] (a2 mod (BASE-1))
        bis    $23, $23, $8     # r8 <- p (compteur)

        .align 5
1:
        ldq    $3,  0($19)      # r3 <- c2[i]
        ldq    $4,  0($20)      # r4 <- c3[i]
        ldq    $5,  0($21)      # r5 <- c4[i]
        ldq    $6,  0($22)      # r6 <- c5[i]
        addq   $5,  $6,  $6     # r6 <- c4[i] + c5[i]
        cmpult $6,  $5,  $7
        addq   $0,  $6,  $6     # r6 <- c4[i] + c5[i] + ret0
        cmpult $6,  $0,  $0
        addq   $7,  $0,  $0
        cmpult $3,  $6,  $7
        subq   $3,  $6,  $3     # r3 <- c2[i] - c4[i] - c5[i] - ret0
        addq   $7,  $0,  $0     # r0 <- retenue
        stq    $3,  0($16)      # sauve a0[i]
        addq   $4,  $5,  $4     # r4 <- c3[i] + c4[i]
        cmpult $4,  $5,  $5
        addq   $4,  $1,  $4     # r4 <- c3[i] + c4[i] + ret1
        cmpult $4,  $1,  $1
        addq   $5,  $1,  $1     # r1 <- retenue
        stq    $4,  0($17)      # sauve a1[i]
        ldq    $7,  0($18)      # r7 <- a2[i]
        addq   $4,  $2,  $4     # r4 <- a1[i] + ret2
        cmpult $4,  $2,  $2
        addq   $7,  $4,  $4     # r4 <- a1[i] + a2[i] + ret2
        cmpult $4,  $7,  $5
        addq   $5,  $2,  $2
        subq   $3,  $4,  $4     # r4 <- a0[i] - a1[i] - a2[i] - ret2
        cmpult $3,  $4,  $5
        addq   $2,  $5,  $2     # r2 <- retenue
        stq    $4,  0($18)      # sauve a2[i]
        addq   $4,  $24, $24    # mise � jour de a2 mod (BASE-1)
        cmpult $24, $4,  $4
        addq   $4,  $24, $24
        lda    $8,  -1($8)      # avance les pointeurs
        lda    $16, 8($16)
        lda    $17, 8($17)
        lda    $18, 8($18)
        lda    $19, 8($19)
        lda    $20, 8($20)
        lda    $21, 8($21)
        lda    $22, 8($22)
        bne    $8,  1b

        addq   $0,  $2,  $2     # r2 <- retenue finale sortant de a2
        addq   $1,  $2,  $2
        ldq    $3,  0($18)
        addq   $2,  $3,  $2

        # propage la retenue sur a1 (0,-1,-2)
        beq    $0,  2f
        bis    $23, $23, $8
1:
        ldq    $3,  0($16)
        subq   $3,  $0,  $0
        stq    $0,  0($16)
        cmpult $3,  $0,  $0
        beq    $0,  2f
        lda    $8,  -1($8)
        lda    $16, 8($16)
        bne    $8,  1b
        subq   $1,  $0,  $1
2:

        # recycle la retenue sur a2 (-1,0,1)
        beq    $1,  3f
        bis    $23, $23, $8     # r8 <- p
        sll    $23, 3,   $0
        subq   $17, $0,  $19    # r19 <- &a1
        subq   $19, $0,  $16    # r16 <- &a0
        subq   $31, $1,  $0     # r0 <- -r1  
        subq   $2,  $1,  $2     # ret(a2) -= 3*r1
        subq   $2,  $1,  $2
        subq   $2,  $1,  $2
1:
        ldq    $3,  0($16)      # a0 -= r1
        addq   $3,  $0,  $3
        stq    $3,  0($16)
        cmpult $3,  $0,  $3
        sra    $0,  1,   $0
        addq   $3,  $0,  $0
        beq    $0,  2f
        lda    $8,  -1($8)
        lda    $16, 8($16)
        bne    $8,  1b
        br     $31, 3f
        
2:
        ldq    $3,  0($19)      # a1 += r1
        addq   $3,  $1,  $3
        stq    $3,  0($19)
        cmpult $3,  $1,  $3
        sra    $1,  1,   $1
        addq   $3,  $1,  $1
        lda    $19, 8($19)
        bne    $1,  2b
3:

        # Ici -r2 contient la retenue sortant de a2 (entre -3 et 8) et
        # r24 contient le r�sidu de a2 modulo BASE-1.
        # On doit recycler la retenue sortante et faire en sorte que le
        # nombre final soit non nul, divisible par 3
        # -> ajouter (x-r2)*BASE^p + x
        # avec 2x + r24 - r2 = 0 mod 3 et 0 < x-r2 <= BASE-2

        subq   $24, $2,  $0     # r0 <- x = r24 - r2 mod (BASE-1)
        cmpult $24, $0,  $24
        sra    $2,  63,  $3
        addq   $24, $3,  $24
        subq   $0,  $24, $0
        subq   $0,  $2,  $1     # r1:(-r2) <- x-r2 mod BASE^2
        cmpult $0,  $1,  $2
        addq   $2,  $3,  $2
        addq   $31, 9,   $3     # r3 <-  9 (correctif si x - r2 <  BASE/2)
        subq   $31, 9,   $4     # r4 <- -9 (correctif si x - r2 >= BASE/2)
	cmovgt $2,  $3,  $4     # x - r2 < 0       =>  9  9
	cmovlt $2,  $4,  $3     # x - r2 >= BASE   => -9 -9
	cmovlt $1,  $4,  $3     # x - r2 >= BASE/2 => -9 -9
        addq   $3,  $0,  $0     # corrige x et x-r2
        addq   $3,  $1,  $1
        bis    $23, $23, $8     # ajoute x
1:
        ldq    $3,  0($17)
        addq   $3,  $0,  $3
        cmpult $3,  $0,  $0
        stq    $3,  0($17)
        beq    $0,  2f
        lda    $8,  -1($8)
        lda    $17, 8($17)
        bne    $8,  1b
2:
        addq   $0,  $1,  $1    # a2[p] <- x-r2 + ret
        stq    $1,  0($18)

        # a2 <- a2/(-3), a0 <- a0 + a2/(-3), a1 <- a1 - a2/(-3)
        ldq    $16, _a_         # r16 <- &a0
        s8addq $23, $16, $17    # r17 <- &a1
        s8addq $23, $17, $18    # r18 <- &a2
        lda    $0, 0x5555($31)
        sll    $0,  16,  $1
        or     $1,  $0,  $0
        sll    $0,  32,  $1
        or     $1,  $0,  $0     # r0 <- (BASE-1)/3
        bis    $31, $31, $1     # r1 <- 0 (retenue(a2*(BASE-1)/3))
        bis    $31, $31, $2     # r2:r3 <- 0 (retenue(a2/(-3)))
        bis    $31, $31, $3
        bis    $31, $31, $4     # r4 <- 0 (retenue(a0+a2))
        bis    $31, $31, $5     # r5 <- 0 (retenue(a1-a2))
        addq   $23, 1,   $8     # r8 <- p+1
        .align 5
1:
        ldq    $6,  0($18)      # r6 <- a2[i]
        mulq   $6,  $0,  $7     # r7:r6 <- a2[i]*(BASE-1)/3
        umulh  $6,  $0,  $6
        addq   $1,  $7,  $7     # r7:r1 <- a2[i]*(BASE-1)/3 + ret
        cmpult $7,  $1,  $1
        addq   $6,  $1,  $1
        addq   $7,  $2,  $2     # r2:r3 <- a2/(-3)[i] + ret
        cmpult $2,  $7,  $7
        addq   $3,  $2,  $2
        cmpult $2,  $3,  $3
        addq   $7,  $3,  $3
        stq    $2,  0($18)      # sauve a2/(-3)[i]
        ldq    $6,  0($16)      # r6 <- a0[i]
        addq   $2,  $6,  $6     # r6:r4 <- a0[i] + a2/(-3)[i] + ret
        cmpult $6,  $2,  $7
        addq   $4,  $6,  $6
        cmpult $6,  $4,  $4
        addq   $7,  $4,  $4
        stq    $6,  0($16)      # sauve a0[i]
        ldq    $6,  0($17)      # r6 <- a1[i]
        addq   $2,  $5,  $5     # r6:r5 <- a1[i] - a2/(-3)[i] + ret
        cmpult $5,  $2,  $7
        subq   $6,  $5,  $5
        cmpult $6,  $5,  $6
        stq    $5,  0($17)      # sauve a1[i]
        addq   $6,  $7,  $5
        lda    $8,  -1($8)      # avance les pointeurs
        lda    $16, 8($16)
        lda    $17, 8($17)
        lda    $18, 8($18)
        bne    $8,  1b

        subq   $4,  1,  $4
        subq   $5,  1,  $5
        bis    $23, $23, $7     # r7 <- p
        addq   $23, $23, $8     # r8 <- 2p

        # propage la retenue sortant de a0 (-1,0,1)
1:
        ldq    $0,  0($16)
        addq   $0,  $4,  $0
        stq    $0,  0($16)
        cmpult $0,  $4,  $0
        sra    $4,  1,   $4
        addq   $0,  $4,  $4
        beq    $4,  2f
        lda    $8,  -1($8)
        lda    $16, 8($16)
        bne    $8,  1b

        # propage la retenue sortant de a1 (-1,0,1)
2:
        ldq    $0,  0($17)
        subq   $0,  $5,  $1
        stq    $1,  0($17)
        cmpult $0,  $1,  $0
        sra    $5,  1,   $5
        addq   $0,  $5,  $5
        beq    $5,  3f
        lda    $7,  -1($7)
        lda    $17, 8($17)
        bne    $7,  2b

        # recycle la retenue sortant de a2 (r4 - r5 + BASE)
        # on incorpore le chiffre a2[p]-1 et on laisse 1 dans a2[p] pour
        # absorber une retenue �ventuelle
3:
        addq   $31, 1,   $1
        addq   $31, 1,   $2
        ldq    $0,  -8($18)
        stq    $2,  -8($18)
        cmpult $0,  1,   $2
        subq   $0,  1,   $0
        subq   $1,  $2,  $1
        addq   $4,  $0,  $0
        cmpult $0,  $0,  $2
        addq   $2,  $1,  $1
        cmpult $0,  $5,  $2
        subq   $0,  $5,  $0
	subq   $2,  $1,  $1
        ldq    $18, _a_
        ldq    $2,  0($18)
        subq   $2,  $0,  $3
        stq    $3,  0($18)
        cmpult $2,  $3,  $0
        addq   $0,  $1,  $0
        beq    $0,  5f
4:
        lda    $18, 8($18)
        ldq    $2,  0($18)
        subq   $2,  $0,  $3
        stq    $3,  0($18)
        cmpult $2,  $3,  $1
        sra    $0,  1,   $0
        addq   $0,  $1,  $0
        bne    $0,  4b
5:

        # termin�
L(done):
        ldq    $26, _ra_
        bis    $22, $22, $30    # nettoie la pile
        ret    $31, ($26),1

        #undef _a_
        #undef _b_
        #undef _c_
        #undef _p_
        #undef _ra_

#undef L
#ifdef debug_mmul
        .end sn_mmul_buggy
#else
        .end sn_mmul
#endif
#undef L
#endif /* assembly_sn_mmul */
#if !defined(assembly_sn_mmul) || defined(debug_mmul)
        REPLACE(sn_mmul)
#endif

                        # +---------------------------+
                        # |  Carr� modulo BASE^n + 1  |
                        # +---------------------------+

   #  void xn(msqr)(chiffre *a, long n)
   #
   #  entr�e :
   #  a = naturel de longueur n+1
   #
   #  contrainte : n > 0
   #
   #  sortie :
   #  a <- a^2 mod (BASE^n + 1), le chiffre de poids fort vaut 0 ou 1

#ifdef assembly_sn_msqr
#define L(x) .Lsn_msqr_##x

        .align 5
#ifdef debug_mmul
        .globl sn_msqr_buggy
        .ent   sn_msqr_buggy
sn_msqr_buggy:
        .frame $30,0,$26,0
        .prologue 1
        ldgp   $gp,  0($27)
#else
        .globl sn_msqr
        .ent   sn_msqr
sn_msqr:
        .frame $30,0,$26,0
        .prologue 1
        ldgp   $gp,  0($27)
L(nogp):
#endif
        
        #define _a_  $16
        #define _n_  $17

        # normalise a
        s8addq  _n_, _a_, $20
        ldq     $0,  0($20)
        bis     _a_, _a_, $20
        bis     _n_, _n_, $2
        bsr     $27, sn_decloop
        bis     _a_, _a_, $20
        bis     _n_, _n_, $2
        bsr     $27, sn_incloop
        s8addq  _n_, _a_, $20

        # cas o� a = BASE^n -> res = 1
        beq     $0,  L(norm)
        stq     $0,  0(_a_)
        stq     $31, 0($20)
        ret     $31, ($26),1

         # op�rande normalis�, non �gal � BASE^n
         # si n est petit ou non divisible par 3, carrz ordinaire puis r�duction
        .align 5
L(norm):
        stq     $0,  0($20)
        cmpule  _n_, msqr_lim, $0
        bne     $0, L(simple)
        lda    $0, 0x5555($31)
        sll    $0,  16,  $1
        or     $1,  $0,  $0
        sll    $0,  32,  $1
        or     $1,  $0,  $0     # r0 <- (BASE-1)/3
        umulh  $0,  _n_, $0
        addq   $0,  1,   $0     # r0 <- ceil(n/3) = p
        addq   $0,  _n_, $1     # r1 <- n+p, nul mod 4 ssi n = 3p
        and    $1,  3,   $1
        beq    $1,  L(decomp)
        
L(simple):
        s4addq _n_, 8,   $0     # r�serve 2n+4 chiffres dans la pile
        sll    $0,  2,   $0
        subq   $30, $0,  $30
        stq    _a_, 0($30)      # sauve a,n et l adresse de retour
        stq    _n_, 8($30)
        stq    $26, 16($30)
        lda    $18, 24($30)     # r18 <- adresse tampon
        bsr    $26, .Lsn_toomsqr_nogp # c <- a^2
        br     $31, .Lsn_mmul_simple_aux # continue avec mmul
    
        #undef _a_
        #undef _n_
        
        # si n est divisible par 3, on d�compose en un carr�
        # modulo BASE^p + 1 et un modulo BASE^(2p) - BASE^p + 1
        .align 5
L(decomp):

        #define _a_   0($30)
        #define _b_   8($30)
        #define _c_  32($30)
        #define _p_  16($30)
        #define _ra_ 24($30)
        
        # r�serve 6p+4 chiffres dans la pile
        sll    $0,  1,   $1
        s4addq $0,  $1,  $1
        s8addq $1,  32,  $1
        subq   $30, $1,  $30
        stq    $16, _a_
        stq    $16, _b_
        stq    $0,  _p_
        stq    $26, _ra_

         # d�compose a modulo BASE^(2p) - BASE^p + 1 et BASE^p + 1
        bis    $16, $16, $17       # r17 <- &a
        bis    $0,  $0,  $18       # r18 <- p
        bsr    $26, .Lsn_mred_nogp # d�compose a sur place

         # a[2p..3p] <- a^2 mod BASE^p + 1
        ldq    $16, _a_
        ldq    $17, _p_
        sll    $17, 1,   $0
        s8addq $0,  $16, $16    # r16 <- &a[2p]
        bsr    $26, .Lsn_msqr_nogp
        
        # c[2p..6p-1] <- a^2 mod (BASE^(2p) - BASE^p + 1), non r�duit
        ldq    $16, _a_
        lda    $18, _c_
        ldq    $17, _p_
        sll    $17, 1,   $17    # r17 <- 2p
        s8addq $17, $18, $18    # r18 <- &c[2p]
        bsr    $26, .Lsn_toomsqr_nogp
        br     $31, .Lsn_mmul_decomp_aux # continue avec mmul

        #undef _a_
        #undef _b_
        #undef _c_
        #undef _p_
        #undef _ra_

#undef L
#ifdef debug_mmul
        .end sn_msqr_buggy
#else
        .end sn_msqr
#endif
#undef L
#endif /* assembly_sn_msqr */
#if !defined(assembly_sn_msqr) || defined(debug_mmul)
        REPLACE(sn_msqr)
#endif

                      # +------------------------------+
                      # |  Papillon modulo BASE^n + 1  |
                      # +------------------------------+

   # void xn(butterfly1)(chiffre *a, chiffre *b, long n, long q, int s)
   #
   #  entr�e :
   #  a = naturel de longueur n+1
   #  b = naturel de longueur n+1 non confondu avec a
   #  q = entier positif ou nul
   #  s = 0 ou 1
   #
   #  contraintes : n >= 3 et si q est impair, n doir �tre pair
   #
   #  sortie :
   #  a <- a + (-1)^s * b * 2^(q/2) mod (BASE^n + 1)
   #  b <- a - (-1)^s * b * 2^(q/2) mod (BASE^n + 1)
   #
   #  remarque : 2^(1/2) = BASE^(3n/4)*(BASE^(n/2) + 1) mod (BASE^n + 1)

#ifdef assembly_sn_butterfly
#define L(x) .Lsn_butterfly_##x

        .align 5
#ifdef debug_butterfly
        .globl sn_butterfly_buggy
        .ent   sn_butterfly_buggy
sn_butterfly_buggy:
        .frame $30,0,$26,0
        .prologue 1
        ldgp   $gp,  0($27)
#else
        .globl sn_butterfly
        .ent   sn_butterfly
sn_butterfly:
        .frame $30,0,$26,0
        .prologue 1
        ldgp   $gp,  0($27)
#endif

	# sauvegarde les param�tres
	#define _a_  $22
	#define _b_  $23
	#define _n_  $24
	#define _q_  $19
	#define _s_  $28
	
	bis    $16, $16, _a_
	bis    $17, $17, _b_
	bis    $18, $18, _n_
	bis    $20, $20, _s_

        # force 2 <= a[n] <= BASE-3 pour absorber les retenues
	s8addq _n_, _a_, $0	# r0 <- &a[n]
	ldq    $1,  0($0)		# r1 <- a[n]
	cmpult $1,  2,   $2	# r2 <- a[n] < 2 ?
	addq   $1,  2,   $3	# r3 <- a[n] >= BASE-2 ?
	cmpult $3,  $1,  $3
	subq   $2,  $3,  $2	# r2 <- 1 si a[n] < 2, -1 si a[n] >= BASE-2, 0 sinon
	beq    $2,  2f		# pas besoin de correction
	sll    $2,  1,   $2	# r2 <- 2 ou -2
	addq   $2,  $1,  $1	# a <- a + r2*(BASE^n + 1)
	stq    $1,  0($0)
	bis    _a_, _a_, $0
1:
	ldq    $1,  0($0)
	addq   $1,  $2,  $1
	stq    $1,  0($0)
	cmpult $1,  $2,  $1
	sra    $2,  2,   $2
	addq   $1,  $2,  $2
	lda    $0,  8($0)
	bne    $2,  1b
2:

	# si q est impair, multiplie b par BASE^(n/2) + 1
	blbc   _q_, L(q_even)
	s4subq _n_, _n_, $0	# q <- q + 3*n*HW/2
	sll    $0,  5,   $0
	addq   _q_, $0,  _q_
	s8addq _n_, _b_, $7	# r7 <- b[n]
	ldq    $7,  0($7)
	srl    _n_, 1,   $2	# r2 <- -n/2
	subq   $31, $2,  $2
	and    $2,  31,  $3	# r3 <- (-n/2) mod 32
	bic    $2,  31,  $2	# r2 <- -32*ceil(n/64)
	sll    $3,  3,   $4
	subq   _b_, $4,  $16	# r16 <- &a0[-(-n/2) mod 32]
	s4addq _n_, $16, $18	# r18 <- &a1[-(-n/2) mod 32]
	s4addq _n_, $16, $20	# r20 <- &a1[-(-n/2) mod 32]
	bis    $16, $16, $21	# r21 <- &a0[-(-n/2) mod 32]
	subq   $4,  $3,  $4	# r27 <- adresse de saut dans addsubloop
	lda    $27, sn_addsubloop
	s8addq $4,  $27, $27
	bis    $31, $31, $0	# r0 <- 0 (retenue(b0+b1)
	srl    $7,  1,   $1	# r1 <- floor(b[n]/2) (retenue(b0-b1)
	subq   $7,  $1,  $8	# r8 <- ceil(b[n]/2)
	jsr    $27, ($27)	# b0 <- b0-b1-b[n]/2, b1 <- b1+b0
	addq   $0,  $8,  $8	# b[n] <- retenue sortant du haut
	stq    $8,  0($20)
	addq   $7,  $1,  $1	# r1:r0 <- retenue sortant du bas
	cmpult $1,  $7,  $0
	ldq    $2,  0($21)	# cumule � b1[0]
	subq   $2,  $1,  $1
	stq    $1,  0($21)
	cmpult $2,  $1,  $1
	addq   $1,  $0,  $0	# r0 <- nouvelle retenue
	lda    $20, 8($21)
	subq   $31, 1,   $2
	bsr    $27, sn_decloop	# la propage sur b1 (pas de d�bordement)
	.align 5
L(q_even):
        
	# d�compose le d�calage en nombre et fraction de chiffre
	srl    _q_, 1,   $8	# r8 <- (q/2) mod HW = k
	and    $8,  63,  $8
	srl    _q_, 7,   _q_	# q <- (q/2)/HW
1:
	cmpult _q_, _n_, $1
	subq   _q_, _n_, _q_	# r�duit q modulo n
	xor    _s_, 1,   _s_
	beq    $1, 1b
	addq   _q_, _n_, _q_
	
	# b <- b*2^k mod (BASE^n + 1), normalis�
	beq    $8,  1f
	addq   $31, 64,  $7	# r7 <- 64-k
	subq   $7,  $8,  $7
	subq   $31, _n_, $2	# r2 <- -n
	and    $2,  31,  $3	# r3 <- (-n) mod 32
	bic    $2,  31,  $2	# r2 <- -32*ceil(n/32)
	sll    $3,  3,   $4
	subq   _b_, $4,  $16	# r16 <- &b[-(-n) mod 32]
	subq   _b_, $4,  $20	# r20 <- &b[-(-n) mod 32]
	s4addq $3,  $3,  $3
	lda    $27, sn_shuploop	# r27 <- adresse de saut dans shuploop
	s4addq $3,  $27, $27
	bis    $31, $31, $0	# r0 <- 0 (retenue)
	jsr    $27, ($27)		# b[0..n-1] <<= k
	ldq    $1,  0($16)	# d�cale le dernier chiffre dans r0:r1
	sll    $1,  $8,  $3
	bis    $0,  $3,  $0
	srl    $1,  $7,  $1
	br     $31, 2f
	.align 5
1:
	s8addq _n_, _b_, $20	# cas k=0: normalise b
	ldq    $0,  0($20)	# r0:r1 <- b[n]
	bis    $31, $31, $1
2:
	ldq    $2,  0(_b_)	# traite le premier chiffre � part
	subq   $2,  $0,  $0
	stq    $0,  0(_b_)
	cmpult $2,  $0,  $0
	addq   $1,  $0,  $0	# r0 <- retenue sur b[1]
	lda    $20, 8(_b_)	# r20 <- &b[1]
	subq   _n_, 1,   $2	# r2 <- n-1
	bsr    $27, sn_decloop	# b[1..n-1] -= ret
	bis    _b_, _b_, $20	# recycle la retenue sortante
	bis    _n_, _n_, $2
	bsr    $27, sn_incloop
	bis    $0,  $0,  $7	# r7 <- retenue

	# x <- b*BASE^q mod (BASE^n + 1)
	bne    _q_, 1f		# si q = 0, n effectue pas la copie
	bis    $31, $31, $8	# r8 <- 0 (nb chiffres empil�s)
	bis    _b_, _b_, $18	# r18 <- b = x
	s8addq _n_, _b_, $20
	stq    $7,   0($20)	# sauve b[n]
	br     $31, L(add_sub)
	.align 5
1:
	addq   _n_, 2,   $8	# r8 <- n+1, arrondi au pair sup�rieur
	bic    $8,  1,   $8
	sll    $8,  3,   $0	# r�serve ce nombre de chiffres dans la pile
	subq   $30, $0,  $30
	subq   _n_, _q_, $6	# si q <= n-q, d�cale vers le haut
	cmpule _q_, $6,  $5	# sinon d�cale vers le bas
	bne    $5,  L(shift_up)

	# ici q > n-q, multiplie par BASE^(n-q) et inverse le signe
	xor    _s_, 1,   _s_
	s8addq $6,  _b_, $16	# r16 <- &b[n-q]
	subq   $31, _q_, $2
	and    $2,  31,  $3	# r3 <- 8*((-q) mod 32)
	sll    $3,  3,   $3
	lda    $27, sn_cpuploop
	subq   $16, $3,  $16	# cadre les pointeurs
	subq   $30, $3,  $20
	addq   $27, $3,  $27
	bic    $2,  31,  $2	# r2 <- -32*ceil(q mod 32)
	jsr    $27, ($27)	# x[0..q-1] <- b[n-q..n-1]
	bis    _b_, _b_, $16	# x[q] <- b[n] - b[0]
	ldq    $1,  0($16)
	subq   $7,  $1,  $1
	stq    $1,  0($20)
	cmpult $7,  $1,  $0
	br     $31, 2f
	.align 5
1:
	ldq    $1,  0($16)	# retranche b[1..q-1]
	addq   $1,  $0,  $1
	cmpult $1,  $0,  $0
	subq   $31, $1,  $1
	stq    $1,  0($20)
	cmpult $31, $1,  $1
	addq   $1,  $0,  $0
2:
	lda    $6, -1($6)
	lda    $16, 8($16)
	lda    $20, 8($20)
	bne    $6,  1b
	stq    $31, 0($20)	# x[n] <- 0
	subq   $31, 1,   $2
	bis    $30, $30, $20
	bsr    $27, sn_incloop	# propage la retenue
	bis    $30, $30, $18	# r18 <- &x
	br     $31, L(add_sub)

	# ici q >= n-q, multiplie par BASE^q	
	.align 5
L(shift_up):
	s8addq _q_, $30, $20	# r20 <- &x[q]
	subq   _q_, _n_, $2
	and    $2,  31,  $3	# r3 <- 8*((q-n) mod 32)
	sll    $3,  3,   $3
	lda    $27, sn_cpuploop
	subq   _b_, $3,  $16	# cadre les pointeurs
	subq   $20, $3,  $20
	addq   $27, $3,  $27
	bic    $2,  31,  $2	# r2 <- -32*ceil((n-q) mod 32)
	jsr    $27, ($27)	# x[q..n-1] <- b[0..n-q-1]
	addq   $31, 1,   $0
	stq    $0,  0($20)	# x[n] <- 1
	bis    $30, $30, $20	# r20 <- &x[0]
	beq    _q_, 3f
	ldq    $1,  0($16)
	subq   $0,  $1,  $1	# x[0] <- 1 - b[q]
	cmpult $0,  $1,  $0
	stq    $1,  0($20)
	br     $31, 2f		# retranche b[1..q-1]
	.align 5
1:
	ldq    $1,  0($16)
	addq   $1,  $0,  $1
	cmpult $1,  $0,  $0
	subq   $31, $1,  $1
	stq    $1,  0($20)
	cmpult $31, $1,  $1
	addq   $1,  $0,  $0
2:
	lda    _q_, -1(_q_)
	lda    $16, 8($16)
	lda    $20, 8($20)
	bne    _q_, 1b
	addq   $0,  $7,  $0	# ret += b[n]
	subq   $31, 1,   $2
	bsr    $27, sn_decloop	# propage la retenue
	bis    $30, $30, $18	# r18 <- &x

	# a <- a - (-1)^s*x, b <- a + (-1)^s*x
L(add_sub):
	bis    _a_, _a_, $16	# r16 <- &a
	bis    _b_, _b_, $20	# r20 <- &b
	addq   _n_, 1,   $2
	subq   $31, $2,  $2
	and    $2,  31,  $3	# r3 <- (-n-1) mod 32
	bic    $2,  31,  $2	# r2 <- -32*ceil((n+1)/32)
	sll    $3,  3,   $0
	subq   $16, $0,  $16	# cadre les pointeurs
	subq   $18, $0,  $18
	subq   $20, $0,  $20
	lda    $27, sn_addsubloop
	subq   $0,  $3,  $0
	s8addq $0,  $27, $27	# r27 <- adresse de saut dans addsubloop
	cmoveq _s_, $16, $21	# r21 <- &a si s = 0
	cmovne _s_, $20, $21	# r21 <- &a si s = 1
	cmovne _s_, $16, $20	# r20 <- &b si s = 1
	bis    $31, $31, $0	# init retenues
	bis    $31, $31, $1
	jsr    $27, ($27)		# effectue l addition-soustraction

	# termin�
	s8addq $8,  $30, $30	# nettoie la pile
	ret    $31, ($26),1
	
	#undef _a_
	#undef _b_
	#undef _n_
	#undef _q_
	#undef _s_
	
#ifdef debug_butterfly
        .end sn_butterfly_buggy
#else
        .end sn_butterfly
#endif
#undef L
#endif /* assembly_sn_butterfly */
