// file kernel/n/x86-64/cmp.S: comparison of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Comparaison                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/


# long xn(cmp)(chiffre *a, long la, chiffre *b, long lb)
#
# entr�e :
# a = naturel de longueur la
# b = naturel de longueur lb
#
# sortie :
## 1 si a > b, 0 si a = b, -1 si a < b

#ifdef assembly_sn_cmp
#undef L
#define L(x) .Lsn_cmp_##x

QUICKENTER(sn_cmp)
        

        # supprime les z�ros de t�te du plus long argument
1:
        cmpq   %rsi,    %rcx
        je     3f
        ja     2f

        testq  $-1,   -8(%rdi,%rsi,8)
        jne    L(big)
        decq   %rsi
        jmp    1b

2:
        testq  $-1,   -8(%rdx,%rcx,8)
        jne    L(small)
        decq   %rcx
        jmp    1b

3:
        # ici, la == lb, compare les chiffres
        jrcxz  L(equal)
4:
        movq -8(%rdi,%rcx,8), %rax
        cmpq -8(%rdx,%rcx,8), %rax
        ja     L(big)
        jb     L(small)
        loop   4b

L(equal):
        movq   $0,      %rax
        ret
        
L(small):
        movq   $-1,     %rax
        ret
        
L(big):
        movq   $1,      %rax
        ret
        
                
#endif /* assembly_sn_cmp */
