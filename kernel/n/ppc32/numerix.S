// file kernel/n/ppc32/numerix.S: assembly code for mode = SLONG
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                 Fonctions assembleur pour le mode SLONG               |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* fichier de configuration sp�cifique � la machine cible */
#include "../../config.h"

/* d�boguage */
#include "dumpreg.S"
#define TRACE(msg) \
	stmw  r0,  -128(r1)    @\
	mfxer r31	       @\
	mflr  r30	       @\
	mfctr r29	       @\
	mfcr  r28	       @\
	stmw  r28, -144(r1)    @\
	stwu  r1,  -200(r1)    @\
	bcl  20,31,9f	       @\
	.asciz msg	       @\
	.align 2               @\
9:			       @\
	mflr  r3	       @\
	bl    _printf$LDBLStub @\
	lwz   r1,     0(r1)    @\
	lmw   r28, -144(r1)    @\
	mtxer r31	       @\
	mtlr  r30	       @\
	mtctr r29	       @\
	mtcr  r28	       @\
	lwz   r0,  -128(r1)    @\
	lmw   r2,  -120(r1)

/* remplacement d une fonction d�sactiv�e */
#define REPLACE(nom) \
L##nom:                   @\
	mflr r0		  @\
	stwu r1, -48(r1)  @\
	stw  r0,  44(r1)  @\
	bl   _##nom	  @\
	lwz  r0,  44(r1)  @\
	mtlr r0		  @\
	addi r1, r1, 48   @\
	blr

/* addition/soustraction */
#include "add.S"

/* multiplication/carr� */
#include "mul_n2.S"
#include "karatsuba.S"
#include "toom.S"

/* division/racine carr�e */
#include "div_n2.S"
#include "sqrt_n2.S"
#include "burnikel.S"

/* op�rations modulo BASE^n +/- 1 */
#include "mmod.S"
#include "smod.S"
	
/* exponentiation modulaire */
#include "montgomery.S"
        
/* pgcd */
#include "gcd_n2.S"

/* divers */
#include "cmp.S"
#include "shift.S"
