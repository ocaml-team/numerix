// file kernel/n/c/karp.c: Karp-Markstein division
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Division de Karp et Markstein                     |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                         /* +-------------+
                            |  Inversion  |
                            +-------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la+1

  contraintes :
  la >= 2, le bit de poids fort de a vaut 1

  sortie :
  d <- approx(BASE^(2*la)/a) avec a*(d-2) < BASE^(2la) <= a*d
*/

void xn(karpinv)(chiffre *a, long la, chiffre *b) {
  long p,q;
  chiffre *x,*y,*z;

  /* si a est petit, division modulaire sans reste */
  if (la <= karpinv_lim) {
    x = xn(alloc)(2*la+1);
    xn(clear)(x,2*la); x[2*la] = 1;
    xn(moddiv)(x,la+1,a,la,b,0);
  }

  /* sinon, d�coupe a en 2 */
  else {
    q = la/2 + 2; p = la-q;
    xn(check_lmax)(la+p+2,0); /* v�rifie que la+p+2 < LMAX */
    x = xn(alloc)(3*la+q+3); y = x + q+1; z = y + la+p+2;

    /* inverse la partie haute */
    xn(karpinv)(a+p,q,x);

    /* premi�re tranche de p+2 chiffres, avec reste */
    xn(move)(x+q-p-1,p+2,b+q-1);
    if (x[q-p-3] > (chiffre)(BASE_2)) xn(inc1)(b+q-p-1,p+2);
    xn(clear)(y,la+p+1); y[la+p+1] = 1;
    xn(remdiv)(y,p+2,a,la,b+q-1);

    /* deuxi�me tranche de q-1 chiffres, sans reste */
    xn(fftmul)(x,q+1,y+p,q,z);
    if (z[q] > (chiffre)(BASE_2)) xn(inc1)(z+q+1,q);
    if (z[2*q] == 0) xn(move)(z+q+1,q-1,b); else xn(fill)(b,q-1);
  }

  /* termin� */
  xn(inc1)(b,la+1);
  xn(free)(x);

}

                /* +-------------------------------+
                   |  Division avec et sans reste  |
                   +-------------------------------+ */

/*
  entr�e :
  a = naturel de longueur lb+lc
  b = naturel de longueur lb
  c = naturel de longueur lc
  rem = 0 ou 1 ou 2

  contraintes :
  lc > 0, lb >= 2
  a < BASE^lc*b
  le bit de poids fort de b vaut 1

  sortie si rem = 0 :
    a <- ind.
    c <- approx(a/b) avec -b < a - b*c < b
  sortie si rem = 1 :
    a <- a mod b
    c <- floor(a/b)
  sortie si rem = 2 :
    a <- ind.
    c <- approx(a/b) avec -b < a - b*c < b et c = floor(a/b) si c[0] = 0
*/

#ifndef assembly_sn_karpdiv
#ifdef debug_karpdiv
void xn(karpdiv)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem);
void xn(karpdiv_buggy)
#else
void xn(karpdiv)
#endif
(chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem) {
  long p,q,r,s;
  chiffre *x,*y,*z,*t;

  /* petite division -> moddiv */
  if ((lc <= karpdiv_lim_2)                     ||
      (lb <= karpdiv_lim_1)                     ||
      ((2*lc <= 3*lb) && (lb <= karpdiv_lim_2))   ) {
    xn(moddiv)(a,lc,b,lb,c,rem);
    return;
  }

  if (lc < lb-1) { /* petit quotient */

    /* abandonne p chiffres pour la division sans reste */
    p = lb-lc-1;
    t = a+p; b += p; lb -= p;

    /* a/BASE^(p+lc) >= b/BASE^p => c = BASE^lc - 1 */
    if (xn(cmp)(t+lc,lc+1,b,lc+1) == 0) {
      xn(fill)(c,lc);
      if (rem) xn(remdiv)(a,lc,b-p,lb+p,c);
      return;
    }

    /* sauve la taille du quotient pour le dernier reste */
    s = (rem) ? lc : 0;

  }
  else { /* grand quotient */
    p = 0;
    s = 0;
    t = a;
  }

  /* divise b en 2 et copie a si on en a besoin pour le dernier reste */
  q = (lb+1)/2; if (lc < q-1) q = lc+1;
  if (s) {
    x = xn(alloc)(3*q+2+lb+lc); y = x + q+1; z = y + q+1;
    xn(move)(t,lb+lc,z+q); t = z + q;
  } else {
    x = xn(alloc)(3*q+2); y = x + q+1; z = y + q+1;
  }

  /* calcule l'inverse des q chiffres de poids fort de b */
  xn(karpinv)(b+lb-q,q,x);

  /* r <- nb de chiffres du quotient pour la premi�re division */
  r = lc%(q-1); if (r == 0) r = q-1;

  /* division par tranches de q-1 chiffres */
  lc -= r; t += lc; c += lc;
  while(lc) {

    /* t = t0 + BASE^lb-1*t1, y <- t1*x */
    xn(fftmul)(x,q+1,t+lb-1,r+1,y);
    if (y[q] > (chiffre)(BASE_2)) xn(inc1)(z,r+1);

    /* c <- max(BASE^r - 1, round(y/BASE^(q+1))) */
    if (z[r] == 0) xn(move)(z,r,c); else xn(fill)(c,r);

    /* calcule le reste */
    xn(remdiv)(t,r,b,lb,c);

    /* tranche suivante */
    t -= q-1; c -= q-1; lc -= q-1; r = q-1;
  }

  /* derni�re tranche, sans reste */
  xn(fftmul)(x,q+1,t+lb-1,r+1,y);
  if (y[q] > (chiffre)(BASE_2)) xn(inc1)(z,r+1);
  if (z[r] == 0) xn(move)(z,r,c); else xn(fill)(c,r);

  xn(free)(x);

  /* calcule le reste si demand� */
  if ((rem==1) || ((rem==2) && (c[0]==0))) xn(remdiv)(a,(s) ? s : r,b-p,lb+p,c);

}

#endif /* assembly_sn_karpdiv */

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#ifdef debug_karpdiv
void xn(karpdiv_buggy)  (chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem);
void xn(karpdiv)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem) {
  long la = lc+lb;
  chiffre *x,*y;
  int ok;

  /* validit� des longueurs ? */
 if ((lb < 2) || (la < lb))
      xn(internal_error)("error, karpdiv is called with lb < 2 or la < lb",0);

  /* le bit de poids fort de b est non nul ? */
  if (b[lb-1] < (chiffre)(BASE_2))
      xn(internal_error)("error, karpdiv is called with msb(b) = 0",0);

  /* a < BASE^lc*b ? */
  if (xn(cmp)(a+lc,lb,b,lb) >= 0)
      xn(internal_error)("error, karpdiv is called with a >= BASE^lc*b",2,a,la,b,lb);

  /* effectue la division douteuse */
  x = xn(alloc)(2*la); y = x+la;
  xn(move)(a,la,x);
  xn(karpdiv_buggy)(a,lc,b,lb,c,rem);

  /* calcule a - b*c */
  if (lc < lb) xn(fftmul)(b,lb,c,lc,y); else xn(fftmul)(c,lc,b,lb,y);
  if (xn(sub)(x,la,y,la,y)) {

    /* n�gatif : v�rifie que a - bc + b > 0 */
    ok = ((rem != 1) && (xn(inc)(y,la,b,lb)) && (xn(cmp)(y,la,y,0)));
  }
  else {

    /* positif ou nul : v�rifie que a - bc < b */
    ok = (xn(cmp)(y,la,b,lb) < 0);

    /* si rem = 1, v�rifie que a_sortie = a_entr�e - b*c */
    if ((ok) && (rem == 1)) ok = (xn(cmp)(a,lb,y,lb) == 0);
  }

  if (!ok) {
    if (rem == 1) xn(internal_error)("error in karpdiv", 4,x,la,b,lb,c,lc,a,lb);
    else          xn(internal_error)("error in karpdiv", 3,x,la,b,lb,c,lc);
  }
  xn(free)(x);

}
#endif /* debug_karpdiv */

