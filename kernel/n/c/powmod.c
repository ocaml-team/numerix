// file kernel/n/c/powmod.c: modular exponentiation
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                      Exponentiation modulaire                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                         /* +------------+
                            |  Contr�le  |
                            +------------+ */

/*
  Algorithme ternaire pour contr�le : on d�compose b en base 3 et on commence
  par les poids faibles
*/
/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur lc
  d = naturel de longueur lc

  contraintes :
  lb > 0, lc > 0, b[lb-1] > 0, c[lc-1] > 0
  d non confondu avec a,b,c

  sortie :
  d <- a^b mod c
*/

#ifdef debug_powmod
void xn(powmod_3)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, long lc, chiffre *d) {

  if (!la) { /* ---------------------------------------- cas a = 0 */
    xn(clear)(d,lc);
    return;
  }

  else if (lc == 1) { /* ------------------------------ cas lc = 1 */
    chiffre a0,a1,*x,u,v,w;
    long l;

    a0 = xn(mod_1)(a,la,c[0]);

    /* recopie b pour les divisions par 3 */
    x = xn(alloc)((lb < lc) ? lc : lb);
    xn(move)(b,lb,x);

    /* exponentiation trichotomique */
    for(d[0]=1, l=lb; l;) {
      switch(xn(div_1)(x,l,3,x)) {
      case 2: xn(mul_0)(a0,d[0],&u,&v); xn(div_0)(u,v,c[0],&w,d);
      case 1: xn(mul_0)(a0,d[0],&u,&v); xn(div_0)(u,v,c[0],&w,d);
      }
      xn(sqr_0)(a0,   &u,&v); xn(div_0)(u,v,c[0],&w,&a1);
      xn(mul_0)(a0,a1,&u,&v); xn(div_0)(u,v,c[0],&w,&a0);
      while ((l) && (!x[l-1])) l--;
    }
    xn(free)(x);
    return;
  }

  else { /* ---------------------------------------- cas lc > 1 */
    chiffre *aa,*bb,*cc,*x,*y,r;
    long l,n;

    /* m�moire n�cessaire : lc pour aa, lb pour bb, 3*lc pour les calculs */
    l  = lb + 4*lc;
    if ((la >= lc) && (l < 2*la-lc+2)) l = 2*la-lc+2;

    /* d�calage � appliquer � c pour avoir msb = 1 */
    for (n=0, r=c[lc-1]; (r & (BASE_2)) == 0; n++, r <<= 1);
    if (n) l += lc;

    x = xn(alloc)(l); y = x;

    /* cc <- c*2^n */
    if (n) {
      xn(shift_up)(c,lc,y,n);
      cc = y;
      y += lc;
    }
    else cc = c;

    /* aa <- a mod c */
    if (la >= lc) {
      y[la] = xn(shift_up)(a,la,y,n);
      xn(karpdiv)(y,la+1-lc,cc,lc,y+la+1,1);
      if (n) xn(shift_down)(y,lc,y,n);
    }
    else {
      xn(move)(a,la,y);
      xn(clear)(y+la,lc-la);
    }
    aa = y; y += lc;

    /* copie b */
    xn(move)(b,lb,y);
    bb = y; y += lb;

    /* d <- 1 */
    d[0] = 1; xn(clear)(d+1,lc-1);

    /* exponentiation trichotomique */
    for (l = lb; l; ) {
      switch(xn(div_1)(bb,l,3,bb)) {
      case 1: xn(fftmul)(aa,lc,d,lc,y);
              xn(karpdiv)(y,lc,cc,lc,d,1);
              xn(move)(y,lc,d);

      case 0: xn(fftsqr)(aa,lc,y);
              xn(karpdiv)(y,lc,cc,lc,y+2*lc,1);
              break;

      case 2: xn(fftsqr)(aa,lc,y);
              xn(karpdiv)(y,lc,cc,lc,y+2*lc,1);
              xn(fftmul)(y,lc,d,lc,y+lc);
              xn(karpdiv)(y+lc,lc,cc,lc,d,1);
              xn(move)(y+lc,lc,d);
      }
      xn(fftmul)(aa,lc,y,lc,y+lc);
      xn(karpdiv)(y+lc,lc,cc,lc,y,1);
      xn(move)(y+lc,lc,aa);

      while ((l) && (!bb[l-1])) l--;
    }

    /* r�duit d mod c */
    if (n) {
      y[lc] = xn(shift_up)(d,lc,y,n);
      xn(div_n2)(y,1,cc,lc,y+lc+1);
      xn(shift_down)(y,lc,d,n);
    }

    xn(free)(x);
    return;

  }

}
#endif /* debug_powmod */



                      /* +----------------------------+
                         |  Exponentiation modulaire  |
                         +----------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur lc
  d = naturel de longueur lc

  contraintes :
  lb > 0, lc > 0, b[lb-1] > 0, c[lc-1] > 0
  d non confondu avec a,b,c

  sortie :
  d <- a^b mod c
*/

#ifdef debug_powmod
void xn(powmod_buggy)
#else
void xn(powmod)
#endif
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, long lc, chiffre *d) {
  chiffre u,v,*x,*y,*z,*t,r;
  long i,j,l,n;

  /* Algorithme de la fen�tre coulissante sur 4 bits.
     On pr�calcule les puissances impaires de a modulo c de a^1 jusqu'� a^15.
     Lorsqu'on rencontre un bit de b valant 1, on exmaine les trois bits
     suivants pour d�terminer le nombre d'�l�vations au carr� � effectuer
     (tableau itab)  et par quelle puissance de a il faut multiplier le
     r�sidu obtenu (tableau jtab).

                        0 1 2 3 4 5 6 7  8  9  10  11  12  13  14  15 */
  static long itab[] = {1,1,1,1,1,1,1,1, 1, 4,  3,  4,  2,  4,  3,  4};
  static long jtab[] = {0,0,0,0,0,0,0,0, 1, 9,  5, 11,  3, 13,  7, 15};


  if (!la) {xn(clear)(d,lc); return;} /*---------- cas a = 0 */

  /* cherche le premier bit de b */
  for (i=HW-1, r = b[--lb]; (r & (BASE_2)) == 0; r <<= 1, i--);

  if (lc == 1) { /* ------------------------------ cas lc = 1 */
#ifdef ndouble
    ndouble s,u,v;

    s = (ndouble)c[0];
    u = xn(mod_1)(a,la,s);

    for(v = u, r <<= 1; i+lb; r <<= 1, i--) {
      v = (v*v) % s;
      if (!i) {r = b[--lb]; i = HW;}
      if (r & (BASE_2)) v = (u*v) % s;
    }
    d[0] = v;
    return;
#else
    chiffre x,y,z;

    u = xn(mod_1)(a,la,c[0]);

    for(v = u, r <<= 1; i+lb; r <<= 1, i--) {
      xn(sqr_0)(v,&x,&y); xn(div_0)(x,y,c[0],&z,&v);
      if (!i) {r = b[--lb]; i = HW;}
      if (r & (BASE_2)) {xn(mul_0)(u,v,&x,&y); xn(div_0)(x,y,c[0],&z,&v);}
    }
    d[0] = v;
    return;
#endif /* ndouble */
  }

  /* d�calage � appliquer � c pour avoir msb = 1 */
  for (n=0, j=c[lc-1]; (j & (BASE_2)) == 0; n++, j <<= 1);
  xn(check_lmax)(lc,1);

  if (la == 1) { /* ----------------- cas lc > 1, la = 1 */

    /* m�moire n�cessaire :
        copie de c d�cal�e -> lc si n > 0,  0 sinon
        tampon de calcul   -> 4*lc+2
    */
    if (n) {
      x = xn(alloc)(5*lc+2);
      xn(shift_up)(c,lc,x,n);
      c = x;
      y = c + lc;
    }
    else {x = xn(alloc)(4*lc+2); y = x;}
    z = y + 2*lc + 1;

    /* exponentiation dichotomique classique */
    y[0] = a[0]; xn(clear)(y+1,lc-1);
    for (r <<= 1; i+lb; r <<= 1, i--) {
      xn(sqr)(y,lc,z);
      if (!i) {r = b[--lb]; i = HW;}
      if (r & (BASE_2)) {z[2*lc] = xn(mul_1)(z,2*lc,a[0],z); l=lc+1;} else l=lc;
      xn(div)(z,l,c,lc,y);
      t=y; y=z; z=t;
    }

  }

  else { /* ------------------------------ cas lc > 1, la > 1 */

    /* m�moire n�cessaire :
        copie de c d�cal�e          -> lc si n > 0,           0 sinon
        tampon de calcul            -> 4*lc
        tampon pour diviser a par c -> 2*la-lc+2 si la >= lc, 0 sinon
        copie de a,a^3..a^15        -> 8*lc
    */

    l = 12*lc; if (2*la-lc+2 > l) l = 2*la-lc+2;
    if (n) {
      x = xn(alloc)(l+lc);
      xn(shift_up)(c,lc,x,n);
      c = x;
      y = c + lc;
    }
    else {x = xn(alloc)(l); y = x;}

    /* a <- a mod c*2^n */
    xn(move)(a,la,y); a = y; y += lc;
    if (la < lc) xn(clear)(a+la,lc-la);
    else {a[la] = 0; xn(div) (a,la-lc+1,c,lc,a+la+1);}

    /* d <- a^2 mod c*2^n */
    xn(sqr)(a,lc,y);
    xn(div)(y,lc,c,lc,y+2*lc);
    xn(move)(y,lc,d);

    /* calcule a^3..a^15 mod c*2^n */
    for (j=0; j<7; j++, y+=lc) {
      xn(mul)(d,lc,y-lc,lc,y);
      xn(div)(y,lc,c,lc,y+2*lc);
    }

    /* uv <- les HW+4 premiers bits de b */
    l = i+1; u = r << 4; v = r >> (HW-4);
    if ((l < 4) && (lb)) {
      u = b[--lb];
      v += u >> (HW+l-4); u <<= 4-l;
      l += HW;
    }

    /* initialise y avec l'une des puissances calcul�es */
    i=itab[v]; j=jtab[v];
    if ((i > 1) || (l == 1)) {
      xn(move)(a + (j/2)*lc,lc,y);
      v = (v << i) + (u >> (HW-i)); u <<= i;
      l -= i;
    }
    else {
      xn(move)(d,lc,y);
      v = (v << 2) + (u >> (HW-2)); u <<= 2;
      l -= 2;
    }
    z = y + 2*lc;

    /* exponentiation dichotomique avec fen�tre coulissante */
    while(l+lb) {

      /* recharge uv s'il reste moins de 4 bits */
      if ((l < 4) && (lb)) {
	u = b[--lb];
	v += u >> (HW+l-4); u <<= 4-l;
	l += HW;
      }
      i=itab[v&15]; j=jtab[v&15];
      v = (v << i) + (u >> (HW-i)); u <<= i;
      l -= i;

      /* y <- y^(2^i)*a^j */
      for(; i; i--) {
        xn(sqr)(y,lc,z);
        xn(div)(z,lc,c,lc,y);
        t=y; y=z; z=t;
      }
      if (j) {
        xn(mul)(y,lc, a+(j/2)*lc,lc, z);
        xn(div)(z,lc,c,lc,y);
        t=y; y=z; z=t;
      }
    }
  }

  /* divise le dernier reste par c */
  if (n) {
    y[lc] = xn(shift_up)(y,lc,y,n);
    xn(div_n2)(y,1,c,lc,z);
    xn(shift_down)(y,lc,d,n);
  }
  else xn(move)(y,lc,d);
  xn(free)(x);

}

/* contr�le */
#ifdef debug_powmod
void xn(powmod)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, long lc, chiffre *d) {
  chiffre *x;

  x = xn(alloc)(lc);
  xn(powmod_buggy)(a,la,b,lb,c,lc,x);
  xn(powmod_3)    (a,la,b,lb,c,lc,d);
  if (xn(cmp)(d,lc,x,lc))
      xn(internal_error)("error in powmod",5,a,la,b,lb,c,lc,x,lc,d,lc);

  xn(free)(x);

}
#endif /* debug_powmod */

