// file kernel/n/c/pow.c: exponentiation
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Exponentiation                           |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                           /* +-------------------+
                              |  Puissance p-�me  |
                              +-------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb

  contraintes :
  p > 0, la > 0, a[la-1] > 0, (lb-1)*HW >= p*nbits(a)
  b non confondu avec a

  sortie :
  b <- a^p
  retourne la longueur effective de b
*/

#ifndef assembly_sn_pow
long xn(pow)(chiffre *a, long la, chiffre *b, long p) {
  chiffre *x,*y;
  long i,n,l;

  x = xn(alloc)(p*la);

  /* compte le nombre d'op�rations pour finir dans b */
  for (i=0, n=1; p >= 2*n; n <<= 1) if (!(p&n)) i++;
  if (i&1) {y = x; x = b; b = y;}

  /* exponentiation dichotomique */
  xn(move)(a,la,b); l = la;
  for (n >>= 1; n; n >>= 1) {
    xn(fftsqr)(b,l,x); l <<= 1; while (!x[l-1]) l--;
    if (p&n) {xn(fftmul)(x,l,a,la,b); l += la;  while (!b[l-1]) l--;}
    else     {y = x; x = b; b = y;}
  }

  xn(free)(x);
  return(l);
}
#endif /* assembly_sn_pow */

                            /* +----------------+
                               |  Racine p-�me  |
                               +----------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur ceil(la/p)

  contraintes :
  p >= 2, la > 0, a[la-1] > 0
  b non confondu avec a

  sortie :
  b <- floor(a^(1/p))
  retourne 1 si a^(1/p) est entier, 0 sinon
*/

#ifndef assembly_sn_root
#ifdef debug_root
long xn(root_buggy)
#else
long xn(root)
#endif
(chiffre *a, long la, chiffre *b, long p) {
  chiffre c,*x,*y,*z;
  long lb,lx,ly,lz,r,n,u,v,w,exact;

  lb = (la+p-1)/p;   /* longueur r�sultat */
  exact = 0;         /* � priori, racine non enti�re */

  /*
    Algorithme de Newton : on choisit b_0 >= floor(a^(1/p)) et on calcule
    de proche en proche b_{n+1} = b_n - (b_n^p - a)/(p*b_n^(p-1))
    Le r�sultat est le premier b_n tel que b_n^p <= a.
  */

  /* m�moire temporaire :
     x = p*b^(p-1) -> (p-1)*lb + 2 chiffresw = v & (HW-1);
     y = BASE*b^p  -> p*lb + 2     chiffres
     z = (y-a)/x   -> lb + 2       chiffres
  */
  x = xn(alloc)(2*p*lb+6); y = x + (p-1)*lb + 2; z = y + p*lb + 2;

  /*
    Choix de b_0 : soit u = floor(nbits(a)/(2p)) ~ nbits(b)/2.
    Si u <= HW on prend b_0 = 2^(ceil(nbits(a)/p)) - 1
    Sinon on calcule r�cursivement la racine p-�me de a/2^(p*u) et on prend
    b_0 = 2^u*(cette racine) + 2^u - 1.
  */
  for (n=la*HW, c=a[la-1]; (c & (BASE_2)) == 0; c <<= 1, n--);
  v = n/p; u = v/2;
  if (u <= HW) {

      /* b <- 2^ceil(n/p) - 1 */
      if (n%p) v++;
      w = v & (HW-1); v /= HW;
      xn(fill)(b,v);
      b[v] = ((chiffre)1<<w) - 1;
      if (v < lb-1) xn(clear)(b,lb-v-1);
  }
  else {

      /* x <- a/2^(p*u) */
      v = p*u; w = v & (HW-1); v /= HW;
      c = xn(shift_down)(a+v,la-v,x,w);
      for (lx = la-v; x[lx-1] == 0; lx--);

      /* b <- 2^u*floor(x^(1/p)) + 2^u - 1 */
      w = u & (HW-1); v = u/HW;
      xn(fill)(b,v);
      xn(root)(x,lx,b+v,p);
      lx = (lx + p-1)/p;
      xn(clear)(b+v+lx,lb-v-lx);
      xn(inc1)(b+v,lb-v);
      xn(shift_up)(b+v,lb-v,b+v,w);
      xn(dec1)(b+v,lb-v);
  }

  while(1) {

      /* x <- p*b^(p-1), y <- BASE*(b^p - a). Quitte la boucle si b^p <= a */
      lx = xn(pow)(b,lb,x,p-1);
      y[0] = 0; xn(fftmul)(x,lx,b,lb,y+1);
      for (ly = lb+lx; y[ly] == 0; ly--); if (ly < la) break;
      r = xn(mul_1)(x,lx,p,x);
#if chiffres_per_long == 2
      while (r) {x[lx++] = r; r >>= HW;}
#else
      if (r) x[lx++] = r;
#endif
      c = xn(dec)(y+1,ly,a,la); if (c) break;
      for (; (ly) && (!y[ly]); ly--); if (!ly) {exact = 1; break;}

      /* b <- floor(b - y/x) = b - floor((y-1)/x) - 1
         On calcule floor((y-1)/x) par division sans reste avec
         un chiffre apr�s la virgule
      */
      xn(dec1)(y+1,ly); for (; (ly) && (!y[ly]); ly--);

      if (lx == 1) {
          xn(div_1)(y+1,ly,x[0],z+1);
          for (lz = ly; (lz) && (!z[lz]); lz--);
          xn(dec)(b,lb,z+1,lz);
      }
      else if (ly >= lx) {

          /* d�cale x et y pour avoir msb(x) = 1 */
          for (r=0, c=x[lx-1]; (c&(BASE_2)) == 0; r++, c <<= 1);
          if (r) {
              xn(shift_up)(x,lx,x,r);
              c = xn(shift_up)(y+1,ly,y+1,r); if (c) y[++ly] = c;
          }

          /* prolonge y si la division risque de d�border */
          if (y[ly] >= x[lx-1]) y[++ly] = 0;

          /* z <- approx(BASE*y/x), b <- b - floor(z/BASE) */
          lz = ly-lx+1;
          xn(karpdiv)(y,lz,x,lx,z,2);
          for (lz--; (lz) && (!z[lz]); lz--);
          xn(dec)(b,lb,z+1,lz);
      }

      xn(dec1)(b,lb);

  } /* while(1) */

  xn(free)(x);
  return(exact);

}
#endif /* assembly_sn_root */

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#ifdef debug_root
long xn(root_buggy)(chiffre *a, long la, chiffre *b, long p);

long xn(root)(chiffre *a, long la, chiffre *b, long p) {
  long lb = (la+p-1)/p, lx, exact, ok;
  chiffre *x;

  exact = xn(root_buggy)(a,la,b,p); while ((lb) && (!b[lb-1])) lb--;

  x = xn(alloc)(p*lb);
  lx = xn(pow)(b,lb,x,p);
  switch(xn(cmp)(x,lx,a,la)) {
      case 1:  ok = 0;     break;
      case 0:  ok = exact; break;
      default: ok = (!exact);
               if (!xn(inc1)(b,lb)) {
                   lx = xn(pow)(b,lb,x,p);
                   ok &= (xn(cmp)(x,lx,a,la) == 1);
               }
               xn(dec1)(b,lb);
  }

  if (!ok) xn(internal_error)("error in root",2,a,la,b,lb);

  xn(free)(x);
  return(exact);

}
#endif /* debug_root */
