// file kernel/n/c/alloc.c: temporary storage allocator
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                          Allocation de m�moire                        |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#ifdef debug_alloc

/* compteur d'allocation */
static long nalloc = 0;
long xn(get_alloc_count)() {return(nalloc);}

/* marques pour tester les d�bordements */
#if HW == 16
#define magic_1 31415
#define magic_2 27182
#elif HW == 32
#define magic_1 3141592653UL
#define magic_2 2718281828UL
#elif HW == 64
#define magic_1 3141592653589793238UL
#define magic_2 2718281828459045235UL
#endif


/* alloue un bloc de n chiffres */
chiffre *xn(alloc)(unsigned long n) {
    chiffre *p;

    if (n >= LMAX) xn(internal_error)("alloc request >= LMAX",0);

    p = (chiffre *)malloc((n+chiffres_per_long+2)*sizeof(chiffre));
    if (p == NULL) xn(internal_error)("out of memory",0);

    /* stocke la longueur demand�e et une marque � chaque extr�mit� */
    *(p++) = n;
#if chiffres_per_long == 2
    *(p++) = n >> HW;
#endif
    *(p++) = magic_1;
    p[n]   = magic_2;

    nalloc++;
    return(p);
}

/* lib�ration */
void xn(free)(chiffre *x) {
    long n;

    if (x == NULL) return;
    if (nalloc) nalloc--;
    else xn(internal_error)("unexpected call to xn(free)",0);

    if (*(--x) == magic_1) {
        n = *(--x);
#if chiffres_per_long == 2
        n = (n << HW) + *(--x);
#endif
        if (x[n+1+chiffres_per_long] == magic_2) {free(x); return;}
    }

    xn(internal_error)("buffer overflow (n layer)",0);
}

#endif
