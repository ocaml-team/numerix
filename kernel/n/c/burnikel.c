// file kernel/n/c/burnikel.c: Burnikel-Ziegler division
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Division de Burnikel et Ziegler                   |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                              /* +------------+
                                 |  Division  |
                                 +------------+ */

/*
  entr�e :
  a = naturel de longueur lc+lb
  b = naturel de longueur lb
  c = naturel de longueur lc

  contraintes :
  lb >= 2, lc > 0, le bit de poids fort de b est non nul,
  a < BASE^lc*b
  a,b,c non confondus

  sortie :
  a <- a mod b
  c <- floor(a/b)
*/


#ifndef assembly_sn_burnidiv
#ifdef debug_burnidiv
void xn(burnidiv_buggy)
#else
void xn(burnidiv)
#endif
(chiffre *a, long lc, chiffre *b, long lb, chiffre *c) {
  long p,q,r;
  chiffre *x;

  /* petite division => div_n2 */
  if ((lb <= burnidiv_lim) || (lc <= div_small_c_lim)) {xn(div_n2)(a,lc,b,lb,c); return;}

  /* cas r�cursif, d�coupe b en deux et divise par tranches de q chiffres */
  p = lb/2; q = lb-p;
  r = lc%q; if (r == 0) r = q;
  x = xn(alloc_tmp)(lb);

  lc -= r; a += lc; c += lc;
  while(lc>=0) {

    /* divise a1 par b1 en tronquant le quotient � BASE^r - 1 */
    if (xn(cmp)(a+p+r,q,b+p,q)) xn(burnidiv)(a+p,r, b+p,q, c);
    else {
      xn(clear)(a+p+r,q);
      xn(inc)(a+p,q+r,b+p,q);
      xn(fill)(c,r);
    }

    /* a0 + BASE^p*r1 -= c*b0, et corrige si < 0 */
    if (r < p) xn(toommul)(b,p,c,r,x); else xn(toommul)(c,r,b,p,x);
    if (xn(dec)(a,lb+1,x,p+r)) {
      do xn(dec1)(c,r); while (xn(inc)(a,lb+1,b,lb) == 0);
    }

    /* tranche de q chiffres suivante */
    a -= q; c -= q; lc -= q; r = q;

  }

  xn(free_tmp)(x);
}
#endif /* assembly_sn_burnidiv */

                         /* +------------+
                            |  Contr�le  |
                            +------------+ */

#ifdef debug_burnidiv
void xn(burnidiv_buggy)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);
void xn(burnidiv)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c) {
  long la = lc+lb;
  chiffre *x, *y;

  x = xn(alloc_tmp)(2*la); y = x+la;

  /* validit� des longueurs ? */
  if ((lb < 2) || (la < lb))
    xn(internal_error)("error, burnidiv is called with lb < 2 or la < lb",0);

  /* le bit de poids fort de b est non nul ? */
  if ((b[lb-1] & BASE_2) == 0)
    xn(internal_error)("error, burnidiv is called with msb(b) = 0",0);

  /* a < BASE^lc*b ? */
  if (xn(cmp)(a+lc,lb,b,lb) >= 0)
    xn(internal_error)("error, burnidiv is called with a >= BASE^lc*b",2,a,la,b,lb);

  /* effectue la division douteuse */
  xn(move)(a,la,x);
  xn(burnidiv_buggy)(a,lc,b,lb,c);

  /* a_entr�e = a_sortie + b*c et a_sortie < b ?*/
  if (lc < lb) xn(toommul)(b,lb,c,lc,y); else xn(toommul)(c,lc,b,lb,y);
  if (     (xn(inc)(y,la,a,lb) != 0)
        || (xn(cmp)(x,la,y,la) != 0)
        || (xn(cmp)(a,la,b,lb) >= 0)) {
    xn(internal_error)("error in burnidiv", 4,x,la,b,lb,a,la,c,lc);
  }

  xn(free_tmp)(x);

}
#endif /* debug_burnidiv */

