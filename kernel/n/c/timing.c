// file kernel/n/c/timing.c: timing of n-functions
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |           Chronom�trage des fonctions internes de Numerix             |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include <time.h>
#include "../h/numerix.h"

/*
  si just_below est d�fini, les divisions et racines carr�es effectuent
  des tests o� le quotient/radical est l�g�rement inf�rieur � un entier
  pour v�rifier le bon fonctionnement des algorithmes dans ces cas.
*/
#undef just_below

                              /* +------------+
                                 |  Papillon  |
                                 +------------+ */

/* taille impaire, >= 4 */
void xn(size_but)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 4) *la = 4;
  *la |= 1;
  *lb = *la;
  *lc = -(*la);
  *ld = *lc;
}

/* q = 2*HW */
void xn(calc_but1)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(butterfly)(a,b,la-1,2*HW,0);
}

/* q = 2*HW+1 */
void xn(calc_but2)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(butterfly)(a,b,la-1,2*HW+1,0);
}

/* q = 2*HW+2 */
void xn(calc_but3)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(butterfly)(a,b,la-1,2*HW+2,0);
}


                        /* +--------------------------+
                           |  Combinaison de r�sidus  |
                           +--------------------------+ */

void xn(size_join)(long *la, long *lb, long *lc, long *ld) {
  long h,k,n,p,q;

  if (*la < 40) *la = 40;
  k = *la/20;
  h = *la/(6*k)-1;
  n = (2*h+2)*k;
  p = (2*h+1)*k;
  q = (2*h)*k;

  *lb = -n - p - q;
  *lc = 0;
  *ld = 0;
}

void xn(calc_join)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  long h,k,n,p,q;

  k = la/20;
  h = la/(6*k)-1;
  n = (2*h+2)*k;
  p = (2*h+1)*k;
  q = (2*h)*k;

  xn(clear)(b,n+p+q);
  xn(ssub)(a,la,b,n);
  xn(ssub)(a,la,b+n,p);
  xn(ssub)(a,la,b+n+p,q);

  xn(sjoin3)(b,h,k);  
}

                        /* +------------------------+
                           |  Multiplication/Carr�  |
                           +------------------------+ */

/* ---------------------------------------- Tailles */

/* la, lb quelconques, force 0 < lb <= la */
void xn(size_mul)(long *la, long *lb, long *lc, long *ld) {
  long x;
  if (!*la) (*la)++; if (!*lb) (*lb)++;
  if (*la < *lb) {x = *la; *la = *lb; *lb = x;}
  *lc = -(*la + *lb);
  *ld = 0;
}

/* la = 1.5*lb > 0 */
void xn(size_mul_2)(long *la, long *lb, long *lc, long *ld) {
  if (!*lb) (*lb)++;
  *la = (3* *lb + 1)/2;
  *lc = -(*la + *lb);
  *ld = 0;
}

/* pour le carr�, la > 0 */
void xn(size_sqr)(long *la, long *lb, long *lc, long *ld) {
  if (!*la) (*la)++;
  *lb = -2*(*la);
  *lc = 0;
  *ld = 0;
}

/* ---------------------------------------- algorithmes quadratiques */

void xn(calc_mul_n2)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(mul_n2)(a,la,b,lb,c);
}

void xn(calc_sqr_n2)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(sqr_n2)(a,la,b);
}

/* ---------------------------------------- algorithme de Karatsuba */

void xn(calc_karamul)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(karamul)(a,la,b,lb,c);
}

void xn(calc_karasqr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(karasqr)(a,la,b);
}

/* ---------------------------------------- algorithme de Toom */

void xn(calc_toommul)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(toommul)(a,la,b,lb,c);
}

void xn(calc_toomsqr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(toomsqr)(a,la,b);
}

/* ---------------------------------------- mult./carr� par FFT */

void xn(calc_fftmul)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(fftmul)(a,la,b,lb,c);
}

void xn(calc_fftsqr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(fftsqr)(a,la,b);
}

                   /* +-----------------------------------+
                      |  Multiplication/carr� modulaires  |
                      +-----------------------------------+ */

/* ---------------------------------------- mod BASE^n + 1 */

void xn(size_mmul)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 4) *la = 4;
  *lb = *la;
  *lc = -(*la);
  *ld = 0;
}

void xn(size_msqr)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 4) *la = 4;
  *lb = -(*la);
  *lc = 0;
  *ld = 0;
}

void xn(calc_mmul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(mmul)(a,b,la-1);
}

void xn(calc_msqr)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(msqr)(a,la-1);
}

/* ---------------------------------------- mod BASE^n - 1 */

void xn(size_smul)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 2) *la = 2;
  *lb = *la;
  *lc = -(*la);
  *ld = 0;
}

void xn(size_ssqr)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 2) *la = 2;
  *lb = -(*la);
  *lc = 0;
  *ld = 0;
}

void xn(calc_smul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(smul)(a,la,b,la,c,la);
}

void xn(calc_ssqr)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  xn(ssqr)(a,la,b,la);
}

                              /* +------------+
                                 |  Division  |
                                 +------------+ */

/* ---------------------------------------- Tailles */

/* la = 2lb, lb >= 2 */
void xn(size_div)(long *la, long *lb, long *lc, long *ld) {
  if (*lb < 2) *lb = 2;
  *la = 2*(*lb);
  *lc = -(*la - *lb);
  *ld = -(*la);
}

/* la = 1.5lb, lb >= 2 */
void xn(size_div_2)(long *la, long *lb, long *lc, long *ld) {
  if (*lb < 2) *lb = 2;
  *la = (3*(*lb))/2;
  *lc = -(*la - *lb);
  *ld = -(*la);
}

/* la = 3lb, lb >= 2 */
void xn(size_div_3)(long *la, long *lb, long *lc, long *ld) {
  if (*lb < 2) *lb = 2;
  *la = (3*(*lb));
  *lc = -(*la - *lb);
  *ld = -(*la);
}

void xn(size_inv)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 2) *la = 2;
  *lb = -(*la + 1);
  *lc = 0;
  *ld = 0;
}


/* ---------------------------------------- algorithme quadratique */

#ifndef just_below
/* division arbitraire, force a[la-1] < b[lb-1] */
void xn(calc_div_n2)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  
  b[lb-1] |= (BASE_2);
  if (a[la-1] >= b[lb-1]) a[la-1] = b[lb-1] - 1;
  xn(move)(a,la,d);
  xn(div_n2)(d,la-lb,b,lb,c);

}

#else
/* force a = -1 mod b */
void xn(calc_div_n2)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  long l = la-lb;

  b[lb-1] |= (BASE_2);
  if (l < lb) xn(toommul)(b,lb,a,l,d); else xn(toommul)(a,l,b,lb,d);
  xn(dec1)(d,la);
  xn(div_n2)(d,la-lb,b,lb,c);

}
#endif /* just_below */

/* ---------------------------------------- algorithme de Burnikel et Ziegler */

#ifndef just_below
/* division arbitraire, force a[la-1] < b[lb-1] */
void xn(calc_burnidiv)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  
  b[lb-1] |= (BASE_2);
  if (a[la-1] >= b[lb-1]) a[la-1] = b[lb-1] - 1;
  xn(move)(a,la,d);
  xn(burnidiv)(d,la-lb,b,lb,c);

}

#else
/* force a = -1 mod b */
void xn(calc_burnidiv)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  long l = la-lb;

  b[lb-1] |= (BASE_2);
  if (l > 2) xn(clear)(a,l/2);
  if (l < lb) xn(toommul)(b,lb,a,l,d); else xn(toommul)(a,l,b,lb,d);
  xn(dec1)(d,la);
  xn(burnidiv)(d,la-lb,b,lb,c);

}
#endif /* just_below */

/* ---------------------------------------- division modulaire */

void xn(calc_modinv)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  chiffre *x;
  
  a[la-1] |= (BASE_2);
  x = xn(alloc)(2*la+1);
  xn(clear)(x,2*la); x[2*la] = 1;
  xn(moddiv)(x,la+1,a,la,b,0);
  xn(inc1)(b,la+1);
  xn(free)(x);
}

#ifndef just_below
/* division arbitraire, force a[la-1] < b[lb-1] */
void xn(calc_moddivnr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  
  b[lb-1] |= (BASE_2);
  if (a[la-1] >= b[lb-1]) a[la-1] = b[lb-1] - 1;
  xn(move)(a,la,d);
  xn(moddiv)(d,la-lb,b,lb,c,0);

}

void xn(calc_moddivr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  
  b[lb-1] |= (BASE_2);
  if (a[la-1] >= b[lb-1]) a[la-1] = b[lb-1] - 1;
  xn(move)(a,la,d);
  xn(moddiv)(d,la-lb,b,lb,c,1);

}

#else
/* force a = -1 mod b */
void xn(calc_moddivnr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  long l = la-lb;

  b[lb-1] |= (BASE_2);
  if (l > 2) xn(clear)(a,l/2);
  if (l < lb) xn(fftmul)(b,lb,a,l,d); else xn(fftmul)(a,l,b,lb,d);
  xn(dec1)(d,la);
  xn(moddiv)(d,la-lb,b,lb,c,0);

}

void xn(calc_moddivr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  long l = la-lb;

  b[lb-1] |= (BASE_2);
  if (l > 2) xn(clear)(a,l/2);
  if (l < lb) xn(fftmul)(b,lb,a,l,d); else xn(fftmul)(a,l,b,lb,d);
  xn(dec1)(d,la);
  xn(moddiv)(d,la-lb,b,lb,c,1);

}
#endif /* just_below */

/* ---------------------------------------- algorithme de Karp et Markstein */

void xn(calc_karpinv)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  
  a[la-1] |= (BASE_2);
  xn(karpinv)(a,la,b);
}

#ifndef just_below
/* division arbitraire, force a[la-1] < b[lb-1] */
void xn(calc_karpdivnr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  
  b[lb-1] |= (BASE_2);
  if (a[la-1] >= b[lb-1]) a[la-1] = b[lb-1] - 1;
  xn(move)(a,la,d);
  xn(karpdiv)(d,la-lb,b,lb,c,0);
}

void xn(calc_karpdivr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  
  b[lb-1] |= (BASE_2);
  if (a[la-1] >= b[lb-1]) a[la-1] = b[lb-1] - 1;
  xn(move)(a,la,d);
  xn(karpdiv)(d,la-lb,b,lb,c,1);
}

#else
/* force a = -1 mod b */
void xn(calc_karpdivnr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  long l=la-lb;
  
  b[lb-1] |= (BASE_2);
  if (a[la-1] >= b[lb-1]) a[la-1] = b[lb-1] - 1;
  if (l > 2)  xn(clear)(a,l/2);
  if (l < lb) xn(fftmul)(b,lb,a,l,d); else xn(fftmul)(a,l,b,lb,d);
  xn(dec1)(d,la);
  xn(karpdiv)(d,la-lb,b,lb,c,0);
}

void xn(calc_karpdivr)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  long l=la-lb;
  
  b[lb-1] |= (BASE_2);
  if (a[la-1] >= b[lb-1]) a[la-1] = b[lb-1] - 1;
  if (l > 2)  xn(clear)(a,l/2);
  if (l < lb) xn(fftmul)(b,lb,a,l,d); else xn(fftmul)(a,l,b,lb,d);
  xn(dec1)(d,la);
  xn(karpdiv)(d,la-lb,b,lb,c,1);
}

#endif /* just_below */

                            /* +-----------------+
                               |  Racine carr�e  |
                               +-----------------+ */

/* ---------------------------------------- Taille */

void xn(size_sqrt)(long *la, long *lb, long *lc, long *ld) {
  (*la) = 2*(*lb); if (*la < 2) *la = 2;
  *lb = -(*la)/2;
  *lc = -(*la);
  *ld = 0;
}

/* ---------------------------------------- algorithme quadratique */

#ifndef just_below
/* force BASE/16 <= a[la-1] < BASE/4 */
void xn(calc_sqrt_n2)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  chiffre x;
  x = a[la-1] & (BASE_2/2 - 1);
  if (x < BASE_2/8) x += BASE_2/8;
  a[la-1] = x;
  xn(move)(a,la,c);
  xn(sqrt_n2)(c,la,b);
}

#else
/* force a = carr� - 1 */
void xn(calc_sqrt_n2)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  chiffre x;

  x = a[la/2-1] & (BASE_2 - 1);
  if (x < BASE_2/2) x += BASE_2/2;
  a[la/2-1] = x;
  xn(toomsqr)(a,la/2,c);
  xn(dec1)(c,la);
  xn(sqrt_n2)(c,la,b);
}

#endif /* just_below */

/* ---------------------------------------- algorithme de Zimmermann */

#ifndef just_below
/* force BASE/16 <= a[la-1] < BASE/4 */
void xn(calc_zimsqrt)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  chiffre x;
  x = a[la-1] & (BASE_2/2 - 1);
  if (x < BASE_2/8) x += BASE_2/8;
  a[la-1] = x;
  xn(move)(a,la,c);
  xn(zimsqrt)(c,la,b);
}

#else

/* force a = carr� - 1 */
void xn(calc_zimsqrt)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  chiffre x;

  x = a[la/2-1] & (BASE_2 - 1);
  if (x < BASE_2/2) x += BASE_2/2;
  a[la/2-1] = x;
  xn(toomsqr)(a,la/2,c);
  xn(dec1)(c,la);
  xn(zimsqrt)(c,la,b);
}

#endif /* just_below */

/* ---------------------------------------- racine carr�e modulaire */

#ifndef just_below
/* force BASE/16 <= a[la-1] < BASE/4 */
void xn(calc_modsqrt)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  chiffre x;
  x = a[la-1] & (BASE_2/2 - 1);
  if (x < BASE_2/8) x += BASE_2/8;
  a[la-1] = x;
  xn(move)(a,la,c);
  xn(modsqrt)(c,la,b);
}

#else

/* force a = carr� - 1 */
void xn(calc_modsqrt)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  chiffre x;

  x = a[la/2-1] & (BASE_2 - 1);
  if (x < BASE_2/2) x += BASE_2/2;
  a[la/2-1] = x;
  if (la > 8) {xn(clear)(a,la/4);xn(dec1)(a,la/2);}
  xn(fftsqr)(a,la/2,c);
  xn(dec1)(c,la);
  xn(modsqrt)(c,la,b);
}

#endif /* just_below */


                            /* +----------------+
                               |  Racine p-�me  |
                               +----------------+ */

void xn(size_root3)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 1) *la = 1;
  *lb = -(*la + 2)/3;
  *lc = 0;
  *ld = 0;
}

void xn(size_root5)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 1) *la = 1;
  *lb = -(*la + 4)/5;
  *lc = 0;
  *ld = 0;
}

void xn(calc_root3)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  a[la-1] |= 1;
  xn(root)(a,la,b,3);
}

void xn(calc_root5)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
  a[la-1] |= 1;
  xn(root)(a,la,b,5);
}

                      /* +----------------------------+
                         |  Exponentiation modulaire  |
                         +----------------------------+ */


void xn(size_powm)(long *la, long *lb, long *lc, long *ld) {
  if (*la < 1) *la = 1;
  *lb = *la;
  *lc = *la;
  *ld = -(*la);
}

void xn(calc_powm0)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {

  b[la-1] |= 1;
  c[la-1] |= 2;
  c[0] &= -2;
  xn(powmod)(a,la,b,la,c,la,d);
}

void xn(calc_powm1)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {

  b[la-1] |= 1;
  c[la-1] |= 1;
  c[0] |= 1;
  xn(powmod_mg)(a,la,b,la,c,la,d);
}

                                /* +--------+
                                   |  PGCD  |
                                   +--------+ */

void xn(size_gcd)(long *la, long *lb, long *lc, long *ld) {
    *lb = *la;
    *lc = 0;
    *ld = 0;
}

void xn(calc_gcd_n2)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
    chiffre *x[6];
    long i,l[6],n;
    n = (la > lb) ? la : lb;
    x[0] = xn(alloc)(6*n);
    for (i=1; i<6; i++) x[i] = x[i-1] + n;
    if (!a[la-1]) a[la-1] = 1;
    if (!b[lb-1]) b[lb-1] = 1;
    xn(move)(a,la,x[0]); l[0] = la;
    xn(move)(b,lb,x[1]); l[1] = lb;
    xn(gcd_n2)(x,l,0);
    xn(free)(x[0]);
}

void xn(calc_gcdex_n2)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
    chiffre *x[6];
    long i,l[6],n;
    n = (la > lb) ? la : lb;
    x[0] = xn(alloc)(6*n);
    for (i=1; i<6; i++) x[i] = x[i-1] + n;
    if (!a[la-1]) a[la-1] = 1;
    if (!b[lb-1]) b[lb-1] = 1;
    xn(move)(a,la,x[0]); l[0] = la;
    xn(move)(b,lb,x[1]); l[1] = lb;
    xn(gcd_n2)(x,l,1);
    xn(free)(x[0]);
}

void xn(calc_lehmer)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
    chiffre *x[6];
    long i,l[6],n;
    n = (la > lb) ? la : lb;
    x[0] = xn(alloc)(6*n);
    for (i=1; i<6; i++) x[i] = x[i-1] + n;
    if (!a[la-1]) a[la-1] = 1;
    if (!b[lb-1]) b[lb-1] = 1;
    xn(move)(a,la,x[0]); l[0] = la;
    xn(move)(b,lb,x[1]); l[1] = lb;
    xn(lehmer)(x,l,0);
    xn(free)(x[0]);
}

void xn(calc_lehmer_ex)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, chiffre *d) {
    chiffre *x[6];
    long i,l[6],n;
    n = (la > lb) ? la : lb;
    x[0] = xn(alloc)(6*n);
    for (i=1; i<6; i++) x[i] = x[i-1] + n;
    if (!a[la-1]) a[la-1] = 1;
    if (!b[lb-1]) b[lb-1] = 1;
    xn(move)(a,la,x[0]); l[0] = la;
    xn(move)(b,lb,x[1]); l[1] = lb;
    xn(lehmer)(x,l,1);
    xn(free)(x[0]);
}

                   /* +----------------------------------+
                      |  Description des chronom�trages  |
                      +----------------------------------+ */

typedef void (xn(size))(long *, long *, long *, long *);
typedef void (xn(calc))(chiffre *, long, chiffre *, long, chiffre *, chiffre *);

typedef struct {
  int      sel;       /* op�ration s�lectionn�e */
  char     *name;     /* nom symbolique         */
  char     *desc;     /* description            */
  xn(size) *size;     /* calcul de taille       */
  xn(calc) *calc;     /* fonction de calcul     */
} test_t;

/*
  size(&la, &lb, &lc, &ld) :

    d�termine  les tailles  de  quatre naturels  a,b,c,d  � partir  de
    tailles al�atoires donn�es en entr�e.  En sortie, une taille nulle
    signifie que le  naturel associ� ne sera pas  utilis� ; une taille
    n�gative signifie  que le naturel  sera utilis� comme  r�sultat et
    qu'il n'a pas  besoin d'�tre initialis�. Dans ce  cas, la taille �
    allouer pour ce naturel est la valeur absolue du nombre retourn�.

  calc(a,la, b,lb, c,d) :

    effectue  un calcul  sur  les entiers  naturels a,b,c,d.  Ceux-cis
    doivent avoir  les tailles retourn�es par la  fonction [size]. Les
    op�randes  pour  lesquels  [size]  retourne  une  taille  positive
    doivent �tre  initialis�s, les autres  param�tres peuvent contenir
    des valeurs quelconques.
*/

                      /* +----------------------------+
                         |  Liste des chronom�trages  |
                         +----------------------------+ */

test_t test[] = {

/* papillon */
{0, NULL,          "Butterfly",                    NULL,           NULL              },
{0, "but1",        "butterfly, q=2*HW",            xn(size_but),   xn(calc_but1)     },
{0, "but2",        "butterfly, q=2*HW+1",          xn(size_but),   xn(calc_but2)     },
{0, "but3",        "butterfly, q=2*HW+2",          xn(size_but),   xn(calc_but3)     },

/* sjoin3 */
{0, NULL,          "sjoin3",                       NULL,           NULL              },
{0, "join",        "sjoin3",                       xn(size_join),   xn(calc_join)     },
 
/* multiplication */
{0, NULL,          "Multiplication n by n",        NULL,           NULL              },
{0, "mul_n2",      "schoolbook multiplication",    xn(size_mul),   xn(calc_mul_n2)   },
{0, "karamul",     "Karatsuba multiplication",     xn(size_mul),   xn(calc_karamul)  },
{0, "toommul",     "Toom multiplication",          xn(size_mul),   xn(calc_toommul)  },
{0, "fftmul",      "FFT multiplication",           xn(size_mul),   xn(calc_fftmul)   },

/* multiplication avec des op�randes de longueurs diff�rentes */
{0, NULL,          "Multiplication 1.5n by n",     NULL,           NULL              },
{0, "mul_n2_2",    "schoolbook multiplication",    xn(size_mul_2), xn(calc_mul_n2)   },
{0, "karamul_2",   "Karatsuba multiplication",     xn(size_mul_2), xn(calc_karamul)  },
{0, "toommul_2",   "Toom multiplication",          xn(size_mul_2), xn(calc_toommul)  },
{0, "fftmul_2",    "FFT multiplication",           xn(size_mul_2), xn(calc_fftmul)   },

/* carr� */
{0, NULL,          "Square",                       NULL,           NULL              },
{0, "sqr_n2",      "schoolbook square",            xn(size_sqr),   xn(calc_sqr_n2)   },
{0, "karasqr",     "Karatsuba square",             xn(size_sqr),   xn(calc_karasqr)  },
{0, "toomsqr",     "Toom square",                  xn(size_sqr),   xn(calc_toomsqr)  },
{0, "fftsqr",      "FFT sqr",                      xn(size_sqr),   xn(calc_fftsqr)   },

/* multiplication modulaire */
{0, NULL,          "Modular multiplication",       NULL,           NULL              },
{0, "mmul",        "multiplication mod BASE^n+1",  xn(size_mmul),  xn(calc_mmul)     },
{0, "smul",        "multiplication mod BASE^n-1",  xn(size_smul),  xn(calc_smul)     },

/* carr� modulaire */
{0, NULL,          "Modular square",               NULL,           NULL              },
{0, "msqr",        "square mod BASE^n+1",          xn(size_msqr),  xn(calc_msqr)     },
{0, "ssqr",        "square mod BASE^n-1",          xn(size_ssqr),  xn(calc_ssqr)     },

/* division 2n par n */
{0, NULL,          "Division 2n by n",             NULL,           NULL              },
{0, "div_n2",      "Knuth division",               xn(size_div),   xn(calc_div_n2)   },
{0, "burnidiv",    "Burnikel-Ziegler division",    xn(size_div),   xn(calc_burnidiv) },
{0, "modinv",      "Modular inversion w/o rem",    xn(size_inv),   xn(calc_modinv)   },
{0, "moddivnr",    "Modular division w/o rem",     xn(size_div),   xn(calc_moddivnr) },
{0, "moddivr",     "Modular division with rem",    xn(size_div),   xn(calc_moddivr)  },
{0, "karpinv",     "Karp-Markstein inv. w/o rem",  xn(size_inv),   xn(calc_karpinv)  },
{0, "karpdivnr",   "Karp-Markstein div. w/o rem",  xn(size_div),   xn(calc_karpdivnr)},
{0, "karpdivr",    "Karp-Markstein div. with rem", xn(size_div),   xn(calc_karpdivr) },

/* division 1.5n par n */
{0, NULL,          "Division 1.5n by n",           NULL,           NULL              },
{0, "div_n2_2",    "Knuth division",               xn(size_div_2), xn(calc_div_n2)   },
{0, "burnidiv_2",  "Burnikel-Ziegler division",    xn(size_div_2), xn(calc_burnidiv) },
{0, "moddivnr_2",  "Modular division w/o rem",     xn(size_div_2), xn(calc_moddivnr) },
{0, "moddivr_2",   "Modular division with rem",    xn(size_div_2), xn(calc_moddivr)  },
{0, "karpdivnr_2", "Karp-Markstein div. w/o rem",  xn(size_div_2), xn(calc_karpdivnr)},
{0, "karpdivr_2",  "Karp-Markstein div. with rem", xn(size_div_2), xn(calc_karpdivr) },

/* division 3n par n */
{0, NULL,          "Division 3n by n",             NULL,           NULL              },
{0, "div_n2_3",    "Knuth division",               xn(size_div_3), xn(calc_div_n2)   },
{0, "burnidiv_3",  "Burnikel-Ziegler division",    xn(size_div_3), xn(calc_burnidiv) },
{0, "moddivnr_3",  "Modular division w/o rem",     xn(size_div_3), xn(calc_moddivnr) },
{0, "moddivr_3",   "Modular division with rem",    xn(size_div_3), xn(calc_moddivr)  },
{0, "karpdivnr_3", "Karp-Markstein div. w/o rem",  xn(size_div_3), xn(calc_karpdivnr)},
{0, "karpdivr_3",  "Karp-Markstein div. with rem", xn(size_div_3), xn(calc_karpdivr) },

/* racine carr�e */
{0, NULL,          "Square root",                  NULL,           NULL              },
{0, "sqrt_n2",     "schoolbook square root",       xn(size_sqrt),  xn(calc_sqrt_n2)  },
{0, "zimsqrt",     "Zimmermann square root",       xn(size_sqrt),  xn(calc_zimsqrt)  },
{0, "modsqrt",     "Modular square root",          xn(size_sqrt),  xn(calc_modsqrt)  },

/* racine p-�me */
{0, NULL,          "pth root",                     NULL,           NULL              },
{0, "root3",       "cubic root",                   xn(size_root3), xn(calc_root3)    },
{0, "root5",       "quintic root",                 xn(size_root5), xn(calc_root5)    },

/* exponentiation modulaire */
{0, NULL,          "Modular exponentiation",       NULL,           NULL              },
{0, "powm0",       "mod. exp., even modulus",      xn(size_powm),  xn(calc_powm0)    },
{0, "powm1",       "mod. exp., odd modulus",       xn(size_powm),  xn(calc_powm1)    },

/* pgcd */
{0, NULL,          "Greatest common divisor",      NULL,           NULL              },
{0, "gcd_n2",      "Euclide algorithm",            xn(size_gcd),   xn(calc_gcd_n2)   },
{0, "gcdex_n2",    "Extended Euclide algorithm",   xn(size_gcd),   xn(calc_gcdex_n2) },
{0, "lehmer",      "Lehmer algorithm",             xn(size_gcd),   xn(calc_lehmer)   },
{0, "lehmer_ex",   "Extended Lehmer algorithm",    xn(size_gcd),   xn(calc_lehmer_ex)},
};
#define nbtests (sizeof(test)/sizeof(test_t))

                    /* +---------------------------------+
                       |  Chronom�trage d'une op�ration  |
                       +---------------------------------+ */

/*
   entr�e :
   t     = num�ro de l'op�ration � chronom�trer
   temps = dur�e en secondes d'un chronom�trage
   bits  = taille en bits d'un op�rande
   unit  = p�riode de l'horloge en micro-secondes

   sortie :
   m = dur�e moyenne d'une op�ration
   n = nombre d'op�rations effectu�es
*/

void chronometre(int t, long *m, long *n, double temps, long bits, double unit) {
  long    la,  lb,  lc,  ld;    /* longueurs des op�randes         */
  chiffre *a,  *b,  *c,  *d;    /* op�randes pour la pr�-mesure    */
  chiffre **aa,**bb,**cc,**dd;  /* op�randes pour la mesure r�elle */
  long    i,j,p;
  double  t0,t1;                /* compteurs de tics   */

/* nb maxi de tests */
#define nmax 10000

  /* taille des op�randes */
  la = lb = lc = ld = (bits+HW-1)/HW;
  test[t].size(&la, &lb, &lc, &ld);

  /* Compte le nb d'it�rations pour atteindre la dur�e demand�e. */
  /* On effectue une r�gle de 3 � partir du nb d'it�rations      */
  /* constat� pour une dur�e de 1s.                              */
  a = xn(alloc)(abs(la)); if (la > 0) xn(random)(a,la);
  b = xn(alloc)(abs(lb)); if (lb > 0) xn(random)(b,lb);
  c = xn(alloc)(abs(lc)); if (lc > 0) xn(random)(c,lc);
  d = xn(alloc)(abs(ld)); if (ld > 0) xn(random)(d,ld);

  if (temps < 0) {
    for (i=temps; i<0; i++) test[t].calc(a,la,b,lb,c,d);
    t1 = 0;
  }
  else for (*n=0, t0=clock(), t1=0; t1 < CLOCKS_PER_SEC; (*n)++) {
    test[t].calc(a,la,b,lb,c,d);
    t1 = clock() - t0;
    if (t1 < 0) {t0 = clock(); t1 = 0; *n = -1;}
  }

  if (temps > t1/CLOCKS_PER_SEC) {
    *n = *n*temps*(CLOCKS_PER_SEC/t1);
    if (*n <= 0)    *n = 1;

    /* tire min(n,nmax) jeux d'op�randes au hasard */
    p = *n; if (p > nmax) p = nmax;
    aa = (chiffre **)malloc(p*sizeof(chiffre *));
    bb = (chiffre **)malloc(p*sizeof(chiffre *));
    cc = (chiffre **)malloc(p*sizeof(chiffre *));
    dd = (chiffre **)malloc(p*sizeof(chiffre *));
    if (dd == NULL) {printf("out of memory\n"); fflush(stdout); exit(1);}
    for (i=0; i < p; i++) {
      if (la > 0) {aa[i] = xn(alloc)(la); xn(random)(aa[i],la);} else aa[i] = a;
      if (lb > 0) {bb[i] = xn(alloc)(lb); xn(random)(bb[i],lb);} else bb[i] = b;
      if (lc > 0) {cc[i] = xn(alloc)(lc); xn(random)(cc[i],lc);} else cc[i] = c;
      if (ld > 0) {dd[i] = xn(alloc)(ld); xn(random)(dd[i],ld);} else dd[i] = d;
    }

    /* chronom�tre les n ex�cutions */
    for (i=0, t0=clock(); i < *n; i++) {
      j = i % nmax;
      test[t].calc(aa[j],la,bb[j],lb,cc[j],dd[j]);
    }
    t1 = clock() - t0;
    if (t1 < 0) {
      /* d�bordement de l'horloge. Avec un peu de chance �a n'a d�bord�
         qu'une fois, */
      t1 += 1L << (8*sizeof(clock_t)-2);
      t1 += 1L << (8*sizeof(clock_t)-2);
      t1 += 1L << (8*sizeof(clock_t)-2);
      t1 += 1L << (8*sizeof(clock_t)-2);
    }

    /* lib�re la m�moire */
    for (i = p-1; i>=0; i--) {
      if (ld > 0) xn(free)(dd[i]);
      if (lc > 0) xn(free)(cc[i]);
      if (lb > 0) xn(free)(bb[i]);
      if (la > 0) xn(free)(aa[i]);
    }
    free(dd);    free(cc);    free(bb);    free(aa);
  }
  xn(free)(d); xn(free)(c); xn(free)(b); xn(free)(a);


  /* temps moyen */
  *m = (t1*unit + *n/2)/(*n);
}

                    /* +--------------------------------+
                       |  Initialise un entier naturel  |
                       +--------------------------------+ */

/* entr�e :
   a = naturel de longueur la
   s = cha�ne hexad�cimale

   sortie :
   a <- valeur(s) mod BASE^la si s != NULL
   a <- random sinon
*/

void read_hexstring(chiffre *a, long la, char *s) {
    long i,l;

    if (s) {
        xn(clear)(a,la);
        for (i=0, l=strlen(s), s+=l-1; (l) && (la); i+=4, s--, l--) {
            if      ((s[0] >= '0') && (s[0] <= '9')) a[0] += (chiffre)(s[0] - '0'     ) << i;
            else if ((s[0] >= 'A') && (s[0] <= 'F')) a[0] += (chiffre)(s[0] - 'A' + 10) << i;
            else if ((s[0] >= 'a') && (s[0] <= 'f')) a[0] += (chiffre)(s[0] - 'a' + 10) << i;
            else {
                printf("invalid hexadecimal string\n");
                fflush(stdout);
                exit(1);
            }
            if (i+4 == HW) {a++; la--; i=-4;}
        }
    } else xn(random)(a,la);
}
   

           /* +---------------------------------------------------+
              |  Effectue une op�ration avec arguments sp�cifi�s  |
              +---------------------------------------------------+ */

/* entr�e :
   t = num�ro de l'op�ration
   sa,sb = cha�nes hexad�cimales pour a,b (NULL pour un argument non sp�cifi�)

   sortie :
   aucune
*/

void single_run(int t, char *sa, char *sb) {
  long    la,  lb,  lc,  ld;
  chiffre *a,  *b,  *c,  *d;

  /* initialise les longueurs */
  la = lb = lc = ld = 0;
  if (sa) la = (4*strlen(sa))/HW;
  if (sb) lb = (4*strlen(sb))/HW;
  test[t].size(&la, &lb, &lc, &ld);

  /* initialise les arguments */
  a = xn(alloc)(abs(la)); if (la > 0) read_hexstring(a,la,sa);
  b = xn(alloc)(abs(lb)); if (lb > 0) read_hexstring(b,lb,sb);
  c = xn(alloc)(abs(lc)); if (lc > 0) xn(random)(c,lc);
  d = xn(alloc)(abs(ld)); if (ld > 0) xn(random)(d,ld);

  /* ex�cute le calcul */
  test[t].calc(a,la,b,lb,c,d);
  xn(free)(d); xn(free)(c); xn(free)(b); xn(free)(a);

}


                      /* +-----------------------------+
                         |  Conversion string -> long  |
                         +-----------------------------+ */

/*
  entr�e :
  s = cha�ne de caract�res

  sortie :
  retourne le nombre positif d�sign� par s s'il est valide, -1 sinon

  remarque :
  reconna�t les suffixes k (=10^3), K (=2^10), m (=10^6), M (=2^20), w (=HW)
*/

long getlong(char *s) {
  long res;

  /* lit la partie d�cimale */
  if ((*s > '9') || (*s < '0')) res = 1;
  else for (res=0; (*s >= '0') && (*s <= '9'); s++) res = res*10 + *s - '0';

  /* lit les suffixes */
  for(; (*s) && (res >= 0); s++) switch(*s) {
  case 'k' : res *= 1000;     break;
  case 'K' : res *= 1024;     break;
  case 'm' : res *= 1000000;  break;
  case 'M' : res *= 1048576;  break;
  case 'w' : res *= HW;       break;
  default  : res = -1;
  }

  /* retourne la valeur ou -1 en cas d'erreur */
  return(res);
}



                         /* +-----------------------+
                            |  Programme principal  |
                            +-----------------------+ */

int main(int argc, char **argv) {

#define default_time   10
#define default_bits 1000
  double temps = default_time;
  long   bits  = default_bits;
  double unit  = 1.0e6/CLOCKS_PER_SEC;

  int help = 0, list = 0, range = 0;
  long start = bits, stop = bits, step = 1;
  unsigned long seed = time(NULL);
  char *sa = NULL, *sb = NULL, *res = NULL;
  FILE *fres = NULL;

  long i,j,m,n;
  char **s;

  /* analyse la ligne de commande */
  for (j=argc-1, s=argv+1; (j) && (!help); j--, s++) {

    /* options sans argument */
    if (!strcmp(*s,"-h"))      {help = 1; continue;}
    if (!strcmp(*s,"-l"))      {list = 1; continue;}
    if (!strcmp(*s,"-ns"))     {unit = 1.0e9/CLOCKS_PER_SEC; continue;}
    if (!strcmp(*s,"-us"))     {unit = 1.0e6/CLOCKS_PER_SEC; continue;}
    if (!strcmp(*s,"-ms"))     {unit = 1.0e3/CLOCKS_PER_SEC; continue;}

    /* nom d'un algorithme � contr�ler */
    for (i = 0; i < nbtests; i++) {
      if ((test[i].name) && (strcmp(*s, test[i].name) == 0)) break;
    }
    if (i < nbtests) {test[i].sel ^= 1; continue;}

    /* options � arguments */
    if (j > 1) {
      if (!strcmp(*s,"-seed"))   {seed  = atol(*++s);    j--; continue;}
      if (!strcmp(*s,"-time"))   {temps = atof(*++s);    j--; continue;}
      if (!strcmp(*s,"-bits"))   {bits  = getlong(*++s); j--; continue;}
      if (!strcmp(*s,"-a"))      {sa    = *++s;          j--; continue;}
      if (!strcmp(*s,"-b"))      {sb    = *++s;          j--; continue;}
      if (!strcmp(*s,"-o"))      {res   = *++s;          j--; continue;}

      if ((!strcmp(*s,"-range")) && (j > 3)) {
        start = getlong(*++s);
        stop  = getlong(*++s);
        step  = getlong(*++s);
        range = 1;
        j    -= 3;
        continue;
      }
    }

    /* option non reconnue */
    printf("syntax error at: %s\ntype %s -h for help\n",*s,argv[0]);
    exit(1);
  }

  /* mode d'emploi */
  if (help) {
    printf("usage: %s <options> <tests>\n",argv[0]);
    printf("options:\n");
    printf("-h           print this help\n");
    printf("-l           print available tests\n");
    printf("-seed s      initialize the random generator\n");
    printf("-bits b      number of bits for an operand (default %d)\n", default_bits);
    printf("-range a b c test for bits = a,a+c,..,b\n");
    printf("-ns          display time in nanoseconds\n");
    printf("-us          display time in microseconds (default)\n");
    printf("-ms          display time in miliseconds\n");
    printf("-time t      duration in seconds for each test (default %d)\n",default_time);
    printf("-a <hex>     set the first operand\n");
    printf("-b <hex>     set the second operand\n");
    printf("-o <file>    send also output to <file>\n");
    exit(0);
  }

  if (list) {
    printf("available tests: (select twice to unselect)\n");
    for (i = 0; i < nbtests; i++) {
      if (test[i].name) printf("%-16s%s\n", test[i].name,    test[i].desc);
      else              printf("\n-------------------- %s\n",test[i].desc);
    }
    exit(0);
  }

  if ((bits < 0) || (start < 0) || (stop < 0) || (step < 0)) {
    printf("invalid bit or range specification\n");
    exit(1);
  }

  /* initialisation des g�n�rateurs al�atoires */
  srandom(seed);

  /* ouvre le fichier de sortie en mode append */
  if (res) {
      fres = fopen(res,"a");
      if (fres == NULL) {
          printf("unable to open %s\n",res);
          fflush(stdout);
          exit(1);
      }
  }

  /* tests pour a et/ou b impos� */
  if ((sa) || (sb)) {
    for (i=0; i < nbtests; i++) if (test[i].sel) {
        printf("%-16s", test[i].name);
        if (res) fprintf(fres,"%-16s", test[i].name);
        single_run(i,sa,sb);
        printf("ok\n");
        if (res) fprintf(fres,"ok\n");
    }
  }

  /* tests pour une seule longueur */
  else if (!range) {
    for (i=0; i < nbtests; i++) if (test[i].sel) {
      printf("%-16s", test[i].name);
      if (res) fprintf(fres,"%-16s", test[i].name);
      chronometre(i,&m,&n,temps,bits,unit);
      printf("%10ld (%ld)\n", m,n);
      if (res) fprintf(fres,"%10ld (%ld)\n", m,n);
    }
  }

  /* tests pour une plage de longueurs */
  else {

    /* recopie la ligne de commande */
    printf("#");
    for (j=argc, s=argv; j; j--, s++) printf(" %s",*s);
    printf("\n"); fflush(stdout);
    if (res) {
      fprintf(fres,"#");
      for (j=argc, s=argv; j; j--, s++) fprintf(fres," %s",*s);
      fprintf(fres,"\n");
    }

    /* noms des algorithmes */
    printf("#           bits");
    for (i=0; i < nbtests; i++) if (test[i].sel) printf("%16s", test[i].name);
    printf("\n");
    if (res) {
      fprintf(fres,"#           bits");
      for (i=0; i < nbtests; i++) if (test[i].sel) fprintf(fres,"%16s", test[i].name);
      fprintf(fres,"\n");
    }

    for (bits = start; bits <= stop; bits += step) {
      printf("%16ld", bits);
      if (res) fprintf(fres,"%16ld", bits);
      for (i=0; i < nbtests; i++) if (test[i].sel) {
        chronometre(i,&m,&n,temps,bits,unit);
        printf("%16ld",m);
        if (res) fprintf(fres,"%16ld",m);
      }
      printf("\n");
      if (res) fprintf(fres,"\n");
    }
  }

  /* termin� */
  fflush(stdout);
  if (res) {fflush(fres); fclose(fres);}
  return(0);
}

