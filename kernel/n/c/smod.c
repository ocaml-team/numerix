// file kernel/n/c/smod.c: operations on residues modulo BASE^n - 1
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Arithm�tique modulo BASE^n - 1                    |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                    /* +---------------------------------+
                       |  Comparaison modulo BASE^n - 1  |
                       +---------------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb

  sortie :
  retourne 1 si a = b mod (BASE^n - 1), 0 sinon
*/

#if defined(debug_smul) || defined(debug_sjoin)
int xn(sequal)(chiffre *a, long la, chiffre *b, long lb, long n) {
  chiffre *x,r;
  long i;

  x = xn(alloc_tmp)(n); xn(clear)(x,n);

  /* x <- a mod (BASE^n - 1) */
  for (r=0; la >= 0; a+=n, la-=n) r += xn(inc)(x,n,a, (la > n) ? n : la);
  while (r) r = xn(inc)(x,n,&r,1);

  /* x <- x - b mod (BASE^n - 1) */
  for (r=0; lb >= 0; b+=n, lb-=n) r += xn(dec)(x,n,b, (lb > n) ? n : lb);
  while (r) r = xn(dec)(x,n,&r,1);


  /* v�rifie que x = 0 mod (BASE^n - 1) */
  for (r=x[0], i=1; (i < n) && (x[i] == r); i++);
  xn(free_tmp)(x);

  return(((i == n) && ((r == 0) || (r == (BASE_2)+(BASE_2-1)))));
}
#endif /* defined(debug_smul) || defined(debug_sjoin) */


              /* +----------------------------------+
                 |  Soustraction modulo BASE^n - 1  |
                 +----------------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb > 0

  sortie :
  b <- (a - b) mod (BASE^lb - 1), non normalis�
*/

#ifndef assembly_sn_ssub
void xn(ssub)(chiffre *a, long la, chiffre *b, long lb) {
  long i;

  /* cas a petit : effectue la soustraction � la main
     rmq: ce cas est inutilis� car les seuls clients de ssub sont remdiv et
     remsqrt, et ils passeront un a beaucoup plus long que b. */
  if (la < lb) {
    chiffre r,s,t;
    for (i=0, r=0; i<la; i++) {
      s = a[i] - r; r &= (s == (BASE_2)+(BASE_2-1));
      t = s - b[i]; r |= (t > s);
      b[i] = t;
    }
    for (; i<lb; i++) {
      t = -r - b[i]; r |= (t != 0);
      b[i] = t;
    }
    if (r) while(xn(dec1)(b,lb));
    return;
  }

  /* cas a grand : soustraction incr�mentations par tranches de lb chiffres */
  if (xn(sub)(a,lb,b,lb,b)) while(xn(dec1)(b,lb));
  for (la -= lb, a += lb; la > 0; la -= lb, a += lb) {
    i = (la < lb) ? la : lb;
    if (xn(inc)(b,lb,a,i)) while(xn(inc1)(b,lb));
  }
}
#endif /* assembly_sn_ssub */


 /* +----------------------------------------------------------------------+
    |  R�duction modulo BASE^(n/2)+1, ..., BASE^(n/2^k)+1, BASE^(n/2^k)-1  |
    +----------------------------------------------------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur n + k non confondu avec a

  contraintes : k >= 0, n divisible par 2^k, n > 0

  sortie :
  pour i=0..k-1,
  b[n-n/2^i+i..n-n/2^(i+1)+i] <- a mod (BASE^(n/2^(i+1)) + 1), normalis�
  b[n-n/2^k+k..n+k-1]         <- a mod (BASE^(n/2^k) - 1), non normalis�
*/

void xn(sred_k)(chiffre *a, long la, chiffre *b, long n, long k);

#ifndef assembly_sn_sred_k
void xn(sred_k)(chiffre *a, long la, chiffre *b, long n, long k) {
  chiffre *x, r;

  /* b <- a mod (BASE^n - 1) */
  if (la <= n) {xn(move)(a,la,b); xn(clear)(b+la,n-la);}
  else {
    xn(move)(a,n,b);
    for (r=0, a+=n, la-=n; la >= 0; a+=n, la-=n) {
      r += xn(inc)(b,n,a,(la > n) ? n : la);
    }
    while (r) r = xn(inc)(b,n,&r,1);
  }
  if (k==0) return;

  /* x <- a mod (BASE^(n/2) - 1), b <- a mod (BASE^(n/2) + 1) */
  n >>= 1;
  x = xn(alloc_tmp)(n);
  r = xn(add)(b,n,b+n,n,x); while(r) r = xn(inc1)(x,n);
  r = xn(dec)(b,n,b+n,n); b[n] = xn(inc)(b,n,&r,1);

  for (k--, b+=n+1; k; k--, b+=n+1) {

    /* x <- a mod (BASE^(n/2^i) - 1), b <- a mod (BASE^(n/2^i) + 1) */
    n >>= 1;
    r = xn(sub)(x,n,x+n,n,b); b[n] = xn(inc)(b,n,&r,1);
    r = xn(inc)(x,n,x+n,n); while(r) r = xn(inc1)(x,n);
  }

  /* recopie a mod (BASE^(n/2^k) - 1) dans b et s'en va */
  xn(move)(x,n,b);
  xn(free_tmp)(x);

}
#endif /* assembly_sn_sred_k */


                  /* +------------------------------------+
                     |  Multiplication modulo BASE^n - 1  |
                     +------------------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur n

  contraintes : n > 0, 0 < lb <= la

  sortie :
  c <- a*b mod (BASE^n - 1)
*/

#ifndef assembly_sn_smul
#ifdef debug_smul
void xn(smul_buggy)
#else
void xn(smul)
#endif
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, long n) {

  long p,k;
  chiffre *x,r;

  /* k <- nombre de r�ductions � effectuer */
  for (p=n, k=0; ((p&1) == 0) && (p > smul_lim_even); k++, p >>= 1);

  /* m�moire de travail */
  x = xn(alloc_tmp)(2*(n+p+k));

  /* r�duit a et b */
  xn(sred_k)(a,la,x,    n,k);
  xn(sred_k)(b,lb,x+n+k,n,k);

  /* c <- a*b mod (BASE^p - 1) */
  a = x+n+k-p; b = a+n+k; c = c+n-p;
  xn(toommul)(a,p,b,p,b+p);
  r = xn(add)(b+p,p,b+2*p,p,c); while (r) r = xn(inc1)(c,p);

  while (p < n) {

    /* a <- a_orig*b mod (BASE^p + 1) */
    a -= p+1; b -= p+1;
    xn(mmul)(a,b,p);

    /* c <- (c-a)/2 mod (BASE^p - 1) */
    r = xn(dec)(c,p,a,p); r += xn(dec)(c,p,a+p,1);
    while (r) r = xn(dec)(c,p,&r,1);
    if (xn(shift_down)(c,p,c,1)) c[p-1] |= BASE_2;

    /* c <- a + BASE^p*c = a_orig*b mod (BASE^(2p) - 1) */
    r  = xn(add)(a,p,c,p,c-p);
    r  = xn(inc)(c,p,&r,1);
    r += xn(inc)(c,p,a+p,1);
    c -= p; p <<= 1;
    while (r) r = xn(inc)(c,p,&r,1);
  }

  /* termin� */
  xn(free_tmp)(x);

}
#endif /* assembly_sn_smul */


/* --------------------  Contr�le */

#ifdef debug_smul
void xn(smul_buggy) (chiffre *a, long la, chiffre *b, long lb, chiffre *c, long n);
void xn(smul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c, long n) {
  long l = la+lb;
  chiffre *x;

  x = xn(alloc_tmp)(l);
  xn(toommul)(a,la,b,lb,x);
  xn(smul_buggy)(a,la,b,lb,c,n);
  if (!xn(sequal)(c,n,x,l,n))
      xn(internal_error)("error in smul",3, a,la,b,lb,c,n);

  xn(free_tmp)(x);

}
#endif /* debug_smul */

                       /* +---------------------------+
                          |  Carr� modulo BASE^n - 1  |
                          +---------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur n

  contraintes : n > 0, la > 0

  sortie :
  b <- a^2 mod (BASE^n - 1)
*/

#ifndef assembly_sn_ssqr
#ifdef debug_smul
void xn(ssqr_buggy)
#else
void xn(ssqr)
#endif
(chiffre *a, long la, chiffre *b, long n) {
  long p,k;
  chiffre *x,r;

  /* k <- nombre de r�ductions � effectuer */
  for (p=n, k=0; ((p&1) == 0) && (p > ssqr_lim_even); k++, p >>= 1);

  /* m�moire de travail */
  x = xn(alloc_tmp)(n+k+2*p);

  /* r�duit a */
  xn(sred_k)(a,la,x,n,k);

  /* b <- a^2 mod (BASE^p - 1) */
  a = x+n+k-p; b = b+n-p;
  xn(toomsqr)(a,p,a+p);
  r = xn(add)(a+p,p,a+2*p,p,b); while (r) r = xn(inc1)(b,p);

  while (p < n) {

    /* a <- a_orig^2 mod (BASE^p + 1) */
    a -= p+1;
    xn(msqr)(a,p);

    /* b <- (b-a)/2 mod (BASE^p - 1) */
    r = xn(dec)(b,p,a,p); r += xn(dec)(b,p,a+p,1);
    while (r) r = xn(dec)(b,p,&r,1);
    if (xn(shift_down)(b,p,b,1)) b[p-1] |= BASE_2;

    /* b <- a + BASE^p*b = a_orig^2 mod (BASE^(2p) - 1) */
    r  = xn(add)(a,p,b,p,b-p);
    r  = xn(inc)(b,p,&r,1);
    r += xn(inc)(b,p,a+p,1);
    b -= p; p <<= 1;
    while (r) r = xn(inc)(b,p,&r,1);
  }

  /* termin� */
  xn(free_tmp)(x);

}
#endif /* assembly_sn_ssqr */


/* --------------------  Contr�le */

#ifdef debug_smul
void xn(ssqr_buggy) (chiffre *a, long la, chiffre *b, long n);
void xn(ssqr)(chiffre *a, long la, chiffre *b, long n) {
  long l = 2*la;
  chiffre *x;

  x = xn(alloc_tmp)(l);
  xn(toomsqr)(a,la,x);
  xn(ssqr_buggy)(a,la,b,n);
  if (!xn(sequal)(b,n,x,l,n))
      xn(internal_error)("error in ssqr",2, a,la,b,n);

  xn(free_tmp)(x);

}
#endif /* debug_smul */

                      /* +----------------------------+
                         |  Combinaison de 3 r�sidus  |
                         +----------------------------+ */

/*
  entr�e :
  a = naturel de longueur n+p+q
  n = (2h+2)k, p = (2h+1)k, q = (2h)k

  contraintes : h >= 2, k >= 2

  sortie :
  a <- x mod ppcm(BASE^n - 1, BASE^p - 1, BASE^q - 1) normalis�
  avec
    a[0..n-1]       = x mod (BASE^n - 1),
    a[n..n+p-1]     = x mod (BASE^p - 1),
    a[n+p..n+p+q-1] = x mod (BASE^q - 1)

  remarque : ppcm = produit/(BASE^k - 1)/(BASE^(2k) - 1)
*/

#ifndef assembly_sn_sjoin3
#ifdef debug_sjoin
void xn(sjoin3_buggy)
#else
void xn(sjoin3)
#endif
(chiffre *a, long h, long k) {
  long i, q = 2*h*k, p = q+k, n = p+k;
  chiffre *b = a+n, *c = b+p, r,s;
  int cnul;

  /* normalise a */
  if (!xn(inc1)(a,n)) xn(dec1)(a,n);

  /* b <- (a - b) mod (BASE^p - 1), normalis� vers le haut */
  r = xn(sub)(a,p, b,p,b);
  s = xn(inc)(b,p, a+p,k);
  if (r > s) while (xn(dec1)(b,p)); else if (r < s) while (xn(inc1)(b,p));
  if (!xn(dec1)(b,p)) xn(inc1)(b,p);

  /* c <- (c - a) + (BASE^k + 1)*b - (BASE^(2*k) - 1) mod (BASE^q - 1) */
  s  = xn(dec)(c,q,a,q);       s += xn(dec)(c,q,a+q,2*k);
  r  = xn(inc)(c,q,b,q);       r += xn(inc)(c,q,b+q,k);
  r += xn(inc)(c+k,q-k,b,q-k); r += xn(inc)(c,q,b+q-k,2*k);
  s += xn(dec1)(c+2*k,q-2*k);  r += xn(inc1)(c,q);
  if      (r > s) {r -= s; while (r) r = xn(inc)(c,q,&r,1);}
  else if (r < s) {s -= r; while (s) s = xn(dec)(c,q,&s,1);}

  /* c = 0 mod (BASE^q - 1) ? */
  if ((c[0] == 0) || (c[0] == (BASE_2)+(BASE_2-1))) {
    for (i=1; (i < q) && (c[i] == c[0]); i++);
    cnul = (i == q);
  } else cnul = 0;

  /* si oui, c:b <- (BASE^q - 1):b + 1 */
  if (cnul) {
    if (c[0] == 0) xn(dec1)(c,q);
    xn(inc1)(b,p+q);
  }

  /* sinon, c:b <- b + (BASE^p - 1)*(c/(1-BASE^(2*k)) - 1) + BASE^q */
  else {
    xn(inc)(c+2*k,q-2*k,c,q-2*k);
    xn(dec1)(c,q);
    xn(dec)(b,p+q,c,q);
    xn(inc1)(b+q,p);
  }

  /* c:b:a <- a + (BASE^n - 1)*(c:b)/(1-BASE^k) */
  xn(inc)(b+k,2*q,b,2*q);
  xn(dec)(a,n+p+q,b,p+q);

}
#endif /* assembly_sn_sjoin3 */


/* -------------------- Contr�le */

#ifdef debug_sjoin
void xn(sjoin3_buggy)(chiffre *a, long h, long k);
void xn(sjoin3)(chiffre *a, long h, long k) {
  chiffre *x,*y,*z;
  long q = 2*h*k, p = q+k, n = p+k, l = n+p+q;

  if ((k < 2) || (h < 2))
    xn(internal_error)("error, sjoin3 is called with k < 2 or h < 2",0);

  /* copie les donn�es dans x,y,z.
     Ne pas utiliser alloc_tmp, a risque d'�tre gros */
  x = xn(alloc)(n+p+q); y = x+n; z = y+p;
  xn(move)(a,n+p+q,x);

  /* effectue le calcul douteux */
  xn(sjoin3_buggy)(a,h,k);

  /* v�rifie les congruences */
  if (   (!xn(sequal)(a,l,x,n,n))
      || (!xn(sequal)(a,l,y,p,p))
      || (!xn(sequal)(a,l,z,q,q)))
      xn(internal_error)("error in sjoin3", 4,x,n,y,p,z,q,a,l);

  xn(free)(x);

}
#endif /* debug_sjoin */

