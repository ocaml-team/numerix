// file kernel/n/c/add.c: addition/subtraction of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Addition/soustraction                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Addition
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb <= la
  c = naturel de longueur la

  sortie :
  c <- a + b
  retourne la retenue
*/
#ifndef assembly_sn_add
chiffre xn(add)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
#ifdef ndouble
  ndouble r;
  long    i;

  for (r=0, i=0; i<lb; i++)
       {r += (ndouble)a[i] + (ndouble)b[i]; c[i] = r; r >>= HW;}
  for ( ; i < la; i++) {r += (ndouble)a[i]; c[i] = r; r >>= HW;}
  return(r);
#else
  chiffre r,s,t;
  long i;

  for (r=0, i=0; i<lb; i++) {
    s = r + a[i]; r  = (s < r);
    t = s + b[i]; r |= (t < s);
    c[i] = t;
  }
  for (; i<la; i++) {
    s = r + a[i]; r = (s < r);
    c[i] = s;
  }
  return(r);
#endif /* ndouble */
}
#endif /* assembly_sn_add */

/* ---------------------------------------- Incr�mentation
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb avec lb <= la

  sortie :
  a <- a + b
  retourne la retenue

  remarque :
  xn(inc) peut �tre utilis� pour diviser par 1 - BASE^p si p >= 1
  la version assembleur x86 ne peut �tre utilis�e dans ce but que
  pour p >= 2
*/
#ifndef assembly_sn_inc
chiffre xn(inc)(chiffre *a, long la, chiffre *b, long lb) {
#ifdef ndouble
  ndouble r;
  long    i;

  for (r=0, i=0; i < lb; i++)
                {r += (ndouble)a[i] + (ndouble)b[i]; a[i] = r; r >>= HW;}
  for ( ; (i < la) && (r); i++) {r += (ndouble)a[i]; a[i] = r; r >>= HW;}
  return(r);
#else
  chiffre r,s,t;
  long i;

  for (r=0, i=0; i<lb; i++) {
    s = r + a[i]; r  = (s < r);
    t = s + b[i]; r |= (t < s);
    a[i] = t;
  }
  for (; (i<la) && (r); i++) {
    s = 1 + a[i]; r = (s == 0);
    a[i] = s;
  }
  return(r);
#endif /* ndouble */
}
#endif /* assembly_sn_inc */

/* ---------------------------------------- Incr�mentation de 1
  entr�e :
  a = naturel de longueur la

  sortie :
  a <- a + 1
  retourne la retenue
*/
#ifndef assembly_sn_inc1
chiffre xn(inc1)(chiffre *a, long la) {
#ifdef ndouble
  ndouble r;
  long    i;

  for (r=1, i=0; (i < la) && (r); i++)
    {r += (ndouble)a[i]; a[i] = r; r >>= HW;}
  return(r);
#else
  chiffre r,s;
  long i;

  for (r=1, i=0; (i<la) && (r); i++) {
    s = 1 + a[i]; r = (s == 0);
    a[i] = s;
  }
  return(r);
#endif /* ndouble */
}
#endif /* assembly_sn_inc1 */

/* ---------------------------------------- Soustraction
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb <= la
  c = naturel de longueur la

  sortie :
  c <- a - b
  retourne la retenue
*/
#ifndef assembly_sn_sub
chiffre xn(sub)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
#ifdef zdouble
  zdouble r;
  long    i;

  for (r=0, i=0; i < lb; i++)
       {r += (ndouble)a[i] - (ndouble)b[i]; c[i] = r; r >>= HW;}
  for ( ; i < la; i++) {r += (ndouble)a[i]; c[i] = r; r >>= HW;}
  return(-r);
#else
  chiffre r,s,t;
  long i;

  for (r=0, i=0; i<lb; i++) {
    s = a[i] - r; r &= (s == (BASE_2)+(BASE_2-1));
    t = s - b[i]; r |= (t > s);
    c[i] = t;
  }
  for (; i<la; i++) {
    s = a[i] - r; r &= (s == (BASE_2)+(BASE_2-1));
    c[i] = s;
  }
  return(r);
#endif /* zdouble */
}
#endif /* assembly_sn_sub */

/* ---------------------------------------- D�cr�mentation
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb avec lb <= la

  sortie :
  a <- a - b
  retourne la retenue
*/
#ifndef assembly_sn_dec
chiffre xn(dec)(chiffre *a, long la, chiffre *b, long lb) {
#ifdef ndouble
  zdouble r;
  long    i;

  for (r=0, i=0; i < lb; i++)
                {r += (ndouble)a[i] - (ndouble)b[i]; a[i] = r; r >>= HW;}
  for ( ; (i < la) && (r); i++) {r += (ndouble)a[i]; a[i] = r; r >>= HW;}
  return(-r);
#else
  chiffre r,s,t;
  long i;

  for (r=0, i=0; i<lb; i++) {
    s = a[i] - r; r &= (s == (BASE_2)+(BASE_2-1));
    t = s - b[i]; r |= (t > s);
    a[i] = t;
  }
  for (; (i<la) && (r); i++) {
    s = a[i] - r; r = (s == (BASE_2)+(BASE_2-1));
    a[i] = s;
  }
  return(r);
#endif /* zdouble */
}
#endif /* assembly_sn_dec */

/* ---------------------------------------- D�cr�mentation de 1
  entr�e :
  a = naturel de longueur la

  sortie :
  a <- a - 1
  retourne la retenue
*/
#ifndef assembly_sn_dec1
chiffre xn(dec1)(chiffre *a, long la) {
#ifdef ndouble
  zdouble r;
  long    i;

  for (r=-1, i=0; (i < la) && (r); i++)
    {r += (ndouble)a[i]; a[i] = r; r >>= HW;}
  return(-r);
#else
  chiffre r,s;
  long i;

  for (r=1, i=0; (i<la) && (r); i++) {
    s = a[i] - r; r = (s == (BASE_2)+(BASE_2-1));
    a[i] = s;
  }
  return(r);
#endif /* zdouble */
}
#endif /* assembly_sn_dec1 */

