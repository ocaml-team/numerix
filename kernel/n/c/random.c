// file kernel/n/c/random.c: random numbers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                            Nombres al�atoires                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Naturel al�atoire
  entr�e :
  a = naturel de longueur la

  sortie :
  a <- chiffres al�atoires
*/

/*
  Sous Linux-i386, RAND_MAX = 2^31 - 1.
  Sur la machine beaune de l'Inria, RAND_MAX = 2^15-1 ... et [man random]
  indique que random retourne 31 bits, ce qui est confirm� exp�rimentalement.

  Je suppose que de mani�re g�n�rale, RAND_MAX = 2^x - 1 avec x >= 31.
  Si x >= HW on peut utiliser directement le r�sultat de random(),
  sinon on calcule HW bits al�atoires avec deux ou quatre appels � random()

  Pour garantir une identit� de r�sultats entre les diff�rents modes,
  on tire un nombre pair de chiffres lorsque chiffres_per_long = 2.
*/

#ifdef RAND_MAX
#if RAND_MAX < 0x7fffffff
#undef RAND_MAX
#define RAND_MAX 0x7fffffff
#endif
#else
#define RAND_MAX 0x7fffffff
#endif

#if RAND_MAX >= (BASE_2) + (BASE_2-1)
void xn(random)(chiffre *a, long la) {
  long i;
  for (i=0; i < la; a[i++] = random());
#if chiffres_per_long == 2
  if (la&1) random();
#endif
}

#elif RAND_MAX >= (BASE_2 >> (HW/2-1)) - 1
void xn(random)(chiffre *a, long la) {
  long i;
  chiffre mask = ((chiffre)1<<(HW/2)) - 1;
  for (i=0; i < la; a[i++] = (random() & mask) + ((chiffre)(random()) << (HW/2)));
#if chiffres_per_long == 2
  if (la&1) {random(); random();}
#endif
}

#elif RAND_MAX >= (BASE_2 >> (3*HW/4-1)) - 1
void xn(random)(chiffre *a, long la) {
  long i;
  chiffre mask = ((chiffre)1<<(HW/4)) - 1, c;
  for (i=0; i < la; i++) {
      c = random() & mask;
      c = (c << (HW/4)) + (random() & mask);
      c = (c << (HW/4)) + (random() & mask);
      c = (c << (HW/4)) + (random() & mask);
      a[i] = c;
  }
#if chiffres_per_long == 2
  if (la&1) {random(); random(); random(); random();}
#endif
}

#else
#error "unexpected small value for RAND_MAX"
#endif

