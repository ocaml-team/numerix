// file kernel/n/c/fftmul.c: multiplication and remainder with FFT
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |            Multiplication par transform�e de Fourier rapide           |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* D�finir le symbole verbose pour afficher les param�tres utilis�s */
#undef verbose


                      /* +------------------+
                         |  Multiplication  |
                         +------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb <= la
  c = naturel de longuaur la+lb non confondu avec a ou b

  sortie :
  c <- a*b
*/

#ifndef assembly_sn_fftmul
#ifdef debug_fftmul
void xn(fftmul_buggy)
#else
void xn(fftmul)
#endif
(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
  long f,i,j,k, l = la+lb, n1,n2,n3,p;
  chiffre *x,*y,*z,*t;
#ifdef verbose
  static long lsav = 0;
#endif

  /*
    choix de l'algorithme :
    on cherche k tel que fftmul_tab[k-1] < l <= fftmull_tab[k]
    si k = 0, on effectue une multiplication compl�te
    si k = 1, on utilise smul(12*[f..f+2])
    si k = 2, on utilise smul(24*[f..f+2])
    sinon, on utilise FFT(2^(k+4)*[f..f+2])

    soit i = 12,24 ou 2^(k+4) : on d�termine f de sorte que 6*i*f soit l�g�rement
    inf�rieur � la longueur l du produit � calculer. Les p = l - 6*i*f chiffres
    manquants sont calcul�s par une multiplication d'appoint.
    Pour k = 2 ou 3 (smul), on prend 6*i*f >= (9/10)*l,
    pour k > 3 (FFT),       on prend 6*i*f >= (19/20)*l.
  */
static long fftmul_tab[] = {fftmul_lim,
                            fftmul_lim_1,
                            fftmul_lim_2,
                            fftmul_lim_3,
                            fftmul_lim_4,
                            fftmul_lim_5,
                            fftmul_lim_6,
                            fftmul_lim_7,
                            fftmul_lim_8};

  /* aiguillage selon la longueur */
  if (2*lb <= fftmul_tab[0]) {xn(toommul)(a,la,b,lb,c); return;}
  for (k=1; (k < sizeof(fftmul_tab)/sizeof(long)) && (l > fftmul_tab[k]); k++);

  /* utilisation de smul */
  if (k <= 2) {

    i = 12*k;
    f = (l - l/10 + 6*i - 1)/(6*i);
    p = l - 6*i*f;
    if (p < 0) {
#ifndef use_clong
      f--;  p += 6*i; if (p < 0)
#endif
        p = 0;}
#ifdef verbose
    if (l > lsav) {
      printf(" l=%ld k=%ld f=%ld i=%ld p=%ld ",l,k,f,i,p);
      lsav = l;
    }
#endif

    x = xn(alloc)((6*f+3)*i + p);
    y = x + (2*f+2)*i; z = y + (2*f+1)*i; t = z + 2*f*i;

    /* multiplications mod (BASE^((2f+2)i - 1), (BASE^((2f+1)i - 1), (BASE^(2fi) - 1) */
    xn(smul)( a,la,b,lb, x, y-x);
    xn(smul)( a,la,b,lb, y, z-y);
    xn(smul)( a,la,b,lb, z, t-z);

    if (p) {

      /* calcule a*b mod BASE^p */
      xn(fftmul)(a,p, b,(lb >= p) ? p : lb, c);

      /* r�percute sur les r�sidus pr�c�dents */
      if (xn(sub)(z,p,c,p,t)) xn(dec1)(z+p,t-z);
      if (xn(sub)(y,p,c,p,z)) xn(dec1)(y+p,z-y);
      if (xn(sub)(x,p,c,p,y)) xn(dec1)(x+p,y-x);
    }

    /* recombinaison */
    xn(sjoin3)(x+p,f,i);
    xn(move)(x+p,l-p,c+p);
    xn(free)(x);
  }

  /* utilisation de FFT */
  else {
    k += 4;
    f = (l  - l/20 + ((long)6<<k)-1)/((long)6<<k);
    i = (k < hw+2) ? 1 : (k==hw+2) ? 2 : (long)1 << (k-2-hw);
    n1 = 4*f + 4 + i; n1 -= n1 & (i-1); n1 = xn(fft_improve)(n1,i);
    n2 = 4*f + 2 + i; n2 -= n2 & (i-1); n2 = xn(fft_improve)(n2,i);
    n3 = 4*f     + i; n3 -= n3 & (i-1); n3 = xn(fft_improve)(n3,i);
    if (2*k <= HW) {
      f = (n3-1)/4;
      if (4*f+3 > n2) f = (n2-3)/4;
      if (4*f+5 > n1) f = (n1-5)/4;
    } else {
      f = (n3-2)/4;
      if (4*f+4 > n2) f = (n2-4)/4;
      if (4*f+6 > n1) f = (n1-6)/4;
    }
    p = l - ((long)6<<k)*f; if (p < 0) p = 0;
#ifdef verbose
    if (l > lsav) {
      printf(" l=%ld k=%ld f=%ld i=%ld p=%ld n=%ld n=%ld n=%ld ",l,k,f,(long)1<<k,p,n1,n2,n3);
      lsav = l;
    }
#endif

    /* alloue un tampon suffisament grand pour tous les calculs interm�diaires */
    xn(check_lmax)(6*f,k); /* v�rifie que (6<<k)*f < LMAX */
    j = ((6*f+3)<<k) + p;
    i = (4*f+2*n3+5)<<k; if (j < i) j = i;
    i = (2*f+2*n2+4)<<k; if (j < i) j = i;
    i = (2*n1+2)<<k;     if (j < i) j = i;
    x = xn(alloc)(j);

    /* multiplication mod (BASE^((2f+2)<<k) - 1) */
    y = x; z = x + ((n1+1)<<k);
    xn(fft_split)(a,la,y,n1,k,2*f+2); xn(fft)(y,n1,k);
    xn(fft_split)(b,lb,z,n1,k,2*f+2); xn(fft)(z,n1,k);
    for (j=0; j>>k == 0; j++) xn(mmul)(y+(n1+1)*j,z+(n1+1)*j,n1);
    xn(fft_inv)(y,n1,k); xn(fft_merge)(y,y,n1,k,2*f+2);

    /* multiplication mod (BASE^((2f+1)<<k) - 1) */
    y += ((2*f+2)<<k); z = y + ((n2+1)<<k);
    xn(fft_split)(a,la,y,n2,k,2*f+1); xn(fft)(y,n2,k);
    xn(fft_split)(b,lb,z,n2,k,2*f+1); xn(fft)(z,n2,k);
    for (j=0; j>>k == 0; j++) xn(mmul)(y+(n2+1)*j,z+(n2+1)*j,n2);
    xn(fft_inv)(y,n2,k); xn(fft_merge)(y,y,n2,k,2*f+1);

    /* multiplication mod (BASE^((2f)<<k) - 1) */
    y += ((2*f+1)<<k); z = y + ((n3+1)<<k);
    xn(fft_split)(a,la,y,n3,k,2*f); xn(fft)(y,n3,k);
    xn(fft_split)(b,lb,z,n3,k,2*f); xn(fft)(z,n3,k);
    for (j=0; j>>k == 0; j++) xn(mmul)(y+(n3+1)*j,z+(n3+1)*j,n3);
    xn(fft_inv)(y,n3,k); xn(fft_merge)(y,y,n3,k,2*f);

    if (p) {

      /* calcule a*b mod BASE^p */
      xn(fftmul)(a,p, b,(lb >= p) ? p : lb, c);


      /* r�percute sur les r�sidus pr�c�dents */
      y = x + ((2*f+2)<<k);
      z = y + ((2*f+1)<<k);
      t = z + ((2*f)<<k);
      if (xn(sub)(z,p,c,p,t)) xn(dec1)(z+p,t-z);
      if (xn(sub)(y,p,c,p,z)) xn(dec1)(y+p,z-y);
      if (xn(sub)(x,p,c,p,y)) xn(dec1)(x+p,y-x);
    }

    /* recombinaison */
    xn(sjoin3)(x+p,f,(long)1<<k);
    xn(move)(x+p,l-p,c+p);
    xn(free)(x);
  }

}
#endif /* assembly_sn_fftmul */


/* -------------------- Contr�le */

#ifdef debug_fftmul
void xn(fftmul_buggy)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);
void xn(fftmul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
  chiffre *d;

  /* compare les r�sultats produits par fftmul_buggy et par toommul */
  d = xn(alloc)(la+lb);
  xn(toommul)(a,la,b,lb,d);
  xn(fftmul_buggy)(a,la,b,lb,c);
  if (xn(cmp)(c,la+lb,d,la+lb))
      xn(internal_error)("error in fftmul",4,a,la,b,lb,c,la+lb,d,la+lb);

  xn(free)(d);
}
#endif /* debug_fftmul */

                   /* +------------------------+
                      |  Reste d'une division  |
                      +------------------------+ */

/*
  entr�e :
  a = naturel de longueur lb+lc
  b = naturel de longueur lb
  c = naturel de longuaur lc

  contraintes :
  0 < lc < lb, le bit de poids fort de b est non nul,
  -b <= a - b*c < b
  a,b,c non confondus

  sortie :
  c <- floor(a/b)
  a <- a mod b
*/

#ifndef assembly_sn_remdiv
void xn(remdiv)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c) {
  long f,i,j,k,l = lb+1, n1,n2,n3,p;
  chiffre *x,*y,*z,*t;
#ifdef verbose
  static long lsav = 0;
#endif

  /*
    choix de l'algorithme :
    on cherche k tel que remdiv_tab[k-1] < l <= remdiv_tab[k]
    si k = 0, on effectue une multiplication compl�te
    si k = 1, on utilise smul(12*[f..f+2])
    si k = 2, on utilise smul(24*[f..f+2])
    sinon, on utilise FFT(2^(k+4)*[f..f+2])

    soit i = 12,24 ou 2^(k+4) : on d�termine f de sorte que 6*i*f soit l�g�rement
    inf�rieur � la longueur l du produit � calculer. Les p = l - 6*i*f chiffres
    manquants sont calcul�s par une multiplication d'appoint.
    Pour k = 1 ou 2 (smul), on prend 6*i*f >= (9/10)*l,
    pour k > 2 (FFT),       on prend 6*i*f >= (19/20)*l.
  */
static long remdiv_tab[] = {remdiv_lim,
                            fftmul_lim_1,
                            fftmul_lim_2,
                            fftmul_lim_3,
                            fftmul_lim_4,
                            fftmul_lim_5,
                            fftmul_lim_6,
                            fftmul_lim_7,
                            fftmul_lim_8};

  /* si c > 0, diminue c d'une unit� pour avoir a - b*c >= 0 */
  if (xn(dec1)(c,lc)) {xn(clear)(c,lc); return;}

  /* petite multiplication */
  if (lc <= remdiv_tab[0]) {
#ifdef verbose
    if (l > lsav) {
      printf(" lc=%ld k=0 ",lc);
      lsav = l;
    }
#endif
    x = xn(alloc)(lb+lc);
    xn(toommul)(b,lb,c,lc,x);
    xn(sub)(a,l,x,l,x);
  }
  else {
    for (k=1; (k < sizeof(remdiv_tab)/sizeof(long)) && (l > remdiv_tab[k]); k++);

    /* utilisation de smul */
    if (k <= 2) {

      i = 12*k;
      f = (l - l/10 + 6*i - 1)/(6*i);
      p = l - 6*i*f;
      if (p < 0) {
#ifndef use_clong
        f--;  p += 6*i; if (p < 0)
#endif
          p = 0;}
#ifdef verbose
      if (l > lsav) {
        printf(" l=%ld k=%ld f=%ld i=%ld p=%ld ",l,k,f,i,p);
        lsav = l;
      }
#endif

      x = xn(alloc)((6*f+3)*i + 3*p);
      y = x + (2*f+2)*i; z = y + (2*f+1)*i; t = z + 2*f*i;

      /* multiplications mod (BASE^((2f+2)i - 1), (BASE^((2f+1)i - 1), (BASE^(2fi) - 1) */
      xn(smul)( b,lb,c,lc, x, y-x); xn(ssub)( a,lb+lc, x, y-x);
      xn(smul)( b,lb,c,lc, y, z-y); xn(ssub)( a,lb+lc, y, z-y);
      xn(smul)( b,lb,c,lc, z, t-z); xn(ssub)( a,lb+lc, z, t-z);
    }

    /* utilisation de FFT */
    else {
      k += 4;
      f = (l  - l/20 + ((long)6<<k)-1)/((long)6<<k);
      i = (k < hw+2) ? 1 : (k==hw+2) ? 2 : (long)1 << (k-2-hw);
      n1 = 4*f + 4 + i; n1 -= n1 & (i-1); n1 = xn(fft_improve)(n1,i);
      n2 = 4*f + 2 + i; n2 -= n2 & (i-1); n2 = xn(fft_improve)(n2,i);
      n3 = 4*f     + i; n3 -= n3 & (i-1); n3 = xn(fft_improve)(n3,i);
      if (2*k <= HW) {
        f = (n3-1)/4;
        if (4*f+3 > n2) f = (n2-3)/4;
        if (4*f+5 > n1) f = (n1-5)/4;
      } else {
        f = (n3-2)/4;
        if (4*f+4 > n2) f = (n2-4)/4;
        if (4*f+6 > n1) f = (n1-6)/4;
      }
      p = l - ((long)6<<k)*f; if (p < 0) p = 0;
#ifdef verbose
      if (l > lsav) {
        printf(" l=%ld k=%ld f=%ld i=%ld p=%ld n=%ld n=%ld n=%ld ",l,k,f,(long)1<<k,p,n1,n2,n3);
        lsav = l;
      }
#endif

      /* alloue un tampon suffisament grand pour tous les calculs interm�diaires */
      xn(check_lmax)(6*f,k); /* v�rifie que (6<<k)*f < LMAX */
      j = ((6*f+3)<<k) + 3*p;
      i = (4*f+2*n3+5)<<k; if (j < i) j = i;
      i = (2*f+2*n2+4)<<k; if (j < i) j = i;
      i = (2*n1+2)<<k;     if (j < i) j = i;
      x = xn(alloc)(j);

      /* multiplication mod (BASE^((2f+2)<<k) - 1) */
      y = x; z = x + ((n1+1)<<k);
      xn(fft_split)(c,lc,y,n1,k,2*f+2); xn(fft)(y,n1,k);
      xn(fft_split)(b,lb,z,n1,k,2*f+2); xn(fft)(z,n1,k);
      for (j=0; j>>k == 0; j++) xn(mmul)(y+(n1+1)*j,z+(n1+1)*j,n1);
      xn(fft_inv)(y,n1,k); xn(fft_merge)(y,y,n1,k,2*f+2);

      /* multiplication mod (BASE^((2f+1)<<k) - 1) */
      y += ((2*f+2)<<k); z = y + ((n2+1)<<k);
      xn(fft_split)(c,lc,y,n2,k,2*f+1); xn(fft)(y,n2,k);
      xn(fft_split)(b,lb,z,n2,k,2*f+1); xn(fft)(z,n2,k);
      for (j=0; j>>k == 0; j++) xn(mmul)(y+(n2+1)*j,z+(n2+1)*j,n2);
      xn(fft_inv)(y,n2,k); xn(fft_merge)(y,y,n2,k,2*f+1);

      /* multiplication mod (BASE^((2f)<<k) - 1) */
      y += ((2*f+1)<<k); z = y + ((n3+1)<<k);
      xn(fft_split)(c,lc,y,n3,k,2*f); xn(fft)(y,n3,k);
      xn(fft_split)(b,lb,z,n3,k,2*f); xn(fft)(z,n3,k);
      for (j=0; j>>k == 0; j++) xn(mmul)(y+(n3+1)*j,z+(n3+1)*j,n3);
      xn(fft_inv)(y,n3,k); xn(fft_merge)(y,y,n3,k,2*f);

      /* retranche les r�sidus de a */
      y = x + ((2*f+2)<<k); xn(ssub)( a,lb+lc, x, y-x);
      z = y + ((2*f+1)<<k); xn(ssub)( a,lb+lc, y, z-y);
      t = z + ((2*f)<<k);   xn(ssub)( a,lb+lc, z, t-z);
      i = (long)1 << k;
    }

    if (p) {

      /* calcule a - b*c mod BASE^p */
      xn(fftmul)(b,p, c,(lc >= p) ? p : lc, t+p);
      xn(sub)(a,p,t+p,p,t+p);

      /* r�percute sur les r�sidus pr�c�dents */
      if (xn(sub)(z,p,t+p,p,t)) xn(dec1)(z+p,t-z);
      if (xn(sub)(y,p,t+p,p,z)) xn(dec1)(y+p,z-y);
      if (xn(sub)(x,p,t+p,p,y)) xn(dec1)(x+p,y-x);
      xn(move)(t+p,p,x);
    }

    /* recombinaison */
    xn(sjoin3)(x+p,f,i);
  }

  /* v�rifie que a - b*c < b, sinon corrige c */
  if (xn(cmp)(x,l,b,lb) < 0) {xn(move)(x,lb,a);}
  else {xn(inc1)(c,lc); xn(sub)(x,l,b,lb,a);}

  /* termin� */
  xn(free)(x);

}
#endif /* assembly_remdiv */



                           /* +---------+
                              |  Carr�  |
                              +---------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longuaur 2*la, non confondu avec a

  sortie :
  b <- a^2
*/

#ifndef assembly_sn_fftsqr
#ifdef debug_fftmul
void xn(fftsqr_buggy)
#else
void xn(fftsqr)
#endif
(chiffre *a, long la, chiffre *b) {
  long f,i,j,k, l = 2*la, n1,n2,n3,p;
  chiffre *x,*y,*z,*t;
#ifdef verbose
  static long lsav = 0;
#endif

  /*
    choix l'algorithme :
    on cherche k tel que fftsqr_tab[k-1] < l <= fftsqr_tab[k]
    si k = 0, on effectue un carr� complet
    si k = 1, on utilise ssqr(12*[f..f+2])
    si k = 2, on utilise ssqr(24*[f..f+2])
    sinon, on utilise FFT(2^(k+4)*[f..f+2])

    soit i = 12,24 ou 2^(k+4) : on d�termine f de sorte que 6*i*f soit l�g�rement
    inf�rieur � la longueur l du carr� � calculer. Les p = l - 6*i*f chiffres
    manquants sont calcul�s par un carr� d'appoint.
    Pour k = 2 ou 3 (ssqr), on prend 6*i*f >= (9/10)*l,
    pour k > 3 (FFT),       on prend 6*i*f >= (19/20)*l.
  */
static long fftsqr_tab[] = {fftsqr_lim,
                            fftsqr_lim_1,
                            fftsqr_lim_2,
                            fftsqr_lim_3,
                            fftsqr_lim_4,
                            fftsqr_lim_5,
                            fftsqr_lim_6,
                            fftsqr_lim_7,
                            fftsqr_lim_8};

  /* aiguillage selon la longueur */
  if (l <= fftsqr_tab[0]) {xn(toomsqr)(a,la,b); return;}
  for (k=1; (k < sizeof(fftsqr_tab)/sizeof(long)) && (l > fftsqr_tab[k]); k++);

  /* utilisation de ssqr */
  if (k <= 2) {

    i = 12*k;
    f = (l - l/10 + 6*i - 1)/(6*i);
    p = l - 6*i*f;
    if (p < 0) {
#ifndef use_clong
      f--;  p += 6*i; if (p < 0)
#endif
        p = 0;}
#ifdef verbose
    if (l > lsav) {
      printf(" l=%ld k=%ld f=%ld i=%ld p=%ld ",l,k,f,i,p);
      lsav = l;
    }
#endif

    x = xn(alloc)((6*f+3)*i + p);
    y = x + (2*f+2)*i; z = y + (2*f+1)*i; t = z + 2*f*i;

    /* carr�s mod (BASE^((2f+2)i - 1), (BASE^((2f+1)i - 1), (BASE^(2fi) - 1) */
    xn(ssqr)( a,la, x, y-x);
    xn(ssqr)( a,la, y, z-y);
    xn(ssqr)( a,la, z, t-z);

    if (p) {

      /* calcule a^2 mod BASE^p et r�percute sur les r�sidus pr�c�dents */
      xn(fftsqr)(a,p,b);
      if (xn(sub)(z,p,b,p,t)) xn(dec1)(z+p,t-z);
      if (xn(sub)(y,p,b,p,z)) xn(dec1)(y+p,z-y);
      if (xn(sub)(x,p,b,p,y)) xn(dec1)(x+p,y-x);
    }

    /* recombinaison */
    xn(sjoin3)(x+p,f,i);
    xn(move)(x+p,l-p,b+p);
    xn(free)(x);

  }

  /* utilisation de FFT */
  else {

    k += 4;
    f = (l - l/20 +((long)6<<k)-1)/((long)6<<k);
    i = (k < hw+2) ? 1 : (k==hw+2) ? 2 : (long)1 << (k-2-hw);
    n1 = 4*f + 4 + i; n1 -= n1 & (i-1); n1 = xn(fft_improve)(n1,i);
    n2 = 4*f + 2 + i; n2 -= n2 & (i-1); n2 = xn(fft_improve)(n2,i);
    n3 = 4*f     + i; n3 -= n3 & (i-1); n3 = xn(fft_improve)(n3,i);
    if (2*k <= HW) {
      f = (n3-1)/4;
      if (4*f+3 > n2) f = (n2-3)/4;
      if (4*f+5 > n1) f = (n1-5)/4;
    } else {
      f = (n3-2)/4;
      if (4*f+4 > n2) f = (n2-4)/4;
      if (4*f+6 > n1) f = (n1-6)/4;
    }
    p = l - ((long)6<<k)*f; if (p < 0) p = 0;
#ifdef verbose
    if (l > lsav) {
      printf(" l=%ld k=%ld f=%ld i=%ld p=%ld n=%ld n=%ld n=%ld ",l,k,f,(long)1<<k,p,n1,n2,n3);
      lsav = l;
    }
#endif

    /* alloue un tampon suffisament grand pour tous les calculs interm�diaires */
    xn(check_lmax)(6*f,k); /* v�rifie que (6<<k)*f < LMAX */
    j = ((6*f+3)<<k) + p;
    i = (4*f+n3+4)<<k; if (j < i) j = i;
    i = (2*f+n2+3)<<k; if (j < i) j = i;
    i = (n1+1)<<k;     if (j < i) j = i;
    x = xn(alloc)(j);

    /* carr� mod (BASE^((2f+2)<<k) - 1) */
    y = x;
    xn(fft_split)(a,la,y,n1,k,2*f+2); xn(fft)(y,n1,k);
    for (j=0; j>>k == 0; j++) xn(msqr)(y+(n1+1)*j,n1);
    xn(fft_inv)(y,n1,k); xn(fft_merge)(y,y,n1,k,2*f+2);

    /* carr� mod (BASE^((2f+1)<<k) - 1) */
    y += ((2*f+2)<<k);
    xn(fft_split)(a,la,y,n2,k,2*f+1); xn(fft)(y,n2,k);
    for (j=0; j>>k == 0; j++) xn(msqr)(y+(n2+1)*j,n2);
    xn(fft_inv)(y,n2,k); xn(fft_merge)(y,y,n2,k,2*f+1);

    /* carr� mod (BASE^((2f)<<k) - 1) */
    y += ((2*f+1)<<k);
    xn(fft_split)(a,la,y,n3,k,2*f); xn(fft)(y,n3,k);
    for (j=0; j>>k == 0; j++) xn(msqr)(y+(n3+1)*j,n3);
    xn(fft_inv)(y,n3,k); xn(fft_merge)(y,y,n3,k,2*f);

    if (p) {

      /* calcule a^2 mod BASE^p */
      xn(fftsqr)(a,p,b);

      /* r�percute sur les r�sidus pr�c�dents */
      y = x + ((2*f+2)<<k);
      z = y + ((2*f+1)<<k);
      t = z + ((2*f)<<k);
      if (xn(sub)(z,p,b,p,t)) xn(dec1)(z+p,t-z);
      if (xn(sub)(y,p,b,p,z)) xn(dec1)(y+p,z-y);
      if (xn(sub)(x,p,b,p,y)) xn(dec1)(x+p,y-x);
    }

    /* recombinaison */
    xn(sjoin3)(x+p,f,(long)1<<k);
    xn(move)(x+p,l-p,b+p);
    xn(free)(x);
  }

}
#endif /* assembly_sn_fftsqr */


/* -------------------- Contr�le */

#ifdef debug_fftmul
void xn(fftsqr_buggy)(chiffre *a, long la, chiffre *b);
void xn(fftsqr)(chiffre *a, long la, chiffre *b) {
  chiffre *d;

  /* compare les r�sultats produits par fftsqr_buggy et par toomsqr */
  d = xn(alloc)(2*la);
  xn(toomsqr)(a,la,d);
  xn(fftsqr_buggy)(a,la,b);
  if (xn(cmp)(b,2*la,d,2*la))
      xn(internal_error)("error in fftsqr",3,a,la,b,2*la,d,2*la);

  xn(free)(d);
}
#endif /* debug_fftmul */

                      /* +-----------------------------+
                         |  Reste d'une racine carr�e  |
                         +-----------------------------+ */


/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la/2

  contraintes :
  la > 0, pair.
  le bit de poids fort de b est non nul,
  -b <= a - b^2/4 < b
  a,b non confondus

  sortie :
  b <- 2*floor(sqrt(a))
  a <- a - b^2/4
*/

#ifndef assembly_sn_remsqrt
void xn(remsqrt)(chiffre *a, long la, chiffre *b) {
  long f,i,j,k,l = la/2+1, n1,n2,n3,p;
  chiffre *x,*y,*z,*t;
#ifdef verbose
  static long lsav = 0;
#endif

  /*
    choix de l'algorithme :
    on cherche k tel que remsqrt_tab[k-1] < l <= remsqrt_tab[k]
    si k = 0, on effectue un carr� complet
    si k = 1, on utilise ssqrl(12*[f..f+2])
    si k = 2, on utilise ssqrl(24*[f..f+2])
    sinon, on utilise FFT(2^(k+4)*[f..f+2])

    soit i = 12,24 ou 2^(k+4) : on d�termine f de sorte que 6*i*f soit l�g�rement
    inf�rieur � la longueur l du carr� � calculer. Les p = l - 6*i*f chiffres
    manquants sont calcul�s par un carr� d'appoint.
    Pour k = 1 ou 2 (ssqr), on prend 6*i*f >= (9/10)*l,
    pour k > 2 (FFT),       on prend 6*i*f >= (19/20)*l.
  */
 static long remsqrt_tab[] = {remsqrt_lim,
                            fftsqr_lim_1,
                            fftsqr_lim_2,
                            fftsqr_lim_3,
                            fftsqr_lim_4,
                            fftsqr_lim_5,
                            fftsqr_lim_6,
                            fftsqr_lim_7,
                            fftsqr_lim_8};

  /* divise b par 2 et diminue d'une unit� pour avoir a - b^2 >= 0 */
  xn(shift_down)(b,l-1,b,1);
  xn(dec1)(b,l-1);

  /* petit carr� */
  if (l <= remsqrt_tab[0]) {
#ifdef verbose
    if (l > lsav) {
      printf(" l=%ld k=0 ",l); fflush(stdout);
      lsav = l;
    }
#endif
    x = xn(alloc)(2*l-2);
    xn(toomsqr)(b,l-1,x);
    xn(sub)(a,l,x,l,x);
  }
  else {
    for (k=1; (k < sizeof(remsqrt_tab)/sizeof(long)) && (l > remsqrt_tab[k]); k++);

    /* utilisation de ssqr */
    if (k <= 2) {

      i = 12*k;
      f = (l - l/10 + 6*i - 1)/(6*i);
      p = l - 6*i*f;
      if (p < 0) {
#ifndef use_clong
        f--;  p += 6*i; if (p < 0)
#endif
          p = 0;}
#ifdef verbose
      if (l > lsav) {
        printf(" l=%ld k=%ld f=%ld i=%ld p=%ld ",l,k,f,i,p); fflush(stdout);
        lsav = l;
      }
#endif

      x = xn(alloc)((6*f+3)*i + 3*p);
      y = x + (2*f+2)*i; z = y + (2*f+1)*i; t = z + 2*f*i;

      /* carr�s mod (BASE^((2f+2)i - 1), (BASE^((2f+1)i - 1), (BASE^(2fi) - 1) */
      xn(ssqr)( b,l-1, x, y-x); xn(ssub)( a,la, x, y-x);
      xn(ssqr)( b,l-1, y, z-y); xn(ssub)( a,la, y, z-y);
      xn(ssqr)( b,l-1, z, t-z); xn(ssub)( a,la, z, t-z);
    }

    /* utilisation de FFT */
    else {
      k += 4;
      f = (l  - l/20 + ((long)6<<k)-1)/((long)6<<k);
      i = (k < hw+2) ? 1 : (k==hw+2) ? 2 : (long)1 << (k-2-hw);
      n1 = 4*f + 4 + i; n1 -= n1 & (i-1); n1 = xn(fft_improve)(n1,i);
      n2 = 4*f + 2 + i; n2 -= n2 & (i-1); n2 = xn(fft_improve)(n2,i);
      n3 = 4*f     + i; n3 -= n3 & (i-1); n3 = xn(fft_improve)(n3,i);
      if (2*k <= HW) {
        f = (n3-1)/4;
        if (4*f+3 > n2) f = (n2-3)/4;
        if (4*f+5 > n1) f = (n1-5)/4;
      } else {
        f = (n3-2)/4;
        if (4*f+4 > n2) f = (n2-4)/4;
        if (4*f+6 > n1) f = (n1-6)/4;
      }
      p = l - ((long)6<<k)*f; if (p < 0) p = 0;
#ifdef verbose
      if (l > lsav) {
        printf(" l=%ld k=%ld f=%ld i=%ld p=%ld n=%ld n=%ld n=%ld ",l,k,f,(long)1<<k,p,n1,n2,n3); fflush(stdout);
        lsav = l;
      }
#endif

      /* alloue un tampon suffisament grand pour tous les calculs interm�diaires */
      xn(check_lmax)(6*f,k); /* v�rifie que (6<<k)*f < LMAX */
      j = ((6*f+3)<<k) + 3*p;
      i = (4*f+n3+4)<<k; if (j < i) j = i;
      i = (2*f+n2+3)<<k; if (j < i) j = i;
      i = (n1+1)<<k;     if (j < i) j = i;
      x = xn(alloc)(j);

      /* carr� mod (BASE^((2f+2)<<k) - 1) */
      y = x;
      xn(fft_split)(b,l-1,y,n1,k,2*f+2); xn(fft)(y,n1,k);
      for (j=0; j>>k == 0; j++) xn(msqr)(y+(n1+1)*j,n1);
      xn(fft_inv)(y,n1,k); xn(fft_merge)(y,y,n1,k,2*f+2);

      /* carr� mod (BASE^((2f+1)<<k) - 1) */
      y += ((2*f+2)<<k);
      xn(fft_split)(b,l-1,y,n2,k,2*f+1); xn(fft)(y,n2,k);
      for (j=0; j>>k == 0; j++) xn(msqr)(y+(n2+1)*j,n2);
      xn(fft_inv)(y,n2,k); xn(fft_merge)(y,y,n2,k,2*f+1);

      /* carr� mod (BASE^((2f)<<k) - 1) */
      y += ((2*f+1)<<k);
      xn(fft_split)(b,l-1,y,n3,k,2*f); xn(fft)(y,n3,k);
      for (j=0; j>>k == 0; j++) xn(msqr)(y+(n3+1)*j,n3);
      xn(fft_inv)(y,n3,k); xn(fft_merge)(y,y,n3,k,2*f);

      /* retranche les r�sidus de a */
      y = x + ((2*f+2)<<k); xn(ssub)( a,la, x, y-x);
      z = y + ((2*f+1)<<k); xn(ssub)( a,la, y, z-y);
      t = z + ((2*f)<<k);   xn(ssub)( a,la, z, t-z);
      i = (long)1 << k;
    }

    if (p) {

      /* calcule a - b^2 mod BASE^p */
      xn(fftsqr)(b,p,t+p);
      xn(sub)(a,p,t+p,p,t+p);

      /* r�percute sur les r�sidus pr�c�dents */
      if (xn(sub)(z,p,t+p,p,t)) xn(dec1)(z+p,t-z);
      if (xn(sub)(y,p,t+p,p,z)) xn(dec1)(y+p,z-y);
      if (xn(sub)(x,p,t+p,p,y)) xn(dec1)(x+p,y-x);
      xn(move)(t+p,p,x);
    }

    /* recombinaison */
    xn(sjoin3)(x+p,f,i);
  }

  /* d�cale b et v�rifie que a - b^2/4 <= b, sinon corrige b */
  xn(shift_up)(b,l-1,b,1);
  if (xn(cmp)(x,l,b,l-1) <= 0) {xn(move)(x,l-1,a);}
  else {b[0]++; xn(sub)(x,l,b,l-1,a); xn(inc1)(b,l-1);}

  /* termin� */
  xn(free)(x);

}
#endif /* assembly_remsqrt */
