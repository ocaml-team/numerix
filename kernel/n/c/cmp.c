// file kernel/n/c/cmp.c: comparison of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Comparaison                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/


/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb

  sortie :
  1 si a > b, 0 si a = b, -1 si a < b
*/

#ifndef assembly_sn_cmp
long xn(cmp)(chiffre *a, long la, chiffre *b, long lb) {

  /* �limine les z�ros de t�te */
  while ((la > 0) && (a[la-1] == 0)) la--;
  while ((lb > 0) && (b[lb-1] == 0)) lb--;

  /* compare les longeurs */
  if (la < lb) return(-1);
  if (la > lb) return( 1);

  /* compare les chiffres */
  while ((la > 0) && (a[la-1] == b[la-1])) la--;
  if (la == 0) return(0);
  if (a[la-1] < b[la-1]) return(-1);
  return(1);

}
#endif /* assembly_sn_cmp */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb

  contraintes :
  lb >= la, lb > 0, le chiffre de poids fort de b est non nul

  sortie :
  1 si 2a > b, 0 si 2a = b, -1 si 2a < b
*/
#ifndef assembly_sn_cmp2
long xn(cmp2)(chiffre *a, long la, chiffre *b, long lb) {
#ifdef zdouble
  zdouble r;

  /* a trop court => 2a < b */
  if (la < lb-1) return(-1);

  /*
    m�mes longueurs � 1 pr�s : calcule 2a-b par chiffres d�croissants
    et arr�te d�s que le signe de la diff�rence est connu
  */
  if (la == lb) r = 2*((ndouble)a[lb-1]) - (ndouble)b[lb-1];
  else          r =                      - (ndouble)b[lb-1];
  for (lb--; (lb) && (r <= 0) && (r >= -1); lb--) {
    r = (r << HW) + 2*((ndouble)a[lb-1]) - (ndouble)b[lb-1];
  }

  return((r > 0) ? 1 : (r) ? -1 : 0);
#else
  chiffre r,s,t;

  /* a trop court => 2a < b */
  if (la < lb-1) return(-1);

  /*
    m�mes longueurs � 1 pr�s : calcule 2a-b par chiffres d�croissants
    et arr�te d�s que le signe de la diff�rence est connu
  */
  s = (la == lb) ? a[lb-1] : 0;
  t = 2*s; r = (t < s);
  s = t - b[lb-1]; r -= (s > t);
  for (lb--; (lb) && (r == s) && ((r == 0) || (r == (BASE_2)+(BASE_2-1))); lb--) {
    s = a[lb-1];
    t = 2*s;         r += (t < s);
    s = t - b[lb-1]; r -= (s > t);
  }

  return((r == 1) ? 1 : (r) ? -1 : (s != 0));
#endif /* zdouble */
}
#endif /* assembly_sn_cmp2 */

