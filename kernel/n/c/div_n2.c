// file kernel/n/c/div_n2.c: O(n^2) division of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Division quadratique                          |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                        /* +-------------------------+
                           |  Division par un long   |
                           +-------------------------+ */

/*
  entr�e :
  a = naturel de longueur la >= 0
  b = long > 0
  c = naturel de longueur la, peut �tre confondu avec a

  sortie :
  c <- floor(a/b)
  retourne a mod b
*/
#ifndef assembly_sn_div_1
unsigned long xn(div_1)(chiffre *a, long la, unsigned long b, chiffre *c) {

#ifdef ndouble
  ndouble r;

#if chiffres_per_long == 2
  if (b > 2*BASE_2) { /* division � deux chiffres */
    ndouble b0,b1,q,s,t;
    long l;

    /* �vacue les petits dividendes */
    if (la == 0) return(0);
    if (la == 1) {r = (ndouble)a[0]; c[0] = 0; return(r);}

    /* d�cale b pour avoir b1 >= BASE/2 */
    for (l=HW; (b & SIGN_m) == 0; l--, b<<=1);
    b0 = b & (2*BASE_2-1);
    b1 = b >> HW;

    /* divise a*2^(HW-l) par b*2^(HW-l) */
    a += la-3; c += la-2; la -= 2;
    r = ((ndouble)a[2] << HW) + (ndouble)a[1];
    c[1] = 0;
    s = r >> l;
    for (; la>=0; la--, a--, c--) {
      if (la) r = (r<<HW) + (ndouble)a[0]; else r <<= HW;
      q = s/b1;
      s = ((s - q*b1) << HW) + ((r >> l) & (2*BASE_2-1));
      t = q*b0;
      while (s < t) {q--; t -= s; s = b;}
      c[0] = q;
      s -= t;
    }

    /* retourne le reste d�cal� de HW-l bits */
    return(s>>(HW-l));

  }
#endif /* chiffres_per_long == 2 */

  for (r = 0, a+=la-1, c+=la-1; la ; la--, a--, c--) {
    r = (r << HW) + (ndouble)a[0];
    c[0] = r/b;
    r %= b;
  }

  return(r);

#else /* pas de ndouble */
  chiffre q,r0,r1;

#if chiffres_per_long == 2
  if (b > 2*BASE_2) { /* division � deux chiffres */
    chiffre b0,b1,s0,s1,t0,t1;
    long l;

    /* �vacue les petits dividendes */
    if (la == 0) return(0);
    if (la == 1) {r0 = a[0]; c[0] = 0; return((unsigned long)r0);}

    /* d�cale b pour avoir b1 >= BASE/2 */
    for (l=HW; (b & SIGN_m) == 0; l--, b<<=1);
    b0 = b & (2*BASE_2-1);
    b1 = b >> HW;

    /* divise a*2^(HW-l) par b*2^(HW-l) */
    a += la-3; c += la-2; la -= 2;
    r0 = a[1]; r1 = a[2];
    c[1] = 0;
    s1 = r1 >> l; s0 = (r1 << (HW-l)) + (r0 >> l);
    for (; la>=0; la--, a--, c--) {
      r1 = r0; r0 = (la) ? a[0] : 0;
      if (s1 < b1) {
	xn(div_0)(s0,s1,b1,&q,&t1);
	s1 = t1; s0 = (r1 << (HW-l)) + (r0 >> l);
	xn(mul_0)(b0,q,&t0,&t1);
      }
      else {
	q  = 0;  /* en fait BASE, mais ici s0 < b0 donc on va d�cr�menter */
	s1 = s0; /* q dans la boucle suivante, ce qui donnera bien BASE-1 */
	s0 = (r1 << (HW-l)) + (r0 >> l);
	t1 = b0;
	t0 = 0;
      }
      while ((s1 < t1) || ((s1 == t1) && (s0 < t0))) {
	q--;
	t1 -= s1 + (t0 < s0); t0 -= s0;
	s0 = b0; s1 = b1;
      }
      c[0] = q;
      s1 -= t1 + (s0 < t0); s0 -= t0;
    }

    /* retourne le reste d�cal� de HW-l bits */
    return( (s0 >> (HW-l)) + (s1 << l) );

  }
#endif /* chiffres_per_long == 2 */

  for (q=0, a+=la-1, c+=la-1; la ; la--, a--, c--) {
    r1 = q;  r0 = a[0];
    xn(div_0)(r0,r1,b,c,&q);
  }

  return(q);

#endif /* ndouble */
}
#endif /* assembly_sn_div_1 */

                   /* +-----------------------------------+
                      |  Division sans reste par un long  |
                      +-----------------------------------+ */
/*
  entr�e :
  a = naturel de longueur la >= 0
  b = long > 0

  sortie :
  retourne a mod b
*/
#ifndef assembly_sn_mod_1
unsigned long xn(mod_1)(chiffre *a, long la, unsigned long b) {

#ifdef ndouble
  ndouble r;

#if chiffres_per_long == 2
  if (b > 2*BASE_2) { /* division � deux chiffres */
    ndouble b0,b1,q,s,t;
    long l;

    /* �vacuel les petits dividendes */
    if (la == 0) return(0);
    if (la == 1) return((ndouble)a[0]);

    /* d�cale b pour avoir b >= BASE/2 */
    for (l=HW; (b & SIGN_m) == 0; l--, b<<=1);
    b0 = b & (2*BASE_2-1);
    b1 = b >> HW;

    /* divise a*2^(HW-l) par b*2^(HW-l) */
    a += la-3; la -= 2;
    r = ((ndouble)a[2] << HW) + (ndouble)a[1];
    s = r >> l;
    for (; la>=0; la--, a--) {
      if (la) r = (r<<HW) + (ndouble)a[0]; else r <<= HW;
      q = s/b1;
      s = ((s - q*b1) << HW) + ((r >> l) & (2*BASE_2-1));
      t = q*b0;
      while (s < t) {q--; t -= s; s = b;}
      s -= t;
    }

    /* retourne le reste d�cal� de HW-l bits */
    return(s>>(HW-l));

  }
#endif /* chiffres_per_long == 2 */

  for (r = 0, a+=la-1; la ; la--, a--) {
    r = (r << HW) + (ndouble)a[0];
    r %= b;
  }

  return(r);

#else /* pas de ndouble */
  chiffre c,q,r0,r1;

#if chiffres_per_long == 2
  if (b > 2*BASE_2) { /* division � deux chiffres */
    chiffre b0,b1,s0,s1,t0,t1;
    long l;

    /* �vacue les petits dividendes */
    if (la == 0) return(0);
    if (la == 1) return((unsigned long)a[0]);

    /* d�cale b pour avoir b1 >= BASE/2 */
    for (l=HW; (b & SIGN_m) == 0; l--, b<<=1);
    b0 = b & (2*BASE_2-1);
    b1 = b >> HW;

    /* divise a*2^(HW-l) par b*2^(HW-l) */
    a += la-3; c += la-2; la -= 2;
    r0 = a[1]; r1 = a[2];
    s1 = r1 >> l; s0 = (r1 << (HW-l)) + (r0 >> l);
    for (; la>=0; la--, a--, c--) {
      r1 = r0; r0 = (la) ? a[0] : 0;
      if (s1 < b1) {
	xn(div_0)(s0,s1,b1,&q,&t1);
	s1 = t1; s0 = (r1 << (HW-l)) + (r0 >> l);
	xn(mul_0)(b0,q,&t0,&t1);
      }
      else {
	q  = 0;  /* en fait BASE, mais ici s0 < b0 donc on va d�cr�menter */
	s1 = s0; /* q dans la boucle suivante, ce qui donnera bien BASE-1 */
	s0 = (r1 << (HW-l)) + (r0 >> l);
	t1 = b0;
	t0 = 0;
      }
      while ((s1 < t1) || ((s1 == t1) && (s0 < t0))) {
	q--;
	t1 -= s1 + (t0 < s0); t0 -= s0;
	s0 = b0; s1 = b1;
      }
      s1 -= t1 + (s0 < t0); s0 -= t0;
    }

    /* retourne le reste d�cal� de HW-l bits */
    return( (s0 >> (HW-l)) + (s1 << l) );

  }
#endif /* chiffres_per_long == 2 */

  for (q=0, a+=la-1, c+=la-1; la ; la--, a--, c--) {
    r1 = q;  r0 = a[0];
    xn(div_0)(r0,r1,b,&c,&q);
  }

  return(q);

#endif /* ndouble */
}
#endif /* assembly_sn_mod_1 */


                   /* +------------------------+
                      |  Division quadratique  |
                      +------------------------+ */

/*
  entr�e :
  a = naturel de longueur lc+lb
  b = naturel de longueur lb
  c = naturel de longueur lc

  contraintes :
  lb >= 2, lc > 0, le bit de poids fort de b est non nul,
  a < BASE^lc*b
  a,b,c non confondus

  sortie :
  a <- a mod b
  c <- floor(a/b)
*/

#ifndef assembly_sn_div_n2
#ifdef debug_div_n2
void xn(div_n2_buggy)
#else
void xn(div_n2)
#endif
(chiffre *a, long lc, chiffre *b, long lb, chiffre *c) {
#ifdef zdouble
  chiffre q, b1=b[lb-1];
  ndouble r;
  zdouble z;
  long i;

  for(a += lc-1, c += lc-1; lc; lc--,a--,c--) {

    /* quotient approch�, peut �tre trop grand d'au plus 2 unit�s */
    if (a[lb] >= b1) {q = (BASE_2)+(BASE_2-1);}
    else {
      r = (ndouble)a[lb-1] + ((ndouble)a[lb] << HW);
      q = r/b1;
    }

    /* a <- a - q*b */
    for(r=0, z=0, i=0; i<lb; i++) {
      r += q*(ndouble)b[i];
      z += (ndouble)a[i] - (r & ((BASE_2)+(BASE_2-1)));
      a[i] = z;
      r >>=HW; z >>= HW;
    }
    z += (ndouble)a[lb] - r;
    a[lb] = z;

    /* corrige le quotient et le reste si < 0 */
    while(a[lb]) {q--; xn(inc)(a,lb+1,b,lb);}

    /* quotient d�finitif */
    c[0] = q;
  }

#else /* pas de doubles */
  chiffre b1=b[lb-1], q,r,s,t;
  long i;

  for(a += lc-1, c += lc-1; lc; lc--,a--,c--) {

    /* quotient approch�, peut �tre trop grand d'au plus 2 unit�s */
    if (a[lb] >= b1) {q = (BASE_2)+(BASE_2-1);}
    else {
      r = a[lb-1]; s = a[lb];
      xn(div_0)(r,s,b1,&q,&t);
    }

    /* a <- a - q*b */
    for(r=0, i=0; i<lb; i++) {
      xn(mul_0)(q,b[i],&s,&t);
      s += r; r = t + (s < r);
      s = a[i] - s; r += a[i] < s;
      a[i] = s;
    }
    a[lb] -= r;

    /* corrige le quotient et le reste si < 0 */
    while(a[lb]) {q--; xn(inc)(a,lb+1,b,lb);}

    /* quotient d�finitif */
    c[0] = q;
  }
#endif /* zdouble */
}
#endif /* assembly_sn_div_n2 */

                         /* +------------+
                            |  Contr�le  |
                            +------------+ */

#ifdef debug_div_n2
void xn(div_n2_buggy)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);
void xn(div_n2)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c) {
  long la = lc+lb;
  chiffre *x, *y;

  x = xn(alloc_tmp)(2*la); y = x+la;

  /* validit� des longueurs ? */
  if ((lb < 2) || (la < lb))
      xn(internal_error)("error, div_n2 is called with lb < 2 or la < lb",0);

  /* le bit de poids fort de b est non nul ? */
  if (b[lb-1] < BASE_2)
    xn(internal_error)("error, div_n2 is called with msb(b) = 0",0);

  /* a < BASE^(la-lb)*b ? */
  if (xn(cmp)(a+lc,lb,b,lb) >= 0)
       xn(internal_error)("error, div_n2 is called with a >= BASE^lc*b",2,a,la,b,lb);

  /* effectue la division douteuse */
  xn(move)(a,la,x);
  xn(div_n2_buggy)(a,lc,b,lb,c);

  /* a_entr�e = a_sortie + b*c et a_sortie < b ?*/
  if (lc < lb) xn(toommul)(b,lb,c,lc,y); else xn(toommul)(c,lc,b,lb,y);
  if (     (xn(inc)(y,la,a,lb) != 0)
        || (xn(cmp)(x,la,y,la) != 0)
        || (xn(cmp)(a,la,b,lb) >= 0))
      xn(internal_error)("error in div_n2", 4,x,la,b,lb,a,la,c,lc);

  xn(free_tmp)(x);

}
#endif /* debug_div_n2 */

