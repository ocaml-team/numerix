// file kernel/n/c/montgomery.c: Montgomery modular exponentiation
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                   Exponentiation modulaire de Montgomery              |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                         /* +----------------------+
                            |  Division modulaire  |
                            +----------------------+ */

/*
  entr�e :
  a = naturel de longueur 2n+1
  c = naturel de longueur n
  d = -1/c mod BASE

  contraintes :
  n >= 2, a,c non confondus

  sortie :
  a[n..2n-1] <- a[0..2n-1]/BASE^n mod c, non normalis�
*/

#ifndef assembly_sn_mgdiv_n2
void xn(mgdiv_n2)(chiffre *a, chiffre *c, chiffre d, long n) {
#ifdef ndouble
  ndouble r,m;
  long i,j;

  a[2*n] = 0;

  for (i=0; i<n; i++,a++) {
    m = (d*a[0]) & ((BASE_2)+(BASE_2-1));
    for (r=0, j=0; j<n; j++) {
      r += (ndouble)a[j] + m*(ndouble)c[j];
      a[j] = r;
      r >>= HW;
    }
    while(r) {r += (ndouble)a[j]; a[j++] = r; r >>= HW;}
  }

  if (a[n]) xn(dec)(a,n,c,n);
#else
  chiffre m,r,s,t;
  long i,j;

  a[2*n] = 0;

  for (i=0; i<n; i++,a++) {
    m = d*a[0];
    for (r=0, j=0; j<n; j++) {
      xn(mul_0)(m,c[j],&s,&t);
      s += r; r = t + (s < r);
      t = s + a[j]; r += (t < s);
      a[j] = t;
    }
    while(r) {s = r + a[j]; r = (s < r); a[j++] = s;}
  }

  if (a[n]) xn(dec)(a,n,c,n);

#endif /* ndouble */
}
#else
void xn(mgdiv_n2)(chiffre *a, chiffre *c, chiffre d, long n);
#endif /* assembly_sn_mgdiv_n2 */


                       /* +---------------------------+
                          |  Inversion modulo BASE^n  |
                          +---------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la

  contrainte : a impair

  sortie :
  b <- -1/a mod BASE^la
*/

#ifndef assembly_sn_mginv
void xn(mginv)(chiffre *a, long la, chiffre *b) {
  long p,q,n;
  chiffre *x,r;

  x = xn(alloc)((5*la)/2);

  /* x[0] <- -1/a[0] mod BASE */
  for (p=1, r=1; p<HW; p <<= 1, r *= r*a[0] + 2);
  x[0] = r;

  /* double la pr�cision jusqu'� atteindre la */
  for (p=la, n=0; p > 1; p = (p+1) >> 1, n++);
  while (n) {
    n--;
    q = p-1; if (((p+q) << n) < la) q++;
    xn(fftmul)(a,p+q,x,p,x+2*q);
    xn(inc1)(x+p+2*q,q);
    xn(fftmul)(x,q,x+p+2*q,q,x+p);
    p += q;
  }

  /* recopie le r�sultat */
  xn(move)(x,la,b);
  xn(free)(x);

}
#endif /* assembly_sn_mginv */


                   /* +-----------------------------------+
                      |  Division modulaire avec inverse  |
                      +-----------------------------------+ */

/*
  entr�e :
  a = naturel de longueur 2n+1
  c = naturel de longueur n
  d = -1/c mod BASE^n

  contraintes :
  n > 0, a,c,d non confondus

  sortie :
  a[n..2n-1] <- a[0..2n-1]/BASE^n mod c, non normalis�
*/

void xn(mgdiv_i)(chiffre *a, chiffre *c, chiffre *d, long n) {
  long k;
  chiffre *x;

  if (n <= montgomery_lim_2) {

    /* cas n petit : on calcule x = d*a[0..n-1] mod BASE^n avec toommul
       puis c*x mod BASE^(n+k)-1 o� k est tel que n+k est multiple de 12.
       Comme c*x = a[0..n-1] mod BASE^n, cela permet de reconstituer c*x. */

    a[2*n] = 0;
    k = 12 - (n%12); if (k == 12) k = 0;
    x = xn(alloc_tmp)(2*n+k);

    xn(toommul)(a,n,d,n,x);
    xn(smul)(x,n,c,n,x+n,n+k); if (!xn(inc1)(x+n,n+k)) xn(dec1)(x+n,n+k);
    xn(inc)(a,2*n+1,x+n,n+k);
    if (k) xn(inc)(a+n+k,n-k+1,a,n-k+1); else xn(inc)(a+n,n+1,a,n);
    if (a[2*n]) xn(dec)(a+n,n,c,n);

    xn(free_tmp)(x);
  }

  else {

    /* cas n grand : on calcule c*x par une multiplication compl�te.
       On pourrait acc�l�rer le calcul avec des multiplications modulaires
       puisqu'on conna�t d�j� la moiti� basse de c*x ... � voir.           */

    a[2*n] = 0;
    x = xn(alloc)(3*n);

    xn(fftmul)(a,n,d,n,x);
    xn(fftmul)(x,n,c,n,x+n);
    xn(inc)(a,2*n+1,x+n,2*n);
    if (a[2*n]) xn(dec)(a+n,n,c,n);

    xn(free)(x);

  }

}

                         /* +--------------------+
                            |  mgdiv adapt� � n  |
                            +--------------------+ */

extern inline void xn(mgdiv)(chiffre *a, chiffre *c, chiffre *d, long n) {
  if (n <= montgomery_lim_1) xn(mgdiv_n2)(a,c,*d,n);
  else                       xn(mgdiv_i) (a,c,d,n);
}

                      /* +----------------------------+
                         |  Exponentiation modulaire  |
                         +----------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur lc
  d = naturel de longueur lc

  contraintes :
  lb > 0, b[lb-1] > 0, lc >= 2, c impair, c[lc-1] > 0
  d non confondu avec a,b,c

  sortie :
  d <- a^b mod c
*/

#ifdef debug_powmod
void xn(powmod_mg_buggy)
#else
void xn(powmod_mg)
#endif
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, long lc, chiffre *d) {
  chiffre u,v,*x,*y,*z,*t,*r;
  long i,j,l,n;

  /* Algorithme de la fen�tre coulissante sur 4 bits.
     On pr�calcule les puissances impaires de a modulo c de a^1 jusqu'� a^15.
     Lorsqu'on rencontre un bit de b valant 1, on exmaine les trois bits
     suivants pour d�terminer le nombre d'�l�vations au carr� � effectuer
     (tableau itab)  et par quelle puissance de a il faut multiplier le
     r�sidu obtenu (tableau jtab).

                        0 1 2 3 4 5 6 7  8  9  10  11  12  13  14  15 */
  static long itab[] = {1,1,1,1,1,1,1,1, 1, 4,  3,  4,  2,  4,  3,  4};
  static long jtab[] = {0,0,0,0,0,0,0,0, 1, 9,  5, 11,  3, 13,  7, 15};

  if (!la) {xn(clear)(d,lc); return;} /* a = 0 => d = 0 */

  /* d�calage � appliquer � c pour avoir msb = 1 */
  for (n=0, i=c[lc-1]; (i & (BASE_2)) == 0; n++, i <<= 1);

  /* m�moire n�cessaire :
      tampon pour diviser a*BASE^lc par c -> 2*la+lc+2
      inverse de c                        -> lc ou 1
      tampon de calcul                    -> 4*lc+2
      copie de a,a^3..a^15                -> 8*lc
  */

  if (lc > montgomery_lim_1) {l = 13*lc+2; i = 2*(la+1)+lc;}
  else                       {l = 12*lc+3; i = 2*(la+lc+1);}
  if (i > l) l = i;
  x = xn(alloc)(l);
  if (n) {y = x+l-lc; xn(shift_up)(c,lc,y,n);} else y = c;

  /* a <- a*BASE^lc mod c */
  xn(clear)(x,lc); x[la+lc] = xn(shift_up)(a,la,x+lc,n);
  xn(div)(x,la+1,y,lc,x+la+lc+1);
  if (n) xn(shift_down)(x,lc,x,n);
  a = x; y = a+lc;

  /* r <- -1/c mod BASE^lc */
  if (lc > montgomery_lim_1) {r = x+l-lc; xn(mginv)(c,lc,r);}
  else {r = x+l-1; for (i=1, *r=1; i<HW; i <<= 1, *r *= (*r)*c[0] + 2);}

  /* d <- a^2*BASE^lc mod c */
  xn(sqr)(a,lc,y);
  xn(mgdiv)(y,c,r,lc);
  xn(move)(y+lc,lc,d);

  /* calcule a^3..a^15 mod c*2^n */
  for (i=0; i<7; i++, y+=lc) {
    xn(mul)(d,lc,y-lc,lc,y);
    xn(mgdiv)(y,c,r,lc);
    xn(move)(y+lc,lc,y);
  }

  /* uv <- les HW+4 premiers bits de b */
  for (l=HW, u = b[--lb]; (u & (BASE_2)) == 0; u <<= 1, l--);
  v = u >> (HW-4); u <<= 4;
  if ((l < 4) && (lb)) {
    u = b[--lb];
    v += u >> (HW+l-4); u <<= 4-l;
    l += HW;
  }

  /* initialise y avec l'une des puissances calcul�es */
  i=itab[v]; j=jtab[v];
  if ((i > 1) || (l == 1)) {
    xn(move)(a + (j/2)*lc,lc,y+lc);
    v = (v << i) + (u >> (HW-i)); u <<= i;
    l -= i;
  }
  else {
    xn(move)(d,lc,y+lc);
    v = (v << 2) + (u >> (HW-2)); u <<= 2;
    l -= 2;
  }
  z = y + 2*lc + 1;

  /* exponentiation dichotomique avec fen�tre coulissante */
  while(l+lb) {

    /* recharge uv s'il reste moins de 4 bits */
    if ((l < 4) && (lb)) {
      u = b[--lb];
      v += u >> (HW+l-4); u <<= 4-l;
      l += HW;
    }
    i=itab[v&15]; j=jtab[v&15];
    v = (v << i) + (u >> (HW-i)); u <<= i;
    l -= i;

    /* y <- y^(2^i)*a^j */
    for(; i; i--) {
      xn(sqr)(y+lc,lc,z);
      xn(mgdiv)(z,c,r,lc);
      t=y; y=z; z=t;
    }
    if (j) {
      xn(mul)(y+lc,lc, a+(j/2)*lc,lc, z);
      xn(mgdiv)(z,c,r,lc);
      t=y; y=z; z=t;
    }
  }

  /* divise le dernier reste par BASE^lc */
  xn(move)(y+lc,lc,y); xn(clear)(y+lc,lc);
  xn(mgdiv)(y,c,r,lc);
  if (xn(cmp)(y+lc,lc,c,lc) >= 0) xn(sub)(y+lc,lc,c,lc,d);
  else xn(move)(y+lc,lc,d);

  /* termin� */
  xn(free)(x);

}

/* contr�le */
#ifdef debug_powmod
void xn(powmod_mg)
(chiffre *a, long la, chiffre *b, long lb, chiffre *c, long lc, chiffre *d) {
  chiffre *x;

  x = xn(alloc)(lc);
  xn(powmod_mg_buggy)(a,la,b,lb,c,lc,x);
  xn(powmod_3)       (a,la,b,lb,c,lc,d);
  if (xn(cmp)(d,lc,x,lc))
      xn(internal_error)("error in powmod_mg",5,a,la,b,lb,c,lc,x,lc,d,lc);

  xn(free)(x);

}
#endif /* debug_powmod */

