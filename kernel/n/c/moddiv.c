// file kernel/n/c/moddiv.c: division/square root with modular remainder
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |            Division et racine carr�e avec reste modulaire             |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                     /* +-------------------------------+
                        |  Division avec et sans reste  |
                        +-------------------------------+ */

/*
  entr�e :
  a = naturel de longueur lc+lb
  b = naturel de longueur lb
  c = naturel de longueur lc
  rem = 0 ou 1 ou 2

  contraintes :
  lb >= 2, lc > 0, le bit de poids fort de b est non nul,
  a < BASE^lc*b
  a,b,c non confondus

  sortie si rem = 0 :
    a <- ind.
    c <- approx(a/b) avec -ceil(log_2(lb))*BASE^(lb-1) < a - b*c < b
  sortie si rem = 1 :
    a <- a mod b
    c <- floor(a/b)
  sortie si rem = 2 :
    a <- ind.
    c <- approx(a/b) avec -ceil(log_2(lb))*BASE^(lb-1) < a - b*c < b
         et c = floor(a/b) si c[0] = 0
*/
#ifndef assembly_sn_moddiv
#ifdef debug_moddiv
void xn(moddiv_buggy)
#else
void xn(moddiv)
#endif
(chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem) {
  long p,q,q1,r;
  chiffre *x = NULL;

  p = lb/2; q = lb-p; q1 = q-1;
  if (q1 < p) {q++; q1++; p--;}

  /* r <- nb de chiffres du quotient pour la premi�re division sans reste */
  if (lc <= q1) {q=lc+1; q1=lc; p=lb-q; r=lc;}
  else {r = lc%q1; if (r == 0) r = q1;}

  /* division avec reste par tranches de q-1 chiffres */
  lc -= r; a += lc; c += lc;
  if ((lc) || (rem)) x = xn(alloc)(q+q1);
  while(lc) {

    /* si lb ou r est assez petit, on utilise burnidiv ou div_n2 */
    if      (lb <= moddiv_lim)      xn(burnidiv) (a,r,b,lb,c);
    else if (r  <= div_small_c_lim) xn(div_n2)(a,r,b,lb,c);

    /* sinon, division sans reste puis calcul modulaire du reste */
    else {
      if (xn(cmp)(a+p+r,q,b+p,q)) {
        xn(move)(a+p,q+r,x);
        xn(moddiv)(x,r,b+p,q,c,0);
      }
      else xn(fill)(c,r);
      xn(remdiv)(a,r,b,lb,c);
    }

    /* tranche suivante */
    a -= q1; c -= q1; lc -= q1; r = q1;
  }

  if (rem) { /* derni�re division, avec reste si n�cessaire */

    /* si lb ou r est assez petit, on utilise burnidiv ou div_n2 */
    if      (lb <= moddiv_lim)      xn(burnidiv) (a,r,b,lb,c);
    else if (r  <= div_small_c_lim) xn(div_n2)(a,r,b,lb,c);

    /* sinon, division sans reste puis calcul modulaire du reste */
    else {
      if (xn(cmp)(a+p+r,q,b+p,q)) {
        xn(move)(a+p,q+r,x);
        xn(moddiv)(x,r,b+p,q,c,0);
      }
      else xn(fill)(c,r);
      if ((rem == 1) || (c[0] == 0)) xn(remdiv)(a,r,b,lb,c);
    }
  }
  else { /* derni�re division, sans reste */
    p += q1-r; q = lb-p;
    if (xn(cmp)(a+p+r,q,b+p,q)) {
      if (q <= moddiv_lim)           xn(burnidiv) (a+p,r,b+p,q,c);
      else if (r <= div_small_c_lim) xn(div_n2)(a+p,r,b+p,q,c);
      else                           xn(moddiv)  (a+p,r,b+p,q,c,0);
    }
    else xn(fill)(c,r);
  }

  xn(free)(x); /* on peut avoir x = NULL ici */

}
#endif /* assembly_sn_moddiv */

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#ifdef debug_moddiv
void xn(moddiv_buggy)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem);
void xn(moddiv)(chiffre *a, long lc, chiffre *b, long lb, chiffre *c, int rem) {
  long la = lc+lb,l;
  chiffre *x,*y,r;
  int ok;

  /* validit� des longueurs ? */
  if ((lb < 2) || (la < lb))
      xn(internal_error)("error, moddiv is called with lb < 2 or la < lb",0);

  /* le bit de poids fort de b est non nul ? */
  if ((b[lb-1] & (BASE_2)) == 0)
      xn(internal_error)("error, moddiv is called with msb(b) = 0",0);

  /* a < BASE^lc*b ? */
  if (xn(cmp)(a+lc,lb,b,lb) >= 0)
    xn(internal_error)("error, moddiv is called with a >= BASE^lc*b",2,a,la,b,lb);


  /* effectue la division douteuse */
  x = xn(alloc)(2*la); y = x+la;
  xn(move)(a,la,x);
  xn(moddiv_buggy)(a,lc,b,lb,c,rem);

  /* calcule a - b*c */
  if (lc < lb) xn(fftmul)(b,lb,c,lc,y); else xn(fftmul)(c,lc,b,lb,y);
  if (xn(sub)(x,la,y,la,y)) {

    /* n�gatif : v�rifie que a - bc + ceil(log_2(b))*BASE^(lb-1) > 0 */
    if (rem != 1) {
      for (l=1, r=0; l<lb; l<<=1, r++);
      ok =((xn(inc)(y+lb-1,lc+1,&r,1)) && (xn(cmp)(y,la,&r,0)));
    }
    else ok = 0;
  }
  else {

    /* positif ou nul : v�rifie que a - bc < b */
    ok = (xn(cmp)(y,la,b,lb) < 0);

    /* si rem = 1, v�rifie que a_sortie = a_entr�e - b*c */
    if ((ok) && (rem==1)) ok = (xn(cmp)(a,lb,y,lb) == 0);
  }

  if (!ok) {
    if (rem == 1) xn(internal_error)("error in moddiv", 4,x,la,b,lb,c,lc,a,lb);
    else          xn(internal_error)("error in moddiv", 3,x,la,b,lb,c,lc);
  }
  xn(free)(x);

}
#endif /* debug_moddiv */

                            /* +-----------------+
                               |  Racine carr�e  |
                               +-----------------+ */
/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la/2

  contraintes :
  la > 0, la pair, BASE/16 <= a[la-1] < BASE/4
  a,b non confondus

  sortie :
  b <- 2*floor(sqrt(a))
  a <- a - b^2/4
*/

#ifndef assembly_sn_modsqrt
#ifdef debug_modsqrt
void xn(modsqrt_buggy)
#else
void xn(modsqrt)
#endif
(chiffre *a, long la, chiffre *b) {
  long p,q;
  chiffre *x;

  /* petite racine -> zimsqrt */
  if (la <= modsqrt_lim) {xn(zimsqrt)(a,la,b); return;}

  /* cas r�cursif : divise a en 2 et calcule la racine du haut */
  p = la/4; q = la/2-p;
  if (p == q) {p--; q++;}
  x = xn(alloc)(p+2*q);
  xn(move)(a+p,p+2*q,x);
  xn(modsqrt)(x+p,2*q,b+p);

  /* divise le reste par 2b[p..p+q-1] */
  /* Rmq : on pourrait gagner du temps ici car karpdiv va recalculer
     l'inverse de la moiti� haute de 2b[p..p+q-1] � partir de z�ro,
     alors qu'on pourrait profiter de l'inverse du quart haut, d�j�
     calcul� dans l'appel r�cursif � modsqrt ... � voir.
  */
  if (xn(cmp)(x+p,q,b+p,q)) xn(karpdiv)(x,p,b+p,q,b,0); else xn(fill)(b,p);
  xn(free)(x);

  /* d�cale le quotient */
  if (xn(shift_up)(b,p,b,1)) b[p]++;

  /* calcule le reste */
  xn(remsqrt)(a,la,b);

}
#endif /* assembly_sn_modsqrt */

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#ifdef debug_modsqrt
void xn(modsqrt_buggy)(chiffre *a, long la, chiffre *b);
void xn(modsqrt)(chiffre *a, long la, chiffre *b) {
  long lb = la/2;
  chiffre *x,*y, r;

  /* v�rifie que la est pair > 0 et BASE/16 <= a[la-1] < BASE/4 */
  if ((la%2) || (la < 2))
    xn(internal_error)("error, modsqrt is called with la odd or la < 2",0);

  r = a[la-1] >> (HW-4);
  if ((r == 0) || (r > 3))
    xn(internal_error)("error, modsqrt is called without BASE/16 <= msb(la) < BASE/4",1,a,la);

  /* calcule la racine carr�e douteuse */
  x = xn(alloc)(2*la); y = x + la;
  xn(move)(a,la,x);
  xn(modsqrt_buggy)(a,la,b);

  /* v�rifie que a_entr�e = a_sortie + (b/2)^2 et a_sortie <= b */
  xn(fftsqr)(b,lb,y);
  r = xn(shift_down)(y,la,y,2);
  if (r == 0) r = xn(inc)(y,la,a,lb);
  if (r == 0) r = xn(cmp)(x,la,y,la);
  if (r == 0) r = (xn(cmp)(a,lb,b,lb) > 0);

  if (r) xn(internal_error)("error in modsqrt", 3,x,la,b,lb,a,lb);

  xn(free)(x);

}
#endif /* debug_modsqrt */
