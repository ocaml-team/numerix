// file kernel/n/c/sqrt_n2.c: O(n^2) square root of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                          Racine carr�e quadratique                    |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                      /* +-----------------------------+
                         |  Racine carr�e quadratique  |
                         +-----------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur la/2

  contraintes :
  la > 0, la pair, BASE/16 <= a[la-1] < BASE/4
  a,b non confondus

  sortie :
  b <- 2*floor(sqrt(a))
  a <- a - b^2/4
*/

#ifndef assembly_sn_sqrt_n2
#ifdef debug_sqrt_n2
void xn(sqrt_n2_buggy)
#else
void xn(sqrt_n2)
#endif
(chiffre *a, long la, chiffre *b) {
#ifdef zdouble
  chiffre u,v;
  ndouble x;
  zdouble z;
  long i,lb;

  /* avance a et b aux chiffres de t�te */
  a += la-2; b += la/2 - 1;

  /* b[0] <- 2*floor(sqrt(a[0]+BASE*a[1])), a <- a - b^2/4 */
  x = (ndouble)a[0] + ((ndouble)a[1] << HW);
  for (u=BASE_2, v=(u+x/u)/2; v < u; u=v, v=(u+x/u)/2);
  a[0] = x - u*u;
  a[1] = 0;
  b[0] = u << 1;

  /* calcule les chiffres suivants par divisions */
  for (lb=2, la-=2, a-=2, b--; la; lb++, la-=2, a-=2, b--) {

    /* quotient approch�, peut �tre trop grand d'une ou deux unit�s */
    if ((ndouble)a[lb] >= (ndouble)b[lb-1]) v = (BASE_2)+(BASE_2-1);
    else {
      x = (ndouble)a[lb-1] + ((ndouble)a[lb] << HW);
      v = x/(ndouble)b[lb-1];
    }

    /* a <- a - v*b - v^2, b <- b + 2*v */
    b[0] = v;
    for (x=0, z=0, i=0; i<lb; i++) {
      x += v*(ndouble)b[i];
      z += (ndouble)a[i] - (x & ((BASE_2)+(BASE_2-1)));
      a[i] = z;
      x >>= HW; z >>= HW;
    }
    z += (ndouble)a[lb] - x;
    a[lb] = z;
    b[0] = v << 1;
    if (v & (BASE_2)) b[1]++;

    /* corrige le quotient et le reste si < 0 */
    while(a[lb]) {
      xn(dec1)(b,lb);
      xn(inc)(a,lb+1,b,lb);
      b[0]--;
    }

  }

#else /* pas de doubles */
  chiffre u,v,x,y;
  long i,lb;

  /* avance a et b aux chiffres de t�te */
  a += la-2; b += la/2 - 1;

  /* b[0] <- 2*floor(sqrt(a[0]+BASE*a[1])), a <- a - b^2/4 */
  for (u = 0, v = BASE_2; v; v >>= 1) {
    u += v;
    xn(sqr_0)(u,&x,&y);
    if ((a[1] < y) || ((a[1] == y) && (a[0] < x))) u -= v;
  }
  a[0] -= u*u;
  a[1] = 0;
  b[0] = u << 1;

  /* calcule les chiffres suivants par divisions */
  for (lb=2, la-=2, a-=2, b--; la; lb++, la-=2, a-=2, b--) {

    /* quotient approch�, peut �tre trop grand d'une ou deux unit�s */
    if (a[lb] >= b[lb-1]) v = (BASE_2)+(BASE_2-1);
    else xn(div_0)(a[lb-1],a[lb],b[lb-1],&v,&u);

    /* a <- a - v*b - v^2, b <- b + 2*v */
    b[0] = v;
    for (u=0, i=0; i<lb; i++) {
      xn(mul_0)(v,b[i],&x,&y);
      x += u; u = y + (x < u) + (a[i] < x);
      a[i] -= x;
    }
    a[lb] -= u;
    b[0] = v << 1;
    if (v & (BASE_2)) b[1]++;

    /* corrige le quotient et le reste si < 0 */
    while(a[lb]) {
      xn(dec1)(b,lb);
      xn(inc)(a,lb+1,b,lb);
      b[0]--;
    }

  }
#endif /* ndouble */
}
#endif /* assembly_sn_sqrt_n2 */

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#ifdef debug_sqrt_n2
void xn(sqrt_n2_buggy)(chiffre *a, long la, chiffre *b);
void xn(sqrt_n2)(chiffre *a, long la, chiffre *b) {
  long lb = la/2;
  chiffre *x,*y, r;

  /* v�rifie que la est pair > 0 et BASE/16 <= a[la-1] < BASE/4 */
  if ((la%2) || (la < 2))
      xn(internal_error)("error, sqrt_n2 is called with la odd or la < 2",0);

  if ((a[la-1] < (chiffre)(BASE_2/8)) || (a[la-1] >= (chiffre)(BASE_2/2)))
      xn(internal_error)("error, sqrt_n2 is called without BASE/16 <= msb(la) < BASE/4",1,a,la);

  /* calcule la racine carr�e douteuse */
  x = xn(alloc_tmp)(2*la); y = x + la;
  xn(move)(a,la,x);
  xn(sqrt_n2_buggy)(a,la,b);

  /* v�rifie que a_entr�e = a_sortie + (b/2)^2 et a_sortie <= b */
  xn(toomsqr)(b,lb,y);
  r = xn(shift_down)(y,la,y,2);
  if (r == 0) r = xn(inc)(y,la,a,lb);
  if (r == 0) r = xn(cmp)(x,la,y,la);
  if (r == 0) r = (xn(cmp)(a,lb,b,lb) > 0);

  if (r) xn(internal_error)("error in sqrt_n2", 3,x,la,b,lb,a,lb);

  xn(free_tmp)(x);

}
#endif /* debug_sqrt_n2 */

