// file kernel/n/c/karatsuba.c: Karatsuba multiplication
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Multiplication de Karatsuba                       |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                           /* +------------------+
                              |  Multiplication  |
                              +------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur la+lb, non confondu avec a ou b

  contrainte : 0 < lb <= la

  sortie :
  c <- a*b
*/
#ifndef assembly_sn_karamul
#ifdef debug_karamul
void xn(karamul_buggy)
#else
void xn(karamul)
#endif
(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
  chiffre *d;
  long p,q,r;

  /* petite multiplication -> algorithme en n^2 */
  if (lb <= karamul_lim) {xn(mul_n2)(a,la,b,lb,c); return;}

  /* si lb > ceil(la/2), d�coupage de Karatsuba */
  if (lb > (la+1)/2) {
    q = la/2; p = la-q; r=lb-p;
    d = xn(alloc_tmp)(2*p+2);

    /* calcule a0+a1 et b0+b1 dans c */
    c[p]     = xn(add)(a,p, a+p,q, c     );
    c[2*p+1] = xn(add)(b,p, b+p,r, c+p+1 );

    /* calcule (a0+a1)*(b0+b1), a0*b0, a1*b1 */
    xn(karamul)(c,p+1,   c+p+1,p+1,  d );
    xn(karamul)(a,p,     b,p,        c );
    xn(karamul)(a+p,q,   b+p,r,      c+2*p);

    /* calcule a0*b1 + a1*b0 et l'ajoute � c */
    xn(dec)(d,2*p+1, c,2*p    );
    xn(dec)(d,2*p+1, c+2*p,q+r);
    xn(inc)(c+p,p+q+r, d,2*p+1);

    xn(free_tmp)(d); /* lib�re la m�moire auxilliaire */
  }

  /* si lb <= ceil(la/2), d�coupe b en tranches de la chiffres */
  else {
    p = la % lb; if (p == 0) p = lb;
    xn(karamul)(b,lb,a,p,c);         /* 1�re multiplication */
    d = xn(alloc_tmp)(lb);           /* tampon pour les mult. suivantes */

    /* multiplie les tranches suivantes et les cumule dans c */
    for (a+=p, la-=p, c+=p; la; a+=lb, la-=lb, c+=lb) {
      xn(move)(c,lb,d);
      xn(karamul)(a,lb, b,lb, c);
      xn(inc)(c,2*lb,d,lb);
    }

    xn(free_tmp)(d); /* lib�re la m�moire auxilliaire */
  }

}
#endif /* assembly_sn_karamul */

                                /* +---------+
                                   |  Carr�  |
                                   +---------+ */
/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur 2*la, non confondu avec a

  contrainte : 0 < la

  sortie :
  b <- a^2
*/
#ifndef assembly_sn_karasqr
#ifdef debug_karamul
void xn(karasqr_buggy)
#else
void xn(karasqr)
#endif
(chiffre *a, long la, chiffre *b) {
  chiffre *d;
  long p,q;

  /* petit carr� -> algorithme en n^2 */
  if (la <= karasqr_lim) {xn(sqr_n2)(a,la,b); return;}

  /* d�coupage de Karatsuba */
  q = la/2; p = la-q;
  d = xn(alloc_tmp)(2*p+2);

  /* calcule a0+a1 dans b */
  b[p] = xn(add)(a,p, a+p,q, b);

  /* calcule (a0+a1)^2, a0^2, a1^2 */
  xn(karasqr)(b,p+1, d );
  xn(karasqr)(a,p,   b );
  xn(karasqr)(a+p,q, b+2*p);

  /* calcule 2*a0*a1 et l'ajoute � c */
  xn(dec)(d,2*p+1, b,2*p    );
  xn(dec)(d,2*p+1, b+2*p,2*q);
  xn(inc)(b+p,p+2*q, d,2*p+1);

  xn(free_tmp)(d); /* lib�re la m�moire auxilliaire */
}
#endif /* assembly_sn_karasqr */

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#ifdef debug_karamul
void xn(karamul_buggy)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);
void xn(karasqr_buggy)(chiffre *a, long la, chiffre *b);

void xn(karamul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
  if (la < lb)
      xn(internal_error)("error, karamul is called with la < lb",0);
  xn(karamul_buggy)(a,la,b,lb,c);
  if (!xn(ctrl_mul)(a,la,b,lb,c))
      xn(internal_error)("error in karamul",3,a,la,b,lb,c,la+lb);

}

void xn(karasqr)(chiffre *a, long la, chiffre *b) {
  xn(karasqr_buggy)(a,la,b);
  if (!xn(ctrl_mul)(a,la,a,la,b))
      xn(internal_error)("error in karasqr",2,a,la,b,2*la);

}
#endif /* debug_karamul */

