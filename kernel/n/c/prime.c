// file kernel/n/c/prime.c: primality test
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                           Test de primalit�                           |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* nombres premiers <= 2^10 : bit(crible,i) = 1 ssi 2i+1 est permier */
#if defined(bits_32)
static long crible[16] = {
  0x64B4CB6E, 0x816D129A, 0x864A4C32, 0x2196820D,
  0x5A0434C9, 0xA4896120, 0x29861144, 0x4A2882D1,
  0x32424030, 0x08349921, 0x4225064B, 0x148A4884,
  0x6C304205, 0x0B40B408, 0x125108A0, 0x65048928};
extern inline long small_prime(unsigned long p) {
  return((crible[p>>6] >> ((p>>1) & 31)) & 1);
}
#elif defined(bits_64)
static long crible[8] = {
  0x816D129A64B4CB6E, 0x2196820D864A4C32,
  0xA48961205A0434C9, 0x4A2882D129861144,
  0x0834992132424030, 0x148A48844225064B,
  0x0B40B4086C304205, 0x65048928125108A0};
extern inline long small_prime(unsigned long p) {
  return((crible[p>>7] >> ((p>>1) & 63)) & 1);
}
#endif
#define small_prime_limit (1 << 10)

                          /* +---------------------+
                             |  Test de primalit�  |
                             +---------------------+ */
/*
  entr�e :
  a = naturel de longueur la

  contrainte : a[la-1] > 0

  sortie = 0 : a est compos�,
  sortie = 1 : a passe le test mais il est trop grand pour qu'on soit s�r
               qu'il est premier.
  sortie = 2 : a est premier,
*/

/* le test de primalit� est s�r jusqu'� 2^50 */
#define unknown_prime_limit 50

#ifdef debug_isprime
long xn(isprime_buggy)
#else
long xn(isprime)
#endif
(chiffre *a, long la) {
  long j,k,n,x,small,fnul,gnul;
  unsigned long a0,d,u,v,w;
  chiffre *aa,*f,*g,*f2,*g2,*h2;

  /* a0 <- bits de poids faible de |a| */
#if chiffres_per_long == 1
  a0 = (la) ? a[0] : 0;
#elif chiffres_per_long == 2
  a0 = (la > 1) ? a[1] : 0;
  if (la) a0 = (a0 << HW) + (unsigned long)a[0];
#endif

  /* cas a <= small_prime_limit^2 :
       v�rifie que a est impair ou a = 2,
       consulte le crible si a < small_prime_limit,
       cherche un diviseur premier <= sqrt(a) sinon.
  */
  if ((la <= chiffres_per_long) && (a0 <= small_prime_limit*small_prime_limit)) {

    if ((a0&1) == 0)            return((a0 == 2) << 1);
    if (a0 < small_prime_limit) return(small_prime(a0) << 1);
    for (d=3; d*d <= a0; d+=2) if ((small_prime(d)) && ((a0%d) == 0)) return(0);
    return(2);
  }

  /* cas a > small_prime_limit^2 : v�rifie que a est impair */
  if ((a0&1) == 0) return(0);

  /* n <- HW - nbits(a[la-1]), small <- a < 2^unknown_prime_limit ? */
  for (u=(unsigned long)a[la-1], n=HW; u; u>>=1, n--);
  small = (la*HW - n <= unknown_prime_limit);

  /* cherche un petit diviseur premier */
  for (d=3; d < small_prime_limit; d+=2)
    if ((small_prime(d)) && (xn(mod_1)(a,la,d) == 0)) return(0);

  /* pas trouv� : cherche d tq (a/d) = -1 */
  for (d=5; d < small_prime_limit; d+=2) if ((small_prime(d)) || (d == 15)) {
    for (u=xn(mod_1)(a,la,d), v=d, x=0; u>1; ) {
      while ((u&1) == 0) {u >>= 1; x ^= v^(v>>1);}
      x ^= u&v; w = v%u; v=u; u=w;
    }
    if (x&2) break;
  }

  /* pas trouv� : v�rifie que a n'est pas un carr� */
  if (d >= small_prime_limit) {

    if (n <= 1) {u = HW-2;        v = 1-(la&1);  w = la+v+1;}
    else        {u = n-2 - (n&1); v = la&1;      w = la+v;  }
    aa = xn(alloc)(3*w/2);
    aa[0] = 0; aa[la+v] = xn(shift_up)(a,la,aa+v,u);
    (w > zimsqrt_lim) ? xn(modsqrt)(aa,w,aa+w) : xn(sqrt_n2)(aa,w,aa+w);
    x = xn(cmp)(aa,w,aa,0);
    xn(free)(aa);
    return(x<<small);

  }

  /* d trouv� : effectue un test de Lucas modifi�

     Soit a+1 = 2^k*q avec q impair et i = sqrt(d*(-1)^(d-1)/2)
     (i est alg�brique de degr� 2 sur Z/aZ car (i^2/a) = (a/d) = -1)
     On calcule (1+i)^q mod a puis on �l�ve au carr� k fois.
     Pour a premier, le r�sulat final est 1-d mod a et le premier
     carr� non imaginaire est pr�c�d� d'un imaginaire pur.
  */

  /* k <- v2(a+1) = nb de bits de poids faible de a �gaux � 1
     j <- rang du 2�me bit significatif de a+1
  */
  for (j=k=0; (j < la) && (a[j] == BASE_2+(BASE_2-1)); j++, k+=HW);
  if (j < la) for (u=a[j]; u&1; k++, u>>=1);
  j = la*HW-n-2; if (j < k-1) j = k-1;

  /* allocation de m�moire. Il faut :
          la chiffres pour f,g
	   1 chiffre de garde
       2la+1 chiffres pour f^2,g^2,(f+g)^2
         la chiffres pour d�caler a si n > 0 */
  if (n) {
    g  = xn(alloc)(9*la+4);
    aa = g + 8*la+4;
    xn(shift_up)(a,la,aa,n);
  }
  else {
    g  = xn(alloc)(8*la+4);
    aa = a;
  }
  f = g+la;  g2 = f+la+1;  f2 = g2+2*la+1;  h2 = f2+2*la+1;

  /* f + ig <- (1+i)^(a+1) mod a. Formules :

     (f + ig)^2       = (f^2 + i^2*g^2)           + i*((f+g)^2 - f^2 - g^2)
     (1+i)*(f + ig)^2 = (f^2 + i^2*((f+g)^2-f^2)) + i*((f+g)^2 + (i^2-1)*g^2)
  */
  xn(clear)(g,2*la); f[0] = g[0] = 1; fnul = gnul = 0;
  for (; j ; j--) {

    /* calcule f^2, g^2 et (f+g)^2 */
    if (xn(add)(f,la,g,la,f2)) xn(dec)(f2,la,aa,la);
    xn(sqr)(f2,la,h2);
    xn(sqr)(f, la,f2);
    xn(sqr)(g, la,g2);

    /* s�lectionne les formules � appliquer en fonction du bit j de a+1 */
    if ((j==k) || ((j>k) && ((a[j/HW] >> (j&(HW-1))) & 1))) {

      /* bit = 1 -> g:f <- |i^2-1|*g^2, g2 <- (f+g)^2 - f^2 */
      if (d&2) {v = d+1; w = 1;} else {v = d-1; w = 0;}
      u = xn(mul_1)(g2,2*la,v,g);
      v = xn(sub)(h2,2*la,f2,2*la,g2);

    } else {

      /* bit = 0 -> g:f <- f^2 + g^2 */
      u = xn(add)(f2,2*la,g2,2*la,g);
      w = 1;
      v = 0;

    }

    /* g <- ((f+g)^2 + (-1)^w*(g:f:u)) mod aa */
    if (w) {
      u += xn(sub)(h2,2*la,g,2*la,g);
      f[la] = -u;
      if (u) xn(inc)(f+1,la,aa,la);
    } else f[la] = u + xn(inc)(g,2*la,h2,2*la);
    if (la == 1) g[0] = xn(mod_1)(g,2*la+1,a0); else xn(div)(g,la+1,aa,la,h2);

    /* f <- (f^2 + i^2*(g2:(-v)) mod aa */
    u = xn(mul_1)(g2,2*la,d,f) - v*d;
    if (d&2) u  = -xn(sub)(f2,2*la,f,2*la,f) - u;
    else     u += xn(inc)(f,2*la,f2,2*la);
    f[2*la] = u;
    if (u & BASE_2) xn(inc)(g2,la,aa,la);
    if (la == 1) f[0] = xn(mod_1)(f,2*la+1,a0); else xn(div)(f,la+1,aa,la,h2);

    /* Quand on a atteint l'exposant q (ie. j <= k), on arr�te la boucle 
       lorsque f = 0 mod a ou g = 0 mod a.
       Si la = 1 ou n = 0, on dispose de f mod a et g mod a, il suffit de les
       comparer � z�ro.
       Si la > 1 et n > 0, on dispose de f mod (2^n*a) et g mod (2^n*a).
       Si ces r�sidus sont divisibles par 2^n alors ils sont nuls modulo
       a si et seulement s'ils sont nuls. Sinon, on les r�duit d'abord
       modulo a avant de les comparer � z�ro (ce cas est peu fr�quent car
       il implique q <= n).
    */
    if (j <= k) {
      if ((la > 1) && (f[0] & ((1 << n) - 1))) {
	f2[la] = xn(shift_up)(f,la,f2,n);
	xn(div_n2)(f2,1,aa,la,h2);
	xn(shift_down)(f2,la,f,n);
      }
      if ((la > 1) && (g[0] & ((1 << n) - 1))) {
	g2[la] = xn(shift_up)(g,la,g2,n);
	xn(div_n2)(g2,1,aa,la,h2);
	xn(shift_down)(g2,la,g,n);
      }
      fnul = (xn(cmp)(f,la,f,0) == 0);
      gnul = (xn(cmp)(g,la,g,0) == 0);
      if ((fnul) || (gnul)) break;
    }
  }

  /* cas f = 0 : f <- i^2*g^2 */
  if (fnul) {
    xn(sqr)(g,la,g2);
    u = xn(mul_1)(g2,2*la,d,f);
    if (d&2) {xn(clear)(f2,2*la); u = -xn(sub)(f2,2*la,f,2*la,f) - u;}
    f[2*la] = u;
    if (u & BASE_2) xn(inc)(g2,la,aa,la);
    if (la == 1) f[0] = xn(mod_1)(f,2*la+1,a0); else xn(div)(f,la+1,aa,la,h2);
    gnul = 1;
    j--;
  }

  /* cas g = 0 :
     v�rifie qu'on est pass� par la case f=0 si j < k,
     continue les �l�vations au carr� puis v�rifie que f + i^2 - 1 = 0 mod a
  */
  if ((gnul) && ((fnul) || (j==k))) {
    for (; j ; j--) {
      xn(sqr)(f,la,f2);
      g2 = f2; f2 = f; f = g2;
      if (la == 1) f[0] = xn(mod_1)(f,2*la,a0); else xn(div)(f,la,aa,la,h2);
    }
    if (d&2) {g[0] = d+1; if (xn(dec)(f,la,g,1)) xn(inc)(f,la,aa,la);}
    else     {g[0] = d-1; if (xn(inc)(f,la,g,1)) xn(dec)(f,la,aa,la);}
    if (n) {
      if (la > 1) {f[la] = xn(shift_up)(f,la,f,n); xn(div_n2)(f,1,aa,la,h2);}
      else f[0] = xn(mod_1)(f,la,a0);
    }
    fnul = (xn(cmp)(f,la,f,0) == 0);
  }

  xn(free)(g);
  return(fnul<<small);

}

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#ifdef debug_isprime
long xn(isprime)(chiffre *a, long la) {
  static unsigned long base[] = {2,3,5,7,11,13,17,19,23,29,31};
  chiffre *aa,*x,*y,*z,*t;
  unsigned long a0;
  long b,i,k,n,r;

  if (a[la-1] == 0) {
      xn(internal_error)("error, isprime is called with a[la-1] = 0",0);
  }

  /* cas a <= 1 ou a pair */
  a0 = (la) ? (unsigned long)a[0] : 0;
  if ((la == 1) && (a0 == 1)) r = 0;
  else if ((a0&1) == 0) r = ((la == 1) && (a0 == 2));

  /* cas a <= 120 */
  else if ((la == 1) && (a0 <= 120))
    r = (a0%3) ? (a0%5) ? (a0%7) ? 1 : (a0 == 7) : (a0 == 5) : (a0 == 3);

  /* cas g�n�ral */
  else {

    /* k <- v2(a-1), n <- HW - nbits(a[la-1]) */
    for (k=1; ((a[k/HW] >> (k%HW)) & 1) == 0; k++);
    for (n=0; ((a[la-1] << n) & BASE_2) == 0; n++);

    /* m�moire de travail */
    if (n) {x = xn(alloc)(6*la+4); aa = x+5*la+4; xn(shift_up)(a,la,aa,n);}
    else   {x = xn(alloc)(5*la+4); aa = a;}
    y = x+la+2; z = NULL; t = y + 3*la + 2;

    /* effectue un test de Rabin-Miller pour chaque base du tableau base */
    for (r=1, b=0; (r) && (b < sizeof(base)/sizeof(unsigned long)); b++) {

      xn(clear)(x,la); x[0] = base[b];
      for (i=la*HW-n-2; i>=k; i--) {
	xn(sqr)(x,la,y);
	y[2*la] = ((a[i/HW] >> (i%HW)) & 1) ? xn(mul_1)(y,2*la,base[b],y) : 0;
	if (la == 1) x[0] = xn(mod_1)(y,2*la+1,a0);
	else {
	  y[2*la+1] = xn(shift_up)(y,2*la+1,y,n);
	  xn(div)(y,la+2,aa,la,x);
	  xn(shift_down)(y,la,x,n);
	}
      }
      y[-1] = 1;
      r = (xn(cmp)(x,la,y-1,1) == 0);
      if (!r) {xn(sub)(a,la,x,la,x); r = (xn(cmp)(x,la,y-1,1) == 0);}
      if (!r) {
	for(; (!r) && (i); i--) {
	  xn(move)(x,la,t);
	  xn(sqr)(x,la,y);
	  if (la == 1) x[0] = xn(mod_1)(y,2*la,a0);
	  else {
	    y[2*la] = xn(shift_up)(y,2*la,y,n);
	    xn(div)(y,la+1,aa,la,x);
	    xn(shift_down)(y,la,x,n);
	  }
	  xn(sub)(a,la,x,la,x);
	  r = (xn(cmp)(x,la,y-1,1) == 0);
	}
	/* enregistre la racine carr�e de -1 ou compare � celle enregistr�e */
	if (r) {
	  if (z == NULL) {z = y + 2*la + 2; xn(move)(t,la,z);}
	  else if (xn(cmp)(t,la,z,la)) {
	    xn(sub)(a,la,t,la,t);
	    r = (xn(cmp)(t,la,z,la) == 0);
	  }
	}
      }
    }

    /* lib�re la m�moire de travail */
    xn(free)(x);
    
  }

  /* compare r au r�sultat de isprime_buggy */
  n = xn(isprime_buggy)(a,la);
  if (r != (n != 0)) {
    printf("Numerix kernel: isprime mismatch r=%ld n=%ld arg=",r,n);
    xn(dump)(a,la);
  }
  return(n);
}
#endif
