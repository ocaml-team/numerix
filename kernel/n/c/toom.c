// file kernel/n/c/toom.c: Toom multiplication of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                          Multiplication de Toom                       |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                           /* +------------------+
                              |  Multiplication  |
                              +------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur la + lb non confondu avec a ou b

  contraintes : 0 < lb <= la

  sortie :
  c <- a*b
*/

#ifndef assembly_sn_toommul
#ifdef debug_toommul
void xn(toommul_buggy)
#else
void xn(toommul)
#endif
(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
  chiffre *d,*e,*f,x;
  long i,p,q,r;

  /* petite multiplication -> Karatsuba */
  if (lb <= toommul_lim) {xn(karamul)(a,la,b,lb,c); return;}

  /* si lb > 2*ceil(la/3), d�coupage de Toom */
  p = (la+2)/3; q = la - 2*p; r = lb -2*p;
  if (r > 0) {

    /* m�moire de travail */
    d = xn(alloc_tmp)(6*p+10);
    e = d + 2*p+2;
    f = e + 2*p+2;

    /* d <- (a0 + a1 + a2)(b0 + b1 + b2) = c0 + c1 + c2 + c3 + c4 */
    c[p]     = xn(add)(a,p, a+2*p,q, c);
    c[2*p+1] = xn(add)(b,p, b+2*p,r, c+p+1);
    xn(add)(c,    p+1, a+p,p, c+2*p+2);
    xn(add)(c+p+1,p+1, b+p,p, c+3*p+3);
    xn(toommul)(c+2*p+2,p+1, c+3*p+3,p+1, d);

    /* e <- (a0 - a1 + a2)(b0 - b1 + b2) mod BASE^2p+2  = c0 - c1 + c2 - c3 + c4 */
    xn(dec)(c,p+1,     a+p,p);
    xn(dec)(c+p+1,p+1, b+p,p);
    xn(toommul)(c,p+1, c+p+1,p+1,e);
    if (c[p]     == (BASE_2)+(BASE_2-1)) xn(dec)(e+p+1,p+1, c+p+1,p+1);
    if (c[2*p+1] == (BASE_2)+(BASE_2-1)) xn(dec)(e+p+1,p+1, c,    p+1);

    /* f <- (a0 + BASE*a1 + BASE^2*a2)*(b0 + BASE*b1 + BASE^2*b2)
          = c0 + BASE*c1 + BASE^2*c2 + BASE^3*c3 + BASE^4*c4      */
    c[0]     = a[0];
    c[p+1]   = xn(add)(a+p,p, a+1,p-1, c+1);
    c[p+2]   = xn(inc)(c+2,p, a+2*p,q);
    c[p+3]   = b[0];
    c[2*p+4] = xn(add)(b+p,p, b+1,p-1, c+p+4);
    c[2*p+5] = xn(inc)(c+p+5,p, b+2*p,r);
    xn(toommul)(c,p+3, c+p+3,p+3, f);

    /* c[2p..4p] <- (d+e)/2 = c0 + c2 + c4, d <- (d-e)/2 mod BASE^(2p+1) = c1 + c3 */
    xn(add)(d,2*p+2, e,2*p+2, c+2*p);
    for (i=2*p; i <= 4*p; i++) c[i] = (c[i+1] & 1) ? (c[i]/2) + (BASE_2) : c[i]/2;
    xn(dec)(d,2*p+1, c+2*p,2*p+1);

    /* c[0..2p-1] <- a0*b0 = c0, c[4*p..4p+q+r-1] <- a2*b2 = c4 */
    x = c[4*p];
    xn(toommul)(a,p, b,p, c);
    xn(toommul)(a+2*p,q, b+2*p,r, c+4*p);

    /* c[2p..4p] <- c[2p..4p] - c0 - c4 = c2 */
    x -= xn(dec)(c+2*p,2*p, c,2*p);
    x -= xn(dec)(c+2*p,2*p, c+4*p,q+r);

    /* f <- f - c0 - BASE^2*c2 - BASE^4*c4 = BASE*c1 + BASE^3*c3 */
    xn(dec)(f,  2*p+6, c,    2*p);
    xn(dec)(f+2,2*p+4, c+2*p,2*p); xn(dec)(f+2*p+2,4, &x,1);
    xn(dec)(f+4,2*p+2, c+4*p,q+r);

    /* f <- -(f - BASE*d)/(BASE^2 - 1) = -BASE*c3 */
    xn(dec)(f+1,2*p+5, d,  2*p+2);
    xn(inc)(f+3,2*p+3, f+1,2*p+3);

    if (f[2*p+5]) { /* on a (f[2p+5] != 0) <=> (c3 != 0) */

      /* injecte c3 dans c */
      if ((!xn(dec)(c+3*p,p+q+1, f+1,p+q+1)) && (r>1)) xn(inc1)(c+4*p+q+1,r-1);

      /* d <- d + f/BASE = c1 */
      xn(inc)(d,2*p+1, f+1,2*p+1);
    }

    /* injecte c1 et x dans c */
    xn(inc)(c+p,3*p+q+r, d,2*p+1);
    xn(inc)(c+4*p, q+r,  &x,1);

    xn(free_tmp)(d); /* lib�re la m�moire auxilliaire */
  }

  /* si lb <= 2*ceil(la/3), d�coupe a en tranches de lb chiffres */
  else {
    p = la % lb; if (p == 0) p = lb;
    xn(toommul)(b,lb,a,p,c);         /* 1�re multiplication */
    d = xn(alloc_tmp)(lb);           /* tampon pour les mult. suivantes */

    /* multiplie les tranches suivantes et les cumule dans c */
    for (a+=p, la-=p, c+=p; la; a+=lb, la-=lb, c+=lb) {
      xn(move)(c,lb,d);
      xn(toommul)(a,lb, b,lb, c);
      xn(inc)(c,2*lb,d,lb);
    }
    xn(free_tmp)(d); /* lib�re la m�moire auxilliaire */
  }
}
#endif /* assembly_sn_toommul */

                                /* +---------+
                                   |  Carr�  |
                                   +---------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur 2*la, non confondu avec a

  contraintes : 0 < la

  sortie :
  b <- a^2
*/

#ifndef assembly_sn_toomsqr
#ifdef debug_toommul
void xn(toomsqr_buggy)
#else
void xn(toomsqr)
#endif
(chiffre *a, long la, chiffre *b) {
  chiffre *d,*e,*f,x;
  long i,p,q;

  /* petit carr� -> Karatsuba */
  if (la <= toomsqr_lim) {xn(karasqr)(a,la,b); return;}

  /* d�coupage de Toom */
  p = (la+2)/3; q = la - 2*p;

  /* m�moire de travail */
  d = xn(alloc_tmp)(6*p+10);
  e = d + 2*p+2;
  f = e + 2*p+2;

  /* d <- (a0 + a1 + a2)^2 = b0 + b1 + b2 + b3 + b4 */
  b[p] = xn(add)(a,p, a+2*p,q, b);
  xn(add)(b, p+1, a+p,p, b+2*p+2);
  xn(toomsqr)(b+2*p+2,p+1, d);

  /* e <- (a0 - a1 + a2)^2 mod BASE^2p+2  = b0 - b1 + b2 - b3 + b4 */
  xn(dec)(b,p+1, a+p,p);
  xn(toomsqr)(b,p+1,e);
  if (b[p] == (BASE_2)+(BASE_2-1)) {
    xn(dec)(e+p+1,p+1, b,p+1);
    xn(dec)(e+p+1,p+1, b,p+1);
  }

  /* f <- (a0 + BASE*a1 + BASE^2*a2)^2
        = b0 + BASE*b1 + BASE^2*b2 + BASE^3*b3 + BASE^4*b4      */
  b[0]     = a[0];
  b[p+1]   = xn(add)(a+p,p, a+1,p-1, b+1);
  b[p+2]   = xn(inc)(b+2,p, a+2*p,q);
  xn(toomsqr)(b,p+3, f);

  /* b[2p..4p] <- (d+e)/2 = c0 + c2 + c4, d <- (d-e)/2 mod BASE^(2p+1) = b1 + b3 */
  xn(add)(d,2*p+2, e,2*p+2, b+2*p);
  for (i=2*p; i <= 4*p; i++) b[i] = (b[i+1] & 1) ? (b[i]/2) + (BASE_2) : b[i]/2;
  xn(dec)(d,2*p+1, b+2*p,2*p+1);

  /* b[0..2p-1] <- a0^2 = c0, b[4*p..4p+q+r-1] <- a2^2 = b4 */
  x = b[4*p];
  xn(toomsqr)(a,p,b);
  xn(toomsqr)(a+2*p,q, b+4*p);

  /* b[2p..4p] <- b[2p..4p] - b0 - b4 = b2 */
  x -= xn(dec)(b+2*p,2*p, b,2*p);
  x -= xn(dec)(b+2*p,2*p, b+4*p,2*q);

  /* f <- f - b0 - BASE^2*b2 - BASE^4*b4 = BASE*b1 + BASE^3*b3 */
  xn(dec)(f,  2*p+6, b,    2*p);
  xn(dec)(f+2,2*p+4, b+2*p,2*p); xn(dec)(f+2*p+2,4, &x,1);
  xn(dec)(f+4,2*p+2, b+4*p,2*q);

  /* f <- -(f - BASE*d)/(BASE^2 - 1) = -BASE*b3 */
  xn(dec)(f+1,2*p+5, d,  2*p+2);
  xn(inc)(f+3,2*p+3, f+1,2*p+3);

  if (f[2*p+5]) { /* on a (f[2p+5] != 0) <=> (b3 != 0) */

    /* injecte b3 dans b */
    if (!xn(dec)(b+3*p,p+q+1, f+1,p+q+1)) xn(inc1)(b+4*p+q+1,q-1);

    /* d <- d + f/BASE = b1 */
    xn(inc)(d,2*p+1, f+1,2*p+1);
  }

  /* injecte b1 et x dans b */
  xn(inc)(b+p,3*p+2*q, d,2*p+1);
  xn(inc)(b+4*p, 2*q,  &x,1);

  xn(free_tmp)(d); /* lib�re la m�moire auxilliaire */
}
#endif /* assembly_sn_toomsqr */

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#ifdef debug_toommul
void xn(toommul_buggy)(chiffre *a, long la, chiffre *b, long lb, chiffre *c);
void xn(toomsqr_buggy)(chiffre *a, long la, chiffre *b);

void xn(toommul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
  chiffre *d;

  /* v�rifie les longueurs des arguments */
  if (la < lb) xn(internal_error)("error, toommul is called with la < lb",0);

  /* compare les r�sultats produits par toommul_buggy et par karamul */
  d = xn(alloc_tmp)(la+lb);
  xn(karamul)(a,la,b,lb,d);
  xn(toommul_buggy)(a,la,b,lb,c);
  if (xn(cmp)(c,la+lb,d,la+lb))
      xn(internal_error)("error in toommul",4,a,la,b,lb,c,la+lb,d,la+lb);
  xn(free_tmp)(d);

}

void xn(toomsqr)(chiffre *a, long la, chiffre *b) {
  chiffre *d;

  /* compare les r�sultats produits par toomsqr_buggy et par karasqr */
  d = xn(alloc_tmp)(2*la);
  xn(karasqr)(a,la,d);
  xn(toomsqr_buggy)(a,la,b);
  if (xn(cmp)(b,2*la,d,2*la))
      xn(internal_error)("error in toomsqr",3,a,la,b,2*la,d,2*la);
  xn(free_tmp)(d);

}
#endif /* debug_toommul */

