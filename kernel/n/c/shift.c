// file kernel/n/c/shift.c: shift of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                                 D�calages                             |
 |                                                                       |
 +-----------------------------------------------------------------------*/


/* ---------------------------------------- D�calage par adresses d�croissantes
  entr�e :
  a = naturel de longueur la > 0
  b = naturel de longueur la
  k = entier tel que 0 <= k < HW

  sortie :
  b <- a >> k
  retourne a mod 2^k

  remarque :
  b peut recouvrir le haut de a
*/
#ifndef assembly_sn_shift_down
chiffre xn(shift_down)(chiffre *a, long la, chiffre *b, int k) {

  if (k) {
    long i;
#ifdef ndouble
    ndouble r;
    for (i=la-1, r=0; i>=0; i--) {r += (ndouble)a[i]; b[i] = r >> k; r <<= HW;}
    return((r >> HW) & (((chiffre)1<<k)-1));
#else
    chiffre r,s;
    for (i=la-1, r=0; i>=0; i--) {
      s = a[i];
      b[i] = (r << (HW-k)) + (s >> k);
      r = s;
    }
    return(r & (((chiffre)1<<k)-1));
#endif /* ndouble */
  } else {
    xn(move)(a,la,b);
    return(0);
  }
}
#endif /* assembly_sn_shift_down */

/* ---------------------------------------- D�calage par adresses croissantes
  entr�e :
  a = naturel de longueur la > 0
  b = naturel de longueur la
  k = entier tel que 0 <= k < HW

  sortie :
  b <- a << k
  retourne les k bits de poids fort de a

  remarque :
  b peut recouvrir le bas de a
*/
#ifndef assembly_sn_shift_up
chiffre xn(shift_up)(chiffre *a, long la, chiffre *b, int k) {

  if (k) {
    long i;
#ifdef ndouble
    ndouble r;
    for (i=0, r=0; i<la; i++) {r += (ndouble)a[i] << k; b[i] = r; r >>= HW;}
    return(r);
#else
    chiffre r,s;
    for (i=0, r=0; i<la; i++) {
      s = a[i];
      b[i] = (s << k) + r;
      r = s >> (HW-k);
    }
    return(r);
#endif
  } else {
    xn(move)(a,la,b);
    return(0);
  }

}
#endif /* assembly_sn_shift_up */

