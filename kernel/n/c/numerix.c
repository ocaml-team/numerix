// file kernel/n/c/numerix.c: operations on natural numbers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                    Op�rations sur les naturels                        |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include "../h/numerix.h"

/* d�boguage */
#include "dump.c"

/* allocation m�moire */
#include "alloc.c"

/* addition/soustraction */
#include "add.c"

/* multiplication/carr� */
#include "mul_n2.c"
#include "karatsuba.c"
#include "toom.c"

/* op�rations modulo BASE^n +/- 1 */
#include "mmod.c"
#include "smod.c"

/* transformation de Fourier */
#include "fft.c"
#include "fftmul.c"

/* division/racine carr�e */
#include "div_n2.c"
#include "sqrt_n2.c"
#include "burnikel.c"
#include "zimmermann.c"
#include "moddiv.c"
#include "karp.c"

/* exponentiation */
#include "pow.c"
#include "powmod.c"
#include "montgomery.c"

/* pgcd */
#include "gcd.c"

/* test de primalit� */
#include "prime.c"

/* divers */
#include "cmp.c"
#include "shift.c"
#include "random.c"

