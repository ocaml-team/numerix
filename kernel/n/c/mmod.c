// file kernel/n/c/mmod.c: operations on residues modulo BASE^n + 1
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                  Arithm�tique modulo BASE^n + 1                       |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                    /* +---------------------------------+
                       |  Comparaison modulo BASE^n + 1  |
                       +---------------------------------+ */

/*
  entr�e :
  a = naturel de longueur n+1
  b = naturel de longueur n+1

  sortie :
  retourne 1 si a = b mod (BASE^n + 1), 0 sinon
*/

#if defined(debug_mmul) || defined(debug_butterfly)
int xn(mequal)(chiffre *a, chiffre *b, long n) {
  chiffre *x,r;
  long i;

  /* x + r*BASE^n <- a - b mod (BASE^n + 1) */
  x = xn(alloc_tmp)(n);
  r  = xn(sub)(a,n,b,n,x);
  r += xn(dec)(x,n,a+n,1);
  r  = xn(inc)(x,n,&r, 1);
  r += xn(inc)(x,n,b+n,1);

  /* v�rifie que x == r */
  x[0] -= r;
  for (i=0; (i < n) && (x[i] == 0); i++);
  xn(free_tmp)(x);

  return((i == n));
}
#endif /* defined(debug_mmul) || defined(debug_butterfly) */


                  /* +------------------------------------+
                     |  Multiplication modulo BASE^n + 1  |
                     +------------------------------------+ */

/*
  entr�e :
  a = naturel de longueur n+1
  b = naturel de longueur n+1 non confondu avec a

  contrainte : n > 0

  sortie :
  a <- (a*b) mod (BASE^n + 1)
  b <- b mod (BASE^n + 1)
*/

#ifndef assembly_sn_mmul
#ifdef debug_mmul
void xn(mmul_buggy)
#else
void xn(mmul)
#endif
(chiffre *a, chiffre *b, long n) {

  /* normalise a et b */
  a[n] = (xn(dec)(a,n,a+n,1)) ? xn(inc1)(a,n) : 0;
  b[n] = (xn(dec)(b,n,b+n,1)) ? xn(inc1)(b,n) : 0;

  /* cas o� l'un des op�randes vaut BASE^n -> res = oppos�(l'autre) */
  if ((a[n]) || (b[n])) {
    if ((a[n]) && (b[n])) {a[0] = 1; a[n] = 0;}
    else {
      chiffre *c = (a[n]) ? b : a;
      chiffre r,s;
      long i;

      for (i=0, r=1; i < n; i++) {
	s = r - c[i]; r = ((r <= 1) && (s <= r)) ? 0 : (BASE_2)+(BASE_2-1);
	a[i] = s;
      }
      a[n] = r+1;
    }
    return;
  }

  /*----------------------------------------------------------------------
    si n est petit ou non divisible par 3, multiplication ordinaire
    puis r�duction :
    a*b = x + y*BASE^n + z*BASE^(2n) = x - y + z mod (BASE^n + 1)
  */
  if ((n <= mmul_lim) || (n%3)) {
    chiffre *c;

    c = xn(alloc_tmp)(2*n);
    xn(toommul)(a,n,b,n,c);
    a[n] = (xn(sub)(c,n,c+n,n,a)) ? xn(inc1)(a,n) : 0;
    xn(free_tmp)(c);
    return;
  }

  /*----------------------------------------------------------------------
    si n est divisible par 3, on d�compose en une multiplication
    modulo BASE^p + 1 et une modulo BASE^(2p) - BASE^p + 1
  */
  {
    long i,p = n/3;
    chiffre *c,r,s;
    unsigned long t;

    /* tampon de travail */
    c = xn(alloc_tmp)(8*p);

    /* c[4p..6p-1] <- a mod (BASE^(2p) - BASE^p + 1) */
    r  = xn(sub)(a,p,   a+2*p,p, c+4*p);
    s  = xn(add)(a+p,p, a+2*p,p, c+5*p);
    s -= xn(dec)(c+5*p,p, &r,1);
    if (s) {xn(dec1)(c+4*p,2*p); xn(inc1)(c+5*p,p);}

    /* c[6p..8p-1] <- b mod (BASE^(2p) - BASE^p + 1) */
    r  = xn(sub)(b,p,   b+2*p,p, c+6*p);
    s  = xn(add)(b+p,p, b+2*p,p, c+7*p);
    s -= xn(dec)(c+7*p,p, &r,1);
    if (s) {xn(dec1)(c+6*p,2*p); xn(inc1)(c+7*p,p);}

    /* c[0..2p-1] <- (a*b) mod (BASE^(2p) - BASE^p + 1) */
    xn(toommul)(c+4*p,2*p, c+6*p,2*p, c);
    r = xn(dec)(c,2*p, c+3*p,p);
    s = xn(inc)(c+p,p, c+2*p,p) - xn(dec)(c,2*p, c+2*p,p);
    if (r > s)      {xn(inc1)(c,2*p); xn(dec1)(c+p,p);}
    else if (r < s) {xn(dec1)(c,2*p); xn(inc1)(c+p,p);}

    /* c[2p..3p] <- a mod (BASE^p + 1) */
    r = xn(add)(a,p, a+2*p,p, c+2*p);
    s = xn(dec)(c+2*p,p, a+p,p);
    c[3*p] = (r >= s) ? r-s : xn(inc1)(c+2*p,p);

    /* c[3p+1..4p+1] <- b mod (BASE^p + 1) */
    r = xn(add)(b,p, b+2*p,p, c+3*p+1);
    s = xn(dec)(c+3*p+1,p, b+p,p);
    c[4*p+1] = (r >= s) ? r-s : xn(inc1)(c+3*p+1,p);

    /* c[2p..3p] <- (a*b) mod (BASE^p + 1) */
    xn(mmul)(c+2*p, c+3*p+1,p);

    /* c[2p..3p] <- (c[2p..3p] - c[0..2p-1])/3 mod (BASE^p + 1)
       pour diviser par 3, on utilise le code avec ndoubles en mode Clong
       et celui avec chiffres en modes Dlong et Slong car il est plus
       rapide. */
    if (xn(dec)(c+2*p,p, c,p)) xn(inc1)(c+2*p,p+1);
    xn(inc)(c+2*p,p+1, c+p,p);

#ifdef use_clong
    for (i=3*p, t=0; i>=2*p; i--) {
      t = (t << HW) + (ndouble)c[i];
      c[i] = t/3;
      t = (t + c[i]) & 3; /* t %= 3; */
    }
    if (t) {
      s = t*((BASE_2)+(BASE_2-1))/3;
      for (i=2*p; i<3*p; i++) {
        t += (ndouble)c[i] + (ndouble)s;
        c[i] = t;
        t >>= HW;
      }
    }
    c[3*p] += t;
#else
    for (i=3*p, r=0; i>=2*p; i--) {
      s = c[i];
      t = r + s; r += (t < r);
      t = t/3 + r*(((BASE_2)+(BASE_2-1))/3);
      r = (t+s) & 3;
      c[i] = t;
    }
    if (r) {
      s = r*(((BASE_2)+(BASE_2-1))/3);
      for (i=2*p; i<3*p; i++) {
	r += s;
	t = c[i] + r; r = (t < r);
	c[i] = t;
      }
    }
    c[3*p] += r;
#endif /* use_clong */

    /* a <- c[0..3p] - (BASE^p - 1)*c[2p..3p] */
    xn(move)(c,3*p+1, a);
    xn(inc)(a,  3*p+1, c+2*p,p+1);
    xn(dec)(a+p,2*p+1, c+2*p,p+1);

    /* termin� */
    xn(free_tmp)(c);
  }

}
#endif /* assembly_sn_mmul */

/* -------------------- Contr�le */

#ifdef debug_mmul
void xn(mmul_buggy)     (chiffre *a, chiffre *b, long n);
void xn(mmul)(chiffre *a, chiffre *b, long n) {
  chiffre *x,*y,*z,r;

  /* tampons pour les op�randes et le r�sultat correct */
  x = xn(alloc_tmp)(4*n+4); y = x+n+1; z = y+n+1;

  /* effectue le calcul dans N et normalise le r�sultat */
  xn(toommul)(a,n+1,b,n+1,x);
  r    = xn(inc)(x,n,x+2*n,2);
  r    = xn(dec)(x,n,&r, 1);
  r   += xn(dec)(x,n,x+n,n);
  x[n] = xn(inc)(x,n,&r, 1);

  /* copie les arguments et effectue le calcul douteux */
  xn(move)(a,n+1,y);
  xn(move)(b,n+1,z);
  xn(mmul_buggy)(a,b,n);

  /* v�rifie que b est conserv� mod BASE^n + 1 et que a == x */
  if ((!xn(mequal)(z,b,n)) || (!xn(mequal)(x,a,n)))
      xn(internal_error)("error in mmul", 5,y,n+1,z,n+1,a,n+1,b,n+1,x,n+1);


  xn(free_tmp)(x);
}
#endif /* debug_mmul */

                       /* +---------------------------+
                          |  Carr� modulo BASE^n + 1  |
                          +---------------------------+ */

/*
  entr�e :
  a = naturel de longueur n+1

  contrainte : n > 0

  sortie :
  a <- a^2 mod (BASE^n + 1)
*/

#ifndef assembly_sn_msqr
#ifdef debug_mmul
void xn(msqr_buggy)
#else
void xn(msqr)
#endif
(chiffre *a, long n) {

  /* normalise a */
  a[n] = (xn(dec)(a,n,a+n,1)) ? xn(inc1)(a,n) : 0;

  /* cas o� a = BASE^n -> res = 1 */
  if (a[n]) {a[0] = 1; a[n] = 0; return;}

  /*----------------------------------------------------------------------
    si n est petit ou non divisible par 3, carr� ordinaire
    puis r�duction :
    a^2 = x + y*BASE^n + z*BASE^(2n) = x - y + z mod (BASE^n + 1)
  */
  if ((n <= msqr_lim) || (n%3)) {
    chiffre *c;

    c = xn(alloc_tmp)(2*n);
    xn(toomsqr)(a,n,c);
    a[n] = (xn(sub)(c,n,c+n,n,a)) ? xn(inc1)(a,n) : 0;
    xn(free_tmp)(c);
    return;
  }

  /*----------------------------------------------------------------------
    si n est divisible par 3, on d�compose en un carr� modulo BASE^p + 1
    et un modulo BASE^(2p) - BASE^p + 1
  */
  {
    long i,p = n/3;
    chiffre *c,r,s;
    unsigned long t;

    /* tampon de travail */
    c = xn(alloc_tmp)(6*p);

    /* c[4p..6p-1] <- a mod (BASE^(2p) - BASE^p + 1) */
    r  = xn(sub)(a,p,   a+2*p,p, c+4*p);
    s  = xn(add)(a+p,p, a+2*p,p, c+5*p);
    s -= xn(dec)(c+5*p,p, &r,1);
    if (s) {xn(dec1)(c+4*p,2*p); xn(inc1)(c+5*p,p);}

    /* c[0..2p-1] <- a^2 mod (BASE^(2p) - BASE^p + 1) */
    xn(toomsqr)(c+4*p,2*p, c);
    r = xn(dec)(c,2*p, c+3*p,p);
    s = xn(inc)(c+p,p, c+2*p,p) - xn(dec)(c,2*p, c+2*p,p);
    if (r > s)      {xn(inc1)(c,2*p); xn(dec1)(c+p,p);}
    else if (r < s) {xn(dec1)(c,2*p); xn(inc1)(c+p,p);}

    /* c[2p..3p} <- a^2 mod (BASE^p + 1) */
    r = xn(add)(a,p, a+2*p,p, c+2*p);
    s = xn(dec)(c+2*p,p, a+p,p);
    c[3*p] = (r >= s) ? r-s : xn(inc1)(c+2*p,p);
    xn(msqr)(c+2*p,p);

    /* c[2p..3p] <- (c[2p..3p] - c[0..2p-1])/3 mod (BASE^p + 1)
       pour diviser par 3, on utilise le code avec ndoubles en mode Clong
       et celui avec chiffres en modes Dlong et Slong car il est plus
       rapide. */
    if (xn(dec)(c+2*p,p, c,p)) xn(inc1)(c+2*p,p+1);
    xn(inc)(c+2*p,p+1, c+p,p);

#ifdef use_clong
    for (i=3*p, t=0; i>=2*p; i--) {
      t = (t << HW) + (ndouble)c[i];
      c[i] = t/3;
      t = (t + c[i]) & 3; /* t %= 3; */
    }
    if (t) {
      s = t*((BASE_2)+(BASE_2-1))/3;
      for (i=2*p; i<3*p; i++) {
        t += (ndouble)c[i] + (ndouble)s;
        c[i] = t;
        t >>= HW;
      }
    }
    c[3*p] += t;
#else
    for (i=3*p, r=0; i>=2*p; i--) {
      s = c[i];
      t = r + s; r += (t < r);
      t = t/3 + r*(((BASE_2)+(BASE_2-1))/3);
      r = (t+s) & 3;
      c[i] = t;
    }
    if (r) {
      s = r*(((BASE_2)+(BASE_2-1))/3);
      for (i=2*p; i<3*p; i++) {
	r += s;
	t = c[i] + r; r = (t < r);
	c[i] = t;
      }
    }
    c[3*p] += r;
#endif /* use_clong */

    /* a <- c[0..3p] - (BASE^p - 1)*c[2p..3p] */
    xn(move)(c,3*p+1, a);
    xn(inc)(a,  3*p+1, c+2*p,p+1);
    xn(dec)(a+p,2*p+1, c+2*p,p+1);

    /* termin� */
    xn(free_tmp)(c);
  }

}
#endif /* assembly_sn_msqr */

/* -------------------- Contr�le */

#ifdef debug_mmul
void xn(msqr_buggy)(chiffre *a,long n);
void xn(msqr)(chiffre *a, long n) {
  chiffre *x,*y,r;

  /* tampons pour les op�randes et le r�sultat correct */
  x = xn(alloc_tmp)(2*n+2); y = x+n+1;

  /* effectue le calcul dans N et normalise le r�sultat */
  xn(toomsqr)(a,n+1,x);
  r    = xn(inc)(x,n,x+2*n,2);
  r    = xn(dec)(x,n,&r, 1);
  r   += xn(dec)(x,n,x+n,n);
  x[n] = xn(inc)(x,n,&r, 1);

  /* copie l'argument et effectue le calcul douteux */
  xn(move)(a,n+1,y);
  xn(msqr_buggy)(a,n);

  /* v�rifie que a == x */
  if (!xn(mequal)(x,a,n))
      xn(internal_error)("error in msqr", 3,y,n+1,a,n+1,x,n+1);

  xn(free_tmp)(x);
}
#endif /* debug_mmul */

                     /* +------------------------------+
                        |  Papillon modulo BASE^n + 1  |
                        +------------------------------+ */

/*
  entr�e :
  a = naturel de longueur n+1
  b = naturel de longueur n+1 non confondu avec a
  q = entier tel que 0 <= q <= 2*n*HW
  s = 0 ou 1

  contraintes : n >= 3 et si q est impair, n doir �tre pair

  sortie :
  a <- a + (-1)^s * b * 2^(q/2) mod (BASE^n + 1)
  b <- a - (-1)^s * b * 2^(q/2) mod (BASE^n + 1)

  remarque :
  2^(1/2) = BASE^(3n/4)*(BASE^(n/2) + 1) mod (BASE^n + 1)
*/
#ifndef assembly_sn_butterfly
#ifdef debug_butterfly
void xn(butterfly_buggy)
#else
void xn(butterfly)
#endif
(chiffre *a, chiffre *b, long n, long q, int s) {

#ifdef ndouble
  long i,j,k,l,m;
  chiffre *x;
  ndouble r;
  zdouble z;

  /* tampon de travail */
  x = xn(alloc_tmp)(n+1);

  /* 2^(q/2) = (-1)^s * BASE^m * 2^k * (BASE^(n/2) + 1)^l */
  /* avec 0 <= m < n, 0 <= k < HW et 0 <= l <= 1          */
  l = q % 2;
  k = q/2 + 3*l*n*HW/4;
  m = k/HW;
  k %= HW;
  /* pas besoin de diviser, m/n <= 7/4 */
  /* v�rifier et remplacer par if     */
  while (m >= n) {m -= n; s ^= 1;}

  /* b <- b * (BASE^(n/2) + 1)^l mod (BASE^n + 1) */
  if (l) {
    xn(move)(b+n/2,n/2+1,x);
    for (r=0, i=0, j=n/2; j<n; i++, j++) {         /* haut <- haut + bas */
      r += (ndouble)b[i] + (ndouble)b[j];
      b[j] = r;
      r >>= HW;
    }
    r += (ndouble)b[n];
    for (z = -r, i=0; i <= n/2; i++) {             /* bas <- bas - haut */
      z += (ndouble)b[i] - (ndouble)x[i];
      b[i] = z;
      z >>= HW;
    }
    for (; (z) && (i < n); i++) {                  /* propage la retenue */
      z += (ndouble)b[i];
      b[i] = z;
      z >>= HW;
    }
    b[n] = (z) ? xn(inc1)(b,n) : 0;
  }

  /* b <- b * 2^k mod (BASE^n + 1) */
  if (k) {
    r = xn(shift_up)(b,n,b,k);                     /* d�cale les n premiers */
    r += (ndouble)b[n] << k;                       /* chiffres sur place    */
    for (z = -r, i=0; (z) && (i < n); i++) {       /* r�injecte la fin du   */
      z += (ndouble)b[i];                          /* n-�me et le (n-1)-�me */
      b[i] = z;
      z >>= HW;
    }
    b[n] = (z) ? xn(inc1)(b,n) : 0;
  }

  /* x <- b * BASE^m mod (BASE^n + 1), normalis� */
  xn(move)(b,n-m,x+m);                             /* haut <- bas */
  if (m) {
    for (z=0, i=0, j=n-m; i < m; i++, j++) {       /* bas <- -haut */
      z -= (ndouble)b[j];
      x[i] = z;
      z >>= HW;
    }
    for (z -= (ndouble)b[n]; (z) && (i < n); i++) {/* propage la retenue */
      z += x[i];
      x[i] = z;
      z >>= HW;
    }
    x[n] = (z) ? xn(inc1)(x,n) : 0;
  } else x[n] = (xn(dec)(x,n,b+n,1)) ? xn(inc1)(x,n) : 0;

  /* force 1 <= a[n] <= BASE-2 pour absorber les retenues */
  if (a[n] == (BASE_2)+(BASE_2-1)) a[n] = (BASE_2)+(BASE_2-2) - xn(dec1)(a,n);
  else if (a[n] == 0) a[n] = 1 + xn(inc1)(a,n);

  /* a <- a + (-1)^s * x, b <- a - (-1)^s * x */
  if (s) {xn(add)(a,n+1,x,n+1,b); xn(dec)(a,n+1,x,n+1);}
  else   {xn(sub)(a,n+1,x,n+1,b); xn(inc)(a,n+1,x,n+1);}

  /* termin� */
  xn(free_tmp)(x);

#else /* ndouble */
  long i,j,k,l,m;
  chiffre *x,r,u,v;

  /* tampon de travail */
  x = xn(alloc_tmp)(n+1);

  /* 2^(q/2) = (-1)^s * BASE^m * 2^k * (BASE^(n/2) + 1)^l */
  /* avec 0 <= m < n, 0 <= k < HW et 0 <= l <= 1          */
  l = q % 2;
  k = q/2 + 3*l*n*HW/4;
  m = k/HW;
  k %= HW;
  /* pas besoin de diviser, m/n <= 7/4 */
  /* v�rifier et remplacer par if     */
  while (m >= n) {m -= n; s ^= 1;}

  /* b <- b * (BASE^(n/2) + 1)^l mod (BASE^n + 1) */
  if (l) {
    xn(move)(b+n/2,n/2+1,x);
    for (r=0, i=0, j=n/2; j<n; i++, j++) {         /* haut <- haut + bas */
      u = b[i] + r; r  = (u < r);
      v = b[j] + u; r |= (v < u);
      b[j] = v;
    }
    b[n] = r;
    for (r=x[n/2], i=0; i <= n/2; i++) {           /* bas <- bas - haut */
      u = x[i] + r; r = (u < r);
      v = b[i] - u; r += (b[i] < u);
      b[i] = v;
    }
    for (; (r) && (i < n); i++) {                  /* propage la retenue */
      u = b[i] - r; r = (b[i] < r);
      b[i] = u;
    }
    if (r) xn(inc1)(b,n+1);
  }

  /* b <- b * 2^k mod (BASE^n + 1) */
  if (k) {
    r = xn(shift_up)(b,n+1,b,k);
    u = b[0] - b[n]; r += (b[0] < b[n]);
    b[0] = u;
    for (i=1; (r) && (i < n); i++) {
      u = b[i] - r; r = (b[i] < r);
      b[i] = u;
    }
    b[n] = (r) ? xn(inc1)(b,n) : 0;
  }

  /* x <- b * BASE^m mod (BASE^n + 1), normalis� */
  xn(move)(b,n-m,x+m);                             /* haut <- bas */
  if (m) {
    for (r=0, i=0, j=n-m; i < m; i++, j++) {       /* bas <- -haut */
      u = -r - b[j]; r |= (u != 0);
      x[i] = u;
    }
    u = b[n] + r; r  = (u < r);
    v = x[i] - u; r += (x[i] < u);
    x[i] = v;
    for (i++; (r) && (i < n); i++) {         /* propage la retenue */
      u = x[i] - r; r = (x[i] < u);
      x[i] = u;
    }
    x[n] = (r) ? xn(inc1)(x,n) : 0;
  } else x[n] = (xn(dec)(x,n,b+n,1)) ? xn(inc1)(x,n) : 0;

  /* force 1 <= a[n] <= BASE-2 pour absorber les retenues */
  if (a[n] == (BASE_2)+(BASE_2-1)) a[n] = (BASE_2)+(BASE_2-2) - xn(dec1)(a,n);
  else if (a[n] == 0) a[n] = 1 + xn(inc1)(a,n);

  /* a <- a + (-1)^s * x, b <- a - (-1)^s * x */
  if (s) {xn(add)(a,n+1,x,n+1,b); xn(dec)(a,n+1,x,n+1);}
  else   {xn(sub)(a,n+1,x,n+1,b); xn(inc)(a,n+1,x,n+1);}

  /* termin� */
  xn(free_tmp)(x);

#endif /* ndouble */
}
#endif /* assembly_sn_butterfly */


/* -------------------- Contr�le */

#ifdef debug_butterfly
void xn(butterfly_buggy)(chiffre *a, chiffre *b, long n, long q, int s);
void xn(butterfly)(chiffre *a, chiffre *b, long n, long q, int s) {
  long k,l;
  chiffre *x,*y,*z,*t,r;

  x = xn(alloc_tmp)(4*n+4); y = x+n+1; z = y+n+1; t = z+n+1;

  /* (-1)^s*2^(q/2) = 2^k ou 2^k + 2^l */
  k = (s*n*HW + q/2 + 3*(q%2)*n*HW/4) % (2*n*HW);
  l = (k + n*HW/2) % (2*n*HW);

  /* x <- 2^(q/2) */
  xn(clear)(x,2*n);
  x[k/HW] = (chiffre)1 << (k%HW);
  if (q%2) x[l/HW] = (chiffre)1 << (l%HW);
  x[n] = (xn(dec)(x,n,x+n,n)) ? xn(inc1)(x,n) : 0;

  /* y <- x * b */
  xn(toommul)(x,n+1,b,n+1,y);
  r    = xn(inc)(y,n,y+2*n,2);
  r    = xn(dec)(y,n,&r,1);
  r   += xn(dec)(y,n,y+n,n);
  y[n] = xn(inc)(y,n,&r, 1);

  /* x <- a + y */
  r    = xn(add)(a,n,y,n,x);
  r    = xn(dec)(x,n,&r, 1);
  r   += xn(dec)(x,n,a+n,1);
  r   += xn(dec)(x,n,y+n,1);
  x[n] = xn(inc)(x,n,&r,1);

  /* y <- a - y */
  r  = xn(sub)(a,n,y,n,y);
  r += xn(dec)(y,n,a+n,1);
  if (xn(inc)(y,n,y+n,1)) r += xn(dec1)(y,n);
  y[n] = xn(inc)(y,n,&r,1);

  /* sauve les arguments et effectue le calcul douteux */
  xn(move)(a,n+1,z);
  xn(move)(b,n+1,t);
  xn(butterfly_buggy)(a,b,n,q,s);
  if ((!xn(mequal)(a,x,n)) || (!xn(mequal)(b,y,n)))
      xn(internal_error)("error in butterfly", 6,
                         z,n+1,t,n+1,a,n+1,b,n+1,x,n+1,y,n+1);


  xn(free_tmp)(x);
}
#endif /* debug_butterfly */

