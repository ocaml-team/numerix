// file kernel/n/c/mul_n2.c: O(n^2) multiplication of natural integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                        Multiplication quadratique                     |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                    /* +------------------------------+
                       |  Multiplication par un long  |
                       +------------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = long >= 0
  c = naturel de longueur la, peut �tre confondu avec a

  sortie :
  c <- a*b
  retourne la retenue
*/
#ifndef assembly_sn_mul_1
unsigned long xn(mul_1)(chiffre *a, long la, unsigned long b, chiffre *c) {
#ifdef ndouble
  ndouble r;
  long i;

#if chiffres_per_long == 2
  if (b > 2*BASE_2) { /* multiplication � deux chiffres */
    ndouble b0 = b & (2*BASE_2-1), b1 = b >> HW;
    ndouble s;

    for (r=s=0, i=0; i<la; i++) {
      r = (s & (2*BASE_2-1)) + (ndouble)a[i] * b0;
      s = (s >> HW) + (r >> HW) + (ndouble)a[i] * b1;
      c[i] = r;
    }
    return(s);
  }
#endif /* chiffres_per_long == 2 */

  for (r = 0, i=0; i < la; i++) {
    r += (ndouble)a[i] * b;
    c[i] = r;
    r >>= HW;
  }
  return(r);

#else
  chiffre r,s,t;
  long i;

#if chiffres_per_long == 2
  if (b > 2*BASE_2) { /* multiplication � deux chiffres */
    chiffre b0 = b & (2*BASE_2-1), b1 = b >> HW;
    chiffre u,v;

    for (r=s=0, i=0; i<la; i++) {
      xn(mul_0)(a[i],b0,&u,&v);
      t = r + u;    v += (t < u);
      xn(mul_0)(a[i],b1,&u,&r);
      u += s;       s = r + (u < s);
      r =  u + v;   s += (r < v);
      c[i] = t;
    }
    return((unsigned long)r + (s << HW));
  }
#endif /* chiffres_per_long == 2 */

  for (r = 0, i=0; i < la; i++) {
    xn(mul_0)(a[i],b,&s,&t);
    s += r;       r = t + (s < r);
    c[i] = s;
  }
  return((unsigned long)r);
#endif /* ndouble */
}
#endif /* assembly_sn_mul_1 */

                     /* +------------------------------+
                        |  Multiplication quadratique  |
                        +------------------------------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur la+lb, non confondu avec a ou b

  contrainte : 0 < lb <= la

  sortie :
  c <- a*b
*/
#ifndef assembly_sn_mul_n2
#ifdef debug_mul_n2
void xn(mul_n2_buggy)
#else
void xn(mul_n2)
#endif
(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
#ifdef ndouble
  ndouble m,r;
  long i,j;

  /* effectue la multiplication chiffre par chiffre */
  xn(clear)(c,la);
  for (i=0; i<lb; i++, b++, c++) {
    for (m=(ndouble)b[0], r=0, j=0; j<la; j++) {
      r += m*(ndouble)a[j] + (ndouble)c[j];
      c[j] = r;
      r >>= HW;
    }
    c[la] = r;
  }
#else
  chiffre r,s,t;
  long i,j;

  /* effectue la multiplication chiffre par chiffre */
  xn(clear)(c,la);
  for (i=0; i<lb; i++, b++, c++) {
    for (r=0, j=0; j<la; j++) {
      xn(mul_0)(a[j],b[0],&s,&t);
      s += r;       r = t + (s < r);
      t = s + c[j]; r += (t < s);
      c[j] = t;
    }
    c[la] = r;
  }
#endif /* ndouble */
}
#endif /* assembly_sn_mul_n2 */

                                /* +---------+
                                   |  Carr�  |
                                   +---------+ */

/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur 2*la, non confondu avec a

  contrainte : la > 0

  sortie :
  b <- a^2
*/
#ifndef assembly_sn_sqr_n2
#ifdef debug_mul_n2
void xn(sqr_n2_buggy)
#else
void xn(sqr_n2)
#endif
(chiffre *a, long la, chiffre *b) {

/* renvoie vers mul_n2 si c'est plus rapide (Pentium4 en mode Clong) */
#if defined(bad_sqr_n2)
    xn(mul_n2)(a,la,a,la,b);

#elif defined(ndouble)
/* calcule s�par�ment les carr�s et les doubles produits */
  ndouble m,r;
  long i,j;

  /* b <- sum(a[i]*a[j]*BASE^(i+j), 0 <= i < j < la */
  xn(clear)(b,la);
  for (i=0; i<la-1; i++) {
    for (m=(ndouble)a[i], r=0, j=i+1; j<la; j++) {
      r += m*(ndouble)a[j] + (ndouble)b[i+j];
      b[i+j] = r;
      r >>= HW;
    }
    b[i+la] = r;
  }

  /* b <- 2*b + sum(a[i]^2*BASE^(2*i), 0 <= i < la) */
  b[2*la-1] = 0; xn(inc)(b,2*la,b,2*la);
  for (i=0, r=0; i<la; i++) {
    r += (ndouble)a[i]*(ndouble)a[i] + (ndouble)b[2*i];
    b[2*i] = r;
    r = (r >> HW) + (ndouble)b[2*i+1];
    b[2*i+1] = r;
    r >>= HW;
  }
#else
/* calcule s�par�ment les carr�s et les doubles produits */
  chiffre r,s,t;
  long i,j;

  /* b <- sum(a[i]*a[j]*BASE^(i+j), 0 <= i < j < la */
  xn(clear)(b,la);
  for (i=0; i<la-1; i++) {
    for (r=0, j=i+1; j<la; j++) {
      xn(mul_0)(a[i],a[j],&s,&t);
      s += r;         r = t + (s < r);
      t = s + b[i+j]; r += (t < s);
      b[i+j] = t;
    }
    b[i+la] = r;
  }

  /* b <- 2*b + sum(a[i]^2*BASE^(2*i), 0 <= i < la) */
  b[2*la-1] = 0; xn(inc)(b,2*la,b,2*la);
  for (i=0, r=0; i<la; i++) {
    xn(sqr_0)(a[i],&s,&t);
    s += r;           r = t + (s < r);
    t = s + b[2*i];   r += (t < s);    b[2*i]   = t;
    t = r + b[2*i+1]; r = (t < r);     b[2*i+1] = t;
  }

#endif /* bad_sqr_n2, ndouble */
}
#endif /* assembly_sn_sqr_n2 */

                              /* +------------+
                                 |  Contr�le  |
                                 +------------+ */

#if defined(debug_mul_n2) || defined(debug_karamul)
/*
  entr�e :
  a = naturel de longueur la
  b = naturel de longueur lb
  c = naturel de longueur la+lb

  contrainte : 0 < lb <= la

  sortie :
  retourne 1 si c = a*b, 0 sinon
*/
int xn(ctrl_mul)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {
  chiffre r,s,t,u,v;
  long i,j,k;

  /* calcule sum(a[i]*b[j], i+j=k, k=0..la+lb-1) */
  for (r=s=t=0, k=0; k<la+lb; k++) {
    for (i=0, j=k; j>=0; i++, j--)
      if ((i < la) && (j < lb)) {
	xn(mul_0)(a[i],b[j],&u,&v);
	r += u; v += (r < u);
	s += v; t += (s < v);
      }
    if (c[k] != r) return(0);
    r = s; s = t; t = 0;
  }

  return(1);
}
#endif /* defined(debug_mul_n2) || defined(debug_karamul) */

#ifdef debug_mul_n2
void xn(mul_n2_buggy) (chiffre *a, long la, chiffre *b, long lb, chiffre *c);
void xn(sqr_n2_buggy) (chiffre *a, long la, chiffre *b);

void xn(mul_n2)(chiffre *a, long la, chiffre *b, long lb, chiffre *c) {

  if (la < lb)
      xn(internal_error)("error, mul_n2 is called with la < lb",0);
  xn(mul_n2_buggy)(a,la,b,lb,c);
  if (!xn(ctrl_mul)(a,la,b,lb,c))
      xn(internal_error)("error in mul_n2",3,a,la,b,lb,c,la+lb);
}

void xn(sqr_n2)(chiffre *a, long la, chiffre *b) {
  xn(sqr_n2_buggy)(a,la,b);
  if (!xn(ctrl_mul)(a,la,a,la,b))
      xn(internal_error)("error in sqr_n2",2,a,la,b,2*la);

}
#endif /* debug_mul_n2 */

