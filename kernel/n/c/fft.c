// file kernel/n/c/fft.c: Fast Fourier transform, low level functions
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                       Transform�e de Fourier rapide                   |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  D�finir le symbole fft_iterative pour utiliser des impl�mentations it�ratives
  de sn_fft et sn_fft_inv. Sinon, on utilise des impl�mentations r�cursives ...
  d�r�cursifi�es. Exp�rimentalement, les version r�cursives sont nettement plus
  rapides pour des grands nombres : gain de 20% pour une multiplication de deux
  nombres de un million de bits sur K7-550 en mode slong (avec k=11). Ce gain
  est probablement d� � une meilleure utilisation de la m�moire cache
  (cf. http://cr.yp.to/papers/m3.pdf).

  Le code it�ratif est conserv� pour comprendre ce qui est fait dans le code
  r�cursif.
*/
#undef fft_iterative

                       /* +--------------------------+
                          |  Transform�e de Fourier  |
                          +--------------------------+ */

/*
  entr�e :
  a = 2^k naturels cons�cutifs de longueurs n+1

  contrainte :
  n doit �tre pair, n >= 2
  n*HW doit �tre divisible par 2^(k-2) : n*HW = p*2^(k-2)

  sortie :
  pour i=0..2^k-1,
       a[i] <- sum(a[j]*2^(p*inv(i,k)*j/2), j=0..2^k-1) mod (BASE^n + 1)
  avec
       inv(i,k) = inversion binaire de i sur k bits
*/

#ifndef assembly_sn_fft

/* -------------------- version it�rative */

#ifdef  fft_iterative
void xn(fft)(chiffre *a, long n, long k) {
  long d,i,j,l,m,q,r;
  chiffre *x, *y;

  for (l=0, d=(long)1<<(k-1); l<k; l++, d>>=1) {
    /*
      calcule les fft sur 2^(l+1) points pour les coefficients
      espac�s de d en d avec d = 2^(k-l-1)

      pour i=0..2^(l+1)-1, pour m=0..d-1,
        a[i*d + m] <-
            sum(a_orig[j*d + m]*2^(p*inv(i,l+1)*j*d/2), j=0..2^(l+1)-1)
            mod (BASE^n + 1)

      r�currence :
      pour i=0..2^l-1, pour m=0..d-1,
        x <- a[2*i*d     + m]
        y <- a[(2*i+1)*d + m]
        a[2*i*d +     m] <- x + y*2^(p*inv(i,l)*d/2) mod (BASE^n + 1)
        a[(2*i+1)*d + m] <- x - y*2^(p*inv(i,l)*d/2) mod (BASE^n + 1)
    */
    for (i=0, x=a, y=a+d*(n+1); i>>l == 0; i++, x=y, y+=d*(n+1)) {

      /* q <- p*inv(i,l)*d */
      for (j=i, q=0, r=l; r; q = (q<<1)+(j&1), j >>= 1, r--);
      q *= (2*n*HW) >> l;

      for (m=0; m<d; m++, x+=n+1, y+=n+1) xn(butterfly)(x,y,n,q,0);
    }
  }
}

#else

/* -------------------- version r�cursive d�r�cursifi�e */

void xn(fft)(chiffre *a, long n, long k) {
  long d,i,j,l,m,q,r;
  chiffre *x,*y;

  /* boucle de r�cursion, j = nb de couples termin�s, d = taille bloc */
  for (l=0, j = (long)1<<(k-1), d = j; l>=0; ) {

    /* q <- p*inv(i,l)*d */
    for (i = j >> (k-l-1), q=0, r=l; r; q = (q<<1)+(i&1), i >>= 1, r--);
    q *= (2*n*HW) >> l;

    /* addition/soustraction/d�calage des deux premiers blocs de taille d */
    for (m=0, x=a, y=a+d*(n+1); m<d; m++, x+=n+1, y+=n+1) {
      xn(butterfly)(x,y,n,q,0);
    }

    /* descend dans la r�cursion ou remonte au bloc suivant */
    if (l+1 < k) {l++; d >>= 1;}
    else {j++; a = y; while ((d&j) == 0) {l--; d <<= 1;}}
  }
}
#endif  /* fft_iterative */
#endif  /* assembly_sn_fft */


                   /* +----------------------------------+
                      |  Transform�e de Fourier inverse  |
                      +----------------------------------+ */
/*
  entr�e :
  a = 2^k naturels cons�cutifs de longueurs n+1
  f = entier naturel tel que 0 < f <= n

  contrainte :
  n doit �tre pair, n >= 2
  n*HW doit �tre divisible par 2^(k-2) : n*HW = p*2^(k-2)

  sortie :
  pour i=0..2^k-1,
       a[i] <- sum(a[j]*2^(-p*i*inv(j,k)/2), j=0..2^k-1) mod (BASE^n + 1)
  avec inv(j,k) = inversion binaire de i sur k bits
*/

#ifndef assembly_sn_fft_inv

/* -------------------- version it�rative */

#ifdef fft_iterative
void xn(fft_inv)(chiffre *a, long n, long k) {
  long d,i,l,m;
  chiffre *x, *y;

  for (l=0, d=1; l<k; l++, d<<=1) {
    /*
      calcule les fft inverses sur 2d points pour les coefficients
      par groupes de 2d avec d = 2^l

      pour i=0..2d-1, pour m=0..2^(k-l-1)-1,
        a[i + 2*m*d] <-
            sum(a_orig[j + 2*m*d]*2^(-p*i*inv(j,l+1)*2^(k-l-2)), j=0..2*d-1)
            mod (BASE^n + 1)

      r�currence :
      pour i=0..d-1, pour m=0..2^(k-l-1)-1,
        x <- a[i + 2*m*d]
        y <- a[i + (2*m+1)*d]
        a[i + 2*m*d]     <- x - y*2^(p*(d-i)*2^(k-l-2)) mod (BASE^n + 1)
        a[i + (2*m+1)*d] <- x + y*2^(p*(d-i)*2^(k-l-2)) mod (BASE^n + 1)
    */
    for (m=0, x=a, y=a+d*(n+1); m>>(k-l-1) == 0; m++, x=y, y+=d*(n+1)) {
      for (i=0; i<d; i++, x+=n+1, y+=n+1)
        xn(butterfly)(x,y,n,(d-i)*((2*n*HW) >> l),1);
    }
  }
}

#else

/* -------------------- version r�cursive d�r�cursifi�e */

void xn(fft_inv)(chiffre *a, long n, long k) {
  long d,i,j,kmax;
  chiffre *x,*y;

  /* boucle de r�cursion, j = nb de couples non trait�s, d = taille bloc */
  for (kmax=k, j=(long)1<<(k-1), d=1, k=1; k<=kmax; ) {

    /* d�calage/addition/soustraction des deux derniers blocs de taille d */
    for (i=0, x=a-(2*d-2)*(n+1), y=x+d*(n+1); i<d; i++, x+=n+1, y+=n+1) {
      xn(butterfly)(x,y,n,(d-i)*((2*n*HW)>>(k-1)),1);
    }

    /* remonte dans la r�cursion ou descend au couple suivant */
    if (d==1) j--;
    if (d&j) {a+= 2*(n+1); d=1; k=1;} else {d <<= 1; k++;}
  }
}
#endif  /* fft_iterative */
#endif  /* assembly_sn_fft_inv */

                  /* +------------------------------------+
                     |  D�composition nombre -> polyn�me  |
                     +------------------------------------+ */

/*
  entr�e :
  a = naturel de longueur la >= 0
  b = 2^k naturels cons�cutifs de longueurs n+1, non confondus avec a
  f = entier naturel tel que 0 < f <= n

  sortie :
  x <- a mod (BASE^(2^k*f) - 1)
  pour i=0..2^k-1, b[i] <- x[i*f .. (i+1)*f-1]
*/
#ifndef assembly_sn_fft_split
void xn(fft_split)(chiffre *a, long la, chiffre *b, long n, long k, long f) {
  long i,l;
  chiffre *x,r;

  /* copie le d�but de a dans b par blocs de f chiffres */
  xn(clear)(b,(n+1)<<k);
  for (i=0, x=b; ((i >> k) == 0) && (la > 0); la -= f, a+=f, x+=n+1, i++) {
    l = (la < f) ? la : f;
    xn(move)(a,l,x);
  }

  /* s'il reste des chiffres dans a, les ajoute � ceux d�j� copi�s
     en propageant les retenues */
  for (i=0, r=0, x=b; la > 0; la -= f, a+=f, x+=n+1, i++) {
    if (i >> k) {x=b; i=0;}
    l = (la < f) ? la : f;
    r =  xn(inc)(x,f,&r,1);
    r += xn(inc)(x,f,a, l);
  }

  /* propage la derni�re retenue */
  for ( ; r; x+=n+1, i++) {if (i >> k) {x=b; i=0;} r = xn(inc1)(x,f);}

}
#endif /* assembly_sn_fft_split */


                  /* +------------------------------------+
                     |  Recomposition polyn�me -> nombre  |
                     +------------------------------------+ */

/*
  entr�e :
  a = 2^k naturels cons�cutifs de longueurs n+1
  b = naturel de longueur f*2^k, peut �tre confondu avec a
  f = entier naturel tel que 0 < f <= n

  sortie :
  a <- ind.
  b <- sum((a[i]/2^k mod (BASE^n+1))*BASE^(i*f), i=0..2^k-1) mod BASE^(f*2^k)-1

  contrainte :
  les nombres a[i] doivent �tre congrus modulo BASE^n + 1 � des entiers
  divisibles par 2^k et compris entre 0 et BASE^n - 1
*/
#ifndef assembly_sn_fft_merge
void xn(fft_merge)(chiffre *a, chiffre *b, long n, long k, long f) {
  long i,m;
  chiffre *x,*y,r;

  /* normalise a[0] */
  if (xn(dec)(a,n,a+n,1)) xn(inc1)(a,n);
  a[n]=0;

  /* normalise et cumule les autres coefficients */
  for (i=1, x=a+n+1, y=a+f; i >> k == 0; i++, x+=n+1, y+=f) {
    if (xn(dec)(x,n,x+n,1)) xn(inc1)(x,n);
    y[n] = xn(add)(x,n,y,n+1-f,y);
  }

  /* r�duction modulo BASE^(f*2^k) - 1 */
  m = f<<k;
  if (xn(inc)(a,m, a+m,n-f)) xn(inc1)(a,m);
  for (i=0; ((i < m) && (a[i] == (BASE_2)+(BASE_2-1))); i++);
  if (i == m) xn(clear)(a,m);

  /* b <- a/2^k */
  r = xn(shift_down)(a,m,b,k);
  b[m-1] += r << (HW-k);

}
#endif /* assembly_sn_fft_merge */

                  /* +-------------------------------------+
                     |  Heuristique pour acc�l�rer la FFT  |
                     +-------------------------------------+ */

/*
   entr�e :
   n = taille des blocs (calculs modulo BASE^n + 1)
   p = diviseur impos� pour n

   sortie :
   retourne une nouvelle valeur de n sup�rieure ou �gale � celle fournie,
   pour laquelle les calculs sont exp�rimentalement plus rapides.
*/

long xn(fft_improve)(long n, long p) {

/* temps en microsecondes pour fftmod(k) (processeur = K7-550, mode slong)

  n |   k=6    k=7    k=8 |   k=9    k=10     k=11 |   k=12   k=13 |
----|---------------------|------------------------|---------------|
 24 |   636   1386   3042 |  6767   16129          |               |
    |---------------------|------------------------|               |
 26 |   781   1689   3750 |                        |               |
 28 |   823   1783   3900 |  8632                  |               |
 30 |   851   1862   4102 |                        |               |
 32 |   870   1879   4064 |  9714    21182   47547 | 107568        |
 34 |  1034   2248   4937 |                        |               |
 36 |   981   2159   4696 | 11166                  |               |
    |---------------------|                        |               |
 38 |  1200   2593   5662 |                        |               |
 40 |  1204   2587   5722 | 13334    29559         |               |
 42 |  1208   2606   5830 |                        |               |
 44 |  1415   3091   6645 | 15467                  |               |
 46 |  1549   3327   7266 |                        |               |
 48 |  1254   2721   6023 | 14538    31774   73077 |               |
    |---------------------|------------------------|               |
 50 |  1671   3612   7852 |                        |               |
 52 |  1729   3722   8087 | 18868                  |               |
 54 |  1598   3482   7680 |                        |               |
 56 |  1886   3992   8712 | 20100    44545         |               |
 58 |  2040   4391   9573 |                        |               |
 60 |  1767   3823   8451 | 20200                  |               |
    |---------------------|                        |               |
 62 |  2252   4845  11050 |                        |               |
 64 |  2190   4471  10255 | 22472    49750  110000 | 242187 531428 |
 66 |  2063   4469  10471 |                        |               |
 68 |  2506   5358  12269 | 26757                  |               |
 70 |  2636   5634  12903 |                        |               |
 72 |  2146   4677  10973 | 24146    54166         |               |
    |---------------------|------------------------|               |
 74 |  2814   5988  13741 |                        |               |
 76 |  2855   6128  13958 | 30469                  |               |
 78 |  2563   5568  12949 |                        |               |
 80 |  2902   6175  14295 | 31250    68571  151667 |               |
 82 |  3201   6875  15703 |                        |               |
 84 |  2729   5934  13724 | 30312                  |               |
    |---------------------|                        |               |
 86 |  3465   7383  16854 |                        |               |
 88 |  3441   7408  16780 | 36790    80555         |               |
 90 |  2979   6506  15151 |                        |               |
 92 |  3740   8016  18363 | 40000                  |               |
 94 |  3889   8340  19238 |                        |               |
 96 |  3065   6645  15461 | 34310    76154  170000 | 373000        |
    |---------------------|------------------------|               |
 98 |  4028   8712  20098 |                        |               |
100 |  4106   8860  20200 | 44002                  |               |
102 |  3644   7882  18532 |                        |               |
104 |  4235   9140  20937 | 45227    99500         |               |
106 |  4529   9951  22000 |                        |               |
108 |  3713   8105  18773 | 41458                  |               |
    |---------------------|                        |               |
110 |  4810  10311  23721 |                        |               |
112 |  4633   9849  22583 | 49500   107222  235000 |               |
114 |  4168   9041  21146 |                        |               |
116 |  5038  10919  24500 | 54444                  |               |
118 |  5328  11469  25843 |                        |               |
120 |  4334   9352  21848 | 47143   106667         |               |
    |---------------------|------------------------|               |
122 |  5626  12024  27162 |                        |               |
124 |  5686  12595  27222 | 59393                  |               |
126 |  4680  10860  23837 |                        |               |
128 |  5644  12406  26666 | 57353   125000  273333 | 589166 1261666|
130 |  6188  13724  29853 |                        |               |
132 |  5050  11647  25000 | 55556                  |               |
    |---------------------|                        |               |
134 |  6360  14214  30625 |                        |               |
136 |  6326  14284  30303 | 66333   144166         |               |
138 |  5546  12687  27917 |                        |               |
140 |  6790  15000  32581 | 70714                  |               |
142 |  7007  15581  33667 |                        |               |
144 |  5181  11915  26579 | 57353   127857  283333 |               |
--- |---------------------|------------------------|---------------|

R�gle empirique : pour n >= 32 viser le prochain multiple de 3 autoris�,
sauf si l'accroissement r�sultant est sup�rieur ou �gal � 10%.
�a ne vaut pas le coup de viser un multiple de 9, on aura chang� de k avant
que ce soit payant. Conclusions identiques en modes clong et dlong.
*/
  long m;

  if (n <= 32) {return(n);}
  m = (n%3) ? (((n+p)%3) ? n+2*p : n+p) : n;
  if (10*m >= 11*n) return(n);
  return(m);
}

