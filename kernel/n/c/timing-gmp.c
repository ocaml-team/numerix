// file kernel/n/c/timing-gmp.c: timing of gmp-functions
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |             Chronom�trage des fonctions internes de GMP               |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <gmp.h>

                      /* +------------------+
                         |  Multiplication  |
                         +------------------+ */

/* la, lb quelconques, force 0 < lb <= la */
void size_mul(long *la, long *lb, long *lc, long *ld) {
  long x;
  if (!*la) (*la)++; if (!*lb) (*lb)++;
  if (*la < *lb) {x = *la; *la = *lb; *lb = x;}
  *lc = -(*la + *lb);
  *ld = 0;
}

/* la = 1.5*lb > 0 */
void size_mul_2(long *la, long *lb, long *lc, long *ld) {
  if (!*lb) (*lb)++;
  *la = (3* *lb + 1)/2;
  *lc = -(*la + *lb);
  *ld = 0;
}

void calc_mul(mpz_t a, mpz_t b, mpz_t c, mpz_t d) {mpz_mul(c,a,b);}

                           /* +---------+
                              |  Carr�  |
                              +---------+ */

/* la > 0 */
void size_sqr(long *la, long *lb, long *lc, long *ld) {
  if (!*la) (*la)++;
  *lb = -2*(*la);
  *lc = 0;
  *ld = 0;
}

void calc_sqr(mpz_t a, mpz_t b, mpz_t c, mpz_t d) {mpz_mul(b,a,a);}

                         /* +------------+
                            |  Division  |
                            +------------+ */

/* la = 2lb, lb >= 2 */
void size_div(long *la, long *lb, long *lc, long *ld) {
  if (*lb < 2) *lb = 2;
  *la = 2*(*lb);
  *lc = -(*la - *lb);
  *ld = -(*la);
}

/* la = 1.5lb, lb >= 2 */
void size_div_2(long *la, long *lb, long *lc, long *ld) {
  if (*lb < 2) *lb = 2;
  *la = (3*(*lb))/2;
  *lc = -(*la - *lb);
  *ld = -(*la);
}

/* la = 3lb, lb >= 2 */
void size_div_3(long *la, long *lb, long *lc, long *ld) {
  if (*lb < 2) *lb = 2;
  *la = (3*(*lb));
  *lc = -(*la - *lb);
  *ld = -(*la);
}

void calc_div(mpz_t a, mpz_t b, mpz_t c, mpz_t d) {mpz_fdiv_qr(c,d,a,b);}

                       /* +-----------------+
                          |  Racine carr�e  |
                          +-----------------+ */

void size_sqrt(long *la, long *lb, long *lc, long *ld) {
  (*la) = 2*(*lb); if (*la < 2) *la = 2;
  *lb = -(*la)/2;
  *lc = -(*la);
  *ld = 0;
}

void calc_sqrt(mpz_t a, mpz_t b, mpz_t c, mpz_t d){mpz_sqrt(b,a);}

                            /* +----------------+
                               |  Racine p-�me  |
                               +----------------+ */

void size_root3(long *la, long *lb, long *lc, long *ld) {
  if (*la < 1) *la = 1;
  *lb = -(*la + 2)/3;
  *lc = 0;
  *ld = 0;
}

void size_root5(long *la, long *lb, long *lc, long *ld) {
  if (*la < 1) *la = 1;
  *lb = -(*la + 4)/5;
  *lc = 0;
  *ld = 0;
}

void calc_root3(mpz_t a, mpz_t b, mpz_t c, mpz_t d){mpz_root(b,a,3);}
void calc_root5(mpz_t a, mpz_t b, mpz_t c, mpz_t d){mpz_root(b,a,5);}

                      /* +----------------------------+
                         |  Exponentiation modulaire  |
                         +----------------------------+ */


void size_powm(long *la, long *lb, long *lc, long *ld) {
  if (*la < 1) *la = 1;
  *lb = *la;
  *lc = *la;
  *ld = -(*la);
}

void calc_powm0(mpz_t a, mpz_t b, mpz_t c, mpz_t d) {mpz_clrbit(c,0); mpz_powm(d,a,b,c);}
void calc_powm1(mpz_t a, mpz_t b, mpz_t c, mpz_t d) {mpz_setbit(c,0); mpz_powm(d,a,b,c);}

                                /* +--------+
                                   |  PGCD  |
                                   +--------+ */

void size_gcd(long *la, long *lb, long *lc, long *ld) {
    *lb = *la;
    *lc = 0;
    *ld = 0;
}

void calc_gcd(mpz_t a, mpz_t b, mpz_t c, mpz_t d) {
    mpz_t r;
    mpz_init(r);
    mpz_gcd(r,a,b);
    mpz_clear(r);
}

void calc_gcd_ex(mpz_t a, mpz_t b, mpz_t c, mpz_t d) {
    mpz_t r,u,v;
    mpz_init(r);
    mpz_init(u);
    mpz_init(v);
    mpz_gcdext(r,u,v,a,b);
    mpz_clear(r);
    mpz_clear(u);
    mpz_clear(v);
}

                   /* +----------------------------------+
                      |  Description des chronom�trages  |
                      +----------------------------------+ */

typedef void gsize(long *, long *, long *, long *   );
typedef void gcalc(mpz_t, mpz_t, mpz_t, mpz_t);

typedef struct {
  int      sel;       /* op�ration s�lectionn�e */
  char     *name;     /* nom symbolique         */
  char     *desc;     /* description            */
  gsize    *size;     /* calcul de taille       */
  gcalc    *calc;     /* fonction de calcul     */
} test_t;


/*
  size(&la, &lb, &lc, &ld) :

    d�termine  les tailles  de  quatre naturels  a,b,c,d  � partir  de
    tailles al�atoires donn�es en entr�e.  En sortie, une taille nulle
    signifie que le  naturel associ� ne sera pas  utilis� ; une taille
    n�gative signifie  que le naturel  sera utilis� comme  r�sultat et
    qu'il n'a pas  besoin d'�tre initialis�. Dans ce  cas, la taille �
    allouer pour ce naturel est la valeur absolue du nombre retourn�.

  calc(a,la, b,lb, c,d) :

    effectue  un calcul  sur  les entiers  naturels a,b,c,d.  Ceux-cis
    doivent avoir  les tailles retourn�es par la  fonction [size]. Les
    op�randes  pour  lesquels  [size]  retourne  une  taille  positive
    doivent �tre  initialis�s, les autres  param�tres peuvent contenir
    des valeurs quelconques.
*/


                      /* +-------------------+
                         |  Liste des tests  |
                         +-------------------+ */

test_t test[] = {

/* multiplication */
{0, NULL,          "Multiplication n by n",        NULL,           NULL              },
{0, "mul",         "multiplication",               size_mul,       calc_mul          },

/* multiplication avec des op�randes de longueurs diff�rentes */
{0, NULL,          "Multiplication 1.5n by n",     NULL,           NULL              },
{0, "mul_2",       "multiplication",               size_mul_2,     calc_mul          },

/* carr� */
{0, NULL,          "Square",                       NULL,           NULL              },
{0, "sqr",         "square",                       size_sqr,       calc_sqr          },

/* division 2n par n */
{0, NULL,          "Division 2n by n",             NULL,           NULL              },
{0, "div",         "division",                     size_div,       calc_div          },

/* division 1.5n par n */
{0, NULL,          "Division 1.5n by n",           NULL,           NULL              },
{0, "div_2",       "division",                     size_div_2,     calc_div          },

/* division 3n par n */
{0, NULL,          "Division 3n by n",             NULL,           NULL              },
{0, "div_3",       "division",                     size_div_3,     calc_div          },

/* racine carr�e */
{0, NULL,          "Square root",                  NULL,           NULL              },
{0, "sqrt",        "square root",                  size_sqrt,      calc_sqrt         },

/* racine p-�me */
{0, NULL,          "pth root",                     NULL,           NULL              },
{0, "root3",       "cubic root",                   size_root3,     calc_root3        },
{0, "root5",       "quintic root",                 size_root5,     calc_root5        },

/* exponentiation modulaire */
{0, NULL,          "Modular exponentiation",       NULL,           NULL              },
{0, "powm0",       "mod. exp., even modulus",      size_powm,      calc_powm0        },
{0, "powm1",       "mod. exp., odd modulus",       size_powm,      calc_powm1        },

/* pgcd */
{0, NULL,          "Greatest common divisor",      NULL,           NULL              },
{0, "gcd",         "gcd",                          size_gcd,       calc_gcd          },
{0, "gcdex",       "extended gcd",                 size_gcd,       calc_gcd_ex       }

};
#define nbtests (sizeof(test)/sizeof(test_t))


                         /* +----------------------+
                            |  Variables globales  |
                            +----------------------+ */

#define HW (8*sizeof(long))          /* taille mot */
gmp_randstate_t gmp_randstate;       /* g�n�rateur al�atoire de gmp */


                    /* +---------------------------------+
                       |  Chronom�trage d'une op�ration  |
                       +---------------------------------+ */

/*
   entr�e :
   t     = num�ro de l'op�ration � chronom�trer
   temps = dur�e en secondes d'un chronom�trage
   bits  = taille en bits d'un op�rande
   unit  = p�riode de l'horloge en micro-secondes

   sortie :
   m = dur�e moyenne d'une op�ration
   n = nombre d'op�rations effectu�es
*/

void chronometre(int t, long *m, long *n, double temps, long bits, double unit) {
  long    la, lb, lc, ld;       /* longueurs des op�randes         */
  mpz_t   a,  b,  c,  d;        /* op�randes pour la pr�-mesure    */
  MP_INT  **aa,**bb,**cc,**dd;  /* op�randes pour la mesure r�elle */
  long    i,j,p;
  double  t0,t1;                /* compteurs de tics   */

/* nb maxi de tests */
#define nmax 10000

  /* taille des op�randes */
  la = lb = lc = ld = (bits+HW-1)/HW;
  test[t].size(&la, &lb, &lc, &ld);

  /* Compte le nb d'it�rations pour atteindre la dur�e demand�e. */
  /* On effectue une r�gle de 3 � partir du nb d'it�rations      */
  /* constat� pour une dur�e de 1s.                              */
  /* Si le nombre d'it�rations est trop grand, tronque � 10000   */
  mpz_init(a); if (la > 0) mpz_urandomb(a, gmp_randstate, la*HW);
  mpz_init(b); if (lb > 0) mpz_urandomb(b, gmp_randstate, lb*HW);
  mpz_init(c); if (lc > 0) mpz_urandomb(c, gmp_randstate, lc*HW);
  mpz_init(d); if (ld > 0) mpz_urandomb(d, gmp_randstate, ld*HW);

  for (*n=0, t0=clock(), t1=0; t1 < CLOCKS_PER_SEC; (*n)++) {
    test[t].calc(a,b,c,d);
    t1 = clock()-t0;
    if (t1 < 0) {t0 = clock(); t1 = 0; *n = -1;}
  }

  if (temps > t1/CLOCKS_PER_SEC) {
    *n = *n*temps*(CLOCKS_PER_SEC/t1);
    if (*n <= 0)    *n = 1;

    /* tire min(n,nmax) jeux d'op�randes au hasard */
    p = *n; if (p > nmax) p = nmax;
#define init(xx,x,lx)                                                  \
  xx = (MP_INT **)malloc(*n*sizeof(mpz_t *));                          \
  if (xx == NULL) {fprintf(stderr,"out of memory\n"); exit(1);}        \
  if (lx > 0) {                                                        \
    for (i=0; i < p; i++) {                                            \
      xx[i] = (MP_INT *)malloc(sizeof(mpz_t));                         \
      if (xx[i] == NULL) {fprintf(stderr,"out of memory\n"); exit(1);} \
      mpz_init(xx[i]);                                                 \
      mpz_urandomb(xx[i],gmp_randstate,lx*HW);                         \
    }                                                                  \
  } else for (i=0; i < p; xx[i++] = x)
    init(aa,a,la);
    init(bb,b,lb);
    init(cc,c,lc);
    init(dd,d,ld);

    /* chronom�tre les n ex�cutions */
    for (i=0, t0=clock(); i < *n; i++) {
      j = i % nmax;
      test[t].calc(aa[j],bb[j],cc[j],dd[j]);
    }
    t1 = clock() - t0;
    if (t1 < 0) {
      /* d�bordement de l'horloge. Avec un peu de chance �a n'a d�bord�
         qu'une fois, */
      t1 += 1L << (8*sizeof(clock_t)-2);
      t1 += 1L << (8*sizeof(clock_t)-2);
      t1 += 1L << (8*sizeof(clock_t)-2);
      t1 += 1L << (8*sizeof(clock_t)-2);
    }

    /* lib�re la m�moire */
    for (i = p-1; i>=0; i--) {
      if (ld > 0) {mpz_clear(dd[i]); free(dd[i]);}
      if (lc > 0) {mpz_clear(cc[i]); free(cc[i]);}
      if (lb > 0) {mpz_clear(bb[i]); free(bb[i]);}
      if (la > 0) {mpz_clear(aa[i]); free(aa[i]);}
    }
    free(dd);     free(cc);     free(bb);     free(aa);
  }
  mpz_clear(d); mpz_clear(c); mpz_clear(b); mpz_clear(a);


  /* temps moyen */
  *m = (t1*unit + *n/2)/(*n);
}

                      /* +-----------------------------+
                         |  Conversion string -> long  |
                         +-----------------------------+ */

/*
  entr�e :
  s = cha�ne de caract�res

  sortie :
  retourne le nombre positif d�sign� par s s'il est valide, -1 sinon

  remarque :
  reconna�t les suffixes k (=10^3), K (=2^10), m (=10^6), M (=2^20), w (=HW)
*/

long getlong(char *s) {
  long res;

  /* lit la partie d�cimale */
  if ((*s > '9') || (*s < '0')) res = 1;
  else for (res=0; (*s >= '0') && (*s <= '9'); s++) res = res*10 + *s - '0';

  /* lit les suffixes */
  for(; (*s) && (res >= 0); s++) switch(*s) {
  case 'k' : res *= 1000;     break;
  case 'K' : res *= 1024;     break;
  case 'm' : res *= 1000000;  break;
  case 'M' : res *= 1048576;  break;
  case 'w' : res *= HW;       break;
  default  : res = -1;
  }

  /* retourne la valeur ou -1 en cas d'erreur */
  return(res);
}



                         /* +-----------------------+
                            |  Programme principal  |
                            +-----------------------+ */

int main(int argc, char **argv) {

#define default_time   10
#define default_bits 1000
  double temps = default_time;
  long   bits  = default_bits;
  double unit  = 1.0e6/CLOCKS_PER_SEC;

  int help = 0, list = 0, range = 0;
  long start = bits, stop = bits, step = 1;
  unsigned long seed = time(NULL);
  char *res = NULL;
  FILE *fres = NULL;

  long i,j,m,n;
  char **s;

  /* analyse la ligne de commande */
  for (j=argc-1, s=argv+1; (j) && (!help); j--, s++) {

    /* options sans argument */
    if (!strcmp(*s,"-h"))      {help = 1; continue;}
    if (!strcmp(*s,"-l"))      {list = 1; continue;}
    if (!strcmp(*s,"-ns"))     {unit = 1.0e9/CLOCKS_PER_SEC; continue;}
    if (!strcmp(*s,"-us"))     {unit = 1.0e6/CLOCKS_PER_SEC; continue;}
    if (!strcmp(*s,"-ms"))     {unit = 1.0e3/CLOCKS_PER_SEC; continue;}

    /* nom d'un algorithme � contr�ler */
    for (i = 0; i < nbtests; i++) {
      if ((test[i].name) && (strcmp(*s, test[i].name) == 0)) break;
    }
    if (i < nbtests) {test[i].sel ^= 1; continue;}

    /* options � arguments */
    if (j > 1) {
      if (!strcmp(*s,"-seed"))   {seed  = atol(*++s);    j--; continue;}
      if (!strcmp(*s,"-time"))   {temps = atof(*++s);    j--; continue;}
      if (!strcmp(*s,"-bits"))   {bits  = getlong(*++s); j--; continue;}
      if (!strcmp(*s,"-o"   ))   {res   = *++s;          j--; continue;}

      if ((!strcmp(*s,"-range")) && (j > 3)) {
        start = getlong(*++s);
        stop  = getlong(*++s);
        step  = getlong(*++s);
        range = 1;
        j    -= 3;
        continue;
      }
    }

    /* option non reconnue */
    printf("syntax error at: %s\ntype %s -h for help\n",*s,argv[0]);
    exit(1);
  }

  /* mode d'emploi */
  if (help) {
    printf("usage: %s <options> <tests>\n",argv[0]);
    printf("options:\n");
    printf("-h           print this help\n");
    printf("-l           print available tests\n");
    printf("-seed s      initialize the random generator\n");
    printf("-bits b      number of bits for an operand (default %d)\n", default_bits);
    printf("-range a b c test for bits = a,a+c,..,b\n");
    printf("-ns          display time in nanoseconds\n");
    printf("-us          display time in microseconds (default)\n");
    printf("-ms          display time in miliseconds\n");
    printf("-time t      duration in seconds for each test (default %d)\n",default_time);
    printf("-o <file>    send also output to <file>\n");
    exit(0);
  }

  if (list) {
    printf("available tests: (select twice to unselect)\n");
    for (i = 0; i < nbtests; i++) {
      if (test[i].name) printf("%-16s%s\n", test[i].name,    test[i].desc);
      else              printf("\n-------------------- %s\n",test[i].desc);
    }
    exit(0);
  }

  if ((bits < 0) || (start < 0) || (stop < 0) || (step < 0)) {
    printf("invalid bit or range specification\n");
    exit(1);
  }

  /* initialisation des g�n�rateurs al�atoires */
  srandom(seed);
  gmp_randinit_default(gmp_randstate);
  gmp_randseed_ui(gmp_randstate,seed);

  /* ouvre le fichier de sortie en mode append */
  if (res) {
      fres = fopen(res,"a");
      if (fres == NULL) {
          printf("unable to open %s\n",res);
          fflush(stdout);
          exit(1);
      }
  }

  /* tests pour une seule longueur */
  if (!range) {
    for (i=0; i < nbtests; i++) if (test[i].sel) {
      printf("%-16s", test[i].name);
      if (res) fprintf(fres,"%-16s", test[i].name);
      chronometre(i,&m,&n,temps,bits,unit);
      printf("%10ld (%ld)\n", m,n);
      if (res) fprintf(fres,"%10ld (%ld)\n", m,n);
    }
  }

  /* tests pour une plage de longueurs */
  else {

    /* recopie la ligne de commande */
    printf("#");
    for (j=argc, s=argv; j; j--, s++) printf(" %s",*s);
    printf("\n"); fflush(stdout);
    if (res) {
      fprintf(fres,"#");
      for (j=argc, s=argv; j; j--, s++) fprintf(fres," %s",*s);
      fprintf(fres,"\n");
    }

    /* noms des algorithmes */
    printf("#           bits");
    for (i=0; i < nbtests; i++) if (test[i].sel) printf("%16s", test[i].name);
    printf("\n");
    if (res) {
      fprintf(fres,"#           bits");
      for (i=0; i < nbtests; i++) if (test[i].sel) fprintf(fres,"%16s", test[i].name);
      fprintf(fres,"\n");
    }

    for (bits = start; bits <= stop; bits += step) {
      printf("%16ld", bits);
      if (res) fprintf(fres,"%16ld", bits);
      for (i=0; i < nbtests; i++) if (test[i].sel) {
        chronometre(i,&m,&n,temps,bits,unit);
        printf("%16ld",m);
        if (res) fprintf(fres,"%16ld",m);
      }
      printf("\n");
      if (res) fprintf(fres,"\n");
    }
  }

  /* termin� */
  return(0);
}

