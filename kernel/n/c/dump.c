// file kernel/n/c/dump.c: debugging facilities
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                                Affichage                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include <stdarg.h>

/* choix du format d'impression en fonction de l'architecture */
#ifdef bits_32
#ifdef use_clong
#define format "%04X"
#else
#define format "%08lX"
#endif
#endif

#ifdef bits_64
#ifdef use_clong
#define format "%08X"
#else
#define format "%016lX"
#endif
#endif

                         /* +----------------------+
                            |  Affiche un naturel  |
                            +----------------------+ */

/*
  entr�e :
  a = naturel de longueur la

  sortie :
  le nombre est affich� en hexad�cimal
*/

void xn(dump)(chiffre *a, long la) {
  long i;

  for (i=la-1; i >= 0; printf(format,a[i--]));
  printf("\n");
  fflush(stdout);
}

                           /* +------------------+
                              |  Erreur interne  |
                              +------------------+ */

/*
   entr�e :
   msg = cha�ne de caract�res
   n   = nombre de naturels � afficher
   ... = suite de couples (naturel, longueur)

   sortie :
   affiche le message et les naturels donn�s, puis termine le programme
*/
void xn(internal_error)(char *msg, int n, ...) {
  chiffre *a;
  long l;
  int i;
  va_list ap;

  va_start(ap,n);
  printf("\nNumerix kernel: %s\n", msg);
  for (i=1; i<=n; i++) {
    a = va_arg(ap, chiffre *);
    l = va_arg(ap, long);
    printf("arg%d = ",i); xn(dump)(a,l);
  }
  fflush(stdout);
  va_end(ap);
  exit(1);

}
