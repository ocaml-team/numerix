// file kernel/x/c/copy.c: extensible integer copying
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                               Copie                                   |
 |                                                                       |
 +-----------------------------------------------------------------------*/


/*----------------------------------------  copie, valeur absolue, oppos�
   entr�e :
   a = entier extensible
   _b = NULL ou pointeur sur un entier extensible
   s = 0 ou 1 ou 2

   sortie :
   si s = 0, b <- a
   si s = 1, b <- |a|
   si s = 2, b <- -a
   si _b != NULL, *_b <- b
   retourne b
   */
xint xx(private_copy)(xint *_b, xint a, long s) {
    long la = xx_lg(a);
    xint b;
    xx_push_roots_2(a,_b);
#ifdef caml_api
#define  a __lr.a
#define _b __lr._b
#endif

    b = xx(enlarge)(_b,la);
    if (b != a) xn(move)(a->val,la,b->val);
    switch(s) {
        case 0: b->hd = a->hd; break;
        case 1: b->hd = la;    break;
        case 2: b->hd = (la) ? a->hd ^ SIGN_m : 0;
    }

    xx_update_and_return(_b,b);

#undef  a
#undef _b
}

#if defined(caml_api) || defined(ocaml_api)

xint xx(copy) (xint *_b, xint a) {return xx(private_copy)(_b,a,0);}
xint xx(abs)  (xint *_b, xint a) {return xx(private_copy)(_b,a,1);}
xint xx(neg)  (xint *_b, xint a) {return xx(private_copy)(_b,a,2);}
xint xx(f_abs)          (xint a) {return xx(private_copy)(xx_null,a,1);}
xint xx(f_neg)          (xint a) {return xx(private_copy)(xx_null,a,2);}

xint xx(copy_out)(xint *_a)      {return xx(private_copy)(xx_null,*_a,0);}
xint xx(look)    (xint *_a)      {return(*_a);}

/* ------------------------------------------------------------ Make_ref
   entr�e :
   a = entier extensible

   sortie :
   retourne une r�f�rence sur a
*/
xint *xx(make_ref)(xint a) {
    long la = xx_lg(a);
    xint *r;
    xx_push_roots_11(a,b);
#ifdef caml_api
#define a __lr.a
#define b __lr.b
#endif

    b = xx(alloc)(0,la);
    xn(move)(a->val,la,b->val);
    b->hd = a->hd;

    r = (xint *)alloc_tuple(1);
    r[0] = b;
    xx_pop_roots();
    return(r);

#undef a
#undef b
}

#endif /* defined(caml_api) || defined(ocaml_api) */


