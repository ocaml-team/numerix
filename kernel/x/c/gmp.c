// file kernel/x/c/gmp.c: Caml/Ocaml interface with Gmp
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Interface Caml/Ocaml <-> Gmp                      |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                            /* +--------------+
                               |  exceptions  |
                               +--------------+ */

#define INTEGER_OVERFLOW   "Numerix kernel: integer overflow"
#define INVALID_STRING     "Numerix kernel: invalid string"
#define MULTIPLE_RESULT    "Numerix kernel: multiple result"
#define NEGATIVE_BASE      "Numerix kernel: negative base"
#define NEGATIVE_EXPONENT  "Numerix kernel: negative exponent"
#define NEGATIVE_INDEX     "Numerix kernel: negative index"
#define NEGATIVE_SIZE      "Numerix kernel: negative size"
/*
#define NUMBER_TOO_BIG     "Numerix kernel: number too big"
#define OUT_OF_MEMORY      "Numerix kernel: out of memory"
*/
#define ZERO_DIVISOR       "Numerix kernel: division by zero"


                               /* +---------+
                                  |  copie  |
                                  +---------+ */

gint gx_make_ref(gint a) {
    gint b;
    gx_alloc_1_1(b,a);
    mpz_set(b->val,a->val);
    return(b);
}

value gx_copy(gint b, gint a) {
    mpz_set(b->val,a->val);
    return(Val_unit);
}

gint gx_copy_out(gint a) {
    gint b;
    gx_alloc_1_1(b,a);
    mpz_set(b->val,a->val);
    return(b);
}

gint gx_look(gint a) {
    return(a);
}

                             /* +------------+
                                |  addition  |
                                +------------+ */

gint gx_f_add(gint a, gint b) {
    gint c;
    gx_alloc_1_2(c,a,b); 
    mpz_add(c->val,a->val,b->val);
    return(c);
}

gint gx_f_add_1(gint a, long b) {
    gint c;
    gx_alloc_1_1(c,a);
    b = Long_val(b);
    (b > 0) ? mpz_add_ui(c->val,a->val,b) : mpz_sub_ui(c->val,a->val,-b);
    return(c);
}

value gx_add(gint c, gint a, gint b) {
    mpz_add(c->val,a->val,b->val);
    return(Val_unit);
}

value gx_add_1(gint c, gint a, long b) {
    b = Long_val(b);
    (b > 0) ? mpz_add_ui(c->val,a->val,b) : mpz_sub_ui(c->val,a->val,-b);
    return(Val_unit);
}

                           /* +----------------+
                              |  soustraction  |
                              +----------------+ */

gint gx_f_sub(gint a, gint b) {
    gint c;
    gx_alloc_1_2(c,a,b); 
    mpz_sub(c->val,a->val,b->val);
    return(c);
}

gint gx_f_sub_1(gint a, long b) {
    gint c;
    gx_alloc_1_1(c,a); 
    b = Long_val(b);
    (b > 0) ? mpz_sub_ui(c->val,a->val,b) : mpz_add_ui(c->val,a->val,-b);
    return(c);
}

value gx_sub(gint c, gint a, gint b) {
    mpz_sub(c->val,a->val,b->val);
    return(Val_unit);
}

value gx_sub_1(gint c, gint a, long b) {
    b = Long_val(b);
    (b > 0) ? mpz_sub_ui(c->val,a->val,b) : mpz_add_ui(c->val,a->val,-b);
    return(Val_unit);
}

                          /* +------------------+
                             |  multiplication  |
                             +------------------+ */

gint gx_f_mul(gint a, gint b) {
    gint c;
    gx_alloc_1_2(c,a,b); 
    mpz_mul(c->val,a->val,b->val);
    return(c);
}

gint gx_f_mul_1(gint a, long b) {
    gint c;
    gx_alloc_1_1(c,a); 
    b = Long_val(b);
    mpz_mul_si(c->val,a->val,b);
    return(c);
}

value gx_mul(gint c, gint a, gint b) {
    mpz_mul(c->val,a->val,b->val);
    return(Val_unit);
}

value gx_mul_1(gint c, gint a, long b) {
    b = Long_val(b);
    mpz_mul_si(c->val,a->val,b);
    return(Val_unit);
}

                         /* +---------------------+
                            |  division g�n�rale  |
                            +---------------------+ */

value gx_gquomod(long mode, gint c, gint d, gint a, gint b) {
    mpz_t aa,bb;

    if ((c == d) && (c)) gx_failwith(MULTIPLE_RESULT);
    if (mpz_sgn(b->val) == 0) gx_failwith(ZERO_DIVISOR);
    switch(Round_val(mode)) {

        case 0:
            if (c) if (d) mpz_fdiv_qr(c->val,d->val,a->val,b->val);
                   else   mpz_fdiv_q (c->val,       a->val,b->val);
            else          mpz_fdiv_r (       d->val,a->val,b->val);
            return(Val_unit);

        case 1:
            mpz_init(bb); mpz_tdiv_q_2exp(bb,b->val,1);
            mpz_init(aa); mpz_add(aa,a->val,bb);
            if (d) {
                if (c) mpz_fdiv_qr(c->val,d->val,aa,b->val);
                else   mpz_fdiv_r (       d->val,aa,b->val);
                mpz_sub(d->val,d->val,bb);
            }
            else mpz_fdiv_q (c->val,aa,b->val);
            mpz_clear(aa);
            mpz_clear(bb);
            return(Val_unit);

        case 2:
            if (c) if (d) mpz_cdiv_qr(c->val,d->val,a->val,b->val);
                   else   mpz_cdiv_q (c->val,       a->val,b->val);
            else          mpz_cdiv_r (       d->val,a->val,b->val);
            return(Val_unit);

        default:
            mpz_init(bb); mpz_tdiv_q_2exp(bb,b->val,1);
            mpz_init(aa); mpz_sub(aa,a->val,bb);

            if (d) {
                if (c) mpz_cdiv_qr(c->val,d->val,aa,b->val);
                else   mpz_cdiv_r (       d->val,aa,b->val);
                mpz_add(d->val,d->val,bb);
            }
            else mpz_cdiv_q (c->val,aa,b->val);
            mpz_clear(aa);
            mpz_clear(bb);
            return(Val_unit);
    }

}

value gx_f_quomod(gint a, gint b) {
    gint c,d;
    value r;
    gx_alloc_n_2_2(r,2,c,d,a,b);
    gx_gquomod(0,c,d,a,b);
    Field(r,0) = (value)c;
    Field(r,1) = (value)d;
    return(r);
}

gint gx_f_quo(gint a, gint b) {
    gint c;
    gx_alloc_1_2(c,a,b);
    gx_gquomod(0,c,NULL,a,b);
    return(c);
}

gint gx_f_mod(gint a, gint b) {
    gint d;
    gx_alloc_1_2(d,a,b);
    gx_gquomod(0,NULL,d,a,b);
    return(d);
}

value gx_f_gquomod(long mode, gint a, gint b) {
    gint c,d;
    value r;
    gx_alloc_n_2_2(r,2,c,d,a,b);
    Field(r,0) = (value)c;
    Field(r,1) = (value)d;
    gx_gquomod(mode,c,d,a,b);
    return(r);
}

gint gx_f_gquo(long mode, gint a, gint b) {
    gint c;
    gx_alloc_1_2(c,a,b);
    gx_gquomod(mode,c,NULL,a,b);
    return(c);
}

gint gx_f_gmod(long mode, gint a, gint b) {
    gint d;
    gx_alloc_1_2(d,a,b);
    gx_gquomod(mode,NULL,d,a,b);
    return(d);
}

value gx_quomod(gint c, gint d, gint a, gint b) {
    return(gx_gquomod(0,c,d,a,b));
}

value gx_quo(gint c, gint a, gint b) {
    return(gx_gquomod(0,c,NULL,a,b));
}

value gx_mod(gint d, gint a, gint b) {
    return(gx_gquomod(0,NULL,d,a,b));
}

value gx_gquo(long mode, gint c, gint a, gint b) {
    return(gx_gquomod(mode,c,NULL,a,b));
}

value gx_gmod(long mode, gint d, gint a, gint b) {
    return(gx_gquomod(mode,NULL,d,a,b));
}

                       /* +-------------------------+
                          |  division � un chiffre  |
                          +-------------------------+ */

long gx_gquomod_1(long mode, gint c, gint a, long b) {
    long d;
    int inc_c;

    b = Long_val(b);
    mode = Round_val(mode);
    if (b == 0) gx_failwith(ZERO_DIVISOR);
    if (b > 0) {

        d = mpz_fdiv_q_ui(c->val,a->val,b);
        switch(mode) {
            case 0: inc_c = 0;         break;
            case 1: inc_c = (2*d >= b);break;
            case 2: inc_c = (d != 0);  break;
            default:inc_c = (2*d > b); break;
        }
    } else {

        d = -mpz_cdiv_q_ui(c->val,a->val,-b);
        mpz_neg(c->val, c->val);
        switch(mode) {
            case 0: inc_c = 0;         break;
            case 1: inc_c = (2*d <= b);break;
            case 2: inc_c = (d != 0);  break;
            default:inc_c = (2*d < b); break;
        }
    }

    if (inc_c) {mpz_add_ui(c->val,c->val,1); d = d - b;}
    return(Val_long(d));
}

long gx_f_gmod_1(long mode, gint a, long b) {
    long d;
    int inc_c;

    b = Long_val(b);
    mode = Round_val(mode);
    if (b == 0) gx_failwith(ZERO_DIVISOR);
    if (b > 0) {

        d = mpz_fdiv_ui(a->val,b);
        switch(mode) {
            case 0: inc_c = 0;         break;
            case 1: inc_c = (2*d >= b);break;
            case 2: inc_c = (d != 0);  break;
            default:inc_c = (2*d > b); break;
        }
    } else {

        d = -mpz_cdiv_ui(a->val,-b);
        switch(mode) {
            case 0: inc_c = 0;         break;
            case 1: inc_c = (2*d <= b);break;
            case 2: inc_c = (d != 0);  break;
            default:inc_c = (2*d < b); break;
        }
    }

    if (inc_c) {d = d - b;}
    return(Val_long(d));
}

value gx_f_quomod_1(gint a, long b) {
    gint c;
    long d;
    value r;
    gx_alloc_n_1_1(r,2,c,a);
    d = gx_gquomod_1(0,c,a,b);
    Field(r,0) = (value)c;
    Field(r,1) = (value)d;
    return(r);
}

gint gx_f_quo_1(gint a, long b) {
    gint c;
    gx_alloc_1_1(c,a);
    gx_gquomod_1(0,c,a,b);
    return(c);
}

value gx_f_gquomod_1(long mode, gint a, long b) {
    gint c;
    long d;
    value r;
    gx_alloc_n_1_1(r,2,c,a);
    d = gx_gquomod_1(mode,c,a,b);
    Field(r,0) = (value)c;
    Field(r,1) = (value)d;
    return(r);
}

gint gx_f_gquo_1(long mode, gint a, long b) {
    gint c;
    gx_alloc_1_1(c,a);
    gx_gquomod_1(mode,c,a,b);
    return(c);
}

long gx_f_mod_1(gint a, long b) {
    return(gx_f_gmod_1)(0,a,b);
}

value gx_quomod_1(gint c, gint a, long b) {
    return(gx_gquomod_1(0,c,a,b));
}

value gx_quo_1(gint c, gint a, long b) {
    gx_gquomod_1(0,c,a,b);
    return(Val_unit);
}

value gx_gquo_1(long mode, gint c, gint a, long b) {
    gx_gquomod_1(mode,c,a,b);
    return(Val_unit);
}


                          /* +------------------+
                             |  valeur absolue  |
                             +------------------+ */

gint gx_f_abs(gint a) {
    gint b;
    gx_alloc_1_1(b,a); 
    mpz_abs(b->val,a->val);
    return(b);
}

value gx_abs(gint b, gint a) {
    mpz_abs(b->val,a->val);
    return(Val_unit);
}

                              /* +----------+
                                 |  Oppos�  |
                                 +----------+ */


gint gx_f_neg(gint a) {
    gint b;
    gx_alloc_1_1(b,a); 
    mpz_neg(b->val,a->val);
    return(b);
}

value gx_neg(gint b, gint a) {
    mpz_neg(b->val,a->val);
    return(Val_unit);
}

                               /* +---------+
                                  |  carr�  |
                                  +---------+ */


gint gx_f_sqr(gint a) {
    gint b;
    gx_alloc_1_1(b,a); 
    mpz_mul(b->val,a->val,a->val);
    return(b);
}

value gx_sqr(gint b, gint a) {
    mpz_mul(b->val,a->val,a->val);
    return(Val_unit);
}


                          /* +------------------+
                             |  Exponentiation  |
                             +------------------+ */


gint gx_f_pow(gint a, long b) {
    gint c;
    b = Long_val(b);
    if (b < 0) gx_failwith(NEGATIVE_EXPONENT);
    gx_alloc_1_1(c,a);
    mpz_pow_ui(c->val,a->val,b);
    return(c);
}

gint gx_f_pow_1(long a, long b) {
    gint c;
    a = Long_val(a);
    b = Long_val(b);
    if (b < 0) gx_failwith(NEGATIVE_EXPONENT);
    gx_alloc_1_0(c);
    if (a >= 0) mpz_ui_pow_ui(c->val,a,b);
    else {
        mpz_ui_pow_ui(c->val,-a,b);
        if (b&1) mpz_neg(c->val,c->val);
    }
    return(c);
}

value gx_pow(gint c, gint a, long b) {
    b = Long_val(b);
    if (b < 0) gx_failwith(NEGATIVE_EXPONENT);
    mpz_pow_ui(c->val,a->val,b);
    return(Val_unit);
}

value gx_pow_1(gint c, long a, long b) {
    a = Long_val(a);
    b = Long_val(b);
    if (b < 0) gx_failwith(NEGATIVE_EXPONENT);
    if (a >= 0) mpz_ui_pow_ui(c->val,a,b);
    else {
        mpz_ui_pow_ui(c->val,-a,b);
        if (b&1) mpz_neg(c->val,c->val);
    }
    return(Val_unit);
}


                     /* +----------------------------+
                        |  Exponentiation modulaire  |
                        +----------------------------+ */

value gx_gpowmod(long mode, gint d, gint a, gint b, gint c) {
    mpz_t c1,c2;

    if (mpz_sgn(b->val) < 0) gx_failwith(NEGATIVE_EXPONENT);
    if (mpz_sgn(c->val) ==0) gx_failwith(ZERO_DIVISOR);
    if (c == d) mpz_init_set(c1,c->val); else memmove(c1,c->val,sizeof(c1));

    mpz_powm(d->val,a->val,b->val,c1);

    switch(Round_val(mode)) {

        case 0:
            mpz_fdiv_r(d->val,d->val,c1);
            break;

        case 1:
            mpz_init(c2);
            mpz_tdiv_q_2exp(c2,c1,1);
            mpz_add(d->val,d->val,c2);
            mpz_fdiv_r(d->val,d->val,c1);
            mpz_sub(d->val,d->val,c2);
            mpz_clear(c2);
            break;

        case 2:
            mpz_cdiv_r(d->val,d->val,c1);
            break;

        default:
            mpz_init(c2);
            mpz_tdiv_q_2exp(c2,c1,1);
            mpz_sub(d->val,d->val,c2);
            mpz_cdiv_r (d->val,d->val,c1);
            mpz_add(d->val,d->val,c2);
            mpz_clear(c2);
            break;
    }

    if (c->val == d->val) mpz_clear(c1);
    return(Val_unit);

}

gint gx_f_powmod(gint a, gint b, gint c) {
    gint d;
    gx_alloc_1_3(d,a,b,c);
    gx_gpowmod(0,d,a,b,c);
    return(d);
}

gint gx_f_gpowmod(long mode, gint a, gint b, gint c) {
    gint d;
    gx_alloc_1_3(d,a,b,c);
    gx_gpowmod(mode,d,a,b,c);
    return(d);
}

value gx_powmod(gint d, gint a, gint b, gint c) {
    return(gx_gpowmod)(0,d,a,b,c);
}

                           /* +-----------------+
                              |  Racine carr�e  |
                              +-----------------+ */

value gx_gsqrt(long mode, gint b, gint a) {
    mpz_t c;

    if (mpz_sgn(a->val) < 0) gx_failwith(NEGATIVE_BASE);
    switch(Round_val(mode)) {

        case 0: mpz_sqrt(b->val,a->val);
                return(Val_unit);

        case 2: mpz_init(c);
                mpz_sqrtrem(b->val,c,a->val);
                if (mpz_sgn(c) > 0) mpz_add_ui(b->val,b->val,1);
                mpz_clear(c);
                return(Val_unit);

        default:mpz_init(c);
                mpz_mul_2exp(c,a->val,2);
                mpz_sqrt(b->val,c);
                mpz_cdiv_q_2exp(b->val,b->val,1);
                mpz_clear(c);
                return(Val_unit);
    }
}

value gx_sqrt(gint b, gint a) {
    if (mpz_sgn(a->val) < 0) gx_failwith(NEGATIVE_BASE);
    mpz_sqrt(b->val,a->val);
    return(Val_unit);
}

gint gx_f_sqrt(gint a) {
    gint b;
    if (mpz_sgn(a->val) < 0) gx_failwith(NEGATIVE_BASE);
    gx_alloc_1_1(b,a);
    mpz_sqrt(b->val,a->val);
    return(b);
}

gint gx_f_gsqrt(long mode, gint a) {
    gint b;
    gx_alloc_1_1(b,a);
    gx_gsqrt(mode,b,a);
    return(b);
}

                           /* +----------------+
                              |  Racine p-�me  |
                              +----------------+ */


value gx_groot(long mode, gint b, gint a, long p) {
    mpz_t c;
    int exact,sa;

    p = Long_val(p);
    if (p <= 0) gx_failwith(NEGATIVE_EXPONENT);
    sa = mpz_sgn(a->val);
    if ((sa < 0) && ((p&1) == 0)) gx_failwith(NEGATIVE_BASE);
    switch(Round_val(mode)) {

        case 0: exact = mpz_root(b->val,a->val,p);
                if ((sa < 0) && (!exact)) mpz_sub_ui(b->val,b->val,1);
                return(Val_unit);

        case 2: exact = mpz_root(b->val,a->val,p);
                if ((sa > 0) && (!exact)) mpz_add_ui(b->val,b->val,1);
                return(Val_unit);

        default:mpz_init(c);
                mpz_mul_2exp(c,a->val,p);
                mpz_root(b->val,c,p);
                if (sa >= 0) mpz_cdiv_q_2exp(b->val,b->val,1);
                else         mpz_fdiv_q_2exp(b->val,b->val,1);
                mpz_clear(c);
                return(Val_unit);
    }

}

value gx_root(gint b, gint a, long p) {
    return(gx_groot(0,b,a,p));
}

gint gx_f_root(gint a, long p) {
    gint b;
    gx_alloc_1_1(b,a);
    gx_groot(0,b,a,p);
    return(b);
}

gint gx_f_groot(long mode, gint a, long p) {
    gint b;
    gx_alloc_1_1(b,a);
    gx_groot(mode,b,a,p);
    return(b);
}

                            /* +---------------+
                               |  factorielle  |
                               +---------------+ */

gint gx_f_fact(long n) {
    gint a;
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_BASE);
    gx_alloc_1_0(a);
    mpz_fac_ui(a->val,n);
    return(a);
}

value gx_fact(gint a, long n) {
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_BASE);
    mpz_fac_ui(a->val,n);
    return(Val_unit);
}

                               /* +--------+
                                  |  Pgcd  |
                                  +--------+ */


gint gx_f_gcd(gint a, gint b) {
    gint d;
    gx_alloc_1_2(d,a,b);
    mpz_gcd(d->val,a->val,b->val);
    return(d);
}

value gx_gcd(gint d, gint a, gint b) {
    mpz_gcd(d->val,a->val,b->val);
    return(Val_unit);
}

value gx_f_gcd_ex(gint a, gint b) {
    gint d,u,v;
    value r;
    gx_alloc_n_3_2(r,3,d,u,v,a,b);
    mpz_gcdext(d->val,u->val,v->val,a->val,b->val);
    mpz_neg(v->val,v->val);
    Field(r,0) = (value) d;
    Field(r,1) = (value) u;
    Field(r,2) = (value) v;
    return(r);
}

value gx_gcd_ex(gint d, gint u, gint v, gint a, gint b) {

    if ((d == u) || (d == v) || (u == v)) gx_failwith(MULTIPLE_RESULT);

    mpz_gcdext(d->val,u->val,v->val,a->val,b->val);
    mpz_neg(v->val,v->val);

    return(Val_unit);
}

value gx_f_cfrac(gint a, gint b) {
    gint d,u,v,p,q;
    value r;
    gx_alloc_n_5_2(r,5,d,u,v,p,q,a,b);

    mpz_gcdext(d->val,u->val,v->val,a->val,b->val);
    mpz_neg(v->val,v->val);

    if (mpz_sgn(d->val) > 0) {
        mpz_divexact(p->val,a->val,d->val);
        mpz_divexact(q->val,b->val,d->val);
    }
    else {
        mpz_set_ui(p->val,1); mpz_set_ui(u->val,1);
        mpz_set_ui(q->val,0); mpz_set_ui(v->val,0);
    }

    Field(r,0) = (value) d;
    Field(r,1) = (value) u;
    Field(r,2) = (value) v;
    Field(r,3) = (value) p;
    Field(r,4) = (value) q;
    return(r);
}

value gx_cfrac(gint d, gint u, gint v, gint p, gint q, gint a, gint b) {
    mpz_t aa,bb;
    int copy_a,copy_b;

    if ((d == u) || (d == v) || (d == p) || (d == q)
                 || (u == v) || (u == p) || (u == q)
                             || (v == p) || (v == q)
                                         || (p == q))
        gx_failwith(MULTIPLE_RESULT);

    copy_a = ((a == d) || (a == u) || (a == v) || (a == p) || (a == q));
    copy_b = ((b == d) || (b == u) || (b == v) || (b == p) || (b == q));
    if (copy_a) mpz_init_set(aa,a->val); else memmove(aa,a->val,sizeof(aa));
    if (copy_b) mpz_init_set(bb,b->val); else memmove(bb,b->val,sizeof(bb));
        
    mpz_gcdext(d->val,u->val,v->val,a->val,b->val);
    mpz_neg(v->val,v->val);

    if (mpz_sgn(d->val) > 0) {
        mpz_divexact(p->val,aa,d->val);
        mpz_divexact(q->val,bb,d->val);
    }
    else {
        mpz_set_ui(p->val,1); mpz_set_ui(u->val,1);
        mpz_set_ui(q->val,0); mpz_set_ui(v->val,0);
    }

    if (copy_a) mpz_clear(aa);
    if (copy_b) mpz_clear(bb);
    return(Val_unit);

}

value gx_cfrac_bytecode(gint *v, int argn) {
    return(gx_cfrac(v[0],v[1],v[2],v[3],v[4],v[5],v[6]));
}


                              /* +-------------+
                                 |  Primalit�  |
                                 +-------------+ */

value gx_isprime(gint a) {
  return(Val_tri(mpz_probab_prime_p(a->val,10)));
}

value gx_isprime_1(long a) {
  mpz_t aa;
  long r;
  mpz_init_set_si(aa,Long_val(a));
  r = Val_tri(mpz_probab_prime_p(aa,10));
  mpz_clear(aa);
  return(r);
}


                           /* +----------------+
                              |  comparaisons  |
                              +----------------+ */

long gx_sgn(gint a) {
    return(Val_long(mpz_sgn(a->val)));
}

long gx_cmp(gint a, gint b) {
    int r = mpz_cmp(a->val,b->val);
    if (r > 0) r = 1; else if (r < 0) r = -1;
    return(Val_long(r));
}

long gx_eq(gint a, gint b) {
    return(Val_bool(mpz_cmp(a->val,b->val) == 0));
}

long gx_neq(gint a, gint b) {
    return(Val_bool(mpz_cmp(a->val,b->val) != 0));
}

long gx_inf(gint a, gint b) {
    return(Val_bool(mpz_cmp(a->val,b->val) < 0));
}

long gx_infeq(gint a, gint b) {
    return(Val_bool(mpz_cmp(a->val,b->val) <= 0));
}

long gx_sup(gint a, gint b) {
    return(Val_bool(mpz_cmp(a->val,b->val) > 0));
}

long gx_supeq(gint a, gint b) {
    return(Val_bool(mpz_cmp(a->val,b->val) >= 0));
}

long gx_cmp_1(gint a, long b) {
    int r = mpz_cmp_si(a->val,Long_val(b));
    if (r > 0) r = 1; else if (r < 0) r = -1;
    return(Val_long(r));
}

long gx_eq_1(gint a, long b) {
    return(Val_bool(mpz_cmp_si(a->val,Long_val(b)) == 0));
}

long gx_neq_1(gint a, long b) {
    return(Val_bool(mpz_cmp_si(a->val,Long_val(b)) != 0));
}

long gx_inf_1(gint a, long b) {
    return(Val_bool(mpz_cmp_si(a->val,Long_val(b)) < 0));
}

long gx_infeq_1(gint a, long b) {
    return(Val_bool(mpz_cmp_si(a->val,Long_val(b)) <= 0));
}

long gx_sup_1(gint a, long b) {
    return(Val_bool(mpz_cmp_si(a->val,Long_val(b)) > 0));
}

long gx_supeq_1(gint a, long b) {
    return(Val_bool(mpz_cmp_si(a->val,Long_val(b)) >= 0));
}

                            /* +--------------+
                               |  conversion  |
                               +--------------+ */

gint gx_of_int(long a) {
    gint b;
    gx_alloc_1_0(b);
    mpz_set_si(b->val,Long_val(a));
    return(b);
}

value gx_copy_int(gint b, long a) {
    mpz_set_si(b->val,Long_val(a));
    return(Val_unit);
}

long gx_int_of(gint a) {
  if (mpz_sizeinbase(a->val,2) > 30) gx_failwith(INTEGER_OVERFLOW);
  return(Val_long(mpz_get_si(a->val)));
}

value gx_copy_string(gint a, char *s) {
    long l,b;
    int sa;

    l = strlen(s);
    switch(s[0]) {
        case '+' : sa = 0; s++; l--; break;
        case '-' : sa = 1; s++; l--; break;
        default  : sa = 0;           break;
    }

    b = 10;
    if ((l >= 2) && (s[0] == '0')) switch(s[1]) {
        case 'x': case 'X': b = 16; s += 2; l -= 2; break;
        case 'o': case 'O': b =  8; s += 2; l -= 2; break;
        case 'b': case 'B': b =  2; s += 2; l -= 2; break;
    }

    if (l == 0) gx_failwith(INVALID_STRING);

    if (mpz_set_str(a->val, s, b) != 0) gx_failwith(INVALID_STRING);
    if (sa) mpz_neg(a->val,a->val);

    return(Val_unit);

}

gint gx_of_string(char *s) {
    gint a;
    gx_alloc_1_1(a,s);
    gx_copy_string(a,s);
    return(a);
}

/* longueur maximale d'une cha�ne retourn�e par xstring_of */
#define max_string (Max_wosize*sizeof(long))

value gx_string_of(gint a) {
    long l;
    char *s;
    int sa = (mpz_sgn(a->val) < 0);

    l = mpz_sizeinbase(a->val,10); if (sa) l++;
    if (l > max_string) {
	gx_alloc_string_1(s,18,a);
        strcpy(s,"<very long number>");
        return((value)s);
    }
    gx_alloc_string_1(s,l,a);
    mpz_get_str(s,10,a->val);
    if (strlen(s) != l) return(copy_string(s)); else return((value)s);
}

char * gx_bstring_of(gint a) {
    long l,i;
    char *s;
    int sa = mpz_sgn(a->val);

    if (sa == 0) {
        gx_alloc_string_1(s,1,a);
        s[0] = '0';
        s[1] = 0;
        return(s);
    }

    l = mpz_sizeinbase(a->val,2); if (sa < 0) l++;
    if (l > max_string-2) {
	gx_alloc_string_1(s,18,a);
        strcpy(s,"<very long number>");
        return(s);
    }
    gx_alloc_string_1(s,l+2,a);
    mpz_get_str(s+2,2,a->val);
    i = 0;
    if (sa < 0) s[i++] = '-';
    s[i++] = '0';
    s[i++] = 'b';
    return(s);
}

char * gx_hstring_of(gint a) {
    long l,i;
    char *s,*t;
    int sa = mpz_sgn(a->val);

    if (sa == 0) {
        gx_alloc_string_1(s,1,a);
        s[0] = '0';
        s[1] = 0;
        return(s);
    }

    l = mpz_sizeinbase(a->val,16); if (sa < 0) l++;
    if (l > max_string-2) {
	gx_alloc_string_1(s,18,a);
        strcpy(s,"<very long number>");
        return(s);
    }
    gx_alloc_string_1(s,l+2,a);
    mpz_get_str(s+2,16,a->val);
    for (t=s+2; *t; t++) if ((*t) >= 'a') (*t) -= 32;
    i = 0;
    if (sa < 0) s[i++] = '-';
    s[i++] = '0';
    s[i++] = 'x';
    return(s);
}

char * gx_ostring_of(gint a) {
    long l,i;
    char *s;
    int sa = mpz_sgn(a->val);

    if (sa == 0) {
        gx_alloc_string_1(s,1,a);
        s[0] = '0';
        s[1] = 0;
        return(s);
    }

    l = mpz_sizeinbase(a->val,8); if (sa < 0) l++;
    if (l > max_string-2) {
	gx_alloc_string_1(s,18,a);
        strcpy(s,"<very long number>");
        return(s);
    }
    gx_alloc_string_1(s,l+2,a);
    mpz_get_str(s+2,8,a->val);
    i = 0;
    if (sa < 0) s[i++] = '-';
    s[i++] = '0';
    s[i++] = 'o';
    return(s);
}

                        /* +----------------------+
                           |  nombres al�atoires  |
                           +----------------------+ */

gmp_randstate_t gx_randstate;
int gx_rand_initialised = 0;

value gx_random_init(long n) {
    if (!gx_rand_initialised) {
        gmp_randinit_default(gx_randstate);
        gx_rand_initialised = 1;
    }
    n = Long_val(n);
    if (n == 0) n = time(NULL);
    gmp_randseed_ui(gx_randstate,n);
    return(Val_unit);
}

gint gx_f_nrandom(long n) {
    gint a;
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_SIZE);
    gx_alloc_1_0(a);
    if (!gx_rand_initialised) gx_random_init(3);
    mpz_urandomb(a->val,gx_randstate,n);
    return(a);
}

gint gx_f_zrandom(long n) {
    gint a;
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_SIZE);
    gx_alloc_1_0(a);
    if (!gx_rand_initialised) gx_random_init(3);
    mpz_urandomb(a->val,gx_randstate,n+1);
    if (mpz_tstbit(a->val,n)) mpz_neg(a->val,a->val);
    mpz_clrbit(a->val,n);
    return(a);
}

gint gx_f_nrandom1(long n) {
    gint a;
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_SIZE);
    gx_alloc_1_0(a);
    if (!gx_rand_initialised) gx_random_init(3);
    mpz_urandomb(a->val,gx_randstate,n);
    mpz_setbit(a->val,n);
    return(a);
}

gint gx_f_zrandom1(long n) {
    gint a;
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_SIZE);
    gx_alloc_1_0(a);
    if (!gx_rand_initialised) gx_random_init(3);
    mpz_urandomb(a->val,gx_randstate,n+1);
    if (mpz_tstbit(a->val,n)) mpz_neg(a->val,a->val);
    mpz_setbit(a->val,n);
    return(a);
}

value gx_nrandom(gint a, long n) {
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_SIZE);
    if (!gx_rand_initialised) gx_random_init(3);
    mpz_urandomb(a->val,gx_randstate,n);
    return(Val_unit);
}

value gx_zrandom(gint a, long n) {
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_SIZE);
    if (!gx_rand_initialised) gx_random_init(3);
    mpz_urandomb(a->val,gx_randstate,n+1);
    if (mpz_tstbit(a->val,n)) mpz_neg(a->val,a->val);
    mpz_clrbit(a->val,n);
    return(Val_unit);
}

value gx_nrandom1(gint a, long n) {
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_SIZE);
    if (!gx_rand_initialised) gx_random_init(3);
    mpz_urandomb(a->val,gx_randstate,n);
    mpz_setbit(a->val,n);
    return(Val_unit);
}

value gx_zrandom1(gint a, long n) {
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_SIZE);
    if (!gx_rand_initialised) gx_random_init(3);
    mpz_urandomb(a->val,gx_randstate,n+1);
    if (mpz_tstbit(a->val,n)) mpz_neg(a->val,a->val);
    mpz_setbit(a->val,n);
    return(Val_unit);
}


                      /* +--------------------------+
                         |  repr�sentation binaire  |
                         +--------------------------+ */


long gx_nbits(gint a) {
    if (mpz_sgn(a->val) == 0) return(Val_long(0));
    else return(Val_long(mpz_sizeinbase(a->val,2)));
}

long gx_lowbits(gint a) {
  return(Val_int(mpz_get_ui(a->val) & 0x7fffffff));
}

long gx_highbits(gint a) {
  long l = mpz_sizeinbase(a->val,2), res;
  mpz_t x;
  mpz_init(x);

  if (l < 31) {
    mpz_mul_2exp(x,a->val,31-l);
    res = mpz_get_ui(x);
    mpz_clear(x);
  }
  else if (l > 31) {
    mpz_tdiv_q_2exp(x,a->val,l-31);
    res = mpz_get_ui(x);
    mpz_clear(x);
  }
  else res = mpz_get_ui(a->val);

  return(Val_long(res));
}

long gx_nth_word(gint a, long n) {
  long n1,n2;

  n = Long_val(n);
  if (n < 0) return(Val_long(0));
  n1 = n/(sizeof(mp_limb_t)/2);
  n2 = 16*(n%(sizeof(mp_limb_t)/2));
  return(Val_long((n1 < abs((a->val[0])._mp_size))
                  ? ((a->val[0])._mp_d[n1] >> n2) & 0xffff
                  : 0));
}

long gx_nth_bit(gint a, long n) {
  long n1,n2;
  n = Long_val(n);
  if (n < 0) return(Val_bool(0));
  n1 = n/(8*sizeof(mp_limb_t));
  n2 = n - (8*sizeof(mp_limb_t))*n1;
  if (n1 >= abs((a->val[0])._mp_size)) return(Val_bool(0));
  return(Val_bool(((a->val[0])._mp_d[n1] >> n2) & 1));
}

                             /* +-------------+
                                |  d�calages  |
                                +-------------+ */


gint gx_f_shl(gint a, long n) {
    gint b;
    n = Long_val(n);
    gx_alloc_1_1(b,a);
    if (n >= 0) mpz_mul_2exp(b->val,a->val,n);
    else mpz_tdiv_q_2exp(b->val,a->val,-n);
    return(b);
}

gint gx_f_shr(gint a, long n) {
    gint b;
    n = Long_val(n);
    gx_alloc_1_1(b,a);
    if (n >= 0) mpz_tdiv_q_2exp(b->val,a->val,n);
    else mpz_mul_2exp(b->val,a->val,-n);
    return(b);
}

value gx_shl(gint b, gint a, long n) {
    n = Long_val(n);
    if (n >= 0) mpz_mul_2exp(b->val,a->val,n);
    else mpz_tdiv_q_2exp(b->val,a->val,-n);
    return(Val_unit);
}

value gx_shr(gint b, gint a, long n) {
    n = Long_val(n);
    if (n >= 0) mpz_tdiv_q_2exp(b->val,a->val,n);
    else mpz_mul_2exp(b->val,a->val,-n);
    return(Val_unit);
}

value gx_f_split(gint a, long n) {
    gint b,c;
    value r;
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_INDEX);
    gx_alloc_n_2_1(r,2,b,c,a);
    mpz_tdiv_q_2exp(b->val,a->val,n);
    mpz_tdiv_r_2exp(c->val,a->val,n);
    Field(r,0) = (value)b;
    Field(r,1) = (value)c;
    return(r);
}

value gx_split(gint b, gint c, gint a, long n) {
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_INDEX);
    if (b == c) gx_failwith(MULTIPLE_RESULT);
    if (a != b) {
        mpz_tdiv_q_2exp(b->val,a->val,n);
        mpz_tdiv_r_2exp(c->val,a->val,n);
    } else {
        mpz_tdiv_r_2exp(c->val,a->val,n);
        mpz_tdiv_q_2exp(b->val,a->val,n);
    }
    return(Val_unit);
}

gint gx_f_join(gint a, gint b, long n) {
    gint c;
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_INDEX);
    gx_alloc_1_2(c,a,b);
    mpz_mul_2exp(c->val,b->val,n);
    mpz_add(c->val,c->val,a->val);
    return(c);
}

value gx_join(gint c, gint a, gint b, long n) {
    mpz_t aa;
    n = Long_val(n);
    if (n < 0) gx_failwith(NEGATIVE_INDEX);
    if (c == a) mpz_init_set(aa,a->val); else memmove(aa,a->val,sizeof(aa));
    mpz_mul_2exp(c->val,b->val,n);
    mpz_add(c->val,c->val,aa);
    if (c == a) mpz_clear(aa);
    return(Val_unit);
}

                             /* +-------------+
                                |  affichage  |
                                +-------------+ */


value gx_dump(gint a) {
    if (a == NULL) printf("(null)\n");
    else {
        printf("(%p) [%d %c %d %p] ",a,
               a->val->_mp_alloc,
               (a->val->_mp_size < 0) ? '-' : '+',
               abs(a->val->_mp_size),
               a->val->_mp_d);
        mpz_out_str(NULL,16,a->val);
        printf("\n");
    }
    fflush(stdout);
    return(Val_unit);
}
