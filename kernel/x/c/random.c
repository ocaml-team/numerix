// file kernel/x/c/random.c: random extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                            Nombres al�atoires                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include <time.h>

/* initialisation du g�n�rateur al�atoire */
#ifdef c_api
void xx(random_init)(long n) {
    if (n == 0) n = time(NULL);
    srandom(n);
}
#elif defined(caml_api) || defined(ocaml_api)
value xx(random_init)(long n) {

    n = Long_val(n);
    if (n == 0) n = time(NULL);
    srandom(n);
    return Val_unit;
}
#endif /* api */

/* ---------------------------------------- Nombre al�atoire
   entr�e :
   _a = NULL ou pointeur vers un entier extensible
   n = longueur >= 0
   mode = long C

   sortie :
   a <- entier al�atoire entre -2^n-1 et 2^n-1
   les bits 0 et 1 de mode restreignent la plage al�atoire de la mani�re suivante :
   b0=0 => le signe est forc� >= 0
   b1=1 => le bit de rang n-1 est forc� = 1 si n > 0

   si _a != NULL, *_a <- a
   retourne a

   erreur :
   NEGATIVE_SIZE si n < 0
*/

xint xx(private_random)(xint *_a, long n, long mode) {
    long l,s;
    xint a;
    xx_push_roots_1(_a);
#ifdef caml_api
#define _a __lr._a
#endif

#if defined(caml_api) || defined(ocaml_api)
    n = Long_val(n);
#endif
    if (n < 0)  xx(failwith)(NEGATIVE_SIZE);
    if (n == 0) {
        a = xx(enlarge)(_a,0);
        a->hd = 0;
        xx_update_and_return(_a,a);
    }

    /* tire la valeur absolue au hasard */
    l = (n + HW-1)/HW; n %= HW;
    a = xx(enlarge)(_a,l);
    xn(random)(a->val,l);

    /* signe */
    if (mode&1) s = (random()&1) ? SIGN_m : 0;
    else        s = 0;

    /* corrige le chiffre de poids fort */
    if (n) a->val[l-1] &= ((chiffre)1 << n) - 1;
    if (mode&2) a->val[l-1] |= (n) ? (chiffre)1 << (n-1) : BASE_2;

    /* longueur et signe du r�sultat */
    xx(make_head)(a,l,s);
    xx_update_and_return(_a,a);

#undef _a
}

#if defined(caml_api) || defined(ocaml_api)
/*
  nrandom(n)  -> [0, 2^n[
  zrandom(n)  -> ]-2^n, 2^n[
  nrandom1(n) -> [2^(n-1), 2^n[
  zrandom1(n) -> ]-2^n, -2^(n-1)] U [2^(n-1), 2^n[
*/

xint xx(nrandom) (xint *_a, long n) {return xx(private_random)(_a,n,0);}
xint xx(zrandom) (xint *_a, long n) {return xx(private_random)(_a,n,1);}
xint xx(nrandom1)(xint *_a, long n) {return xx(private_random)(_a,n,2);}
xint xx(zrandom1)(xint *_a, long n) {return xx(private_random)(_a,n,3);}

xint xx(f_nrandom)         (long n) {return xx(private_random)(xx_null,n,0);}
xint xx(f_zrandom)         (long n) {return xx(private_random)(xx_null,n,1);}
xint xx(f_nrandom1)        (long n) {return xx(private_random)(xx_null,n,2);}
xint xx(f_zrandom1)        (long n) {return xx(private_random)(xx_null,n,3);}

#endif /* defined(caml_api) || defined(ocaml_api) */
