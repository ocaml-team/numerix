// file kernel/x/c/add.c: addition/subtraction of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Addition/soustraction                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                              /* +------------+
                                 |  Addition  |
                                 +------------+ */

/*  
   entr�e :
   a,b = entiers extensibles
   _c = NULL ou pointeur sur un entier extensible
   s = 0 ou 1

   sortie :
   c <- a + (-1)^s*b
   si _c != NULL, *_c <- c
   retourne c
   */
xint xx(private_add)(xint *_c, xint a, xint b, long s) {
    long la = xx_lg(a),  lb = xx_lg(b);
    long sa = xx_sgn(a), sb = xx_sgn(b);
    long lc, sc;
    xint c;
    xx_push_roots_3(a,b,_c);
#ifdef caml_api
#define  a __lr.a
#define  b __lr.b
#define _c __lr._c
#endif

    /* change le signe de b pour une soustraction */
    if (s) sb ^= SIGN_m;

    if (sa == sb) { /* ---------- entiers de m�me signe */

        /* force la >= lb */
        if (la < lb) {
            long l; xint x;
            x = a;  a  = b;  b  = x;
            l = la; la = lb; lb = l;
        }

        /* alloue la+1 chiffres pour c */
        lc = la+1; sc = sa;
        c = xx(enlarge)(_c,lc);

        /* effectue l'addition */
        c->val[la] = (a == c) ? xn(inc)(a->val,la,b->val,lb) :
                                xn(add)(a->val,la,b->val,lb,c->val);
      
    }
    else { /* --------------- entiers de signes oppos�s */

        /* alloue max(la,lb) chiffres pour c */
        lc = (la >= lb) ? la : lb;
        c = xx(enlarge)(_c,lc);

        /* effectue la soustraction */
        if (xn(cmp)(a->val,la,b->val,lb) >= 0) {
            (a == c) ? xn(dec)(a->val,la,b->val,lb) :
                       xn(sub)(a->val,la,b->val,lb,c->val);
            sc = sa;
        }
        else {
            (b == c) ? xn(dec)(b->val,lb,a->val,la) :
                       xn(sub)(b->val,lb,a->val,la,c->val);
            sc = sb;
        }

    }

    /* fixe la longueur et le signe de c */
    xx(make_head)(c,lc,sc);
    xx_update_and_return(_c,c);

#undef  a
#undef  b
#undef _c
}


                           /* +------------------+
                              |  Addition mixte  |
                              +------------------+ */
/*
  entr�e :
  a = entier extensible
  b = long ou Caml/Ocaml int
  _c = NULL ou pointeur sur un entier extensible
  s = 0 ou 1

  sortie :
  c <- a + (-1)^s*b
  si _c != NULL, *_c <- c
  retourne c
*/
xint xx(private_add_1)(xint *_c, xint a, long b, long s) {
    long la = xx_lg(a), sa = xx_sgn(a);
    long sb = b & SIGN_m;
    long lc;
    xint c;
    xx_push_roots_2(a,_c);
#ifdef caml_api
#define  a __lr.a
#define _c __lr._c
#endif

    /* isole la valeur absolue de b et corrige le signe pour une soustraction */
#ifdef c_api
    if (sb) b = -b;
#else
    b = (sb) ? -Long_val(b) : Long_val(b);
#endif
    if (s) sb ^= SIGN_m;

    if (la > chiffres_per_long) {   /* cas |a| grand */

        /* convertit b en tableau si besoin */
#if chiffres_per_long == 1
#define bb (chiffre *)&b
#elif chiffres_per_long == 2
        chiffre bb[] = {b & (2*BASE_2-1), b>>HW};
#endif

        if (sa == sb) {     /* entiers de m�me signe */
            lc = la + 1;
            c = xx(enlarge)(_c,lc);
            if (c != a) xn(move)(a->val,la,c->val);
            c->val[la] = xn(inc)(c->val,la,bb,chiffres_per_long);
        }

        else {          /* entiers de signes oppos�s */
            lc = la;
            c = xx(enlarge)(_c,lc);
            if (c != a) xn(move)(a->val,la,c->val);
            xn(dec)(c->val,la,bb,chiffres_per_long);
        }
    }
#undef bb

    else {                        /* cas |a| petit */
        unsigned long aa, cc;

        /* extrait la valeur de a */
#if chiffres_per_long == 1
	aa = (la) ? a->val[0] : 0;
#elif chiffres_per_long == 2
	aa = (la > 1) ? a->val[1] : 0;
	if (la) aa = (aa << HW) + (unsigned long)a->val[0];
#endif

        if (sa == sb) {     /* entiers de m�me signe */
            lc = chiffres_per_long+1;
            c = xx(enlarge)(_c,lc);
            cc = aa + b;
            c->val[0] = cc;
#if chiffres_per_long == 2
            c->val[1] = cc >> HW;
#endif
            c->val[chiffres_per_long] = (aa > cc);
        }
        else {          /* entiers de signes oppos�s */
            lc = chiffres_per_long;
            c = xx(enlarge)(_c,lc);
            cc = aa - b;
            if (aa < cc) {sa ^= SIGN_m; cc = -cc;}
            c->val[0] = cc;
#if chiffres_per_long == 2
            c->val[1] = cc >> HW;
#endif
        }
    }

    /* longueur et signe du r�sultat */
    xx(make_head)(c,lc,sa);
    xx_update_and_return(_c,c);

#undef  a
#undef _c
}

#if defined(caml_api) || defined(ocaml_api)

xint xx(add)  (xint *_c, xint a, xint b) {return xx(private_add)  (_c,a,b,0);}
xint xx(sub)  (xint *_c, xint a, xint b) {return xx(private_add)  (_c,a,b,1);}
xint xx(add_1)(xint *_c, xint a, long b) {return xx(private_add_1)(_c,a,b,0);}
xint xx(sub_1)(xint *_c, xint a, long b) {return xx(private_add_1)(_c,a,b,1);}

xint xx(f_add)          (xint a, xint b) {return xx(private_add)  (xx_null,a,b,0);}
xint xx(f_sub)          (xint a, xint b) {return xx(private_add)  (xx_null,a,b,1);}
xint xx(f_add_1)        (xint a, long b) {return xx(private_add_1)(xx_null,a,b,0);}
xint xx(f_sub_1)        (xint a, long b) {return xx(private_add_1)(xx_null,a,b,1);}

#endif /* defined(caml_api) || defined(ocaml_api) */


