// file kernel/x/c/gcd.c: Greatest common divisor
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                                  PGCD                                 |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                /* +--------------------------------------+
                   |  D�veloppement en fraction continue  |
                   +--------------------------------------+ */


/*
  entr�e :
  a,b = entiers extensibles
  _d,_u,_v,_p,_q = NULL ou pointeurs vers des entiers extensibles
  mode = long

  sortie :
  calcule d,u,v,p,q tels que :

             u*a - v*b = d = pgcd positif(a,b)
             p*b = q*a = ppcm(a,b) avec le signe de a*b
             p*u - q*v = 1

  pour x = d,u,v,p,q : si _x != NULL, *_x <- x

  valeur de retour : c_api       ml_api
  si mode & 3 = 0  :  NULL         unit
  si mode & 3 = 1  :     d           d
  si mode & 3 = 2  :  NULL      (d,u,v)
  si mode & 3 = 3  :  NULL  (d,u,v,p,q)

  erreur :
  MULTIPLE_RESULT si (_d,_u,_v,_p,_q) contient deux pointeurs non nuls �gaux
*/
#if defined(c_api)
xint
#elif defined(caml_api) || defined(ocaml_api)
value
#endif
xx(private_cfrac)(xint *_d, xint *_u, xint *_v, xint *_p, xint *_q,
                  xint a, xint b, long mode) {
    long la = xx_lg(a),  lb = xx_lg(b);
    long sa = xx_sgn(a), sb = xx_sgn(b);
    int ok, want_d, want_u, want_v, want_p, want_q, want_uvpq;
    chiffre *_x[6], un[1];
    long i,su,_l[6];
    xx_push_roots_75(_d,_u,_v,_p,_q,a,b,d,u,v,p,q);
#ifdef caml_api
#define _d __lr._d
#define _u __lr._u
#define _v __lr._v
#define _p __lr._p
#define _q __lr._q
#define  a __lr.a
#define  b __lr.b
#define  d __lr.d
#define  u __lr.u
#define  v __lr.v
#define  p __lr.p
#define  q __lr.q
#endif

    /* v�rifie que les pointeurs non nuls sont distincts */
    ok = 1;
    if (_d != xx_null) ok &= ((_d != _u) && (_d != _v) && (_d != _p) && (_d != _q));
    if (_u != xx_null) ok &= ((_u != _v) && (_u != _p) && (_u != _q));
    if (_v != xx_null) ok &= ((_v != _p) && (_v != _q));
    if (_p != xx_null) ok &= (_p != _q);
    if (!ok) xx(failwith)(MULTIPLE_RESULT);

    /* r�sultats � calculer */
#if defined(c_api)
    want_d = ((_d != xx_null) || (mode & 3) == 1);
    want_u = (_u != xx_null);
    want_v = (_v != xx_null);
    want_p = (_p != xx_null);
    want_q = (_q != xx_null);
#elif defined(caml_api) || defined(ocaml_api)
    switch(mode & 3) {
        case 0: want_d = 1;
                want_u = want_v = (_u != xx_null);
                want_p = want_q = (_p != xx_null);
                break;
        case 1: want_d = 1; want_u = want_v = want_p = want_q = 0; break;
        case 2: want_d = want_u = want_v = 1; want_p = want_q = 0; break;
        default:want_d = want_u = want_v = want_p = want_q = 1; break;
    }
#endif
    want_uvpq = want_u|want_v|want_p|want_q;

    /* constitue le 6-uplet de calcul */
    _x[0] = xn(alloc)(3*(la+lb)); /* a */
    _x[1] = _x[0] + la;           /* b */
    _x[2] = _x[1] + lb;           /* p */
    _x[3] = _x[2] + la;           /* s */
    _x[4] = _x[3] + lb;           /* q */
    _x[5] = _x[4] + la;           /* r */
    xn(move)(a->val,la,_x[0]); _l[0] = la;
    xn(move)(b->val,lb,_x[1]); _l[1] = lb;

    /* d�compose a/b en fraction continue */
    if ((la) && (lb)) xn(lehmer)(_x,_l, want_uvpq);
    else {un[0] = 1; _x[2] = _x[3] = un; _l[2] = _l[3] = 1; _l[4] = _l[5] = 0;}

    /* fixe les signes et recopie les r�sultats demand�s

               [x2  x4]   [d]   [|a|]
   si x1 = 0 : [x5  x3] * [0] = [|b|] -> |u| = x3, |v| = x4, |p| = x2, |q| = x5
   signes : sa  sb   u   v   p   q    -> su = sp = sa, sv = sq = sb
             +   +   +   +   +   +
             +   -   +   -   +   -
             -   +   -   +   -   + 
             -   -   -   -   -   - 

               [x2  x4]   [0]   [|a|]
   si x0 = 0 : [x5  x3] * [d] = [|b|] -> |u| = x5, |v| = x2, |p| = x4, |q| = x3
   signes : sa  sb   u   v   p   q    -> -su = sp = sa, -sv = sq = sb
             +   +   -   -   +   +
             +   -   -   +   +   -
             -   +   +   -   -   + 
             -   -   +   +   -   - 


    */
    if (_l[1] == 0) {i=0; su=0;} else {i=1; su=SIGN_m;}

#define store(var,index,sgn)                         \
    if (want_##var) do {                             \
        var = xx(enlarge)(_##var,_l[index]);         \
        xn(move)(_x[index],_l[index],var->val);      \
        var->hd = (_l[index]) ? _l[index]|(sgn) : 0; \
        xx(update)(_##var,var);                      \
    } while(0)

    store(d, i,     0);
    store(u, 3+2*i, su^sa);
    store(v, 4-2*i, su^sb);
    store(p, 2+2*i, sa);
    store(q, 5-2*i, sb);
    xn(free)(_x[0]);

#undef store

    /* valeur de retour */
#if defined(c_api)

    return d;

#elif defined(caml_api) || defined(ocaml_api)

    { xint *res;
      switch(mode & 3) {
        case 0: res = (xint *)Val_unit; break;
        case 1: res = (xint *)d;        break;
        case 2: res = (xint *)alloc_tuple(3);
                res[0] = d;
                res[1] = u;
                res[2] = v;
                break;
        default:res = (xint *)alloc_tuple(5);
                res[0] = d;
                res[1] = u;
                res[2] = v;
                res[3] = p;
                res[4] = q;
                break;
      }
      xx_pop_roots();
      return (value)res;
    }

#endif

#undef _d 
#undef _u 
#undef _v 
#undef _p 
#undef _q 
#undef  a 
#undef  b 
#undef  d 
#undef  u 
#undef  v 
#undef  p 
#undef  q 
}

#if defined(caml_api) || defined(ocaml_api)
/* fonctions sp�cialis�es */

value xx(gcd)(xint *_d, xint a, xint b) {
    return xx(private_cfrac)(_d,xx_null,xx_null,xx_null,xx_null,a,b,0);
}

value xx(gcd_ex)(xint *_d, xint *_u, xint *_v, xint a, xint b) {
    return xx(private_cfrac)(_d,_u,_v,xx_null,xx_null,a,b,0);
}

value xx(cfrac)(xint *_d, xint *_u, xint *_v, xint *_p, xint *_q, xint a, xint b) {
    return xx(private_cfrac)(_d,_u,_v,_p,_q,a,b,0);
}

value xx(cfrac_bytecode)(xint **v, int argn) {
    return xx(private_cfrac)(v[0],v[1],v[2],v[3],v[4],(xint)v[5],(xint)v[6],0);
}

value xx(f_gcd)(xint a, xint b) {
    return xx(private_cfrac)(xx_null,xx_null,xx_null,xx_null,xx_null,a,b,1);
}

value xx(f_gcd_ex)(xint a, xint b) {
    return xx(private_cfrac)(xx_null,xx_null,xx_null,xx_null,xx_null,a,b,2);
}

value xx(f_cfrac)(xint a, xint b) {
    return xx(private_cfrac)(xx_null,xx_null,xx_null,xx_null,xx_null,a,b,3);
}

#endif
