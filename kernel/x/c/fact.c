// file kernel/x/c/fact.c: factorial
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Factorielle                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  _a = NULL ou pointeur sur un entier extensible
  n = long ou Caml/Ocaml int >= 0

  sortie :
  a <- n!
  si _a != NULL, *_a <- a
  retourne a

  erreur :
  NEGATIVE_BASE si n < 0
*/

xint xx(fact)(xint *_a, long n) {
    chiffre *x,*y,*u,*v,*w; 
    unsigned long lg[32];
    unsigned long e,i,j,k,l,p,lu,lv,sp;
    static long ftab[] = {1,1,2,6,24,120,720,5040,40320};
    xint a;
    xx_push_roots_1(_a);
#ifdef caml_api
#define _a __lr._a
#endif

    /* traite les petites factorielles � part */
#if defined(caml_api) || defined(ocaml_api)
    n = Long_val(n);
#endif
    if (n <= 8) {
        if (n < 0) xx(failwith)(NEGATIVE_BASE);
        a = xx(enlarge)(_a,1);
        a->val[0] = ftab[n];
        a->hd = 1;
        xx_update_and_return(_a,a);
    }

    /* k <- ceil(log(n)), e <- valuation 2_adique de n! */
    for (k=0, i=n, e=0; i>1; k++, i/=2, e+=i);
    if (n > ((long)1<<k)) k++;
  
    /* taille du r�sultat :

    log(n!) <= (n+1/2)*log(n) - (n-1)*log(e)
            <= (n+1/2)*ceil(log(n))

    On calcule le nombre de chiffres correspondant en divisant au
    pr�alable n par 2*HW de fa�on � �viter un d�passement de
    capacit� en unsigned long.
    */
    i = 2*k*(n/(2*HW)); j = ((2*(n%(2*HW)) + 1)*k + 2*HW-1)/(2*HW) + 1;
    if (i >= LMAX-j) xx(failwith)(NUMBER_TOO_BIG);
    l = i + j;

    /* alloue l chiffres pour le r�sultat et 2l+2 pour les calculs interm�diaires */
    a = xx(enlarge)(_a,l);
    x = xn(alloc)(2*l+2);

    /* Algorithme :

    Soit P_k le produit des entiers impairs dans ]n/2^(k+1), n/2^k]
    et Q_k le produit des entiers impairs dans [1, n/2^k].
    On a n! = (2^e)*Q_0*Q_1*Q_2*...

    On calcule P_k en multipliant entre eux les entiers impairs dans
    l'intervalle ]n/2^(k+1), n/2^k] deux par deux, puis en multipliant
    ces produits deux par deux, etc � l'aide d'une pile.

    A un instant donn� :
    a contient Q_{k+1}*Q_{k+2}*...
    le sommet de pile contient Q_{k+1}
    le reste de la pile contient tous les facteurs attendant leur cofacteur.

    On introduit un nouvel entier impair dans la pile et on effectue toutes
    les multiplications pour lesquelles on dispose des deux facteurs dans
    la pile. Lorsque le parcours de l'intervalle ]n/2^(k+1), n/2^k] est
    termin�, on dispose de Q_k en sommet de pile. On remplace a par a*Q_k
    et on continue.

    Initialement a et la pile sont vides, ce qui �quivaut par convention �
    Q_{k+1} = 1 = Q_{k+1}*Q_{k+2}*... et �vite d'avoir � effectuer des
    multiplications par 1.

    Impl�mentation de la pile : les nombres � empiler sont copi�s � la
    queue-leu-leu dans le tableau x et leurs longueurs dans le tableau lg.
    y d�signe la premi�re position libre dans x et sp la premi�re position
    libre dans lg.

    */


    /*
      multiplication optimis�e : on classe les op�randes par longueur et
      on utilise mul_n2 ou fftmul selon la longueur du plus long op�rande.
      op�randes = (u,lu) et (v,lv), r�sultat = (y,l)
    */
#undef  MULTIPLY
#define MULTIPLY                                               \
  if (lv < lu) {l = lu; lu = lv; lv = l; w = u; u = v; v = w;} \
  if (lu > karamul_lim) xn(fftmul)(v,lv,u,lu,y);               \
  else                  xn(mul_n2)(v,lv,u,lu,y);               \
  l = lu+lv; while (!y[l-1]) l--

    a->hd = 0;                   /* produit initial = 1            */
    sp = 0; y = x;               /* pile initiale vide             */
    i = 3;                       /* 1er entier impair � consid�rer */
    k -= 2; if ((n>>k) < 3) k--; /* 1er exposant � consid�rer      */

    for (; ; k--) {

        /* calcule le produit des impairs de n/2^(k+1) � n/2^k */
        for (p=1; i <= (n>>k); i += 2, p++) {

            /* empile i */
#if chiffres_per_long == 2
            if (i>>HW) {y[0] = i; y[1] = i>>HW; y+=2; lg[sp++] = 2;} else
#endif
                       {y[0] = i;               y++;  lg[sp++] = 1;}

            /* effectue les produits tq. les deux op�randes sont dans la pile */
            for (j=p; (j&1) == 0; j >>= 1) {
                lv = lg[--sp]; v = y - lv;
                lu = lg[--sp]; u = v - lu;
                MULTIPLY;
                xn(move)(y,l,y-lu-lv);
                y -= lu+lv-l; lg[sp++] = l;
            }
        }

        /* termine les multiplications en instance (y compris Q_k = Q_{k+1}*P_k) */
        while (sp > 1) {
            lv = lg[--sp]; v = y - lv;
            lu = lg[--sp]; u = v - lu;
            MULTIPLY;
            xn(move)(y,l,y-lu-lv);
            y -= lu+lv-l; lg[sp++] = l;
        }

        /* multiplie a par Q_k */
        lv = lg[0]; v = x;
        lu = a->hd; u = a->val;
        if (!lu) {xn(move)(v,lv,u); a->hd = lv;} /* lu == 0 <=> a = 1 */
        else{
            MULTIPLY;
            if (k) {xn(move)(y,l,a->val); a->hd = l;}
            else {
                /* c'est le dernier produit: d�cale de e bits au lieu de recopier */
                i = e/HW; j = e%HW;
                a->val[l+i] = xn(shift_up)(y,l,a->val+i,j);
                xn(clear)(a->val,i);
                a->hd = (a->val[l+i]) ? l+i+1 : l+i;
                xn(free)(x);
                xx_update_and_return(_a,a);
            }
        }
    }

#undef _a
}

#if defined(caml_api) || defined(ocaml_api)

xint xx(f_fact)(long n) {return xx(fact)(xx_null,n);}

#endif
