// file kernel/x/c/mul.c: multiplication/square of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                             Multiplication                            |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                          /* +------------------+
                             |  Multiplication  |
                             +------------------+ */

/*
   entr�e :
   a,b = entiers extensibles
   _c = NULL ou pointeur sur un entier extensible

   sortie :
   c <- a * b
   si _c != NULL, *_c <- c
   retourne c
*/
xint xx(mul)(xint *_c, xint a, xint b) {
    long la = xx_lg(a),  lb = xx_lg(b);
    long sa = xx_sgn(a), sb = xx_sgn(b);
    long lc;
    chiffre *aa, *bb;
    int free_a, free_b;
    xint c;
    xx_push_roots_3(a,b,_c);
#ifdef caml_api
#define  a __lr.a
#define  b __lr.b
#define _c __lr._c
#endif

    /* force la >= lb */
    if (la < lb) {
        long l; xint x;
        x = a;  a  = b;  b  = x;
        l = la; la = lb; lb = l;
    }

    /* �vacue le cas b = 0 */
    if (lb == 0) {
        c = xx(enlarge)(_c,0);
        c->hd = 0;
        xx_update_and_return(_c,c);
    }

    /* alloue la+lb chiffres pour c */
    lc = la + lb;
    c = xx(enlarge)(_c,lc);

    /* copie les op�randes s'ils vont �tre �cras�s */
    if (a == c) {
        aa = xn(alloc)(la);
        xn(move)(a->val,la,aa);
        if (b == c) bb = aa; else bb = b->val;
        free_a = 1; free_b = 0;
    } else if (b == c) {
        bb = xn(alloc)(lb);
        xn(move)(b->val,lb,bb);
        aa = a->val; free_a = 0; free_b = 1;
    } else {
        aa = a->val; bb = b->val; free_a = free_b = 0;
    }
      
    /*
      Effectue la multiplication : on bascule vers mul_n2 de suite pour
      les petites multiplications de fa�on � gagner du temps dans les cas
      courants. Les autres multiplications sont prises en charge par fftmul
      qui renverra vers un algorithme plus efficace pour les multiplications
      de taille moyenne.
    */
    (lb > karamul_lim) ? xn(fftmul)(aa,la,bb,lb,c->val) :
                         xn(mul_n2)(aa,la,bb,lb,c->val);

    /* lib�re les copies temporaires */
    if (free_a) xn(free)(aa); else if (free_b) xn(free)(bb);

    /* longueur et signe du r�sultat */
    xx(make_head)(c,lc,sa^sb);
    xx_update_and_return(_c,c);

#undef  a
#undef  b
#undef _c
}

                    /* +------------------------------+
                       |  Multiplication par un long  |
                       +------------------------------+ */

/*
   entr�e :
   a   = entier extensible
   b   = long ou Caml/Ocaml int
   _c = NULL ou pointeur sur un entier extensible

   sortie :
   c <- a * b
  si _c != NULL, *_c <- c
  retourne c
*/
xint xx(mul_1)(xint *_c, xint a, long b) {
    long la = xx_lg(a), sa = xx_sgn(a), sb = b & SIGN_m, lc;
    xint c;
    xx_push_roots_2(a,_c);
#ifdef caml_api
#define  a __lr.a
#define _c __lr._c
#endif

    /* b <- |b| */
#if defined(c_api)
    if (sb) b = -b;
#elif defined(caml_api) || defined(ocaml_api)
    b = (sb) ? -Long_val(b) : Long_val(b);
#endif

    /* �vacue les cas a = 0 ou b = 0 */
    if ((la == 0) || (b == 0)) {
        c = xx(enlarge)(_c,0);
        c->hd = 0;
        xx_update_and_return(_c,c);
    }

    /* cas a*b != 0 */
    lc = la + chiffres_per_long;
    c = xx(enlarge)(_c,lc);
    b = xn(mul_1)(a->val,la,b,c->val);
    c->val[la] = b;
#if chiffres_per_long == 2
    c->val[la+1] = b >> HW;
#endif

    /* longueur et signe du r�sultat */
    xx(make_head)(c,lc,sa^sb);
    xx_update_and_return(_c,c);

#undef  a
#undef _c
}

                               /* +---------+
                                  |  Carr�  |
                                  +---------+ */

/*
   entr�e :
   a   = entier extensible
   _b = NULL ou pointeur sur un entier extensible

   sortie :
   b <- a^2
   si _b != NULL, *_b <- b
   retourne b
*/
xint xx(sqr)(xint *_b, xint a) {
    long la = xx_lg(a), lb;
    chiffre *aa;
    int free_a;
    xint b;
    xx_push_roots_2(a,_b);
#ifdef caml_api
#define  a __lr.a
#define _b __lr._b
#endif

    /* cas a = 0 */
    if (la == 0) {
        b = xx(enlarge)(_b,0);
        b->hd = 0;
        xx_update_and_return(_b,b);
    }

    /* alloue 2*la chiffres pour b */
    lb = 2*la;
    b = xx(enlarge)(_b,lb);

    /* copie a s'il va �tre �cras� */
    if (a == b) {
        aa = xn(alloc)(la);
        xn(move)(a->val,la,aa);
        free_a = 1;
    } else {
        aa = a->val; free_a = 0;
    }

    /*
      Calcule le carr� : on bascule vers sqr_n2 de suite pour
      les petits carr�s de fa�on � gagner du temps dans les cas
      courants. Les autres carr�s sont prises en charge par fftsqr
      qui renverra vers un algorithme plus efficace pour les carr�s
      de taille moyenne.
    */
    (la > karasqr_lim) ? xn(fftsqr)(aa,la,b->val) :
                         xn(sqr_n2)(aa,la,b->val);

    /* lib�re la copie temporaire */
    if (free_a) xn(free)(aa);

    /* longueur du r�sultat */
    xx(make_head)(b,lb,0);
    xx_update_and_return(_b,b);

#undef  a
#undef _b
}

#if defined(caml_api) || defined(ocaml_api)

xint xx(f_mul)  (xint a, xint b) {return xx(mul)  (xx_null,a,b);}
xint xx(f_mul_1)(xint a, long b) {return xx(mul_1)(xx_null,a,b);}
xint xx(f_sqr)  (xint a)         {return xx(sqr)  (xx_null,a);  }

#endif /* defined(caml_api) || defined(ocaml_api) */

