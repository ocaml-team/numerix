// file kernel/x/c/chrono.c: timing facility
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                           Chrononmétrage                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#ifndef __MSVCRT__
#include <sys/times.h>
#endif
#include <time.h>

#ifndef CLK_TCK
#include <unistd.h>
#define CLK_TCK sysconf(_SC_CLK_TCK)
#endif

#if defined(c_api)
void
#elif defined(caml_api) || defined(ocaml_api)
#include <mlvalues.h>
value
#endif
chrono(char *msg) {
  static double tlast = 0;
  double t;
#ifndef __MSVCRT__
  struct tms buf;

  times(&buf);
  t = (double)(buf.tms_utime + buf.tms_stime)/CLK_TCK;
#else
  t = (double)clock()/CLK_TCK;
#endif
  printf("%8.2f %8.2f %s\n",t,t-tlast,msg);
  fflush(stdout);
  tlast = t;

#if defined(caml_api) || defined(ocaml_api)
  return(Val_unit);
#endif
}
