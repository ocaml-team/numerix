// file kernel/x/c/powmod.c: modular exponentiation
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                      Exponentiation modulaire                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a,b,c = entiers extensibles
  _d = NULL ou pointeur sur un entier extensible
  mode = long

  sortie :
  d <- gmod(a^b, c, mode)
  si _d != NULL, *_d <- d
  retourne d

  erreur :
  NEGATIVE_EXPONENT si b < 0
  ZERO_DIVISOR      si c = 0
*/

xint xx(private_powmod)(xint *_d, xint a, xint b, xint c, long mode) {
    long la = xx_lg(a), sa = xx_sgn(a);
    long lb = xx_lg(b), sb = xx_sgn(b);
    long lc = xx_lg(c), sc = xx_sgn(c);
    chiffre *dd;
    xint d;
    long sd;
    int  chd;

    xx_push_roots_4(a,b,c,_d);
#ifdef caml_api
#define  a __lr.a
#define  b __lr.b
#define  c __lr.c
#define _d __lr._d
#endif

    /* contr�le */
    if (sb)  xx(failwith)(NEGATIVE_EXPONENT);
    if (!lc) xx(failwith)(ZERO_DIVISOR);

    /* cas b = 0 */
    if (!lb) {
        if ((lc == 1) && (c->val[0] == 1)) {  /*  |c| = 1 => d = 0 */
            d = xx(enlarge)(_d,0);
            d->hd = 0;
        }
        /* sinon le r�sultat est 1, sauf dans les cas suivants :
           mode            d 
           0     c < 0   1+c
           1     c = 2   1-c
           2     c > 0   1-c
           3     c =-2   1+c
        */
        else {
            switch(mode&3) {
            case 0: chd=(sc != 0); break;
            case 1: chd=((lc==1) && (c->val[0]==2) && (sc == 0)); break;
            case 2: chd=(sc == 0); break;
            default:chd=((lc==1) && (c->val[0]==2) && (sc != 0)); break;
            }
            if (chd) {
                d = xx(enlarge)(_d,lc);
                xn(move)(c->val,lc,d->val);
                xn(dec1)(d->val,lc);
                xx(make_head)(d,lc,SIGN_m);
            }
            else {
                d = xx(enlarge)(_d,1);
                d->val[0] = 1;
                d->hd = 1;
            }
        }
    }

    /* cas b > 0 */
    else {

        d = xx(enlarge)(_d,lc);


        /* alloue une zone temporaire pour d s'il est �gal � un param�tre */
        if ((d == a) || (d == b) || (d == c)) dd = xn(alloc)(lc); else dd = d->val;

        /* dd <- |a|^b mod |c|
           on applique l'algorithme de Montgomery si c est impair assez grand,
           et l'algorithme d'exponentiation modulaire g�n�ral sinon.
        */
        ((lc >= 2) && (c->val[0]&1)) ?
            xn(powmod_mg)(a->val,la,b->val,lb,c->val,lc,dd) :
            xn(powmod)(a->val,la,b->val,lb,c->val,lc,dd);

        /* prise en compte des signes de a^b et de c */
        if ((b->val[0]&1) == 0) sa = 0;
        switch(mode & 3) {
            case 0: chd = ((sa != sc) && (xn(cmp)(dd,lc,dd,0)));        break;
            case 1: chd = ((sa == sc) + xn(cmp2)(dd,lc,c->val,lc) > 0); break;
            case 2: chd = ((sa == sc) && (xn(cmp)(dd,lc,dd,0)));        break;
            default:chd = ((sa != sc) + xn(cmp2)(dd,lc,c->val,lc) > 0); break;
        }
        if (chd) {
            xn(sub)(c->val,lc,dd,lc,d->val);
            if (dd != d->val) xn(free)(dd);
        }
        else if (dd != d->val) {
            xn(move)(dd,lc,d->val);
            xn(free)(dd);
        }
        switch(mode & 3) {
            case 0: sd = sc;                         break;
            case 2: sd = sc ^ SIGN_m;                break;
            default:sd = ((chd) ? sa ^ SIGN_m : sa); break;
        }
        xx(make_head)(d,lc,sd);
    }

    xx_update_and_return(_d,d);

#undef  a
#undef  b
#undef  c
#undef _d
}

#if defined(ocaml_api) || defined(caml_api)

xint xx(gpowmod)(value mode, xint *_d, xint a, xint b, xint c) {
    return xx(private_powmod)(_d,a,b,c,Round_val(mode));
}

xint xx(powmod)(xint *_d, xint a, xint b, xint c) {
    return xx(private_powmod)(_d,a,b,c,0);
}

xint xx(f_gpowmod)(value mode, xint a, xint b, xint c) {
    return xx(private_powmod)(xx_null,a,b,c,Round_val(mode));
}

xint xx(f_powmod)(xint a, xint b, xint c) {
    return xx(private_powmod)(xx_null,a,b,c,0);
}
#endif
