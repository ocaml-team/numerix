// file kernel/x/c/prime.c: primality test
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                           Test de primalit�                           |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a = entier extensible (isprime) ou entier long (isprime_1)

  sortie = 0 : a est compos�,
  sortie = 1 : a passe le test mais |a| est trop grand pour qu'on soit s�r
               que a est premier.
  sortie = 2 : a est premier,
*/
long xx(isprime)(xint a) {return(Val_tri(xn(isprime)(a->val,xx_lg(a))));}

long xx(isprime_1)(long a) {
  long la;
  chiffre aa[2];

#if defined(caml_api) || defined(ocaml_api)
    a = Long_val(a);
#endif
    if (a < 0) a = -a;
#if chiffres_per_long == 2
    if (a >= BASE_2 + BASE_2) {la = 2; aa[1] = a >> HW;}
    else 
#endif
        la = (a != 0);
    aa[0] = a;
    return(Val_tri(xn(isprime)(aa,la)));

}
