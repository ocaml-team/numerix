// file kernel/x/c/convert.c: extensible integer <-> long conversion
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                             Conversion                                |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ------------------------------------------------------------ int -> xint
   entr�e :
   a = long ou Caml/Ocaml int
   _b = NULL ou pointeur sur un entier extensible

   sortie :
   b <- a
   si _b != NULL, *_b <- b
   retourne b
*/
xint xx(copy_int)(xint *_b, long a) {
    long sa = a & SIGN_m;
    xint b;
    xx_push_roots_1(_b);
#ifdef caml_api
#define _b __lr._b
#endif

    /* a <- |a| */
#ifdef c_api
    if (sa) a = -a;
#else
    a = (sa) ? -Long_val(a) : Long_val(a);
#endif

    /* d�compose en 0, 1 ou 2 chiffres */
#if chiffres_per_long == 2
    if (a >= 2*BASE_2) {
        b = xx(enlarge)(_b,2);
        b->val[0] = a;
        b->val[1] = a >> HW;
        b->hd = 2|sa;
    } else
#endif
        if (a) {
            b = xx(enlarge)(_b,1);
            b->val[0] = a;
            b->hd = 1|sa;
        }
        else {
            b = xx(enlarge)(_b,0);
            b->hd = 0;
        }

    xx_update_and_return(_b,b);

#undef _b
}

#if defined(caml_api) || defined(ocaml_api)

xint xx(of_int)(value a) {return xx(copy_int)(xx_null,a);}

#endif

/* ------------------------------------------------------------ xint -> int
   entr�e :
   a = entier extensible

   sortie :
   retourne le long ou Caml/Ocaml int de m�me valeur que a

   erreur :
   INTEGER_OVERFLOW si |a| >= 2^30
*/
long xx(int_of)(xint a) {
    long la = xx_lg(a), sa = xx_sgn(a);
    unsigned long b;

    if (la == 0) return(Val_long(0));

    if (la <= chiffres_per_long) {
        b = a->val[0];
#if chiffres_per_long == 2
        if (la > 1) b += (unsigned long)a->val[1] << HW;
#endif
        if (!(b >> 30)) return((sa) ? Val_long(-b) : Val_long(b));
    }
    xx(failwith)(INTEGER_OVERFLOW);
}


/* -------------------------------------------------------- Structure binaire
   entr�e :
   a = entier extensible

   sortie :
   si a <> 0 -> 1 + ceil(log_2(|a|))
   si a =  0 -> 0
*/
long xx(nbits)(xint a) {
    long la = xx_lg(a),n;
    chiffre u;

    if (la == 0) return(Val_long(0));
    for (n = (la-1)*HW, u = a->val[la-1]; u; u >>= 1, n++);
    return(Val_long(n));
}

/*
  entr�e :
  a = entier extensible

  sortie :
  bits 0..30 de |a|
*/
long xx(lowbits)(xint a) {
    unsigned long la = xx_lg(a), u;

    if (la == 0) return(Val_long(0));
    u = a->val[0];
#if HW == 16
    if (la > 1) u += a->val[1] << HW;
#endif
    return(Val_long(u & 0x7fffffff));
}

/*
  entr�e :
  a = entier extensible

  sortie :
  bits l-31..l-1 de |a|, l = nbits(a)
*/
long xx(highbits)(xint a) {
    unsigned long la = xx_lg(a), u,v;

    if (la == 0) return(Val_long(0));

    u = a->val[la-1];
    if (u >> 30) {while (u>>31) u >>= 1;}
    else {
        v = (la > 1) ? a->val[la-2] : 0;
#if HW == 16
        v <<= HW;
        if (la > 2) v += a->val[la-3];
#endif
        do {u <<= 1; if ((signed long)v < 0) u++; v <<= 1;} while (!(u>>30));
    }
    return(Val_long(u));
}

/*
  entr�e :
  a = entier extensible
  n = indice

  sortie :
  bits 16*(n-1)..16*n-1 de |a|
*/
long xx(nth_word)(xint a, long n) {
    long la = xx_lg(a), p,u;

#if defined(caml_api) || defined(ocaml_api)
    n = Long_val(n);
#endif
    if (n < 0) return(Val_long(0));
    p = 16*(n%(HW/16));
    n = n/(HW/16);
    u = (n < la) ? a->val[n]>>p : 0;
    return(Val_long(u & 0xffff));
}

/*
  entr�e :
  a = entier extensible
  n = indice

  sortie :
  bit n de |a|
*/
long xx(nth_bit)(xint a, long n) {
    long la = xx_lg(a), p,u;

#if defined(caml_api) || defined(ocaml_api)
    n = Long_val(n);
#endif
    if (n < 0) return(Val_bool(0));
    p = n%HW;
    n = n/HW;
    u = (n < la) ? a->val[n]>>p : 0;
    return(Val_bool(u & 1));
}
