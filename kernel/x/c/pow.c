// file kernel/x/c/pow.c: exponentiation
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Exponentiation                           |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a  = entier extensible
  _b = NULL ou pointeur sur un entier extensible
  p  = exposant >= 0

  sortie :
  b <- a^p
  si _b != NULL, *_b <- b
  retourne b

  erreur :
  NEGATIVE_EXPONENT si p < 0
*/

xint xx(pow)(xint *_b, xint a, long p) {
    long la = xx_lg(a), lb,sb,n;
    int free_a;
    chiffre u, *aa;
    xint b;
    xx_push_roots_2(a,_b);
#ifdef caml_api
#define  a __lr.a
#define _b __lr._b
#endif

#if defined(caml_api) || defined(ocaml_api)
    p = Long_val(p);
#endif

    /* signe du r�sultat */
    sb = (p&1) ? xx_sgn(a) : 0;

    /* �vacue les cas p <= 1 ou |a| <= 1 */
    if (p < 0) xx(failwith)(NEGATIVE_EXPONENT);
    if ((p == 0) || ((la==1) && (a->val[0] == 1))) {
        b = xx(enlarge)(_b,1);
        b->val[0] = 1;
        b->hd     = 1|sb;
        xx_update_and_return(_b,b);
    }
    if ((p == 1) || (la == 0)) {
        b = xx(enlarge)(_b,la);
        if (a != b) {xn(move)(a->val,la,b->val); b->hd = a->hd;}
        xx_update_and_return(_b,b);
    }

    /* nbits(b) = p*nbits(a)
       Le test (la < floor(LMAX/p)) garantit que p*(la-1) <= LMAX et p <= LMAX/2
       donc le calcul de lb qui suit est conduit sans d�bordement.
    */
    if (la >= LMAX/p) xx(failwith)(NUMBER_TOO_BIG);
    for (n = 0, u = a->val[la-1]; u; u >>= 1, n++);
    lb = (la-1)*p + (p/HW)*n + ((p%HW)*n + HW-1)/HW;

    /* alloue lb+1 chiffres pour b et copie a s'il va �tre �cras� */
    b = xx(enlarge)(_b,lb+1);
    if (a == b) {
        aa = xn(alloc)(la);
        xn(move)(a->val,la,aa);
        free_a = 1;
    }
    else {aa = a->val; free_a = 0;}

    /* calcule |a|^p */
    lb = xn(pow)(aa,la,b->val,p);
    if (free_a) xn(free)(aa);

    /* longueur et signe du r�sultat */
    b->hd = lb|sb;
    xx_update_and_return(_b,b);

#undef  a
#undef _b
}

/*
  entr�e :
  a  = long ou Caml/Ocaml int
  _b = NULL ou pointeur sur un entier extensible
  p  = exposant >= 0

  sortie :
  b <- a^p
  si _b != NULL, *_b <- b
  retourne b

  erreur :
  NEGATIVE_EXPONENT si p < 0
*/

xint xx(pow_1)(xint *_b, long a, long p) {
    unsigned long la,lb,sb,n,q;
    chiffre aa[2];
    xint b;
    xx_push_roots_1(_b);
#ifdef caml_api
#define _b __lr._b
#endif

#if defined(caml_api) || defined(ocaml_api)
    a = Long_val(a);
    p = Long_val(p);
#endif
    if (p < 0) xx(failwith)(NEGATIVE_EXPONENT);

    /* cas a ou p nul */
    if (p == 0) {
        b = xx(enlarge)(_b,1);
        b->val[0] = 1;
        b->hd = 1;
        xx_update_and_return(_b,b);
    }
    if (a == 0) {
        b = xx(enlarge)(_b,0);
        b->hd = 0;
        xx_update_and_return(_b,b);
    }

    /* signe du r�sultat */
    if (a < 0) {sb = (p&1) ? SIGN_m : 0; a = -a;} else {sb = 0;}

    /* extrait la pertie impaire de a et compte le nombre de bits */
    for (n=0; (a&1) == 0; n++, a >>= 1);
    for (q=1; a >> q; q++);

    /* si q = 1 alors |a| = 2^n donc |b| = 2^(np) */
    if (q == 1) {
        lb = n*(p/HW) + (n*(p%HW))/HW + 1;
        b = xx(enlarge)(_b,lb);
        xn(clear)(b->val,lb-1);
        b->val[lb-1] = (chiffre)1 << ((n*(p%HW))%HW);
        b->hd = lb|sb;
        xx_update_and_return(_b,b);
    }

    /* cas g�n�ral : alloue (n+q)*p bits pour b */
    lb = (n+q)*(p/HW) + ((n+q)*(p%HW) + HW-1)/HW;
    b = xx(enlarge)(_b,lb+1);

    /* convertit a en tableau de chiffres */
#if chiffres_per_long == 2
    if (q > HW) {la = 2; aa[1] = a >> HW;}
    else 
#endif
        la = 1;
    aa[0] = a;

    /* calcule |a|^p dans b */
    q = n*(p/HW) + (n*(p%HW))/HW;
    n = (n*(p%HW))%HW;
    xn(clear)(b->val,q);
    lb = xn(pow)(aa,la,b->val+q,p);
    if (n) {
        b->val[q+lb] = xn(shift_up)(b->val+q,lb,b->val+q,n);
        if (b->val[q+lb]) lb++;
    }

    /* longueur et signe du r�sultat */
    b->hd = (lb+q)|sb;
    xx_update_and_return(_b,b);

#undef _b
}

#if defined(ocaml_api) || defined(caml_api)

xint xx(f_pow)  (xint a, long p) {return xx(pow)  (xx_null,a,p);}
xint xx(f_pow_1)(long a, long p) {return xx(pow_1)(xx_null,a,p);}

#endif

