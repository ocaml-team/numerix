// file kernel/x/c/cmp.c: comparison of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                             Comparaison                               |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a,b = entiers extensibles

  sortie :
  1 si a > b, 0 si a = b, -1 si a < b
*/
long xx(cmp)(xint a, xint b) {
  long la = xx_lg(a), lb = xx_lg(b), sa = xx_sgn(a), sb = xx_sgn(b);

  if (sa) {
    if (sb) return(Val_long(-xn(cmp)(a->val,la,b->val,lb)));
    else return(Val_long(-1));
  }
  else {
    if (sb) return(Val_long(1));
    else return(Val_long(xn(cmp)(a->val,la,b->val,lb)));
  }
}

/*
  entr�e :
  a = entier extensible
  b = entier Caml/Ocaml ou long

  sortie :
  1 si a > b, 0 si a = b, -1 si a < b
*/
long xx(cmp_1)(xint a, long b) {
  long la = xx_lg(a), sa = xx_sgn(a);
  unsigned long aa,cc;

  if (la > chiffres_per_long) return(Val_long((sa) ? -1 : 1));

#if chiffres_per_long == 1
  aa = (la) ? a->val[0] : 0;
#elif chiffres_per_long == 2
  aa = (la > 1) ? a->val[1] : 0;
  if (la) aa = (aa << HW) + (unsigned long)a->val[0];
#endif

#if defined(caml_api) || defined(ocaml_api)
  b = Long_val(b);
#endif
  if (sa) {
    if (b >= 0) return(Val_long(-1));
    cc = aa + b;
    return(Val_long((cc) ? ((cc > aa) ? 1 : -1) : 0));
  }
  else {
    if (b < 0) return(Val_long(1));
    cc = aa - b;
    return(Val_long((cc) ? ((cc > aa) ? -1 : 1) : 0));
  }

}

#if defined(caml_api) || defined(ocaml_api)

value xx(sgn)(xint a){return(Val_long((a->hd > 0) ? 1 : (a->hd < 0) ? -1 : 0));}

value xx(eq)     (xint a,xint b) {return(Val_bool((xx(cmp)  (a,b) == Val_long(0))));}
value xx(neq)    (xint a,xint b) {return(Val_bool((xx(cmp)  (a,b) != Val_long(0))));}
value xx(inf)    (xint a,xint b) {return(Val_bool((xx(cmp)  (a,b) <  Val_long(0))));}
value xx(infeq)  (xint a,xint b) {return(Val_bool((xx(cmp)  (a,b) <= Val_long(0))));}
value xx(sup)    (xint a,xint b) {return(Val_bool((xx(cmp)  (a,b) >  Val_long(0))));}
value xx(supeq)  (xint a,xint b) {return(Val_bool((xx(cmp)  (a,b) >= Val_long(0))));}

value xx(eq_1)   (xint a,long b) {return(Val_bool((xx(cmp_1)(a,b) == Val_long(0))));}
value xx(neq_1)  (xint a,long b) {return(Val_bool((xx(cmp_1)(a,b) != Val_long(0))));}
value xx(inf_1)  (xint a,long b) {return(Val_bool((xx(cmp_1)(a,b) <  Val_long(0))));}
value xx(infeq_1)(xint a,long b) {return(Val_bool((xx(cmp_1)(a,b) <= Val_long(0))));}
value xx(sup_1)  (xint a,long b) {return(Val_bool((xx(cmp_1)(a,b) >  Val_long(0))));}
value xx(supeq_1)(xint a,long b) {return(Val_bool((xx(cmp_1)(a,b) >= Val_long(0))));}

#endif /* defined(caml_api) || defined(ocaml_api) */
