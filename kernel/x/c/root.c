// file kernel/x/c/root.c: pth-root of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                             Racine p-�me                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a   = entier extensible
   _b = NULL ou pointeur sur un entier extensible
  p   = entier > 0
  mode = 0,1 ou 2

  sortie :
  si mode & 3 = 0: b <- floor(a^(1/p))
  si mode & 3 = 1: b <- floor(a^(1/p)+1/2)
  si mode & 3 = 2: b <- ceil(a^(1/p))
  si mode & 3 = 1: b <- ceil(a^(1/p)-1/2) (= floor(a^(1/p)+1/2))
   si _b != NULL, *_b <- b
   retourne b

  erreurs :
  NEGATIVE_EXPONENT si p <= 0
  NEGATIVE_BASE si a < 0 et p est pair
*/

xint xx(private_root)(xint *_b, xint a, long p, long mode) {
    long la = xx_lg(a), sa = xx_sgn(a), lb, q,r, exact;
    chiffre *aa;
    int free_a;
    xint b;
    xx_push_roots_2(a,_b);
#ifdef caml_api
#define  a __lr.a
#define _b __lr._b
#endif

    /* contr�le les arguments */
    if (p <= 0) xx(failwith)(NEGATIVE_EXPONENT);
    if ((sa) && (!(p&1))) xx(failwith)(NEGATIVE_BASE);

    /* cas a = 0 ou p = 1 */
    if (la == 0) {
        b = xx(enlarge)(_b,0);
        b->hd = 0;
        xx_update_and_return(_b,b);
    }
    if (p == 1)  {
        b = xx(enlarge)(_b,la);
        if (a != b) {xn(move)(a->val,la,b->val); b->hd = a->hd;}
        xx_update_and_return(_b,b);
    }

    /* mode = 1 ou 3 -> calcule floor((2^p|a|)^(1/p)) */
    if (mode & 1) {

        /* aa <- 2^p*|a| */
        q = p/HW; r = p & (HW-1);
        aa = xn(alloc)(la+q+1);
        xn(clear)(aa,q);
        aa[la+q] = xn(shift_up)(a->val,la,aa+q,r);
        for (la += q+1; aa[la-1] == 0; la--);
        free_a = 1;

        /* b->val <- floor( (floor((2^p|a|)^(1/p))+1)/2 ) */
        lb = (la+p-1)/p + 1;
        b = xx(enlarge)(_b,lb);
        xn(root)(aa,la,b->val,p);
        b->val[lb-1] = xn(inc1)(b->val,lb-1);
        xn(shift_down)(b->val,lb,b->val,1);

    }

    /* mode = 0 ou 2 -> calcule floor(|a|^(1/p)) */
    else {

        /* b->val = floor(|a|^(1/p)) */
        lb = (la+p-1)/p;
        b = xx(enlarge)(_b,lb+1);
        if (a == b) {
            aa = xn(alloc)(la);
            xn(move)(a->val,la,aa);
            free_a = 1;
        } else {aa = a->val; free_a = 0;}
        exact = xn(root)(aa,la,b->val,p);

        /* si la racine n'est pas enti�re, incr�mente b si a < 0 en mode 0
           ou a > 0 en mode 2 */
        if (sa) mode ^= 2;
        if ((!exact) && (mode & 2)) {b->val[lb] = xn(inc1)(b->val,lb); lb++;}
          
    }


    /* sgn(b) = sgn(a) */
    xx(make_head)(b,lb,sa);
    if (free_a) xn(free)(aa);
    xx_update_and_return(_b,b);
      
#undef  a
#undef _b
}

#if defined(caml_api) || defined(ocaml_api)

xint xx(root)   (            xint *_b, xint a, long p) {return xx(private_root)(_b,     a,Long_val(p),0);}
xint xx(groot)  (value mode, xint *_b, xint a, long p) {return xx(private_root)(_b,     a,Long_val(p),Round_val(mode));}
xint xx(f_root) (                      xint a, long p) {return xx(private_root)(xx_null,a,Long_val(p),0);}
xint xx(f_groot)(value mode,           xint a, long p) {return xx(private_root)(xx_null,a,Long_val(p),Round_val(mode));}

#endif /* api */
