// file kernel/x/c/shift.c: shift of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              D�calages                                |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                             /* +-------------+
                                |  D�calages  |
                                +-------------+ */

/*
   entr�e :
   a  = entier extensible
   _b = NULL ou pointeur sur un entier extensible
   n  = entier non sign�
   sens = 0 ou 1

   sortie :
   si sens = 0: b <- a >> n
   si sens = 1: b <- a << n
   si _b != NULL, *_b <- b
   retourne b
*/
xint xx(private_shift)(xint *_b, xint a, unsigned long n, long sens) {
    long la = xx_lg(a), sa = xx_sgn(a), lb,p;
    chiffre r;
    xint b;
    xx_push_roots_2(a,_b);
#ifdef caml_api
#define  a __lr.a
#define _b __lr._b
#endif

    /* a = 0 ou n = 0 => b <- a */
    if ((la == 0) || (n == 0)) {
        b = xx(enlarge)(_b,la);
        if (a != b) {xn(move)(a->val,la,b->val); b->hd = a->hd;}
    }

    else if (sens) { /* --------------- d�calage � gauche */
        p = (n + HW-1)/HW; n = p*HW - n;
        lb = la + p;
        b = xx(enlarge)(_b,lb);
        /* bug potentiel sur x86 : si n = 0, r << (HW-n) = r (d�calage non effectu�).
           En fait �a ne g�ne pas car on a r = 0 dans ce cas. */
        r = xn(shift_down)(a->val,la,b->val+p,n);
        b->val[p-1] = r << (HW-n);
        xn(clear)(b->val,p-1);
        xx(make_head)(b,lb,sa);
    }

    else { /* ------------------------- d�calage � droite */
        p = (n + HW-1)/HW; n = p*HW - n;
        lb = la - p + 1;
        if (lb <= 0) {b = xx(enlarge)(_b,0); b->hd = 0;}
        else {
            b = xx(enlarge)(_b,lb);
            r = (n) ? a->val[p-1] >> (HW-n) : 0;
            if (p < la) {
                b->val[lb-1] = xn(shift_up)(a->val+p,la-p,b->val,n);
                b->val[0] |= r;
            }
            else b->val[0] = r;
            xx(make_head)(b,lb,sa);
        }
    }

    xx_update_and_return(_b,b);

#undef  a
#undef _b
}

#if defined(caml_api) || defined(ocaml_api)
xint xx(shl)(xint *_b, xint a, long n) {
    return((n >= 0) ? xx(private_shift)(_b,a, Long_val(n),1) :
                      xx(private_shift)(_b,a,-Long_val(n),0));
}
xint xx(shr)(xint *_b, xint a, long n) {
    return((n <  0) ? xx(private_shift)(_b,a,-Long_val(n),1) :
                      xx(private_shift)(_b,a, Long_val(n),0));
}
xint xx(f_shl)(xint a, long n) {
    return((n >= 0) ? xx(private_shift)(xx_null,a, Long_val(n),1) :
                      xx(private_shift)(xx_null,a,-Long_val(n),0));
}
xint xx(f_shr)(xint a, long n) {
    return((n <  0) ? xx(private_shift)(xx_null,a,-Long_val(n),1) :
                      xx(private_shift)(xx_null,a, Long_val(n),0));
}

#endif /* api */

                             /* +-------------+
                                |  D�coupage  |
                                +-------------+ */

/*
   entr�e :
   a = entier extensible
   _b,_c = NULL ou pointeurs vers des entiers extensibles
   n = longueur >= 0

   contraintes :
   en mode Caml/Ocaml, _b et _c ont la m�me validit� (NULL/non NULL)
   en mode C, les validit�s de _b et _c sont ind�pendantes
   lorsque _b et _c sont des pointeurs valides, ils sont distincts

   sortie :
   b <- sgn(a)*floor(|a|/2^n)
   c <- sgn(a)*(|a| mod 2^n)
   si_ b != NULL, *_b <- b 
   si _c != NULL, *_c <- c
   si _b = _c = NULL retourne le couple (b,c) (Caml/Ocaml uniquement)
  
   erreur :
   NEGATIVE_INDEX si n < 0
   MULTIPLE_RESULT si _b == _c != NULL
*/
#if defined(c_api)
void
#elif defined(caml_api) || defined(ocaml_api)
value
#endif
xx(split)(xint *_b, xint *_c, xint a, long n) {
    long la = xx_lg(a), sa = xx_sgn(a), lb,lc,p;
    chiffre r;
    xx_push_roots_32(a,_b,_c,b,c);
#ifdef caml_api
#define  a __lr.a
#define  b __lr.b
#define  c __lr.c
#define _b __lr._b
#define _c __lr._c
#endif

#if defined(caml_api) || defined(ocaml_api)
    n = Long_val(n);
#endif

    /* contr�le les arguments
       L'identit� des validit�s de _b et _c n'est pas v�rifi�e car elle
       est garantie par les fonctions appelantes en mode Caml/Ocaml.
    */
    if (n < 0) xx(failwith)(NEGATIVE_INDEX);
    if (_b == _c) {
        if (_c != xx_null) xx(failwith)(MULTIPLE_RESULT);
#ifdef c_api
        else return;
#endif
    }

    /* traite les cas a = 0 et n = 0 � part */
    if ((la == 0) || (n == 0)) {
        b = xx(enlarge)(_b,la); 
        if (a != b) {xn(move)(a->val,la,b->val); b->hd = a->hd;}
        c = xx(enlarge)(_c,0);
        c->hd = 0;
    }

    else {
        /* taille des r�sultats */
        p = (n + HW-1)/HW; n = p*HW - n;
        lb = la - p + 1; if (lb < 0)  lb = 0;
        lc = p;          if (lc > la) lc = la;
        b = xx(enlarge)(_b,lb); 
        c = xx(enlarge)(_c,lc);

        /* copie les parties basse et haute de a dans b et c */
        if (a != c) xn(move)(a->val,lc,c->val);
        if (lb) {
            r = (n) ? a->val[p-1] >> (HW-n) : 0;
            if (p < la) {
                b->val[lb-1] = xn(shift_up)(a->val+p,la-p,b->val,n);
                b->val[0] |= r;
            }
            else b->val[0] = r;
        }
        if ((lc == p) && (n)) c->val[p-1] &= (((chiffre)1<<(HW-n)) - 1);

        /* signe et longueurs effectives */
        xx(make_head)(b,lb,sa);
        xx(make_head)(c,lc,sa);
    }

    /* mise � jour des pointeurs _b et _c ou retour du couple (b,c) */
#if defined(c_api)

    (_b == xx_null) ? xx(remove)(&b) : xx(update)(_b,b);
    (_c == xx_null) ? xx(remove)(&c) : xx(update)(_c,c);
    return;

#elif defined(caml_api) || defined(ocaml_api)
    if (_b != xx_null) {
        if (*_b != b) modify((value *)_b,(value)b);
        if (*_c != c) modify((value *)_c,(value)c);
        xx_pop_roots();
        return(Val_unit);
    } else {
        xint *r = (xint *)alloc_tuple(2);
        r[0] = b;
        r[1] = c;
        xx_pop_roots();
        return((value)r);
    }
#endif /* api */

#undef  a
#undef  b
#undef  c
#undef _b
#undef _c
}

#if defined(caml_api) || defined(ocaml_api)

value xx(f_split)(xint a, long n) {return(xx(split)(xx_null,xx_null,a,n));}

#endif /* defined(caml_api) || defined(ocaml_api) */

                           /* +-----------------+
                              |  Concat�nation  |
                              +-----------------+ */

/*
   entr�e :
   a,b = entiers extensibles
   _c = NULL ou pointeur sur un entier extensible
   n   = longueur >= 0

   sortie :
   c <- a + b*2^n
   si _c != NULL, *_c <- c
   retourne c

   erreur :
   NEGATIVE_INDEX si n < 0
*/
xint xx(join)(xint *_c, xint a, xint b, long n) {
    long la = xx_lg(a),  lb = xx_lg(b), lc;
    long sa = xx_sgn(a), sb = xx_sgn(b);
    long p;
    chiffre *aa,r;
    int free_a;
    xint c;
    xx_push_roots_3(a,b,_c);
#ifdef caml_api
#define  a __lr.a
#define  b __lr.b
#define _c __lr._c
#endif

#if defined(caml_api) || defined(ocaml_api)
    n = Long_val(n);
#endif
    if (n < 0) xx(failwith)(NEGATIVE_INDEX);

    /* traite le cas b = 0 � part */
    if (lb == 0) {
        c = xx(enlarge)(_c,la);
        if (a != c) {xn(move)(a->val,la,c->val); c->hd = a->hd;}
        xx_update_and_return(_c,c);
    }

    /* taille du r�sultat */
    p = (n + HW-1)/HW; n = p*HW - n;
    lc = lb + p;
    if (lc <= la) lc = la;
    if (sa == sb) lc++;
    c = xx(enlarge)(_c,lc);

    /* si a va �tre �cras�, le recopie */
    if (a == c) {
        aa = xn(alloc)(la);
        xn(move)(c->val,la,aa);
        free_a = 1;
    } else {aa = a->val, free_a = 0;}

    /* c <- b*2^n */
    r = xn(shift_down)(b->val,lb,c->val+p,n);
    if (p) {c->val[p-1] = r << (HW-n); xn(clear)(c->val,p-1);}
    xn(clear)(c->val+lb+p, lc-lb-p);

    /* c <- c + a */
    if (sa == sb)                           xn(inc)(c->val,lc,aa,la);
    else if (xn(cmp)(c->val,lc,aa,la) >= 0) xn(dec)(c->val,lc,aa,la);
    else                          {sb = sa; xn(sub)(aa,la,c->val,la,c->val);}

    /* lib�re a s'il a �t� recopi� */
    if (free_a) xn(free)(aa);

    /* taille et signe du r�sultat */
    xx(make_head)(c,lc,sb);
    xx_update_and_return(_c,c);

#undef  a
#undef  b
#undef _c
}

#if defined(caml_api) || defined(ocaml_api)

xint xx(f_join)(xint a, xint b, long n) {return xx(join)(xx_null,a,b,n);}

#endif /* defined(caml_api) || defined(ocaml_api) */
