// file kernel/x/h/div.h: division of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                                Division                               |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                         /* +---------------------+
                            |  Division g�n�rale  |
                            +---------------------+ */

/*
  entr�e :
  a,b = entiers extensibles
  _c  = NULL ou adresse d'un entier extensible
  _d  = NULL ou adresse d'un entier extensible
  mode = long

  contraintes :
  b != 0
  si _c et _d ne sont pas �gaux � NULL alors ils sont distincts

  sortie :
  si mode & 3 = 0 : c <- floor(a/b),     d <- a - b*c (division tronqu�e    )
  si mode & 3 = 1 : c <- floor(a/b+1/2), d <- a - b*c (division centr�e up  )
  si mode & 3 = 2 : c <- ceil(a/b),      d <- a - b*c (division major�e     )
  si mode & 3 = 3 : c <- ceil(a/b-1/2),  d <- a - b*c (division centr�e down)
  si _c != NULL : *_c <- c
  si _d != NULL : *_d <- d

  valeur de retour  : c_api   ml_api
  si mode & 12 =  0 :  NULL     unit
  si mode & 12 =  4 :     c        c
  si mode & 12 =  8 :     d        d
  si mode & 12 = 12 :  NULL    (c,d)

  erreur :
  ZERO_DIVISOR si b = 0
  MULTIPLE_RESULT si _c == _d != NULL
*/

#if defined(caml_api) || defined(ocaml_api)
value xx(private_quomod)(xint *_c, xint *_d, xint a, xint b, long mode);

value xx(hquomod)  (value mode, xint *_c, xint *_d, xint a, xint b);
value xx(hquo)     (value mode, xint *_c,           xint a, xint b);
value xx(hmod)     (value mode,           xint *_d, xint a, xint b);

value xx(f_hquomod)(value mode,                     xint a, xint b);
value xx(f_hquo)   (value mode,                     xint a, xint b);
value xx(f_hmod)   (value mode,                     xint a, xint b);

value xx(quomod)   (            xint *_c, xint *_d, xint a, xint b);
value xx(quo)      (            xint *_c,           xint a, xint b);
value xx(mod)      (                      xint *_d, xint a, xint b);

value xx(f_quomod) (                                xint a, xint b);
value xx(f_quo)    (                                xint a, xint b);
value xx(f_mod)    (                                xint a, xint b);

#elif defined(c_api)
xint xx(private_quomod)(xint *_c, xint *_d, xint a, xint b, long mode);

extern inline void xx(gquomod)(xint *_c, xint *_d, xint a, xint b, long mode) {       xx(private_quomod)(_c,  _d,   a,b,  0|(mode & 3));}
extern inline xint xx(gquo)   (xint *_c,           xint a, xint b, long mode) {return xx(private_quomod)(_c,  NULL, a,b,  4|(mode & 3));}
extern inline xint xx(gmod)   (          xint *_d, xint a, xint b, long mode) {return xx(private_quomod)(NULL,_d,   a,b,  8|(mode & 3));}
extern inline xint xx(f_gquo) (                    xint a, xint b, long mode) {return xx(private_quomod)(NULL,NULL, a,b,  4|(mode & 3));}
extern inline xint xx(f_gmod) (                    xint a, xint b, long mode) {return xx(private_quomod)(NULL,NULL, a,b,  8|(mode & 3));}

extern inline void xx(quomod) (xint *_c, xint *_d, xint a, xint b           ) {       xx(private_quomod)(_c,  _d,   a,b,  0|0);}
extern inline xint xx(quo)    (xint *_c,           xint a, xint b           ) {return xx(private_quomod)(_c,  NULL, a,b,  4|0);}
extern inline xint xx(mod)    (          xint *_d, xint a, xint b           ) {return xx(private_quomod)(NULL,_d,   a,b,  8|0);}
extern inline xint xx(f_quo)  (                    xint a, xint b           ) {return xx(private_quomod)(NULL,NULL, a,b,  4|0);}
extern inline xint xx(f_mod)  (                    xint a, xint b           ) {return xx(private_quomod)(NULL,NULL, a,b,  8|0);}
#endif /* api */
                      /* +--------------------------+
                         |  Division par un entier  |
                         +--------------------------+ */

/*
   entr�e :
   a  = entier extensible
   b  = long ou Caml/Ocaml int
   _c = NULL ou adresse d'un entier extensible
   mode = long

   sortie :
   si mode & 3 = 0 : c <- floor(a/b),     d <- a - b*c (division tronqu�e)
   si mode & 3 = 1 : c <- floor(a/b+1/2), d <- a - b*c (division centr�e up)
   si mode & 3 = 2 : c <- ceil(a/b),      d <- a - b*c (division major�e)
   si mode & 3 = 3 : c <- ceil(a/b-1/2),  d <- a - b*c (division centr�e down)
   si _c != NULL : *_c <- c

   valeur de retour  : c_api   ml_api
   si mode & 12 =  0 :     d     unit
   si mode & 12 =  4 :     c        c
   si mode & 12 =  8 :     d        d
   si mode & 12 = 12 :     c    (c,d)

   erreur :
   ZERO_DIVISOR si b = 0
*/
#if defined(caml_api) || defined(ocaml_api)
value xx(private_quomod_1)(xint *_c, xint a, long b, long mode);

value xx(gquomod_1)  (value mode, xint *_c, xint a, long b);
value xx(gquo_1)     (value mode, xint *_c, xint a, long b);

value xx(f_gquomod_1)(value mode,           xint a, long b);
value xx(f_gquo_1)   (value mode,           xint a, long b);
value xx(f_gmod_1)   (value mode,           xint a, long b);

value xx(quomod_1)   (            xint *_c, xint a, long b);
value xx(quo_1)      (            xint *_c, xint a, long b);

value xx(f_quomod_1) (                      xint a, long b);
value xx(f_quo_1)    (                      xint a, long b);
value xx(f_mod_1)    (                      xint a, long b);

#elif defined(c_api)
long xx(private_quomod_1)(xint *_c, xint a, long b, long mode);

extern inline long xx(gquomod_1)(xint *_c, xint a, long b, long mode) {return       xx(private_quomod_1)(_c,   a,b,  8|(mode & 3));}
extern inline xint xx(gquo_1)   (xint *_c, xint a, long b, long mode) {return (xint)xx(private_quomod_1)(_c,   a,b,  4|(mode & 3));}
extern inline long xx(gmod_1)   (          xint a, long b, long mode) {return       xx(private_quomod_1)(NULL, a,b,  8|(mode & 3));}
extern inline xint xx(f_gquo_1) (          xint a, long b, long mode) {return (xint)xx(private_quomod_1)(NULL, a,b,  4|(mode & 3));}
extern inline long xx(f_gmod_1) (          xint a, long b, long mode) {return       xx(private_quomod_1)(NULL, a,b,  8|(mode & 3));}
                                                                                    
extern inline long xx(quomod_1) (xint *_c, xint a, long b           ) {return       xx(private_quomod_1)(_c,   a,b,  8|0);}
extern inline xint xx(quo_1)    (xint *_c, xint a, long b           ) {return (xint)xx(private_quomod_1)(_c,   a,b,  4|0);}
extern inline long xx(mod_1)    (          xint a, long b           ) {return       xx(private_quomod_1)(NULL, a,b,  8|0);}
extern inline xint xx(f_quo_1)  (          xint a, long b           ) {return (xint)xx(private_quomod_1)(NULL, a,b,  4|0);}
extern inline long xx(f_mod_1)  (          xint a, long b           ) {return       xx(private_quomod_1)(NULL, a,b,  8|0);}
#endif /* api */
