// file kernel/x/h/gcd.h: Greatest common divisor
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                                  PGCD                                 |
 |                                                                       |
 +-----------------------------------------------------------------------*/

              /* +--------------------------------------+
                 |  D�veloppement en fraction continue  |
                 +--------------------------------------+ */

/*
  entr�e :
  a,b = entiers extensibles
  _d,_u,_v,_p,_q = NULL ou pointeurs vers des entiers extensibles
  mode = long

  sortie :
  calcule d,u,v,p,q tels que :

             u*a - v*b = d = pgcd positif(a,b)
             p*b = q*a = ppcm(a,b) avec le signe de a*b
             p*u - q*v = 1

  pour x = d,u,v,p,q : si _x != NULL, *_x <- x

  valeur de retour : c_api       ml_api
  si mode & 3 = 0  :  NULL         unit
  si mode & 3 = 1  :     d           d
  si mode & 3 = 2  :  NULL      (d,u,v)
  si mode & 3 = 3  :  NULL  (d,u,v,p,q)

  erreur :
  MULTIPLE_RESULT si (_d,_u,_v,_p,_q) contient deux pointeurs non nuls �gaux
*/
#if defined(caml_api) || defined(ocaml_api)
value xx(private_cfrac)(xint *_d, xint *_u, xint *_v, xint *_p, xint *_q, xint a, xint b, long mode);

value xx(gcd)   (xint *_d,                                         xint a, xint b);
value xx(gcd_ex)(xint *_d, xint *_u, xint *_v,                     xint a, xint b);
value xx(cfrac) (xint *_d, xint *_u, xint *_v, xint *_p, xint *_q, xint a, xint b);
value xx(cfrac_bytecode)(xint **v, int argn);
value xx(f_gcd)   (xint a, xint b);
value xx(f_gcd_ex)(xint a, xint b);
value xx(f_cfrac) (xint a, xint b);

#elif defined(c_api)
xint xx(private_cfrac)(xint *_d, xint *_u, xint *_v, xint *_p, xint *_q, xint a, xint b, long mode);

extern inline xint xx(gcd)(xint *_d, xint a, xint b) {
  return xx(private_cfrac)(_d,NULL,NULL,NULL,NULL,a,b,1);
}

extern inline void xx(gcd_ex)(xint *_d, xint *_u, xint *_v, xint a, xint b) {
    xx(private_cfrac)(_d,_u,_v,NULL,NULL,a,b,0);
}

extern inline void xx(cfrac)(xint *_d, xint *_u, xint *_v, xint *_p, xint *_q, xint a, xint b) {
    xx(private_cfrac)(_d,_u,_v,_p,_q,a,b,0);
}

extern inline xint xx(f_gcd)(xint a, xint b) {
    return xx(private_cfrac)(NULL,NULL,NULL,NULL,NULL,a,b,1);
}
#endif /* api */
