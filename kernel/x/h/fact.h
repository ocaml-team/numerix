// file kernel/x/h/fact.h: factorial
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Factorielle                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  _a = NULL ou pointeur sur un entier extensible
  n = long ou Caml/Ocaml int >= 0

  sortie :
  a <- n!
  si _a != NULL, *_a <- a
  retourne a

  erreur :
  NEGATIVE_BASE si n < 0
*/
xint xx(fact)(xint *_a, long n);

extern inline xint xx(f_fact)(long n) {return xx(fact)(NULL,n);}

