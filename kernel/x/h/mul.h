// file kernel/x/h/mul.h: multiplication/square of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                             Multiplication                            |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                          /* +------------------+
                             |  Multiplication  |
                             +------------------+ */

/*
   entr�e :
   a,b = entiers extensibles
   _c = NULL ou pointeur sur un entier extensible

   sortie :
   c <- a * b
   si _c != NULL, *_c <- c
   retourne c
*/
xint xx(mul)(xint *_c, xint a, xint b);

                    /* +------------------------------+
                       |  Multiplication par un long  |
                       +------------------------------+ */

/*
   entr�e :
   a   = entier extensible
   b   = long ou Caml/Ocaml int
   _c = NULL ou pointeur sur un entier extensible

   sortie :
   c <- a * b
  si _c != NULL, *_c <- c
  retourne c
*/
xint xx(mul_1)(xint *_c, xint a, long b);

                               /* +---------+
                                  |  Carr�  |
                                  +---------+ */

/*
   entr�e :
   a   = entier extensible
   _b = NULL ou pointeur sur un entier extensible

   sortie :
   b <- a^2
   si _b != NULL, *_b <- b
   retourne b
*/
xint xx(sqr)(xint *_b, xint a);

extern inline xint xx(f_mul)  (xint a, xint b) {return xx(mul)  (NULL,a,b);}
extern inline xint xx(f_mul_1)(xint a, long b) {return xx(mul_1)(NULL,a,b);}
extern inline xint xx(f_sqr)  (xint a)         {return xx(sqr)  (NULL,a);  }

