// file kernel/x/h/powmod.h: modular exponentiation
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                      Exponentiation modulaire                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a,b,c = entiers extensibles
  _d = NULL ou pointeur sur un entier extensible
  mode = long

  sortie :
  d <- gmod(a^b, c, mode)
  si _d != NULL, *_d <- d
  retourne d

  erreur :
  NEGATIVE_EXPONENT si b < 0
  ZERO_DIVISOR      si c = 0
*/
xint xx(private_powmod)(xint *_d, xint a, xint b, xint c, long mode);

#ifdef c_api
extern inline xint xx(gpowmod)(xint *_d, xint a, xint b, xint c, long mode) {
    return xx(private_powmod)(_d,a,b,c,mode);
}

extern inline xint xx(powmod)(xint *_d, xint a, xint b, xint c) {
    return xx(private_powmod)(_d,a,b,c,0);
}

extern inline xint xx(f_gpowmod)(xint a, xint b, xint c, long mode) {
    return xx(private_powmod)(NULL,a,b,c,mode);
}

extern inline xint xx(f_powmod)(xint a, xint b, xint c) {
    return xx(private_powmod)(NULL,a,b,c,0);
}
#endif
