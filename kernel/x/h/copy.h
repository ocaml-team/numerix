// file kernel/x/h/copy.h: extensible integer copying
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                               Copie                                   |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                   /* +---------------------------------+
                      |  copie, valeur absolue, oppos�  |
                      +---------------------------------+ */

/*
   entr�e :
   a = entier extensible
   _b = NULL ou pointeur sur un entier extensible
   s = 0 ou 1 ou 2

   sortie :
   si s = 0, b <- a
   si s = 1, b <- |a|
   si s = 2, b <- -a
   si _b != NULL, *_b <- b
   retourne b
   */
xint xx(private_copy)(xint *_b, xint a, long s);

extern inline xint xx(copy) (xint *_b, xint a) {return xx(private_copy)(_b,a,0);}
extern inline xint xx(abs)  (xint *_b, xint a) {return xx(private_copy)(_b,a,1);}
extern inline xint xx(neg)  (xint *_b, xint a) {return xx(private_copy)(_b,a,2);}
extern inline xint xx(f_abs)          (xint a) {return xx(private_copy)(NULL,a,1);}
extern inline xint xx(f_neg)          (xint a) {return xx(private_copy)(NULL,a,2);}

#if defined(caml_api) || defined(ocaml_api)

xint xx(copy_out)(xint *_a);
xint xx(look)    (xint *_a);

                             /* +------------+
                                |  Make_ref  |
                                +------------+ */

/*
   entr�e :
   a = entier extensible

   sortie :
   retourne une r�f�rence sur a
*/
xint *xx(make_ref)(xint a);

#elif defined(c_api)

extern inline xint xx(f_copy)(xint a) {return xx(private_copy)(NULL,a,0);}
#endif /* api */
