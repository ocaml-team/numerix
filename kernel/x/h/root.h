// file kernel/x/h/root.h: pth-root of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Racine p-�me                             |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a   = entier extensible
   _b = NULL ou pointeur sur un entier extensible
  p   = entier > 0
  mode = 0,1 ou 2

  sortie :
  si mode & 3 = 0: b <- floor(a^(1/p))
  si mode & 3 = 1: b <- floor(a^(1/p)+1/2)
  si mode & 3 = 2: b <- ceil(a^(1/p))
  si mode & 3 = 1: b <- ceil(a^(1/p)-1/2) (= floor(a^(1/p)+1/2))
   si _b != NULL, *_b <- b
   retourne b

  erreurs :
  NEGATIVE_EXPONENT si p <= 0
  NEGATIVE_BASE si a < 0 et p est pair
*/
xint xx(private_root)(xint *_b, xint a, long p, long mode);

#if defined(caml_api) || defined(ocaml_api)

xint xx(root)   (            xint *_b, xint a, long p);
xint xx(groot)  (value mode, xint *_b, xint a, long p);
xint xx(f_root) (                      xint a, long p);
xint xx(f_groot)(value mode,           xint a, long p);

#elif defined(c_api)

extern inline xint xx(root)   (xint *_b, xint a, long p           ) {return xx(private_root)(_b,  a,p,0);}
extern inline xint xx(groot)  (xint *_b, xint a, long p, long mode) {return xx(private_root)(_b,  a,p,mode);}
extern inline xint xx(f_root) (          xint a, long p           ) {return xx(private_root)(NULL,a,p,0);}
extern inline xint xx(f_groot)(          xint a, long p, long mode) {return xx(private_root)(NULL,a,p,mode);}
#endif /* api */
