// file kernel/x/h/dump.h: debugging facilities
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                                Affichage                              |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* ---------------------------------------- Affichage hexad�cimal
  entr�e :
  a = entier extensible

  sortie :
  affiche a
*/
#if defined(caml_api) || defined(ocaml_api)
value xx(dump)(xint a);
#elif defined(c_api)
void xx(dump)(xint a);
#endif /* api */

/* ---------------------------------------- Erreur interne
   entr�e :
   msg = cha�ne de caract�res
   n   = nombre d'entiers � afficher
   ... = suite d'entiers extensibles

   sortie :
   affiche le message et les entiers donn�s, puis termine le programme
*/
void xx(internal_error)(char *msg, int n, ...)  __attribute__((noreturn));

