// file kernel/x/h/numerix.h: multi-language extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                 D�finitions communes � C/Caml/Ocaml                   |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* v�rifie qu'une et une seule api est d�finie */
#if defined(c_api) + defined(caml_api) + defined(ocaml_api) < 1
#error  "missing xx_api definition"
#elif defined(c_api) + defined(caml_api) + defined(ocaml_api) > 1
#error  "multiple xx_api definitions"
#endif

/*-------------------- fixe le signe et la longueur d'un entier
   entr�e :
   a = entier extensible
   l = longueur >= 0
   s = 0 ou SIGN_m

   sortie :
   diminue l au besoin pour avoir l = 0 ou a->val[l-1] != 0
   et forme l'ent�te de a
*/
extern inline void xx(make_head)(xint a, long l, long s) {
  while ((l) && (!a->val[l-1])) l--;
  a->hd = (l) ? l|(s) : 0;
}

/* -------------------- liste des messages d'erreur */

#define INTEGER_OVERFLOW   "Numerix kernel: integer overflow"
#define INVALID_STRING     "Numerix kernel: invalid string"
#define MULTIPLE_RESULT    "Numerix kernel: multiple result"
#define NEGATIVE_BASE      "Numerix kernel: negative base"
#define NEGATIVE_EXPONENT  "Numerix kernel: negative exponent"
#define NEGATIVE_INDEX     "Numerix kernel: negative index"
#define NEGATIVE_SIZE      "Numerix kernel: negative size"
#define NUMBER_TOO_BIG     "Numerix kernel: number too big"
#define OUT_OF_MEMORY      "Numerix kernel: out of memory"
#define ZERO_DIVISOR       "Numerix kernel: division by zero"

/*-------------------- Liste des fonctions */

#include "add.h"
#include "alloc.h"
#include "cmp.h"
#include "convert.h"
#include "copy.h"
#include "div.h"
#include "dump.h"
#include "fact.h"
#include "gcd.h"
#include "mul.h"
#include "pow.h"
#include "powmod.h"
#include "prime.h"
#include "random.h"
#include "root.h"
#include "shift.h"
#include "sqrt.h"
#include "string.h"

