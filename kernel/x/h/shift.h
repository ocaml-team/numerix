// file kernel/x/h/shift.h: shift of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              D�calages                                |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                             /* +-------------+
                                |  D�calages  |
                                +-------------+ */

/*
   entr�e :
   a  = entier extensible
   _b = NULL ou pointeur sur un entier extensible
   n  = entier non sign�
   sens = 0 ou 1

   sortie :
   si sens = 0: b <- a >> n
   si sens = 1: b <- a << n
   si _b != NULL, *_b <- b
   retourne b
*/
xint xx(private_shift)(xint *_b, xint a, unsigned long n, long sens);

extern inline xint xx(shl)(xint *_b, xint a, long n) {
    return((n >= 0) ? xx(private_shift)(_b,a, n,1)
                    : xx(private_shift)(_b,a,-n,0));
}
extern inline xint xx(shr)(xint *_b, xint a, long n) {
    return((n <  0) ? xx(private_shift)(_b,a,-n,1)
                    : xx(private_shift)(_b,a, n,0));
}
extern inline xint xx(f_shl)(xint a, long n) {
    return((n >= 0) ? xx(private_shift)(NULL,a, n,1)
                    : xx(private_shift)(NULL,a,-n,0));
}
extern inline xint xx(f_shr)(xint a, long n) {
    return((n <  0) ? xx(private_shift)(NULL,a,-n,1)
                    : xx(private_shift)(NULL,a, n,0));
}


                             /* +-------------+
                                |  D�coupage  |
                                +-------------+ */

/*
   entr�e :
   a = entier extensible
   _b,_c = NULL ou pointeurs vers des entiers extensibles
   n = longueur >= 0

   contraintes :
   en mode Caml/Ocaml, _b et _c ont la m�me validit� (NULL/non NULL)
   en mode C, les validit�s de _b et _c sont ind�pendantes
   lorsque _b et _c sont des pointeurs valides, ils sont distincts

   sortie :
   b <- sgn(a)*floor(|a|/2^n)
   c <- sgn(a)*(|a| mod 2^n)
   si_ b != NULL, *_b <- b 
   si _c != NULL, *_c <- c
   si _b = _c = NULL retourne le couple (b,c) (Caml/Ocaml uniquement)
  
   erreur :
   NEGATIVE_INDEX si n < 0
   MULTIPLE_RESULT si _b == _c != NULL
*/
#if defined(caml_api) || defined(ocaml_api)
value xx(split)(xint *_b, xint *_c, xint a, long n);
value xx(f_split)(xint a, long n);
#elif defined(c_api)
void xx(split)(xint *_b, xint *_c, xint a, long n);
#endif /* api */


                           /* +-----------------+
                              |  Concat�nation  |
                              +-----------------+ */

/*
   entr�e :
   a,b = entiers extensibles
   _c = NULL ou pointeur sur un entier extensible
   n   = longueur >= 0

   sortie :
   c <- a + b*2^n
   si _c != NULL, *_c <- c
   retourne c

   erreur :
   NEGATIVE_INDEX si n < 0
*/
xint xx(join)(xint *_c, xint a, xint b, long n);

extern inline xint xx(f_join)(xint a, xint b, long n) {
    return xx(join)(NULL,a,b,n);
}

