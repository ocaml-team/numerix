// file kernel/x/h/alloc.h: memory allocation for extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Allocation de m�moire                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* alloue max(2a,b) chiffres */
xint xx(alloc)(long a, long b);

/* entr�e :
   _x = pointeur vers un entier extensible ou xx_null
   l = longueur >= 0

  sortie :
  si _x est un pointeur valide et si l'entier point� a une capacit� >= l
  alors retourne *_x
  sinon retourne un entier neuf de capacit� >= l
*/
extern inline xint xx(enlarge)(xint *_x, long l) {
    long lx = (_x == xx_null) ? -1 : xx_capacity(*_x);
    return((lx < l) ? xx(alloc)(lx,l) : *_x);
}


#ifdef debug_alloc
long xx(get_alloc_count)();
#endif

/* allocation d'une cha�ne */
#if defined(c_api)
extern inline char *xx(alloc_string)(long l) {
  char *s = (char *)malloc((l)+1);
  if (s == NULL) xx(failwith)(OUT_OF_MEMORY);
  return(s);
}

#elif defined(caml_api) || defined(ocaml_api)
extern inline char *xx(alloc_string)(long l) {return (char *)alloc_string(l);}
#endif
