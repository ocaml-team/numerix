// file kernel/x/h/random.h: random extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                            Nombres al�atoires                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* initialisation du g�n�rateur al�atoire
   si n != 0, le prend comme graine
   si n = 0,  prend l'heure en secondes comme graine */

#if defined(caml_api) || defined(ocaml_api)
value xx(random_init)(long n);
#elif defined(c_api)
void xx(random_init)(long n);
#endif /* api */

/* Nombre al�atoire
   entr�e :
   _a = NULL ou pointeur vers un entier extensible
   n = longueur >= 0
   mode = long C

   sortie :
   a <- entier al�atoire entre -2^n-1 et 2^n-1
   les bits 0 et 1 de mode restreignent la plage al�atoire de la mani�re suivante :
   b0=0 => le signe est forc� >= 0
   b1=1 => le bit de rang n-1 est forc� = 1 si n > 0

   si _a != NULL, *_a <- a
   retourne a

   erreur :
   NEGATIVE_SIZE si n < 0
*/
xint xx(private_random)(xint *_a, long n, long mode);

/*
  nrandom(n)  -> [0, 2^n[
  zrandom(n)  -> ]-2^n, 2^n[
  nrandom1(n) -> [2^(n-1), 2^n[
  zrandom1(n) -> ]-2^n, -2^(n-1)] U [2^(n-1), 2^n[
*/

extern inline xint xx(nrandom) (xint *_a, long n) {return xx(private_random)(_a,n,0);}
extern inline xint xx(zrandom) (xint *_a, long n) {return xx(private_random)(_a,n,1);}
extern inline xint xx(nrandom1)(xint *_a, long n) {return xx(private_random)(_a,n,2);}
extern inline xint xx(zrandom1)(xint *_a, long n) {return xx(private_random)(_a,n,3);}

extern inline xint xx(f_nrandom)         (long n) {return xx(private_random)(NULL,n,0);}
extern inline xint xx(f_zrandom)         (long n) {return xx(private_random)(NULL,n,1);}
extern inline xint xx(f_nrandom1)        (long n) {return xx(private_random)(NULL,n,2);}
extern inline xint xx(f_zrandom1)        (long n) {return xx(private_random)(NULL,n,3);}

