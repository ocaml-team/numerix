// file kernel/x/h/add.h: addition/subtraction of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                         Addition/soustraction                         |
 |                                                                       |
 +-----------------------------------------------------------------------*/


                              /* +------------+
                                 |  Addition  |
                                 +------------+ */

/*  
   entr�e :
   a,b = entiers extensibles
   _c = NULL ou pointeur sur un entier extensible
   s = 0 ou 1

   sortie :
   c <- a + (-1)^s*b
   si _c != NULL, *_c <- c
   retourne c
   */
xint xx(private_add)(xint *_c, xint a, xint b, long s);

                           /* +------------------+
                              |  Addition mixte  |
                              +------------------+ */
/*
  entr�e :
  a = entier extensible
  b = long ou Caml/Ocaml int
   _c = NULL ou pointeur sur un entier extensible
  s = 0 ou 1

  sortie :
  c <- a + (-1)^s*b
  si _c != NULL, *_c <- c
  retourne c
*/
xint xx(private_add_1)(xint *_c, xint a, long b, long s);

extern inline xint xx(add)  (xint *_c, xint a, xint b) {return xx(private_add)  (_c,a,b,0);}
extern inline xint xx(sub)  (xint *_c, xint a, xint b) {return xx(private_add)  (_c,a,b,1);}
extern inline xint xx(add_1)(xint *_c, xint a, long b) {return xx(private_add_1)(_c,a,b,0);}
extern inline xint xx(sub_1)(xint *_c, xint a, long b) {return xx(private_add_1)(_c,a,b,1);}

extern inline xint xx(f_add)          (xint a, xint b) {return xx(private_add)  (NULL,a,b,0);}
extern inline xint xx(f_sub)          (xint a, xint b) {return xx(private_add)  (NULL,a,b,1);}
extern inline xint xx(f_add_1)        (xint a, long b) {return xx(private_add_1)(NULL,a,b,0);}
extern inline xint xx(f_sub_1)        (xint a, long b) {return xx(private_add_1)(NULL,a,b,1);}

