// file kernel/x/h/sqrt.h: square root of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Racine carr�e                            |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a   = entier extensible
   _b = NULL ou pointeur sur un entier extensible
  mode = long

  sortie :
  si mode & 3 = 0: b <- floor(sqrt(a))
  si mode & 3 = 1: b <- floor(sqrt(a)+1/2)
  si mode & 3 = 2: b <- ceil(sqrt(a))
  si mode & 3 = 3: b <- ceil(sqrt(a)-1/2) (= floor(sqrt(a)+1/2))
  si _b != NULL, *_b <- b
  retourne b

  erreur :
  NEGATIVE_BASE si a < 0
*/
xint xx(private_sqrt)(xint *_b, xint a, long mode);

#if defined(caml_api) || defined(ocaml_api)

xint xx(sqrt)   (            xint *_b, xint a);
xint xx(gsqrt)  (value mode, xint *_b, xint a);
xint xx(f_sqrt) (                      xint a);
xint xx(f_gsqrt)(value mode,           xint a);

#elif defined(c_api)

extern inline xint xx(sqrt)   (xint *_b, xint a           ) {return xx(private_sqrt)(_b,  a,0);}
extern inline xint xx(gsqrt)  (xint *_b, xint a, long mode) {return xx(private_sqrt)(_b,  a,mode);}
extern inline xint xx(f_sqrt) (          xint a           ) {return xx(private_sqrt)(NULL,a,0);}
extern inline xint xx(f_gsqrt)(          xint a, long mode) {return xx(private_sqrt)(NULL,a,mode);}

#endif /* api */
