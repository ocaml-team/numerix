// file kernel/x/h/cmp.h: comparison of extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                             Comparaison                               |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a,b = entiers extensibles

  sortie :
  1 si a > b, 0 si a = b, -1 si a < b
*/
long xx(cmp)(xint a, xint b);

/*
  entr�e :
  a = entier extensible
  b = entier Caml/Ocaml ou long

  sortie :
  1 si a > b, 0 si a = b, -1 si a < b
*/
long xx(cmp_1)(xint a, long b);

/* versions sp�cialis�es */
#if defined(caml_api) || defined(ocaml_api)

value xx(sgn)(xint a);

value xx(eq)     (xint a,xint b);
value xx(neq)    (xint a,xint b);
value xx(inf)    (xint a,xint b);
value xx(infeq)  (xint a,xint b);
value xx(sup)    (xint a,xint b);
value xx(supeq)  (xint a,xint b);

value xx(eq_1)   (xint a,long b);
value xx(neq_1)  (xint a,long b);
value xx(inf_1)  (xint a,long b);
value xx(infeq_1)(xint a,long b);
value xx(sup_1)  (xint a,long b);
value xx(supeq_1)(xint a,long b);

#elif defined(c_api)

extern inline long xx(sgn)(xint a){return((a->hd > 0) ? 1 : (a->hd < 0) ? -1 : 0);}

extern inline long xx(eq)     (xint a,xint b) {return((xx(cmp)  (a,b) == 0));}
extern inline long xx(neq)    (xint a,xint b) {return((xx(cmp)  (a,b) != 0));}
extern inline long xx(inf)    (xint a,xint b) {return((xx(cmp)  (a,b) <  0));}
extern inline long xx(infeq)  (xint a,xint b) {return((xx(cmp)  (a,b) <= 0));}
extern inline long xx(sup)    (xint a,xint b) {return((xx(cmp)  (a,b) >  0));}
extern inline long xx(supeq)  (xint a,xint b) {return((xx(cmp)  (a,b) >= 0));}

extern inline long xx(eq_1)   (xint a,long b) {return((xx(cmp_1)(a,b) == 0));}
extern inline long xx(neq_1)  (xint a,long b) {return((xx(cmp_1)(a,b) != 0));}
extern inline long xx(inf_1)  (xint a,long b) {return((xx(cmp_1)(a,b) <  0));}
extern inline long xx(infeq_1)(xint a,long b) {return((xx(cmp_1)(a,b) <= 0));}
extern inline long xx(sup_1)  (xint a,long b) {return((xx(cmp_1)(a,b) >  0));}
extern inline long xx(supeq_1)(xint a,long b) {return((xx(cmp_1)(a,b) >= 0));}
#endif /* api */
