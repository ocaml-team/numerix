// file kernel/x/h/pow.h: exponentiation
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Exponentiation                           |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/*
  entr�e :
  a  = entier extensible
  _b = NULL ou pointeur sur un entier extensible
  p  = exposant >= 0

  sortie :
  b <- a^p
  si _b != NULL, *_b <- b
  retourne b

  erreur :
  NEGATIVE_EXPONENT si p < 0
*/
xint xx(pow)(xint *_b, xint a, long p);

/*
  entr�e :
  a  = long ou Caml/Ocaml int
  _b = NULL ou pointeur sur un entier extensible
  p  = exposant >= 0

  sortie :
  b <- a^p
  si _b != NULL, *_b <- b
  retourne b

  erreur :
  NEGATIVE_EXPONENT si p < 0
*/

xint xx(pow_1)(xint *_b, long a, long p);

extern inline xint xx(f_pow)  (xint a, long p) {return xx(pow)  (NULL,a,p);}
extern inline xint xx(f_pow_1)(long a, long p) {return xx(pow_1)(NULL,a,p);}

