// file kernel/x/h/string.h: conversion from and to strings
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                      Conversion entier <-> cha�ne                     |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                         /* +--------------------+
                            |  cha�ne -> entier  |
                            +--------------------+ */


/*
  entr�e :
  _a = NULL ou pointeur sur entier extensible
  s   = cha�ne de caract�res

  sortie :
  a <- valeur de s
  si _a != NULL, *_a <- a
  retourne a

  erreur :
  INVALID_STRING si s contient un caract�re non reconnu ou s est vide
  ou r�duite au signe et au pr�fixe

  remarques :
  les signes + et - en d�but de cha�ne sont accept�s
  les pr�fixes 0x, 0X, 0o, 0O, 0b, 0B apr�s le signe sont reconnus
  pour une cha�ne hexad�cimale, les lettres a..f et A..F sont accept�es
*/
xint xx(copy_string)(xint *_a, char *s);

extern inline xint xx(of_string)(char *s) {return xx(copy_string)(NULL,s);}

                     /* +-----------------------------+
                        |  entier -> cha�ne d�cimale  |
                        +-----------------------------+ */

/* 
   entr�e :
   a = entier extensible

   sortie :
   retourne la repr�sentation d�cimale de a

   remarques :
   la cha�ne doit �tre lib�r�e par appel � free (c_api)
   la cha�ne cr��e ne contient pas de z�ro non significatif
   le signe n'est indiqu� que si a < 0
*/
char *xx(string_of)(xint a);


                       /* +-------------------------+
                          |  entier -> cha�ne hexa  |
                          +-------------------------+ */

/* 
   entr�e :
   a = entier extensible

   sortie :
   retourne la repr�sentation hexad�cimale de a

   remarques :
   la cha�ne doit �tre lib�r�e par appel � free (c_api)
   les chiffres >= 10 sont cod�s A..F
   la cha�ne cr��e ne contient pas de z�ro non significatif
   le signe n'est indiqu� que si a < 0
*/
char *xx(hstring_of)(xint a);

                      /* +---------------------------+
                         |  entier -> cha�ne octale  |
                         +---------------------------+ */

/* 
   entr�e :
   a = entier extensible

   sortie :
   retourne la repr�sentation octale de a

   remarques :
   la cha�ne doit �tre lib�r�e par appel � free (c_api)
   la cha�ne cr��e ne contient pas de z�ro non significatif
   le signe n'est indiqu� que si a < 0
*/
char *xx(ostring_of)(xint a);

                     /* +----------------------------+
                        |  entier -> cha�ne binaire  |
                        +----------------------------+ */

/* 
   entr�e :
   a = entier extensible

   sortie :
   retourne la repr�sentation binaire de a

   remarques :
   la cha�ne doit �tre lib�r�e par appel � free (c_api)
   la cha�ne cr��e ne contient pas de z�ro non significatif
   le signe n'est indiqu� que si a < 0
*/
char *xx(bstring_of)(xint a);

