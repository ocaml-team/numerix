// file kernel/x/h/convert.h: extensible integer <-> long conversion
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                             Conversion                                |
 |                                                                       |
 +-----------------------------------------------------------------------*/

                            /* +---------------+
                               |  int -> xint  |
                               +---------------+ */

/*
   entr�e :
   a = long ou Caml/Ocaml int
   _b = NULL ou pointeur sur un entier extensible

   sortie :
   b <- a
   si _b != NULL, *_b <- b
   retourne b
*/
xint xx(copy_int)(xint *_b, long a);

extern inline xint xx(of_int)(long a) {return(xx(copy_int)(NULL,a));}

                            /* +---------------+
                               |  xint -> int  |
                               +---------------+ */

/*                                
   entr�e :
   a = entier extensible

   sortie :
   retourne le long ou Caml/Ocaml int de m�me valeur que a

   erreur :
   INTEGER_OVERFLOW si |a| >= 2^30
*/
long xx(int_of)(xint a);


                         /* +---------------------+
                            |  Structure binaire  |
                            +---------------------+ */

/*
   entr�e :
   a = entier extensible

   sortie :
   si a <> 0 -> 1 + ceil(log_2(|a|))
   si a =  0 -> 0
*/
long xx(nbits)(xint a);

/*
  entr�e :
  a = entier extensible

  sortie :
  bits 0..30 de |a|
*/
long xx(lowbits)(xint a);

/*
  entr�e :
  a = entier extensible

  sortie :
  bits l-31..l-1 de |a|, l = nbits(a)
*/
long xx(highbits)(xint a);

/*
  entr�e :
  a = entier extensible
  n = indice

  sortie :
  bits 16*(n-1)..16*n-1 de |a|
*/
long xx(nth_word)(xint a, long n);

/*
  entr�e :
  a = entier extensible
  n = indice

  sortie :
  bit n de |a|
*/
long xx(nth_bit)(xint a, long n);

