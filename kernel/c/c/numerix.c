// file kernel/c/c/numerix.c: C extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                   Entiers extensibles, interface C                    |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include "../h/numerix.h"

#ifdef debug_alloc

/* marques pour tester les débordements */
#if defined(bits_32)
#define magic_1 3141592653UL
#elif defined(bits_64)
#define magic_1 3141592653589793238UL
#endif
#if HW == 16
#define magic_2 27182
#elif HW == 32
#define magic_2 2718281828UL
#elif HW == 64
#define magic_2 2718281828459045235UL
#endif

/* compteur d'allocation */
static long xalloc = 0;
long xx(get_alloc_count)() {return(xalloc);}

#define header_size 3*sizeof(long)+sizeof(chiffre)
#else
#define header_size 2*sizeof(long)
#endif

/* alloue max(2a,b) chiffres */
xint xx(alloc)(long a, long b) {
  long l;
  xint x;

  if (b >= LMAX) xx(failwith)(NUMBER_TOO_BIG);
  l = 2*a;
  if (l >= LMAX) l = LMAX-1;
  if (l < b)     l = b;

  x = (xint)malloc(header_size + l*sizeof(chiffre));
  if (x == NULL) {
    if (l > b) {
      l = b;
      x = (xint)malloc(header_size + l*sizeof(chiffre));
    }
    if (x == NULL) xx(failwith)(OUT_OF_MEMORY);
  }

  x->lmax = l;
  x->hd   = 0;
#ifdef debug_alloc
  x->m1     = magic_1;
  x->val[l] = magic_2;
  xalloc++;
#endif

  return(x);
}

/* libération/remplacement */
#ifdef debug_alloc
void xx(remove)(xint *_x) {
    if ((_x) && (*_x != NULL)) {
        xint x = *_x;

        if (xalloc) xalloc--;
        else xx(internal_error)("unexpected call to xx(free)",0);

        if (   (xx_lg(x) <= xx_capacity(x))
            && (x->m1 == magic_1)
            && (x->val[xx_capacity(x)] == magic_2)) {
            free(*_x);
            return;
        }

        xx(internal_error)("buffer overflow (x layer)",0);
    }
}

void xx(update)(xint *_x, xint y) {
    if ((_x) && (*_x != y)) {
        xx(remove)(_x);
        *_x = y;
    }
}

void xx(free)(xint *_x) {
    if (_x) xx(remove)(_x);
    *_x = NULL;
}
#endif

#include "../../x/c/add.c"
#include "../../x/c/cmp.c"
#include "../../x/c/convert.c"
#include "../../x/c/copy.c"
#include "../../x/c/div.c"
#include "../../x/c/dump.c"
#include "../../x/c/fact.c"
#include "../../x/c/gcd.c"
#include "../../x/c/mul.c"
#include "../../x/c/pow.c"
#include "../../x/c/powmod.c"
#include "../../x/c/prime.c"
#include "../../x/c/random.c"
#include "../../x/c/root.c"
#include "../../x/c/shift.c"
#include "../../x/c/sqrt.c"
#include "../../x/c/string.c"

