// file kernel/c/h/numerix.h: C extensible integer definitions
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                    D�finitions pour l'interface C                     |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#if 0
/* Numerix C-library, public interface

   C extensible integers are defined as pointers to structures containing
   two fields of long type ...

   THIS IS NOT THE REALITY

   ... but it will be enough to make the C compiler happy without having
   to include all the low-level Numerix headers.

   Please,

     1. never make an xint of your own by directing some pointer towards
        a double-long structure. Use only the xx-functions described below
        to build xint values.

     2. never use an xint created for one mode (clong/dlong/slong) with
        xx-functions dealing with another mode.

     3. never use pointer arithmetic on xints.

*/

typedef struct xint_item {long lmax; long hd;} *xint; /* white lie */

/*
  check if one and only one mode is defined and setup the xx macro
  accordingly
*/

#if   defined(use_clong) + defined(use_dlong) + defined(use_slong) < 1
#error  "missing use_xlong definition"
#elif defined(use_clong) + defined(use_dlong) + defined(use_slong) > 1
#error  "multiple use_xlong definitions"
#endif

#if defined(use_clong)
#define xx(nom)  cx_##nom
#elif defined(use_dlong)
#define xx(nom)  dx_##nom
#elif defined(use_slong)
#define xx(nom)  sx_##nom
#endif

/* creation/destruction */
extern inline xint xx(new)() {return(NULL);}
extern inline void xx(free)(xint *_x) {if (_x) {free(*_x); *_x = NULL;}}

#endif /* End of the public interface. Now comes the real one */

#include "../../n/h/numerix.h"
/*
  Un entier extensible est repr�sent� par deux "long" codant la longueur
  maximale , la longueur effective et le signe, suivis du tableau des
  chiffres codant la valeur absolue (encadr� par deux marques si debug_alloc
  est d�fini). Les longueurs sont exprim�es en nombre de chiffres.
  Le deuxi�me mot d'en-t�te du nombre x est interpr�t� comme suit :

    -- le bit de poids fort vaut 0 si x >= 0, 1 si x < 0
    -- les autres bits codent la longueur l avec x = 0 si l = 0
       et BASE^(l-1) <= |x| < BASE^l sinon.
    -- les chiffres d'indice >= l sont ignor�s
*/

typedef struct {       /* entier extensible          */
  long    lmax;        /* longueur maximale          */
  long    hd;          /* longueur r�elle et signe   */
#ifdef debug_alloc
  long    m1;                     /* marque de d�but */
#endif
  chiffre val[1];      /* 1 pour faire plaisir � gcc */
} * xint;

/* cr�ation/destruction */
extern inline xint xx(new)() {return(NULL);}
#ifdef debug_alloc
void xx(free)(xint *_x);
#else
extern inline void xx(free)(xint *_x) {if (_x) {free(*_x); *_x = NULL;}}
#endif

/* acc�s aux champs */
#define xx_capacity(a) ((a) ? (a)->lmax : -1)
#define xx_sgn(a)      ((a)->hd & SIGN_m)
#define xx_lg(a)       ((a)->hd & LONG_m)

/* pointeur nul */
#define xx_null  (xint *)NULL
#define Val_null (xint)  NULL

/* Interface avec le GC (no-op, il n'y a pas de GC en C) */
#define xx_push_roots_1(a)
#define xx_push_roots_2(a,b)
#define xx_push_roots_3(a,b,c)
#define xx_push_roots_4(a,b,c,d)
#define xx_push_roots_5(a,b,c,d,e)
#define xx_push_roots_6(a,b,c,d,e,f)

#define xx_push_roots_11(a,b)         xint b = Val_null
#define xx_push_roots_21(a,b,c)       xint c = Val_null
#define xx_push_roots_32(a,b,c,d,e)   xint d = Val_null, e = Val_null
#define xx_push_roots_42(a,b,c,d,e,f) xint e = Val_null, f = Val_null
#define xx_push_roots_75(a,b,c,d,e,f,g,h,i,j,k,l) \
  xint h = Val_null, i = Val_null, j = Val_null, k = Val_null, l = Val_null

#define xx_pop_roots()

/* suppression, remplacement */
#ifdef debug_alloc
void xx(remove)(xint *_x);
void xx(update)(xint *_x, xint y);
#else
extern inline void xx(remove)(xint *_x) {if (_x) free(*_x);}
extern inline void xx(update)(xint *_x, xint y) {
    if ((_x) && (*_x != y)) {free(*_x); *_x = y;}
}
#endif

/* retour d'une valeur */
#define xx_update_and_return(_x,x) do { \
    xx(update)(_x,x); \
    return(x);        \
} while(0)

/* traitement des erreurs */
extern inline void xx(failwith)(char *msg) __attribute__((noreturn));
extern inline void xx(failwith)(char *msg) {xn(internal_error)(msg,0);}

/* conversions */
#define Val_long(x) x
#define Val_bool(x) x
#define Val_tri(x)  x

/* interface commune � toutes les api */
#define c_api
#include "../../x/h/numerix.h"
