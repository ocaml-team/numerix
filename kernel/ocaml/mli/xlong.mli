(* file kernel/ocaml/mli/xlong.mli: Objective-Caml extensible integer definitions
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                     Définition d'un module Xlong                      |
 |                                                                       |
 +-----------------------------------------------------------------------*)

                           (* +-----------------+
                              |  module _Name_   |
                              +-----------------+ *)

module _Name_ : sig

  type t
  type tref
  val name : unit -> string
  val zero : t
  val one  : t

  external make_ref    :                               t ->               tref   = "xx(make_ref)"
  external copy_in     :               tref ->         t ->               unit   = "xx(copy)"
  external copy_out    :               tref ->                            t      = "xx(copy_out)"
  external look        :               tref ->                            t      = "xx(look)"
  external add         :                               t -> t   ->        t      = "xx(f_add)"
  external add_1       :                               t -> int ->        t      = "xx(f_add_1)"
  external add_in      :               tref ->         t -> t   ->        unit   = "xx(add)"
  external add_1_in    :               tref ->         t -> int ->        unit   = "xx(add_1)"
  external sub         :                               t -> t   ->        t      = "xx(f_sub)"
  external sub_1       :                               t -> int ->        t      = "xx(f_sub_1)"
  external sub_in      :               tref ->         t -> t   ->        unit   = "xx(sub)"
  external sub_1_in    :               tref ->         t -> int ->        unit   = "xx(sub_1)"
  external mul         :                               t -> t   ->        t      = "xx(f_mul)"
  external mul_1       :                               t -> int ->        t      = "xx(f_mul_1)"
  external mul_in      :               tref ->         t -> t   ->        unit   = "xx(mul)"
  external mul_1_in    :               tref ->         t -> int ->        unit   = "xx(mul_1)"
  external quomod      :                               t -> t   ->        t*t    = "xx(f_quomod)"
  external quo         :                               t -> t   ->        t      = "xx(f_quo)"
  external modulo      :                               t -> t   ->        t      = "xx(f_mod)"
  external gquomod     : round_mode ->                 t -> t   ->        t*t    = "xx(f_gquomod)"
  external gquo        : round_mode ->                 t -> t   ->        t      = "xx(f_gquo)"
  external gmod        : round_mode ->                 t -> t   ->        t      = "xx(f_gmod)"
  external quomod_in   :               tref -> tref -> t -> t   ->        unit   = "xx(quomod)"
  external quo_in      :               tref ->         t -> t   ->        unit   = "xx(quo)"
  external mod_in      :                       tref -> t -> t   ->        unit   = "xx(mod)"
  external gquomod_in  : round_mode -> tref -> tref -> t -> t   ->        unit   = "xx(gquomod)"
  external gquo_in     : round_mode -> tref ->         t -> t   ->        unit   = "xx(gquo)"
  external gmod_in     : round_mode ->         tref -> t -> t   ->        unit   = "xx(gmod)"
  external quomod_1    :                               t -> int ->        t*int  = "xx(f_quomod_1)"
  external quo_1       :                               t -> int ->        t      = "xx(f_quo_1)"
  external mod_1       :                               t -> int ->        int    = "xx(f_mod_1)"
  external gquomod_1   : round_mode ->                 t -> int ->        t*int  = "xx(f_gquomod_1)"
  external gquo_1      : round_mode ->                 t -> int ->        t      = "xx(f_gquo_1)"
  external gmod_1      : round_mode ->                 t -> int ->        int    = "xx(f_gmod_1)"
  external quomod_1_in :               tref ->         t -> int ->        int    = "xx(quomod_1)"
  external quo_1_in    :               tref ->         t -> int ->        unit   = "xx(quo_1)"
  external gquomod_1_in: round_mode -> tref ->         t -> int ->        int    = "xx(gquomod_1)"
  external gquo_1_in   : round_mode -> tref ->         t -> int ->        unit   = "xx(gquo_1)"
  external abs         :                               t ->               t      = "xx(f_abs)"
  external abs_in      :               tref ->         t ->               unit   = "xx(abs)"
  external neg         :                               t ->               t      = "xx(f_neg)"
  external neg_in      :               tref ->         t ->               unit   = "xx(neg)"
  external sqr         :                               t ->               t      = "xx(f_sqr)"
  external pow         :                               t -> int ->        t      = "xx(f_pow)"
  external pow_1       :                             int -> int ->        t      = "xx(f_pow_1)"
  external powmod      :                               t -> t   -> t ->   t      = "xx(f_powmod)"
  external gpowmod     : round_mode ->                 t -> t   -> t ->   t      = "xx(f_gpowmod)"
  external sqr_in      :               tref ->         t ->               unit   = "xx(sqr)"
  external pow_in      :               tref ->         t -> int ->        unit   = "xx(pow)"
  external pow_1_in    :               tref ->       int -> int ->        unit   = "xx(pow_1)"
  external powmod_in   :               tref ->         t -> t   -> t ->   unit   = "xx(powmod)"
  external gpowmod_in  : round_mode -> tref ->         t -> t   -> t ->   unit   = "xx(gpowmod)"
  external sqrt        :                               t ->               t      = "xx(f_sqrt)"
  external root        :                               t -> int ->        t      = "xx(f_root)"
  external gsqrt       : round_mode ->                 t ->               t      = "xx(f_gsqrt)"
  external groot       : round_mode ->                 t -> int ->        t      = "xx(f_groot)"
  external sqrt_in     :               tref ->         t ->               unit   = "xx(sqrt)"
  external root_in     :               tref ->         t -> int ->        unit   = "xx(root)"
  external gsqrt_in    : round_mode -> tref ->         t ->               unit   = "xx(gsqrt)"
  external groot_in    : round_mode -> tref ->         t -> int ->        unit   = "xx(groot)"
  external fact        :                             int ->               t      = "xx(f_fact)"
  external fact_in     :               tref ->       int ->               unit   = "xx(fact)"
  external gcd         :                               t -> t   ->        t      = "xx(f_gcd)"
  external gcd_ex      :                               t -> t   ->        t*t*t  = "xx(f_gcd_ex)"
  external cfrac       :                               t -> t   ->     t*t*t*t*t = "xx(f_cfrac)"
  external gcd_in      : tref->                        t -> t   ->        unit   = "xx(gcd)"
  external gcd_ex_in   : tref->tref->tref->            t -> t   ->        unit   = "xx(gcd_ex)"
  external cfrac_in    : tref->tref->tref->tref->tref->t -> t   ->        unit   = "xx(cfrac_bytecode)" "xx(cfrac)"
  external isprime     :                               t ->           tristate   = "xx(isprime)"
  external isprime_1   :                             int ->           tristate   = "xx(isprime_1)"
  external sgn         :                               t ->               int    = "xx(sgn)"
  external cmp         :                               t -> t   ->        int    = "xx(cmp)"
  external cmp_1       :                               t -> int ->        int    = "xx(cmp_1)"
  external eq          :                               t -> t   ->        bool   = "xx(eq)"
  external eq_1        :                               t -> int ->        bool   = "xx(eq_1)"
  external neq         :                               t -> t   ->        bool   = "xx(neq)"
  external neq_1       :                               t -> int ->        bool   = "xx(neq_1)"
  external inf         :                               t -> t   ->        bool   = "xx(inf)"
  external inf_1       :                               t -> int ->        bool   = "xx(inf_1)"
  external infeq       :                               t -> t   ->        bool   = "xx(infeq)"
  external infeq_1     :                               t -> int ->        bool   = "xx(infeq_1)"
  external sup         :                               t -> t   ->        bool   = "xx(sup)"
  external sup_1       :                               t -> int ->        bool   = "xx(sup_1)"
  external supeq       :                               t -> t   ->        bool   = "xx(supeq)"
  external supeq_1     :                               t -> int ->        bool   = "xx(supeq_1)"
  external of_int      :                             int ->               t      = "xx(of_int)"
  external of_string   :                          string ->               t      = "xx(of_string)"
  external of_int_in   :               tref ->       int ->               unit   = "xx(copy_int)"
  external of_string_in:               tref ->    string ->               unit   = "xx(copy_string)"
  external int_of      :                               t ->               int    = "xx(int_of)"
  external string_of   :                               t ->               string = "xx(string_of)"
  external bstring_of  :                               t ->               string = "xx(bstring_of)"
  external hstring_of  :                               t ->               string = "xx(hstring_of)"
  external ostring_of  :                               t ->               string = "xx(ostring_of)"
  external nrandom     :                             int->                t      = "xx(f_nrandom)"
  external zrandom     :                             int->                t      = "xx(f_zrandom)"
  external nrandom1    :                             int->                t      = "xx(f_nrandom1)"
  external zrandom1    :                             int->                t      = "xx(f_zrandom1)"
  external nrandom_in  :               tref ->       int->                unit   = "xx(nrandom)"
  external zrandom_in  :               tref ->       int->                unit   = "xx(zrandom)"
  external nrandom1_in :               tref ->       int->                unit   = "xx(nrandom1)"
  external zrandom1_in :               tref ->       int->                unit   = "xx(zrandom1)"
  val      random_init :                             int->                unit
  external nbits       :                               t ->               int    = "xx(nbits)"
  external lowbits     :                               t ->               int    = "xx(lowbits)"
  external highbits    :                               t ->               int    = "xx(highbits)"
  external nth_word    :                               t -> int ->        int    = "xx(nth_word)"
  external nth_bit     :                               t -> int ->        bool   = "xx(nth_bit)"
  external shl         :                               t -> int ->        t      = "xx(f_shl)"
  external shr         :                               t -> int ->        t      = "xx(f_shr)"
  external split       :                               t -> int ->        t*t    = "xx(f_split)"
  external join        :                               t -> t   -> int -> t      = "xx(f_join)"
  external shl_in      :               tref ->         t -> int ->        unit   = "xx(shl)"
  external shr_in      :               tref ->         t -> int ->        unit   = "xx(shr)"
  external split_in    :               tref -> tref -> t -> int ->        unit   = "xx(split)"
  external join_in     :               tref ->         t -> t   -> int -> unit   = "xx(join)"
  external dump        :                               t ->               unit   = "xx(dump)"
  val toplevel_print   :                               t ->               unit
  val toplevel_print_tref:             tref ->                            unit
  exception Error of string

end
