(* file kernel/ocaml/mli/rfuns.mli: Approximation of usual real-valued functions
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |         Approximation des fonctions usuelles � valeurs r�elles        |
 |                                                                       |
 +-----------------------------------------------------------------------*)

      (* +----------------------------------------------------------+
         |  Approximation des fonctions usuelles � valeurs r�elles  |
         +----------------------------------------------------------+ *)

module Rfuns(E:Int_type) : sig

  (* possible errors are :

     Error "0/0" when a and b are both zero
     Error "f"   when a/b is an invalid argument for function f
  *)
  exception Error of string

  (* [f a b n] returns x such that |2^n*f(a/b) - x| < 1 *)

  val arccos    : E.t -> E.t -> int -> E.t
  val arccosh   : E.t -> E.t -> int -> E.t
  val arccot    : E.t -> E.t -> int -> E.t
  val arccoth   : E.t -> E.t -> int -> E.t
  val arcsin    : E.t -> E.t -> int -> E.t
  val arcsinh   : E.t -> E.t -> int -> E.t
  val arctan    : E.t -> E.t -> int -> E.t
  val arctanh   : E.t -> E.t -> int -> E.t
  val arg       : E.t -> E.t -> int -> E.t
  val cos       : E.t -> E.t -> int -> E.t
  val cosh      : E.t -> E.t -> int -> E.t
  val cosin     : E.t -> E.t -> int -> E.t*E.t
  val cosinh    : E.t -> E.t -> int -> E.t*E.t
  val cot       : E.t -> E.t -> int -> E.t
  val coth      : E.t -> E.t -> int -> E.t
  val exp       : E.t -> E.t -> int -> E.t
  val ln        : E.t -> E.t -> int -> E.t
  val sin       : E.t -> E.t -> int -> E.t
  val sinh      : E.t -> E.t -> int -> E.t
  val tan       : E.t -> E.t -> int -> E.t
  val tanh      : E.t -> E.t -> int -> E.t

  (* [e_f a p n] returns x such that |2^n*f(a/2^p) - x| < 1 *)

  val e_arccos  : E.t -> int -> int -> E.t
  val e_arccosh : E.t -> int -> int -> E.t
  val e_arccot  : E.t -> int -> int -> E.t
  val e_arccoth : E.t -> int -> int -> E.t
  val e_arcsin  : E.t -> int -> int -> E.t
  val e_arcsinh : E.t -> int -> int -> E.t
  val e_arctan  : E.t -> int -> int -> E.t
  val e_arctanh : E.t -> int -> int -> E.t
  val e_arg     : E.t -> int -> int -> E.t
  val e_cos     : E.t -> int -> int -> E.t
  val e_cosh    : E.t -> int -> int -> E.t
  val e_cosin   : E.t -> int -> int -> E.t*E.t
  val e_cosinh  : E.t -> int -> int -> E.t*E.t
  val e_cot     : E.t -> int -> int -> E.t
  val e_coth    : E.t -> int -> int -> E.t
  val e_exp     : E.t -> int -> int -> E.t
  val e_ln      : E.t -> int -> int -> E.t
  val e_sin     : E.t -> int -> int -> E.t
  val e_sinh    : E.t -> int -> int -> E.t
  val e_tan     : E.t -> int -> int -> E.t
  val e_tanh    : E.t -> int -> int -> E.t

  (* [r_f r a b c] returns the integer approximating c*f(a/b) according
                   to the rounding mode r *)

  val r_arccos  : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_arccosh : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_arccot  : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_arccoth : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_arcsin  : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_arcsinh : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_arctan  : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_arctanh : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_arg     : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_cos     : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_cosh    : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_cosin   : round_mode -> E.t -> E.t -> E.t -> E.t*E.t
  val r_cosinh  : round_mode -> E.t -> E.t -> E.t -> E.t*E.t
  val r_cot     : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_coth    : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_exp     : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_ln      : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_sin     : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_sinh    : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_tan     : round_mode -> E.t -> E.t -> E.t -> E.t
  val r_tanh    : round_mode -> E.t -> E.t -> E.t -> E.t

  (* user creation of an r_function *)
  val round : (int -> E.t) -> round_mode -> E.t -> E.t

  (* cache : the Rfuns(E) module stores in an internal cache the approximations
     of some frequently used constants. From these constants, the following
     numbers can be re-computed at practically no cost (a few additions and
     one shift) :

     ln(2)     exp(1)      arctan(1)    
     ln(3)     exp(-1)     arctan(1/2)
     ln(5)     cos(1)      arctan(1/3)
               sin(1)      arctan(1/5)

     cache_bits()  returns the sum of the bit lengths of the internal
                   constants. One bit occupies at most two memory bits.

     clear_cache() restores the internal constants to their initial
                   values, enabling the CG to reclaim memory.

  *)
  val cache_bits  : unit -> int
  val clear_cache : unit -> unit

end
