(* file kernel/ocaml/mli/count.mli: count operations
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                              Statistiques                             |
 |                                                                       |
 +-----------------------------------------------------------------------*)

                           (* +----------------+
                              |  Statistiques  |
                              +----------------+ *)


module Count(A:Int_type) : sig

  type statelt = {
    mutable n:float;  (* nombre d''appels,   number of calls      *)
    mutable s:float;  (* somme des tailles,  sum of operand sizes *)
    mutable m:int     (* taille maximale,    max operand size     *)
  }

  val cadd  : statelt (* add      sub                              *)
  val cmul  : statelt (* mul      sqr                              *)
  val cquo  : statelt (* quo      modulo       quomod              *)
  val cpow  : statelt (* pow      powmod       fact                *)
  val croot : statelt (* sqrt     root                             *)
  val cgcd  : statelt (* gcd      gcd_ex       cfrac       isprime *)
  val cbin  : statelt (* shr      shl          split       join    *)
                      (* nbits    lowbits      highbits    nth_bit *)
                      (* nth_word random                           *)
  val cmisc : statelt (* abs      neg          make_ref    copy_in *)
                      (* copy_out comparaisons conversions         *)

  val clear_stats : unit -> unit (* remise � z�ro,   reset all counters *)
  val print_stats : unit -> unit (* affichage,       print counters     *)

  include Int_type

end
