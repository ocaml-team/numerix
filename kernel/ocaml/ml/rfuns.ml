(* file kernel/ocaml/ml/rfuns.ml: Approximation of usual real-valued functions
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |         Approximation des fonctions usuelles � valeurs r�elles        |
 |                                                                       |
 +-----------------------------------------------------------------------*)

      (* +----------------------------------------------------------+
         |  Approximation des fonctions usuelles � valeurs r�elles  |
         +----------------------------------------------------------+ *)


module Rfuns(E:Int_type) = struct
  module I = Infixes(E)
  open E
  open I

  exception Error of string
  let zero_zero    = Error "0/0"
  let bit_overflow = Error "number too big"


                            (* +---------------+
                               |  Utilitaires  |
                               +---------------+ *)

  (* cr�ation d'une fonction e_f *)
  let emake f =
    fun a p n -> if p >= 0 then f a (shl one p) n else f (shr a p) one n

  (* partie enti�re sup�rieure de i/j, i,j = entiers courts avec j > 0 *)
  let ceil i j = let x = i/j in if i mod j > 0 then x+1 else x

  (* division avec d�calage et arrondi au plus proche *)
  let ( // ) a b n =
         if n = 0 then gquo Nearest_up a b
    else if n > 0 then gquo Nearest_up (shl a n) b
    else               gquo Nearest_up a (shr b n)

  (* d�calage � droite avec arrondi au plus proche *)
  let ( >> ) a n =
    let x = shr a n in if nth_bit a (n-1) then x +. sgn(a) else x

  (* valeur absolue sans recopie inutile *)
  let abs a = if sgn(a) < 0 then neg a else a

                    (* +------------------------------+
                       |  M�morisation de constantes  |
                       +------------------------------+ *)

  type cache_item = {
    c0 : t;                     (* valeur initiale         *)
    n0 : int;                   (* nb de bits pour c0      *)
    mutable calc : int -> unit; (* n -> approx(2^n*c)      *)
    mutable c : t;              (* valeur courante         *)
    mutable n : int;            (* nb de bits pour c       *)
    mutable d : t               (* correction pour arrondi *)
  }

  let cache_list = ref [] (* liste des valeurs m�moris�es *)

  (* Pour obtenir une approximation de x � p bits :

     1. Si p > x.n, recalculer une valeur plus pr�cise par x.calc(q) avec q >= p.
     Cette instruction modifie les champs x.c,x.n et x.d de sorte qu'on
     dispose � pr�sent de q bits. En particulier x.n = q.

     2. Retourner (x.c + (x.d mod 2^(k+1))) / 2^k avec k = x.n - p.
        La division doit �tre effectu�e par d�calage sans arrondi.

     Le r�le de x.d est de modifier la valeur trop pr�cise x.c de fa�on �
     retrouver celle que l'on avait enregistr� la premi�re fois que l'on avait
     calcul� au moins p bits. De la sorte, une augmentation de pr�cision ne
     remet pas en cause les approximations ant�rieures.

     Cr�ation de valeurs � m�moriser :

     [cache f c0 n0] cr�e un enregistrement de type cache_item associ� � la
     constante x valant initialement of_string(c0)/2^n0, puis retourne une
     fonction fx : int -> int -> E.t telle que fx a n retourne approx(a*x*2^n).
     f : int -> E.t est une fonction qui sera appel�e par x.calc pour augmenter
     la pr�cision ; l'argument est le nombre de bits voulus et le r�sultat est
     l'approximation correspondante.

     [cache2 f c0 d0 n0] cr�e deux enregistrements de type cache_item associ�s aux
     constantes x,y valant initialement string_of(c0)/2^n0 et string_of(d0)/2^n0,
     puis retourne le couple (fx,fy) des fonctions associ�es. f : int -> E.t*E.t
     est la fonction utilis�e par x.calc ou y.calc pour calculer simultan�ment
     des nouvelles approximations de x et y ; l'argument est le nombre de bits
     voulus et le r�sultat les approximations correspondantes.

  *)

  let cache,cache2 =

    (* mise � jour de x.c,x.n,x.d *)
    let update x c n =
      let p  = n - x.n in
      let d = match cmp x.c (shr c p) with
        | 1 -> one
        | _ -> zero
      in
        x.c <- c;
        x.n <- n;
        x.d <- (x.d ++ d) << p
    in

    (* recalcul avec q bits *)
    let calc  x   f q = update x (f q) q
    and calc2 x y f q = let c,d = f(q) in update x c q; update y d q
    and dummy       q = failwith "this cannot happen"
    in

    (* fonction d'interrogation *)
    let get x a n =
      if a = 0 then zero
      else begin

        (* On risque d'augmenter n de 10 + taille d'un Ocaml-int.
           V�rifie que �a ne d�borde pas.                          *)
        if n >= max_int - 73 then raise bit_overflow;

        (* n <- n + valuation 2-adique de a
           a <- a/2^v(a)
           l <- ceil(log_2(|a|/2^v(a)))      *)

        let rec clog l b = if b = 0 then l else clog (l+1) (b/2)   in
        let rec loop n a = if a land 1 = 0 then loop (n+1) (a/2)
                           else (clog 0 (Pervasives.abs(a)-1)),n,a in
        let l,n,a = loop n a in

          (* On a besoin de l+n bits pour x. Recalcule avec 10 bits de r�serve
             si la valeur enregistr�e n'est pas assez pr�cise. *)
          if x.n < l+n then x.calc(l+n+10);

          (* arrondit la valeur cach�e � l+n bits puis multiplie par a *)
          if l+n < x.n - nbits(x.c) then zero
          else begin
            let p   = x.n - l - n      in
            let _,d = split x.d (p+1)  in
            let r   = shr (x.c ++ d) p in (r*.a) >> l
          end

      end
    in

      (* cache *)
      (fun f c0 n0 ->
         let c0 = of_string c0 in
         let x = {c0=c0; n0=n0; c=c0; n=n0; d=zero; calc=dummy}
         in
           x.calc <- calc x f;
           cache_list := x :: !cache_list;
           get x),

      (* cache2 *)
      (fun f c0 d0 n0 ->
         let c0 = of_string c0
         and d0 = of_string d0 in
         let x = {c0=c0; n0=n0; c=c0; n=n0; d=zero; calc=dummy}
         and y = {c0=d0; n0=n0; c=d0; n=n0; d=zero; calc=dummy}
         in
           x.calc <- calc2 x y f;
           y.calc <- calc2 x y f;
           cache_list := x :: y :: !cache_list;
           (get x),(get y))


  (* taille courante du cache *)
  let cache_bits() =
    List.fold_left (fun n x -> n + nbits(x.c)) 0 !cache_list

  (* r�initialise le cache *)
  let clear_cache() =
    List.iter (fun x -> x.c <- x.c0; x.n <- x.n0; x.d <- zero) !cache_list

                      (* +----------------------+
                         |  Calcule round(c*x)  |
                         +----------------------+ *)

  (* f = n -> approx(2^n*x) *)
  let rec round_loop f r c n p =

    if n < 0 then raise bit_overflow;

    (* calcule c*f(n) puis ajoute 1/2 (Nearest_xxx) ou 1 (Ceil)
       l'arrondi demand� est la partie enti�re du r�sultat, si elle peut
       �tre d�termin�e.
    *)
    let x = c**f(n) in
    let x = match r with
      | Floor -> x
      | Ceil  -> x ++ (one << n)
      | _     -> x ++ (one << (n-1)) in

    let x,y = split x n in
    let y,z = match sgn(c) with 1 -> y--c, y++c | _ -> y++c, y--c in

    (* on a  x + y/2^n < res < x + z/2^n, y/2^n < 1 et z/2^n > -1

       donc si    0 <= y/2^n  et  z/2^n <= 1  alors floor = x
            si   -1 <= y/2^n  et  z/2^n <= 0  alors floor = x-1
            sinon, il faut recommencer avec plus de bits
    *)
         if (y >=. 0) & not (nth_bit (z-.1) n) then x
    else if (z <=. 0) & not (nth_bit (y+.1) n) then x-.1
    else round_loop f r c (n+p) (2*p)


  (* f = a b n -> approx(2^n*f(a/b))
     exact_f = a b -> Exact(x,y) si f(a/b) = x/y
     Irationnal si f(a/b) n'est pas rationnel

     Retourne une fonction qui calcule l'arrondi selon un mode r
     sp�cifi� de c*f(a/b).

     On teste si a/b = 0/0 -> erreur
     puis si f(a/b) est rationnel -> gquo r (c*x) y
     puis si f(a/b) est invalide  -> erreur
     puis si c = 0 -> 0
     
     Si f(a/b) est irrationnel et c <> 0, on calcule autant de bits de f(a/b)
     que n�cessaire pour d�terminer l'arrondi correct.
  *)
  type result = Exact of int*int | Irrationnal | Invalid of string

  let round f exact_f r a b c =
    if (a =. 0) & (b =. 0) then raise zero_zero
    else match exact_f a b with
      | Invalid s   -> raise (Error s)
      | Exact(x,y)  -> gquo_1 r (c*.x) y
      | Irrationnal ->
          if c =. 0 then zero
          else round_loop (f a b) r c (nbits(c) + 5) 5


  (* idem pour une fonction retournant un couple *)
  type result2 = Exact2 of int*int*int*int | Irrationnal2 | Invalid2 of string

  let round2 = 

    let rec loop f r c n p =

      if n < 0 then raise bit_overflow;

      (* calcule c*f(n) puis ajoute 1/2 (Nearest_xxx) ou 1 (Ceil)
         l'arrondi demand� est la partie enti�re du r�sultat, si elle peut
         �tre d�termin�e.
      *)
      let x1,x2 = f n          in
      let x1,x2 = c**x1, c**x2 in
      let x1,x2 = match r with
        | Floor -> x1,x2
        | Ceil  -> let u = one << n     in (x1++u),(x2++u)
        | _     -> let u = one << (n-1) in (x1++u),(x2++u)
      in
      let x1,y1 = split x1 n
      and x2,y2 = split x2 n in
      let y1,z1,y2,z2 = match sgn(c) with
        | 1 -> y1--c, y1++c, y2--c, y2++c
        | _ -> y1++c, y1--c, y2++c, y2--c in

      let x1,ok =
             if      (y1 >=. 0) & not (nth_bit (z1-.1) n) then x1,   true
        else if      (z1 <=. 0) & not (nth_bit (y1+.1) n) then x1-.1,true
        else                                                   x1,   false in
      let x2,ok =
             if ok & (y2 >=. 0) & not (nth_bit (z2-.1) n) then x2,   true
        else if ok & (z2 <=. 0) & not (nth_bit (y2+.1) n) then x2-.1,true
        else                                                   x2,   false in

        if ok then x1,x2 else loop f r c (n+p) (2*p)
    in

      fun f exact_f r a b c ->
        if (a =. 0) & (b =. 0) then raise zero_zero
        else match exact_f a b with
          | Invalid2 s          -> raise (Error s)
          | Exact2(x1,y1,x2,y2) -> (gquo_1 r (c*.x1) y1), (gquo_1 r (c*.x2) y2)
          | Irrationnal2        -> loop (f a b) r c (nbits(c) + 5) 5


            (* +----------------------------------------------+
               |  Sommation de la s�rie enti�re arctan/argth  |
               +----------------------------------------------+ *)


  (* Entr�e :
     s = bool�en
     a,b = entiers avec 0 <= |a| < |b|/sqrt(2)
     n = nombre de bits d�sir�s

     Sortie :
     retourne approx(2^n*argth(a/b)) (s=false) ou approx(arctan(a/b)) (s=true)

     Algorithme de calcul :
     La somme de la s�rie enti�re est calcul�e terme � terme avec
     regroupement des termes 2 par 2, puis 4 par 4, etc. A un moment
     donn� on dispose de :

     k : on va calculer le terme a^k/(k*b^k)
     p : nombre de termes calcul�s
     r : minorant de log_2(k*b^k/a^k)
     pile : (a0,b0,n0,u0,v0) :: (a1,b1,n1,u1,v1) :: ...

     avec ai = (+/-a^2)^(2^i), bi = b^(2^(i+1)),
     bi/ai >= 2^ni
     ui/vi = fraction en attente pour le groupe de 2^(i+1) termes.

     La somme des p premiers termes de la s�rie est �gale � :

     ((( u(p0)*a(p1)   u(p1) ) a(p2)   u(p2) )        u(pj) )  a*b
     ((( ----------- + ----- )*----- + ----- )* ... + ----- )*-----
     ((( v(p0)*b(p0)   v(p1) ) b(p1)   v(p2) )        v(pj) ) b(pj)

     avec p = 2^p0 + 2^p1 + ... + 2^pj, p0 < p1 < ... < pj.

     Le reste de la s�rie est major� en valeur absolue par 2*a^k/(k*b^k).
     On poursuit les calculs tant que ce majorant est sup�rieur � 1/2^(n+2),
     r contient un majorant de log_2(k*bk/ak), mis � jour incr�mentalement
     � l'aide des ni.

     Si r < n+2, on introduit le terme 1/k dans la pile, et on la r�duit
     en effectuant les additions des groupes ainsi compl�t�s.

     Si r >= n+2, on effectue les additions en instance dans la pile puis
     on tronque la fraction obtenue � n bits.
  *)

  let ps_atan s =

    (* na = nbits(a), nb = nbits(b) *)
    let compute a b na nb n =

      (* empile un nouveau terme *)
      let rec empile u v p r = function
        | [] ->
            let a0 = if s then neg(sqr a) else sqr(a) in
            let b0 = sqr(b)                 in
            let n0 = nbits(b0)-nbits(a0)-1  in
              (r+n0+1),[(a0,b0,n0,u,v)]

        | [(ai,bi,ni,ui,vi)] ->
            let aj = sqr ai                     in
            let bj = sqr bi                     in
            let nj = nbits(bj)-nbits(aj)-1      in
            let uj = ui**(v**bi) ++ u**(vi**ai) in
            let vj = v**vi                      in
              (r+nj-ni+1),[(ai,bi,ni,zero,one); (aj,bj,nj,uj,vj)]

        | (ai,bi,ni,ui,vi)::suite when p land 1 = 1 ->
            let uj = ui**(v**bi) ++ u**(vi**ai) in
            let vj = v**vi                      in
            let r,pile = empile uj vj (p/2) (r-ni) suite in
              r,((ai,bi,ni,zero,one) :: pile)

        | (ai,bi,ni,_,_)::suite -> (r+ni),((ai,bi,ni,u,v)::suite)
      in

      (* effectue les additions restant dans la pile *)
      let rec depile u v p = function
        | [] -> (u,v)

        | (ai,bi,ni,ui,vi)::suite when p land 1 = 1 ->
            depile (ui**v ++ u**(ai**vi)) ((vi**v)**bi) (p/2) suite

        | _::suite -> depile u v (p/2) suite
      in

      (* lance les calculs *)
      let rec loop k r pile = if r < n+2
      then let r,pile = empile one (of_int k) (k/2) r pile in loop (k+2) r pile
      else depile zero one (k/2) pile
      in
      let u,v = loop 1 (nb-na-1) [] in (((a**b)**u) // v) n

    in

    (* acc�l�ration : soit b = qa + r avec |r| <= |a|/2. On a :

        argth(a/b) =  argth(1/q) -  argth(r/(qb-a))
       arctan(a/b) = arctan(1/q) - arctan(r/(qb+a)) 

       Le temps de calcul de compute pour a/b est de l'ordre de

                       2*nbits(b) + nbits(a)
       M(n) * ln(n) * -----------------------
                             nbits(q)

       o� M(n) est le temps, suppos� quasi-lin�aire, d'une multiplication
       dont le r�sultat tient sur n bits. Les d�compositions pr�c�dentes sont
       donc int�ressantes si 7*nbits(a) > 4*nbits(b)
    *)

    let rec accel a b n =
      if (a =. 0) or (n <= 0) then zero
      else if n >= max_int - 2 then raise bit_overflow
      else begin
        let na = nbits(a) and nb = nbits(b) in
        if 7*na <= 4*nb then compute a b na nb n
        else begin
          let q,r = gquomod Nearest_up b a           in
          let x = compute one q 1 (nbits q) (n+2)    in
          let c = if s then q**b ++ a else q**b -- a in
          let y = accel r c (n+2)                    in (x -- y) >> 2
        end
      end

    in accel

                         (* +---------------------+
                            |  Calcul de ln(a/b)  |
                            +---------------------+ *)

  (* Algorithme :

     si a et b sont nuls ou de signes oppos�s, erreur
     sinon, changer les signes et permuter au besoin pour avoir 0 < b < a

     D�terminer p tel que 1/2 < a/(2^p*b) < 2
     Retourner la fraction au besoin pour calculer ln(x) avec 1 <= x < 2

     Comparer x aux valeurs suivantes et changer x en l'expression associ�e
     � l'intervalle contenant x.

     1. x | 1      5/4        3/2        8/5       2  |
        --+-------------------------------------------|
          |     x       3/2x       2x/3       2/x     |
        ----------------------------------------------+


     2. x | 1     16/15       9/8        6/5      5/4 |
        --+-------------------------------------------|
          |     x       9/8x        8x/9      5/4x    |
        ----------------------------------------------+


     3. x | 1    81/80      128/125     31/30      25/24       59/56     16/15|
        --+-------------------------------------------------------------------|
          |    x     80x/81      125x/128     25/24x     24x/25     16/15x    |
        ----------------------------------------------------------------------+


     4. x | 1    161/160     81/80 |
        --+------------------------|
          |    x       81/80x      |
        ---------------------------+

     Calculer le logarithme du nombre obtenu avec ps_atan, puis reconstituer
     ln(a/b) par combinaison lin�aire avec ln(2),ln(3),ln(5).

     Rmq: 
     Le nombre transmis � ps_atan est inf�rieur ou �gal � 1/320
     Les transformations pr�c�dentes d�composent correctement les nombres
     de Hamming jusqu'� 100, � l'exception de 27, 54 et 81.
  *)

  let ln =

    (* puissances de 3 et de 5 *)
    let pow3 = [|1; 3;  9;  27;  81;  243; 729; 2187; 6561; 19683; 59049; 177147|]
    and pow5 = [|1; 5; 25; 125; 625; 3125 |]
    in

    (* Calcul de ln(2), ln(3), ln(5). Formules :

       [ ln(2) ]   [ 118  171  441 ]   [ 2*argth(x0/x1) ]
       [ ln(3) ] = [ 187  271  699 ] * [ 2*argth(y0/y1) ]
       [ ln(5) ]   [ 274  397 1024 ]   [ 2*argth(z0/z1) ]

       avec
       x0 = 2*5^18 - 3^27,     x1 = 2*5^18 + 3^27,      x0/x1 ~ 1/4018
       y0 = 2^38   - 3^2*5^15, y1 = 2^38   + 3^2*5^15,  y0/y1 ~ 1/2501
       z0 = 3^8*5  - 2^15,     z1 = 3^8*5  + 2^15,      z0/z1 ~ 1/1772
    *)
    let x0 = of_string "3797046263" and x1 = of_string "15254992016237"
    and y0 = of_string  "219703819" and y1 = of_string   "549536110069"
    and z0 = of_string         "37" and z1 = of_string          "65573"
    in

    let ax = cache (ps_atan false x0 x1) "315524784848423407784245608" 100
    and ay = cache (ps_atan false y0 y1) "506805081164933785747655905" 100
    and az = cache (ps_atan false z0 z1) "715280331633428905505083056" 100
    in

      (* calcul de ln(a/b) *)
      fun a b n ->

        (* on a besoin de n+4 bits en interne *)
        if n >= max_int - 4 then raise bit_overflow;

        (* change les signes au besoin de fa�on � avoir a > 0, b > 0 *)
        let a,b = match sgn(a),sgn(b) with
          |  1, 1 -> a,b
          | -1,-1 -> neg(a),neg(b)
          |  0, 0 -> raise zero_zero
          |  _, _ -> raise(Error "ln")
        in

        (* classe a,b et multiplie le d�nominateur par 2^p *)
        let s,a,b = match cmp a b with
          | -1 -> -1,b,a
          | _  ->  1,a,b
        in
        let p = nbits(a) - nbits(b) in
        let b = b << p              in

        (* change x en 1/x si x < 1 *)
        let s,p,a,b = match cmp a b with
          | -1 -> -s,-p, b, a
          |  _ ->  s, p, a, b
        in
          (* ici res = s*(ln(a/b) + p*ln(2)) *)

        (*
          extrait les bits de poids fort de a et b pour effectuer les comparaisons
          suivantes en simple pr�cision. On ne garde que 18 bits pour �viter les
          d�bordements.
        *)

        let aa = (highbits(a) lsr 13) land 0x3ffff
        and bb = (highbits(b) lsr 13) land 0x3ffff in

        (* invariant : res = s*(sx*ln(x) + (p+p2)*ln(2) + p3*ln(3) + p5*ln(5)) *)
        let sx, p2, p3, p5 = (1,0,0,0) in

        let (* tableau 1 *)     sx, p2,      p3,      p5,         aa,    bb =
        if  2*aa >  3*bb then
        if  5*aa >  8*bb then  -sx, p2+sx,   p3,      p5,       2*bb,    aa 
                         else   sx, p2-sx,   p3+sx,   p5,       2*aa,  3*bb
        else
        if  4*aa >  5*bb then  -sx, p2-sx,   p3+sx,   p5,       3*bb,  2*aa
                         else   sx, p2,      p3,      p5,         aa,    bb
        in

        let (* tableau 2 *)     sx, p2,      p3,      p5,         aa,    bb =
        if  8*aa >  9*bb then
        if  5*aa >  6*bb then  -sx, p2-2*sx, p3,      p5+sx,    5*bb,  4*aa 
                         else   sx, p2-3*sx, p3+2*sx, p5,       8*aa,  9*bb
        else
        if 15*aa > 16*bb then  -sx, p2-3*sx, p3+2*sx, p5,       9*bb,  8*aa
                         else   sx, p2,      p3,      p5,         aa,    bb
        in

        let (* tableau 3 *)     sx, p2,      p3,      p5,         aa,    bb =
        if 30*aa > 31*bb then
        if 24*aa > 25*bb then
        if 56*aa > 59*bb then  -sx, p2+4*sx, p3-sx,   p5-sx,   16*bb, 15*aa 
                         else   sx, p2-3*sx, p3-sx,   p5+2*sx, 24*aa, 25*bb
                         else  -sx, p2-3*sx, p3-sx,   p5+2*sx, 25*bb, 24*aa
        else
        if 80*aa > 81*bb then
        if 125*aa>128*bb then   sx, p2+7*sx, p3,      p5-3*sx,125*aa,128*bb
                         else   sx, p2-4*sx, p3+4*sx, p5-sx,   80*aa, 81*bb
                         else   sx, p2,      p3,      p5,         aa,    bb
        in

        let (* tableau 4 *)     sx, p2,      p3,      p5 =
        if aa-bb >bb/160 then  -sx, p2-4*sx, p3+4*sx, p5-sx
                         else   sx, p2,      p3,      p5
        in

        (* effectue les transformations sur a,b *)
        let u,v = if p3 < 0 then pow3.(-p3), 1   else 1,pow3.(p3)   in
        let u,v = if p5 < 0 then u*pow5.(-p5), v else u,v*pow5.(p5) in
        let u,v = if p2 < 0 then (u lsl (-p2)),v else u,(v lsl  p2) in
        let a,b = if s=1    then (a*.u, b*.v)    else (b*.v, a*.u)  in

        (* Calcul final. En sortie des r�ductions pr�c�dentes, on a

           -8 <= p2 <= 12,  -11 <= p3 <= 7,  -4 <= p5 <= 5.

           Si |p| n'est pas trop grand, on peut calculer les coefficients
           de ax,ay et az en simple pr�cision. Sinon, on effectue ce
           calcul en multi-pr�cision.
        *)
        if Pervasives.abs(p) <= max_int/512 then begin
          let p2 = s*(p+p2) and p3 = s*p3 and p5 = s*p5 in
          let px = 118*p2 +  187*p3 +  274*p5
          and py = 171*p2 +  271*p3 +  397*p5
          and pz = 441*p2 +  699*p3 + 1024*p5             in
          let l  = n+4                                    in
          let lx = ps_atan false (a--b) (a++b) l          in
          (lx ++ (ax px l) ++ (ay py l) ++ (az pz l)) >> 3
        end
        else begin
          let p2 = s*p2 and p3 = s*p3 and p5 = s*p5       in
          let p  = of_int(s*p)                            in
          let px = p*.118 +. (118*p2 +  187*p3 +  274*p5)
          and py = p*.171 +. (171*p2 +  271*p3 +  397*p5)
          and pz = p*.441 +. (441*p2 +  699*p3 + 1024*p5) in
          if n >= max_int - nbits(p) - 11 then raise bit_overflow;
          let l  = n + nbits(p) + 11                      in
          let lx = ps_atan false (a--b) (a++b) l          in
          (lx ++ (ax 1 l)**px ++ (ay 1 l)**py ++ (az 1 l)**pz) >> (l-n-1)
        end

  let exact_ln a b =
         if eq a b           then Exact(0,1)
    else if sgn(a) <> sgn(b) then Invalid "ln"
    else                          Irrationnal

  let r_ln = round ln exact_ln
  let e_ln = emake ln

                (* +--------------------------------------+
                   |  Calcul de argth(a/b), arccoth(a/b)  |
                   +--------------------------------------+ *)

  (* Algorithme :

     si |a| >= |b|, erreur
     si |a/b| <= 1/128, utiliser ps_atan
     sinon, argth(a/b) <- (1/2)*ln((b+a)/(b-a))

     argcoth(a/b) = argth(b/a)
  *)

  let arctanh a b n =
    if (a =. 0) & (b =. 0)   then raise zero_zero;
    if supeq (abs a) (abs b) then raise (Error "arctanh");
    if nbits(a)-nbits(b) > 7 then ps_atan false a b n
    else if n = min_int      then zero
                             else ln (b ++ a) (b -- a) (n-1)

  let exact_arctanh a b =
         if a =. 0                then Exact(0,1)
    else if supeq (abs a) (abs b) then Invalid "arctanh"
    else                               Irrationnal

  let arccoth       a b = arctanh       b a
  let exact_arccoth a b = exact_arctanh b a

  let r_arctanh = round arctanh exact_arctanh
  let e_arctanh = emake arctanh
  let r_arccoth = round arccoth exact_arccoth
  let e_arccoth = emake arccoth


                       (* +------------------------+
                          |  Calcul de argsh(a/b)  |
                          +------------------------+ *)

  (* Algorithme :
     si b = 0, erreur
     si a = 0, argsh(a/b) <- 0

     sinon, changer les signes pour avoir a >= 0, b > 0
     k <- max(0, n+1-nbits(a))
     a <- a*2^k, b <- b*2^k
     argsh(a/b) <- +/-ln((a + sqrt(a^2+b^2))/b)
  *)
  let arcsinh a b n =
    let s,a,b = match sgn(a),sgn(b) with
      | 0, 0 -> raise zero_zero
      | _, 0 -> raise (Error "arcsinh")
      | 1, 1 ->  1,a,b
      | 1, _ -> -1,a,(neg b)
      | _, 1 -> -1,(neg a),b
      | _, _ ->  1,(neg a),(neg b)
    in
    let na = nbits(a) in
    if (a =. 0) or (n <= min_int+na-1) then zero
    else begin
        let k = max 0 (n+1-na)   in
        if k >= max_int/2 then raise bit_overflow;
        let c = sqr(a) ++ sqr(b) in
        let x = ln ((a << k) ++ (gsqrt Nearest_up (c << (2*k)))) (b << k) (n+2)
        in (if s = 1 then x else neg x) >> 2
      end

  let exact_arcsinh a b =
         if a =. 0 then Exact(0,1)
    else if b =. 0 then Invalid "arcsinh"
    else                Irrationnal

  let r_arcsinh = round arcsinh exact_arcsinh
  let e_arcsinh = emake arcsinh

                       (* +------------------------+
                          |  Calcul de argch(a/b)  |
                          +------------------------+ *)

  (* Algorithme :
     si a et b sont nuls ou a/b < 1, erreur
     sinon, changer les signes pour avoir 0 < b <= a

     k <- max(0, n+2-nbits(a))
     a <- a*2^k, b <- b*2^k
     argch(a/b) <- ln((a + sqrt(a^2-b^2))/b)
  *)
  let arccosh a b n =
    let a,b = match sgn(a),sgn(b) with
      |  1,  1 -> a,b
      | -1, -1 -> (neg a),(neg b)
      |  0,  0 -> raise zero_zero
      |  _,  _ -> raise(Error "arccosh")
    in
    let na = nbits(a) in
    match cmp a b with
      |  0 -> zero
      | -1 -> raise (Error "arccosh")
      | _ when n <= min_int+na-2 -> zero
      | _  -> let k = max 0 (n+2-na)   in
              if k >= max_int/2 then raise bit_overflow;
              let c = sqr(a) -- sqr(b) in
        (ln ((a << k) ++ (gsqrt Nearest_up (c << (2*k)))) (b << k) (n+2)) >> 2

  let exact_arccosh a b =
    let c = cmp a b in
           if c = 0       then Exact(0,1)
      else if c <> sgn(b) then Invalid "arccosh"
      else                     Irrationnal

  let r_arccosh = round arccosh exact_arccosh
  let e_arccosh = emake arccosh


                        (* +-----------------------+
                           |  Calcul de Arg(a+ib)  |
                           +-----------------------+ *)

  (* Algorithme :

     si a = b = 0, erreur
     sinon, changer les signes et permuter au besoin pour avoir 0 <= b <= a

       \_            |            _/
         \_ pi/2 + x | pi/2 - x _/
           \_        |        _/
             \_      |      _/
               \_    |    _/    x
      pi - x     \_  |  _/
      _____________\_|_/____________
                  _/ | \_
      x - pi    _/   |   \_
              _/     |     \_   -x
            _/       |       \_
          _/         |         \_
        _/ -x - pi/2 | x - pi/2  \_
      _/             |             \_


     Soient z = a+ib et z~ = a-ib. Comparer b/a aux valeurs suivantes et changer
     z en l'expression associ�e � l'intervalle correspondant

     1. b/a | 0       1/5         5/12         2/3        1 |
        ----+-----------------------------------------------+
            |    z       (5-i)z      (3+2i)z~     (1+i)z~   |
        ----------------------------------------------------+



     2. b/a | 0       1/18        1/11        1/7       1/5 |
        ----+-----------------------------------------------+
            |    z       (18-i)z      (7+i)z~     (5+i)z~   |
        ----------------------------------------------------+



     3. b/a | 0       1/57       1/30        3/79      1/18 |
        ----+-----------------------------------------------+
            |    z       (57-i)z     (79+3i)z~    (18+i)z~  |
        ----------------------------------------------------+



     4. b/a | 0     1/239       1/114       7/524      1/57 |
        ----+-----------------------------------------------+
            |    z      (239-i)z    (524+7i)z~    (57+i)z~  |
        ----------------------------------------------------+

     En sortie, on a 0 <= b/a < 1/217
     Calculer arctan(b/a) avec ps_atan puis reconstituer l'argument original
     par combinaison lin�aire avec arctan(1/2),arctan(1/3),arctan(1/5) :

     arg(  1 +  i) =   a2 +  a3
     arg(  3 + 2i) =   a2 +  a3 -  a5
     arg(  5 +  i) =               a5
     arg(  7 +  i) =   a2 -  a3
     arg( 18 +  i) =  -a2 +  a3 +  a5
     arg( 79 + 3i) = -2a2 + 3a3
     arg( 57 +  i) =   a2 - 2a3 +  a5
     arg(524 + 7i) =  2a2 -  a3 - 3a5
     arg(239 +  i) =  -a2 -  a3 + 4a5

     a2, a3, a5 sont calcul�s � partir d'arc-tangentes plus petits :

     [ arctan(1/2) ]   [ 26  49  7 ]   [ arctan(1149/125243) ]
     [ arctan(1/3) ] = [ 18  34  5 ] * [ arctan(1/239)       ]
     [ arctan(1/5) ]   [ 11  21  3 ]   [ arctan(369/128467)  ]

  *)

  let arg =

    (* puissances de 2+i et de 5+i (simplifi�es par le pgcd des parties
       r�elles et imaginaires). On n'a pas besoin des puissances de 3+i
       car on utilisera la relation a2 + a3 = pi/4
    *)
    let pow2 = [| (1,0);     (2,1);     (3,4);      (2,11);      (-7,24);
                  (-38,41);  (-117,44); (-278,-29); (-527,-336); (-718,-1199);
                  (-237,-3116) |]
    and pow5 = [| (1,0);     (5,1);     (12,5);     (55,37);     (119,120);
                  (475,719); (828,2035);(2105,11003) |]
    in

    (* m�morisation des arc-tangentes de base *)
    let x0 = of_int 1149 and x1 = of_int 125243
    and y0 = of_int    1 and y1 = of_int    239
    and z0 = of_int  369 and z1 = of_int 128467
    in

    let ax = cache (ps_atan true x0 x1) "11629310050144803718654399912" 100
    and ay = cache (ps_atan true y0 y1)  "5303946455430554113486419863" 100
    and az = cache (ps_atan true z0 z1)  "3641104603484748823493444480" 100
    in


      (* calcul de arg(a+ib) *)
      fun a b n ->

        (* on a besoin de n+3 bits en interne *)
        if n >= max_int - 3 then raise bit_overflow;

        if (a =.0) & (b=.0) then raise zero_zero;

        (* change les signes et permute a,b de fa�on � avoir 0 <= b <= a *)
        (* res = s*arg + p2*a2 + p3*a3 + p5*a5 *)
        let                s,  p2,     p3,     p5,   a, b =
        if b <. 0    then -1,  0,      0,      0,    a, neg(b)
                     else  1,  0,      0,      0,    a, b
        in

        let                s,  p2,     p3,     p5,   a, b =
        if a <. 0    then -s,  p2+4*s, p3+4*s, p5,   neg(a),b
                     else  s,  p2,     p3,     p5,   a, b
        in

        let                s,  p2,     p3,     p5,   a, b =
        if supeq b a then -s,  p2+2*s, p3+2*s, p5,   b, a
                     else  s,  p2,     p3,     p5,   a, b
        in

        (*
          extrait les bits de poids fort de a et b pour effectuer les comparaisons
          suivantes en simple pr�cision. On ne garde que 13 bits pour �viter les
          d�bordements.
        *)

        let na = nbits(a) in
        let aa = int_of(shr a (na-13))
        and bb = int_of(shr b (na-13)) in

        (* res = s*(t*arg + q2*a2 + q3*a3 + q5*a5) + p2*a2 + p3*a3 + p5*a5 *)
        let t,q2,q3,q5 = 1,0,0,0 in

        let (* tableau 1 *)      t, q2,     q3,     q5,   aa,        bb =
        if  12*bb >  5*aa  then 
        if   3*bb >  2*aa  then -t, q2+t,   q3+t,   q5,   aa+bb,     aa-bb
                           else -t, q2+t,   q3+t,   q5-t, 3*aa+2*bb, 2*aa-3*bb
        else
        if   5*bb >    aa  then  t, q2,     q3,     q5+t, 5*aa+bb,   5*bb-aa
                           else  t, q2,     q3,     q5,   aa,        bb
        in

        let (* tableau 2 *)      t, q2,     q3,     q5,   aa,        bb =
        if  11*bb >    aa  then 
        if   7*bb >    aa  then -t, q2,     q3,     q5+t, 5*aa+bb,   aa-5*bb
                           else -t, q2+t,   q3-t,   q5,   7*aa+bb,   aa-7*bb
        else
        if  18*bb >    aa  then  t, q2-t,   q3+t,   q5+t, 18*aa+bb,  18*bb-aa
                           else  t, q2,     q3,     q5,   aa,        bb
        in

        let (* tableau 3 *)      t, q2,     q3,     q5,   aa,        bb =
        if  30*bb >    aa  then 
        if  79*bb >  3*aa  then -t, q2-t,   q3+t,   q5+t, 18*aa+bb,  aa-18*bb
                           else -t, q2-2*t, q3+3*t, q5,   79*aa+3*bb,3*aa-79*bb
        else
        if  57*bb >    aa  then  t, q2+t,   q3-2*t, q5+t, 57*aa+bb,  57*bb-aa
                           else  t, q2,     q3,     q5,   aa,        bb
        in

        let (* tableau 4 *)      t, q2,     q3,     q5 =
        if bb > aa/114     then 
        if bb > (7*aa)/524 then -t, q2+t,   q3-2*t, q5+t
                           else -t, q2+2*t, q3-t,   q5-3*t
        else
        if bb > aa/239     then  t, q2-t,   q3-t,   q5+4*t
                           else  t, q2,     q3,     q5
        in

        (* combine les multiplicateurs *) 
        let u2,v2 = pow2.(Pervasives.abs(q2-q3))
        and u5,v5 = pow5.(Pervasives.abs q5)         in
        let v2    = if q2 >= q3 then v2 else -v2
        and v5    = if q5 >=  0 then v5 else -v5     in
        let u,v   = (u2*u5 - v2*v5), (u2*v5 + v2*u5) in
        let u,v   = match q3 land 7 with
          | 0 ->  u,    v
          | 1 ->  u-v,  u+v
          | 2 -> -v,    u
          | 3 ->  u+v,  v-u
          | 4 -> -u,   -v
          | 5 ->  v-u, -v-u
          | 6 ->  v,   -u
          | _ -> -u-v,  u-v
        in

        let a,b = match s with
          | 1 -> a*.u ++ b*.v, b*.u -- a*.v
          | _ -> a*.u ++ b*.v, a*.v -- b*.u
        in

        let p2 = p2 + s*q2 and p3 = p3 + s*q3 and p5 = p5 + s*q5 in
        let px = 26*p2 +  18*p3 +  11*p5
        and py = 49*p2 +  34*p3 +  21*p5
        and pz =  7*p2 +   5*p3 +   3*p5                         in
        let l  = n+3                                             in
        let at = ps_atan true b a l                              in
          (at ++ (ax px l) ++ (ay py l) ++ (az pz l)) >> (l-n)

  let exact_arg a b =
    if (a >. 0) & (b =. 0) then Exact(0,1) else Irrationnal

  let r_arg = round arg exact_arg
  let e_arg = emake arg


                (* +--------------------------------------+
                   |  Calcul de arctan(a/b), arccot(a/b)  |
                   +--------------------------------------+ *)


  (* arctan(a/b) = arg(u), arccot(a/b) = arg(v)

      a   b |    u     v
     -------+-------------
      +   + |  b+ia   a+ib
      +   - | -b-ia  -a-ib 
      -   + |  b+ia   a+ib
      -   - | -b-ia  -a-ib
  *) 

  let arctan a b n =
    if b <. 0 then arg (neg b) (neg a) n else arg b a n

  let arccot a b n =
    if b <. 0 then arg (neg a) (neg b) n else arg a b n

  let exact_arctan a b =
    if a =. 0 then Exact(0,1) else Irrationnal

  let exact_arccot a b =
    if (a >. 0) & (b =. 0) then Exact(0,1) else Irrationnal

  let r_arctan = round arctan exact_arctan
  let e_arctan = emake arctan
  let r_arccot = round arccot exact_arccot
  let e_arccot = emake arccot


                (* +--------------------------------------+
                   |  Calcul de arcsin(a/b), arccos(a/b)  |
                   +--------------------------------------+ *)

  (* Algorithme :

     Si |a| > |b| ou b = 0, erreur
     Sinon changer les signes au besoin pour avoir 0 < b <= |a|.

     k <- max(0, n+3 + nbits(a) - nbits(b^2))
     c <- ceil(sqrt((b^2 - a^2)*4^k))
     arcsin(a/b) <- arg(c, a*2^k)
  *)

  let arcsin a b n =
    let a,b = match sgn(b) with
      |  1 -> a,b
      | -1 -> (neg a),(neg b)
      |  _ -> raise (if a =. 0 then zero_zero else Error "arcsin")
    in

    if n < 0 then zero
    else if n >= max_int/2 -1 then raise bit_overflow
    else begin
      let b2 = sqr(b)       in
      let c2 = b2 -- sqr(a) in
      if c2 <. 0 then raise (Error "arcsin");
      let k = max 0 (n+3 + nbits(a) - nbits(b2)) in
      (arg (gsqrt Ceil(c2 << (2*k))) (a<<k) (n+2)) >> 2
    end

  let exact_arcsin a b =
         if a =. 0              then Exact(0,1)
    else if sup (abs a) (abs b) then Invalid "arcsin"
    else                             Irrationnal

  let r_arcsin = round arcsin exact_arcsin
  let e_arcsin = emake arcsin

  let arccos a b n =
    let a,b = match sgn(b) with
      |  1 -> a,b
      | -1 -> (neg a),(neg b)
      |  _ -> raise (if a =. 0 then zero_zero else Error "arccos")
    in
    let b2 = sqr(b)       in
    let c2 = b2 -- sqr(a) in
    if c2 <. 0 then raise (Error "arccos")
    else if n <= -2 then zero
    else if n >= max_int/2 -1 then raise bit_overflow
    else let k = max 0 (n+3 + nbits(a) - nbits(b2))
         in (arg (a<<k) (gsqrt Ceil(c2 << (2*k))) (n+2)) >> 2

  let exact_arccos a b =
         if eq a b              then Exact(0,1)
    else if sup (abs a) (abs b) then Invalid "arccos"
    else                             Irrationnal

  let r_arccos = round arccos exact_arccos
  let e_arccos = emake arccos


                 (* +-------------------------------------+
                    |  Sommation de la s�rie enti�re exp  |
                    +-------------------------------------+ *)

  (* Entr�e :
     a,b = entiers, |a| <= |b| 
     n = nb de bits d�sir�s

     Sortie : approx(2^n * (exp(a/b) - 1))

     Algorithme de calcul :
     La somme de la s�rie enti�re est calcul�e terme � terme avec
     regroupement des termes 2 par 2, puis 4 par 4, etc. A un moment
     donn� on dispose de :

     k : on va calculer le terme a^k/(k!*b^k)
     p : nombre de termes d�j� calcul�s
     r : minorant de log_2(k!*b^k/a^k)
     pile : (a0,b0,n0,u0,v0) :: (a1,b1,n1,u1,v1) :: ...

     avec ai = a^(2^i), bi = b^(2^i),
     bi/ai >= 2^ni
     ui/vi = fraction en attente pour le groupe de 2^(i+1) termes.

     La somme des p premiers termes de la s�rie est �gale � :

     ((( u(p0)*a(p1)         )    a(p2)            )              )     a
     ((( ----------- + u(p1) )*----------- + u(p2) )* ... + u(pj) )*-----------
     ((( v(p0)*b(p0)         ) b(p1)*v(p1)         )              ) b(pj)*v(pj)

     avec p = 2^p0 + 2^p1 + ... + 2^pj, p0 < p1 < ... < pj

     Le reste de la s�rie, y compris a^k/(k!*b^k), est major� en valeur absolue
     par 2*a^k/(k!*b^k).

     Si 2*a^k/(k!*b^k) > 1/2^(n+1), on introduit le terme a^k/(k!*b^k) dans la
     pile, et on la r�duit en effectuant les additions des groupes ainsi compl�t�s.

     Sinon, on effectue les additions en instance dans la pile puis on
     tronque la fraction obtenue � n bits.
  *)

  let ps_exp =

    (* na = nbits(a), nb = nbits(b) *)
    let compute a b na nb n =

      (* empile un nouveau terme *)
      let rec empile u v p r = function

        | [] -> (2*r),[(a,b,r,one,one)]

        | [(ai,bi,ni,ui,vi)] ->
            let aj = sqr ai                in
            let bj = sqr bi                in
            let nj = nbits(bj)-nbits(aj)-1 in
            let uj = ui**(v**bi) ++ u**ai  in
            let vj = v**vi                 in
              (r+nj+nbits(vj)-ni-nbits(vi)),[(ai,bi,ni,zero,one); (aj,bj,nj,uj,vj)]

        | (ai,bi,ni,ui,vi)::suite when p land 1 = 1 ->
            let uj = ui**(v**bi) ++ u**ai  in
            let vj = v**vi                 in
            let r,pile = empile uj vj (p/2) (r-ni-nbits(vi)+1) suite in
              r,((ai,bi,ni,zero,one) :: pile)

        | (ai,bi,ni,_,_)::suite -> (r+ni+nbits(v)-1),((ai,bi,ni,u,v)::suite)
      in

      (* effectue les additions restant dans la pile *)
      let rec depile u v p = function
        | [] -> (u,v)

        | (ai,bi,ni,ui,vi)::suite when p land 1 = 1 ->
            depile (ui**v ++ ai**u) ((vi**v)**bi) (p/2) suite

        | _::suite -> depile u v (p/2) suite
      in

      (* lance les calculs *)
      let rec loop k r pile =
        let v = of_int(k) in
          if r+nbits(v)-1 < n+2
          then let r,pile = empile one v (k-1) r pile in loop (k+1) r pile
          else depile zero one (k-1) pile
      in
      let u,v = loop 1 (nb-na-1) [] in ((a**u) // v) n

    in

    (* acc�l�ration : soit b = qa + r avec |r| <= |a|/2. On a

       exp(a/b) = exp(1/q) * exp(-r/qb)

       Le temps de calcul de compute pour a/b est de l'ordre de

                       4*nbits(b) + nbits(a)
       M(n) * ln(n) * -----------------------
                            2*nbits(q)

       o� M(n) est le temps, suppos� quasi-lin�aire, d'une multiplication
       dont le r�sultat tient sur n bits. La d�composition pr�c�dente est
       donc int�ressante si 13*nbits(a) > 8*nbits(b)

       En calculant les deux exponentielles avec n+p bits, on obtient un
       r�sultat correct si p >= 4 et n+p >= 0.
    *)
    let rec accel a b n =
      if (a =. 0) or (n < 0) then zero
      else if n >= max_int - 4 then raise bit_overflow
      else begin
        let na = nbits(a) and nb = nbits(b) in
          if 13*na <= 8*nb then compute a b na nb n
          else begin
            let q,r = gquomod Nearest_up b a        in
            let x = compute one q 1 (nbits q) (n+4) in
            let y = accel (neg r) (b**q) (n+4)      in
              (x ++ y ++ ((x**y) >> (n+4))) >> 4
          end
      end

    in accel

                    (* +------------------------------+
                       |  Calcul de exp(a), a entier  |
                       +------------------------------+ *)

  (* a = entier court, n = nombre de bits demand�s.

     Rmq : 88/61 < log_2(e) < 13/9 donc exp(a) < 2^(13a/9)  si a >= 0
                                        exp(a) < 2^(88a/61) si a < 0

     Algorithme :
     si a =  0, res <- 1
     si a =  1, res <- ps_exp(1)  + 1
     si a = -1, res <- ps_exp(-1) + 1
     si n + 88*a/61 < 0, res <- 0

     dans les autres cas, calculer exp(a) � partir de exp(1) ou exp(-1) par
     exponentiation binaire.

     cas a = 2b, b > 0 :
     calculer exp(b) avec n+p bits, n+2p >= 4 et p >= 13a/18+3

     cas a = 2b, b < 0 :
     calculer exp(b) avec n+p bits, n+2p >= 4 et p >= 44a/61+3

     cas a = 2b+1, b > 0, b = 3 mod 4 :
     calculer exp(2b+2) avec n+p bits et exp(-1) avec n+q bits,
     p >= 3, n+p+q >= 4 et q >= 13(a+1)/9 + 2

     cas a = 2b+1, b > 0, b <> 3 mod 4 :
     calculer exp(2b) avec n+p bits et exp(1) avec n+q bits,
     p >= 5, n+p+q >= 5 et q >= 13(a-1)/9 + 2

     cas a = 2b+1, b < 0, b = 0 mod 8 :
     calculer exp(2b-2) avec n+p bits et exp(1) avec n+q bits,
     p >= 5, n+p+q >= 4 et q >= 88(a-1)/61 + 4

     cas a = 2b+1, b < 0, b <> 0 mod 8 :
     calculer exp(2b+2) avec n+p bits et exp(-1) avec n+q bits,
     p >= 2, n+p+q >= 5 et q >= 88(a+1)/61 + 2

     Dans les cas impair, calculer exp(2b) en premier car cela demande
     en g�n�ral une plus grande pr�cision pour exp(+/-1) que les n+q
     bits n�cessaires, donc on profite de l'effet de cache.
  *)

  let exp_int =

    let f x n = (ps_exp (of_int x) one n) ++ (one << n) in
    let e1  = cache (f(1))  "3445831591435597602840181573642" 100
    and em1 = cache (f(-1))  "466342594412604466517458581545" 100
    in

    let rec loop a n =

      (* cas de base *)
           if a =  0 then one << n
      else if a =  1 then e1  1  n
      else if a = -1 then em1 1  n

      (* pour �viter les d�bordements dans les calculs sur n et a,
         on travaille en multipr�cision *)
      else let aa = of_int a and nn = of_int n in

      if a < 0 then begin

        (* cas e^a < 1/2^n *)
        if nn*.61 ++ aa*.88 <. 0 then zero

        (* cas a pair *)
        else if a land 1 = 0 then begin
          let p = max (ceil (4-n) 2) (int_of (gquo_1 Ceil (aa*.44 +. 183) 61)) in
          if (p > 0) & (n >= max_int - 2*p) then raise bit_overflow;
          sqr(loop (a/2) (n+p)) >> (n+2*p)
        end

        (* cas a impair, b = 0 mod 8 *)
        else if a land 14 = 0 then begin
          let p = 5 in
          if n >= max_int - p then raise bit_overflow;
          let q = max (4-n-p) (int_of (gquo_1 Ceil (aa*.88 +. 156) 61)) in
          let x = loop (a-1) (n+p) in
          let y = e1 1 (n+q)       in
          (x**y) >> (n+p+q)
        end

        (* cas a impair, b <> 0 mod 8 *)
        else begin
          let p = 2 in
          if n >= max_int - p then raise bit_overflow;
          let q = max (5-n-p) (int_of (gquo_1 Ceil (aa*.88 +. 210) 61)) in
          let x = loop (a+1) (n+p) in
          let y = em1 1 (n+q)      in
          (x**y) >> (n+p+q)
        end

      end
      else begin

        (* cas e^a < 1/2^n *)
        if nn*.9 ++ aa*.13 <. 0 then zero

        (* cas a pair *)
        else if a land 1 = 0 then begin
          if n <= min_int + 4 then raise bit_overflow;
          let p = max (ceil (4-n) 2) (int_of (gquo_1 Ceil (aa*.13 +. 54) 18)) in
          if (n >= max_int-p) or (n+p >= max_int-p) then raise bit_overflow;
          sqr(loop (a/2) (n+p)) >> (n+2*p)
        end

        (* cas a impair, b = 3 mod 4 *)
        else if a land 6 = 6 then begin
          let p = 3 in
          if a/9 >= (max_int-4)/13 then raise bit_overflow;
          let q = max (4-n-p) (int_of (gquo_1 Ceil (aa*.13 +. 31) 9)) in
          if n >= max_int-p-q then raise bit_overflow;
          let x = loop (a+1) (n+p) in
          let y = em1 1 (n+q)      in
          (x**y) >> (n+p+q)
        end

        (* cas a impair, b <> 3 mod 4 *)
        else begin
          let p = 5 in
          if a/9 >= (max_int-1)/13 then raise bit_overflow;
          let q = max (5-n-p) (int_of (gquo_1 Ceil (aa*.13 +. 5) 9)) in
          if n >= max_int-p-q then raise bit_overflow;
          let x = loop (a-1) (n+p) in
          let y = e1 1 (n+q)       in
          (x**y) >> (n+p+q)
        end
      end

    in loop


                        (* +----------------------+
                           |  Calcul de exp(a/b)  |
                           +----------------------+ *)

  (* Algorithme :

     si a = b = 0, erreur
     si a < 0 et b = 0, res <- 0

     si a/b < 0 :
     si n + (88/61)*(a/b) < 0, res <- 0
     sinon, soit a = bc + d avec |d| <= b/2
     calculer exp_int(c) avec n+p bits, ps_exp(d/b) avec n+q bits,
     puis tronquer le produit � n bits. Le r�sultat est correct si
     p >= 4, n+p+q >= 5, q >= 88c/61 + 2.

     si a/b > 0 :
     soit a = bc + d avec |d| <= b/2
     calculer exp_int(c) avec n+p bits, ps_exp(d/b) avec n+q bits,
     puis tronquer le produit � n bits. Le r�sultat est correct si
     p >= 4, n+p+q >= 5, q >= 13c/9 + 2.
  *)

  let exp a b n =

      if b =. 0 then match sgn(a) with 
        | 0 -> raise zero_zero
        | 1 -> raise (Error "exp")
        | _ -> zero

      else let a,b = if b >. 0 then a,b else (neg a),(neg b) in

      if a <. 0
      then if a*.88 ++ (b*.61)*.n <. 0 then zero
      else begin

        if n >= max_int - 6 then raise bit_overflow;
        let p = 4                                    in
        let c,d = gquomod Nearest_up a b             in
        let q = max (5-n-p) (int_of (gquo_1 Ceil (c*.88 +. 122) 61)) in
        let x = exp_int (int_of c) (n+p)             in
        let y = ps_exp d b (n+q)                     in
        (x ++ (x << (n+q)) ++ x**y) >> (n+p+q)

      end

      else if a*.13 ++ (b*.9)*.n <. 0 then zero
      else begin

        let c,d = gquomod Nearest_up a b             in
        let q = gquo_1 Ceil (c*.13 +. 18) 9          in
        if q >=. max_int then raise bit_overflow;
        let p = 4                                    in
        let q = max (5-n-p) (int_of q)               in
        if n >= max_int-p-q then raise bit_overflow;
        let x = exp_int (int_of c) (n+p)             in
        let y = ps_exp d b (n+q)                     in
        (x ++ (x << (n+q)) ++ x**y) >> (n+p+q)

      end

  let exact_exp a b =
         if a =. 0              then Exact(1,1)
    else if (a <. 0) & (b =. 0) then Exact(0,1)
    else                             Irrationnal

  let r_exp = round exp exact_exp
  let e_exp = emake exp

                    (* +------------------------------+
                       |  Calcul de ch(a/b), sh(a/b)  |
                       +------------------------------+ *)

  let cosh a b n =
    if n >= max_int-1 then raise bit_overflow;
    ((exp a b (n+1)) ++ (exp (neg a) b (n+1))) >> 2

  let sinh a b n =
    if n >= max_int-1 then raise bit_overflow;
    ((exp a b (n+1)) -- (exp (neg a) b (n+1))) >> 2

  let cosinh a b n =
    if n >= max_int-1 then raise bit_overflow;
    let x = exp a       b (n+1)
    and y = exp (neg a) b (n+1) in (x++y) >> 2, (x--y) >> 2

  let exact_cosinh a b =
    if a =. 0 then Exact2(1,1,0,1) else Irrationnal2

  let exact_cosh a b =
    if a =. 0 then Exact(1,1) else Irrationnal

  let exact_sinh a b =
    if a =. 0 then Exact(0,1) else Irrationnal

  let r_cosinh = round2 cosinh exact_cosinh
  let e_cosinh = emake  cosinh
  let r_cosh   = round  cosh   exact_cosh
  let e_cosh   = emake  cosh
  let r_sinh   = round  sinh   exact_sinh
  let e_sinh   = emake  sinh


                         (* +---------------------+
                            |  Calcul de th(a/b)  |
                            +---------------------+ *)

  (* Algorithme :

     si a = b = 0, erreur sinon si b=0, th(a/b) <- sgn(a)
     si n < 0, th(a/b) <- 0

     sinon, changer les signes au besoin pour avoir a >= 0, b > 0
     th(a/b) = (1 - exp(-2a/b)) / (1 + exp(-2a/b))

     Calculer exp(-2a/b) avec n+p bits, le quotient arrondi est correct si
     p >= 2.
  *)
  let tanh a b n =

    if b =. 0 then begin
      if a =. 0 then raise zero_zero
      else of_int(sgn a) << n
    end
    else if (a =. 0) or (n < 0) then zero

    else begin

      let s,a,b = match sgn(a),sgn(b) with
        | -1, -1 -> false, (neg a), (neg b)
        | -1,  _ -> true,  (neg a), b
        |  _, -1 -> true,  a,       (neg b)
        |  _,  _ -> false, a,       b
      in

      if n >= max_int-2 then raise bit_overflow;
      let x = exp (a*.(-2)) b (n+2) in
      let y = one << (n+2)          in
      ((if s then x--y else y--x) // (x ++ y)) n

    end

  let exact_tanh a b =
         if a =. 0 then Exact(0,1)
    else if b =. 0 then Exact(sgn(a),1)
    else                Irrationnal

  let r_tanh = round tanh exact_tanh
  let e_tanh = emake tanh


                        (* +-----------------------+
                           |  Calcul de coth(a/b)  |
                           +-----------------------+ *)


  (* Algorithme :

     si a = 0, erreur sinon si b = 0, coth(a/b) <- sgn(a)

     sinon, changer les signes au besoin pour avoir a > 0, b > 0
     coth(a/b) = (1 + exp(-2a/b)) / (1 - exp(-2a/b))

     Calculer exp(-2a/b) avec p bits, le quotient arrondi est correct si
     p - n + 2k >= 4 et p + k >= 3, avec k = min(0, nbits(a) - nbits(b))

  *)
  let coth a b n =

    if a =. 0 then raise (if b =. 0 then zero_zero else Error "coth");
    if b =. 0 then of_int(sgn a) << n

    else begin

      let s,a,b = match sgn(a),sgn(b) with
        | -1, -1 -> false, (neg a), (neg b)
        | -1,  _ -> true,  (neg a), b
        |  _, -1 -> true,  a,       (neg b)
        |  _,  _ -> false, a,       b
      in

      let k = min 0 (nbits(a) - nbits(b)) in
      let q = 4 - 2*k                     in
      if n >= max_int - q then raise bit_overflow;
      let p = max (3 - k) (n + q)         in
      let x = exp (a*.(-2)) b p           in
      let y = one << p                    in
      let u = x ++ y                      in
      let v = if s then x--y else y--x    in (u // v) n
                                            
    end

  let exact_coth a b =
         if a =. 0 then Invalid "coth"
    else if b =. 0 then Exact(sgn(a),1)
    else                Irrationnal

  let r_coth = round coth exact_coth
  let e_coth = emake coth


              (* +------------------------------------------+
                 |  Sommation de la s�rie enti�re exp(i*x)  |
                 +------------------------------------------+ *)

  (* Algorithme identique � ps_exp, adapt� au cas complexe. Changer u en u + it
     dans les explications. *)

  let ps_cosin =

    (* na = nbits(a), nb = nbits(b) *)
    let compute a b na nb n =

      (* empile un nouveau terme *)
      let rec empile u t v first p r = function

        | [] -> (2*r),[(a,b,r,one,zero,one)]

        | [(ai,bi,ni,ui,ti,vi)] ->
            let aj = if first then neg(sqr ai) else sqr ai in
            let bj = sqr bi                in
            let nj = nbits(bj)-nbits(aj)-1 in
            let vbi= v**bi                 in
            let uj = ui**vbi ++ u**ai      in
            let tj = ti**vbi ++ t**ai      in
            let vj = v**vi                 in
              (r+nj+nbits(vj)-ni-nbits(vi)),
              [(ai,bi,ni,zero,zero,one); (aj,bj,nj,uj,tj,vj)]

        | (ai,bi,ni,ui,ti,vi)::suite when p land 1 = 1 ->
            let vbi= v**bi                 in
            let uj = ui**vbi ++ u**ai      in
            let tj = ti**vbi ++ t**ai      in
            let vj = v**vi                 in
            let r,pile = empile uj tj vj false (p/2) (r-ni-nbits(vi)+1) suite in
              r,((ai,bi,ni,zero,zero,one) :: pile)

        | (ai,bi,ni,_,_,_)::suite -> if first
          then (r+ni+nbits(v)-1),((ai,bi,ni,t,u,v)::suite)
          else (r+ni+nbits(v)-1),((ai,bi,ni,u,t,v)::suite)
      in

      (* effectue les additions restant dans la pile *)
      let rec depile u t v p = function
        | [] -> (neg(t),u,v)

        | (ai,bi,ni,ui,ti,vi)::suite when p land 1 = 1 ->
            depile (ui**v ++ ai**u) (ti**v ++ ai**t) ((vi**v)**bi) (p/2) suite

        | _::suite -> depile u t v (p/2) suite
      in

      (* lance les calculs *)
      let rec loop k r pile =
        let v = of_int(k) in
          if r+nbits(v)-1 < n+2
          then let r,pile = empile zero one v true (k-1) r pile in loop (k+1) r pile
          else depile zero zero one (k-1) pile
      in

      let u,t,v = loop 1 (nb-na-1) [] in (((a**u) // v) n), (((a**t) // v) n)
    in

    (* acc�l�ration : le temps de calcul de compute est de l'ordre de
       
                       7*nbits(b) + 2*nbits(a)
       M(n) * ln(n) * -------------------------
                            2*nbits(q)


       donc l'acc�l�ration est payante quand 23*nbits(a) > 14*nbits(b).
       Prendre p >= 3, n+p >= 4 pour les exponentielles interm�diaires.
    *)

    let rec accel a b n =
      if (a =. 0) or (n < 0) then (zero,zero)
      else begin
        let na = nbits(a) and nb = nbits(b) in
          if 23*na <= 14*nb then compute a b na nb n
          else begin
            let q,r = gquomod Nearest_up b a            in
            let p = max 3 (4-n)                         in
            if n >= max_int - p then raise bit_overflow;
            let x1,x2 = compute one q 1 (nbits q) (n+p) in
            let y1,y2 = accel (neg r) (b**q) (n+p)      in
              (x1 ++ y1 ++ ((x1**y1 -- x2**y2) >> (n+p))) >> p,
              (x2 ++ y2 ++ ((x1**y2 ++ x2**y1) >> (n+p))) >> p
          end
      end

    in accel

                 (* +------------------------------------+
                    |  Calcul de exp(ia), a entier long  |
                    +------------------------------------+ *)

  (* Algorithme :
     si a =  0, res <- 1
     si n <= 0, res <- 0
     si a =  1, res <- ps_cosin(1) + 1
     si a < 0,  res <- conj(exp(-ia))

     dans les autres cas, calculer exp(ia) � partir de exp(i) par
     exponentiation binaire.

     cas a = 2b :
     calculer exp(ib) avec n+3 bits

     cas a = 2b+1 :
     calculer exp(ib) avec n+3 bits et exp(i) avec n+4 bits.
  *)

  let cosin_int =

    let c1,s1 = cache2 (fun n -> let c,s = ps_cosin one one n in c ++ (one << n),s)
                   "684914542338443705542701930871"
                  "1066691198966369310635496214684" 100 in

    (* k = rang du bit de a � examiner
       u+iv = 2^(n+3(na-k))*exp(i*(a / 2^k)) *)
    let rec loop a k u v n =

      if k < 0 then u,v

      else if nth_bit a k then begin
        let u,v = sqr(u)--sqr(v), (u**v)*.2  in
        let c,s = (c1 1 (n+1)), (s1 1 (n+1)) in
        loop a (k-1) ((c**u--s**v)>>(2*n+4)) ((c**v++s**u)>>(2*n+4)) (n-3)
      end

      else begin
        let u,v = sqr(u)--sqr(v), (u**v)*.2  in
        loop a (k-1) (u>>(n+3)) (v>>(n+3)) (n-3)
      end

    in

      (* lance les calculs *)
      fun a n ->
             if a =. 0 then (one << n),zero
        else if n <= 0 then zero,zero
        else begin
          let na = nbits(a)     in
          if n >= max_int/2 + 1 - 3*na then raise bit_overflow;
          let n =  n + 3*na - 3 in
          let u,v = loop a (na-2) (c1 1 n) (s1 1 n) n in
          if a >. 0 then u,v else u,neg(v)
        end

                        (* +-----------------------+
                           |  Calcul de exp(ia/b)  |
                           +-----------------------+ *)

  let cosin a b n =

         if b =. 0 then raise(if a =. 0 then zero_zero else Error "cosin")
    else if a =. 0 then one << n, zero
    else if n <= 0 then zero, zero
    else begin
      if n >= max_int - 6 then raise bit_overflow;
      let q,r = gquomod Nearest_up a b in
      let c,s = cosin_int q   (n+3)    in
      let u,v = ps_cosin  r b (n+3)    in
      let u   = u ++ (one << (n+3))    in
        (c**u -- s**v)>>(n+6), (c**v ++ s**u)>>(n+6)
    end

  let cos a b n =

         if b =. 0 then raise(if a =. 0 then zero_zero else Error "cos")
    else if a =. 0 then one << n
    else if n <= 0 then zero
    else begin
      if n >= max_int - 6 then raise bit_overflow;
      let q,r = gquomod Nearest_up a b in
      let c,s = cosin_int q   (n+3)    in
      let u,v = ps_cosin  r b (n+3)    in
      let u   = u ++ (one << (n+3))    in
        (c**u -- s**v)>>(n+6)
    end

  let sin a b n =

         if b =. 0 then raise(if a =. 0 then zero_zero else Error "sin")
    else if a =. 0 then zero
    else if n <= 0 then zero
    else begin
      if n >= max_int - 6 then raise bit_overflow;
      let q,r = gquomod Nearest_up a b in
      let c,s = cosin_int q   (n+3)    in
      let u,v = ps_cosin  r b (n+3)    in
      let u   = u ++ (one << (n+3))    in
        (c**v ++ s**u)>>(n+6)
    end

  let exact_cosin a b =
    if a =. 0 then Exact2(1,1,0,1) else Irrationnal2

  let exact_cos a b =
    if a =. 0 then Exact(1,1) else Irrationnal

  let exact_sin a b =
    if a =. 0 then Exact(0,1) else Irrationnal

  let r_cosin = round2 cosin exact_cosin
  let e_cosin = emake  cosin
  let r_cos   = round  cos   exact_cos
  let e_cos   = emake  cos
  let r_sin   = round  sin   exact_sin
  let e_sin   = emake  sin

                  (* +----------------------------------+
                     |  Calcul de tan(a/b), cotan(a/b)  |
                     +----------------------------------+ *)

  (* Algorithme pour tan(a/b) :

     1. p <- 5
     2. x+iy <- approx(2^(n+p) * exp(ia/b))
     3. tant que |x| < 8 : p <- 2*p, recalculer x,y
     4. soit r tel que 2^r < |x| <= 2^(r+1)
        k <- max( n+3-r, 2n+4+p-2r )
        tant que k > 0 : p <- p+k, recalculer x,y,r,k
     5. res <- approx(2^n*x/y)

     Pour cotan(a/b) : idem en permutant x,y
  *)

  let tan,cot =

    let rec loop f n p =
      if n >= (max_int - p - 4)/2 then raise bit_overflow;
      let x,y = f (n+p) in
      let nx = nbits(x) in
      if nx < 4 then begin
        if p >= max_int/2 then raise bit_overflow;
        loop f n (2*p)
      end
      else begin
        let r = let _,t = split x (nx-1) in if t =. 0 then nx-2 else nx-1 in
        let k = max (n+3-r) (2*n+4+p-2*r) in
        if k > 0 then loop f n (p+k) else (y // x) n
      end
    in

    (* tan *)
    (fun a b n ->
       match sgn(a),sgn(b) with
         | 0, 0 -> raise zero_zero
         | _, 0 -> raise (Error "tan")
         | 0, _ -> zero
         | _, _ -> loop (cosin a b) n 5),

    (* cot *)
    (fun a b n ->
       match sgn(a),sgn(b) with
         | 0, 0 -> raise zero_zero
         | _, 0 -> raise (Error "cot")
         | 0, _ -> raise (Error "cot")
         | _, _ -> loop (fun m -> let x,y = cosin a b m in y,x) n 5)
        
  let exact_tan a b =
         if a =. 0 then Exact(0,1)
    else if b =. 0 then Invalid "tan"
    else                Irrationnal

  let exact_cot a b =
    if (a =. 0) or (b =. 0) then Invalid "cot" else Irrationnal

  let r_tan = round tan exact_tan
  let e_tan = emake tan
  let r_cot = round cot exact_cot
  let e_cot = emake cot


                    (* +------------------------------+
                       |  Acc�s externe � round_loop  |
                       +------------------------------+ *)

  let round f r c = round_loop f r c (nbits(c) + 5) 5

end
