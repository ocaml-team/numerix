(* file kernel/ocaml/ml/toplevelcfg.ml: Ocaml toplevel configuration
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                        Configuration du toplevel                      |
 |                                                                       |
 +-----------------------------------------------------------------------*)

open Printf

(* code extrait de mlgmp/install_pp.ml (David.Monniaux@ens-lyon.fr) *)
(* The following code is a hack! *)
type t =
    Lident of string
  | Ldot of t * string
  | Lapply of t * t;;

let install_printer modul nom =
  Topdirs.dir_install_printer Format.std_formatter
  (Obj.magic (Ldot(Ldot(Lident "Numerix", modul), nom)));;

let _ =
  Topdirs.dir_directory "_directory_";
  let modules_list = _Mode_list_ [] in
  printf "ocamlnumx : Ocaml toplevel with big integer libraries\nNumerix submodules :";
  List.iter (fun x -> printf " %s" x) modules_list;
  printf "\nNumerix version    : _numerix_version_\n\n";
  let tlip x = install_printer x "toplevel_print";
               install_printer x "toplevel_print_tref"
  in List.iter tlip modules_list
;;
