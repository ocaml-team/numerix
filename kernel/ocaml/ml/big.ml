(* file kernel/ocaml/ml/big.ml: Objective-Caml bignum interface
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                       D�finition du module Big                        |
 |                                                                       |
 +-----------------------------------------------------------------------*)

                           (* +----------------+
                              |   module Big   |
                              +----------------+ *)

module Big = struct
  open Big_int

  type t     = big_int
  type tref  = t ref
  let name() = String.copy "Big"
  let zero   = zero_big_int
  let one    = unit_big_int
  let two    = big_int_of_int 2
  exception Error of string

  (* gestion des r�f�rences *)
  let make_ref   a = ref a
  let copy_in  r a = r := a
  let copy_out r   = !r
  let look     r   = !r

  (* addition/soustraction/multiplication *)
  let abs        a   =      abs_big_int      a
  let neg        a   =      minus_big_int    a
  let add        a b =      add_big_int      a b
  let add_1      a b =      add_int_big_int  b a
  let sub        a b =      sub_big_int      a b
  let sub_1      a b =      sub_big_int      a (big_int_of_int b)
  let mul        a b =      mult_big_int     a b
  let mul_1      a b =      mult_int_big_int b a
  let sqr        a   =      square_big_int   a
  let abs_in   r a   = r := abs_big_int      a
  let neg_in   r a   = r := minus_big_int    a
  let add_in   r a b = r := add_big_int      a b
  let add_1_in r a b = r := add_int_big_int  b a
  let sub_in   r a b = r := sub_big_int      a b
  let sub_1_in r a b = r := sub_big_int      a (big_int_of_int b)
  let mul_in   r a b = r := mult_big_int     a b
  let mul_1_in r a b = r := mult_int_big_int b a
  let sqr_in   r a   = r := square_big_int   a

  (* division euclidienne *)
  let quomod     a b = if sign_big_int(b) > 0
                       then quomod_big_int a b
                       else let a1 = minus_big_int a
                            and b1 = minus_big_int b in
                            let q,r = quomod_big_int a1 b1
                            in (q,minus_big_int r)

  let quo        a b = let q,_ = quomod a b in q
  let modulo     a b = let _,r = quomod a b in r

  let gquomod m  a b = match m with

    | Floor        -> quomod a b

    | Ceil         -> let q,r = quomod a b in if sign_big_int(r) = 0
                      then q,r
                      else (succ_big_int q),(sub_big_int r b)

    | Nearest_up   -> let a1 = add_big_int a (add_big_int a b)
                      and b1 = add_big_int b b in
                      let q,r = quomod a1 b1 in
                      (q, div_big_int (sub_big_int r b) two)

    | Nearest_down -> let a1 = add_big_int a (add_big_int a b)
                      and b1 = add_big_int b b in
                      let q,r = quomod a1 b1 in if sign_big_int(r) = 0
                      then (pred_big_int q, div_big_int (minus_big_int b) two)
                      else (q, div_big_int (sub_big_int r b) two)

  let gquo       m     a b = let q,_ = gquomod m a b in q
  let gmod       m     a b = let _,r = gquomod m a b in r
  let quomod_in    r s a b = let x,y = quomod    a b in r := x; s := y
  let quo_in       r   a b = let x,_ = quomod    a b in r := x
  let mod_in         s a b = let _,y = quomod    a b in s := y
  let gquomod_in m r s a b = let x,y = gquomod m a b in r := x; s := y
  let gquo_in    m r   a b = let x,_ = gquomod m a b in r := x
  let gmod_in    m   s a b = let _,y = gquomod m a b in s := y

  let quomod_1         a b = let q,r = quomod    a (big_int_of_int b) in q,     (int_of_big_int r)
  let quo_1            a b = let q,_ = quomod    a (big_int_of_int b) in q
  let mod_1            a b = let _,r = quomod    a (big_int_of_int b) in        (int_of_big_int r)
  let gquomod_1      m a b = let q,r = gquomod m a (big_int_of_int b) in q,     (int_of_big_int r)
  let gquo_1         m a b = let q,_ = gquomod m a (big_int_of_int b) in q
  let gmod_1         m a b = let _,r = gquomod m a (big_int_of_int b) in         int_of_big_int r
  let quomod_1_in    r a b = let x,y = quomod    a (big_int_of_int b) in r := x; int_of_big_int y
  let quo_1_in       r a b = let x,y = quomod    a (big_int_of_int b) in r := x
  let gquomod_1_in m r a b = let x,y = gquomod m a (big_int_of_int b) in r := x; int_of_big_int y
  let gquo_1_in    m r a b = let x,y = gquomod m a (big_int_of_int b) in r := x

  (* repr�sentation binaire *)
  let words_in_digit = Nat.length_of_digit/16
  let nat_of b = (Obj.obj(Obj.field (Obj.repr b) 1) : Nat.nat)

  let nth_word x n =
    if n < 0 then 0
    else let n1 = n/words_in_digit
         and n2 = 16*(n mod words_in_digit) in
         if n1 >= num_digits_big_int x then 0
         else begin
           let tmp = Nat.create_nat 2 in
           Nat.blit_nat tmp 1 (nat_of x) n1 1;
           Nat.shift_right_nat tmp 1 1 tmp 0 n2;
           (Nat.nth_digit_nat tmp 1) land 0xffff
         end

  let masque = [| 0x0001; 0x0002; 0x0004; 0x0008;
                  0x0010; 0x0020; 0x0040; 0x0080;
                  0x0100; 0x0200; 0x0400; 0x0800;
                  0x1000; 0x2000; 0x4000; 0x8000 |]
  let nth_bit n k =
    (k >= 0) && ((nth_word n (k/16)) land masque.(k land 15) <> 0)

 let nbits x =
    let a = nat_of x             in
    let l = num_digits_big_int x in
    l * Nat.length_of_digit - (Nat.num_leading_zero_bits_in_digit a (l-1))

  let lowbits x = (Nat.nth_digit_nat (nat_of x) 0) land 0x7fffffff

  let highbits x =
    let a = nat_of x
    and l = num_digits_big_int x in
    let m = Nat.length_of_digit - (Nat.num_leading_zero_bits_in_digit a (l-1)) in
    let tmp = Nat.create_nat 2 in
    if l < 2 then begin
      Nat.blit_nat tmp 1 a (l-1) 1;
      Nat.set_digit_nat tmp 0 0;
    end
    else Nat.blit_nat tmp 0 a (l-2) 2;
    if m < 31 then Nat.shift_left_nat  tmp 0 2 tmp 0 (31-m)
              else Nat.shift_right_nat tmp 1 1 tmp 0 (m-31);
    Nat.nth_digit_nat tmp 1

  (* d�calages *)
  let shl a b =
    if (sign_big_int(a) = 0) or (b <= -nbits(a)) then zero_big_int
    else if b = 0 then a
    else if b > 0 then mult_big_int a (power_int_positive_int 2 b)
    else if sign_big_int(a) > 0 then div_big_int a (power_int_positive_int 2 (-b))
    else minus_big_int(div_big_int (minus_big_int a) (power_int_positive_int 2 (-b)))

  let shr a b =
    if (sign_big_int(a) = 0) or (b >= nbits(a)) then zero_big_int
    else if b = 0 then a
    else if b < 0 then mult_big_int a (power_int_positive_int 2 (-b))
    else if sign_big_int(a) > 0 then div_big_int a (power_int_positive_int 2 b)
    else minus_big_int(div_big_int (minus_big_int a) (power_int_positive_int 2 b))

  let split a b =
    if b < 0 then raise (Error "negative index")
    else if (b = 0) or (sign_big_int(a) = 0) then a,zero_big_int
    else if b >= nbits(a)                    then zero_big_int,a
    else let q,r = quomod_big_int (abs_big_int a) (power_int_positive_int 2 b) in
         if sign_big_int(a) > 0 then q,r else minus_big_int(q),minus_big_int(r)

  let join a b c =
    if c < 0 then raise (Error "negative index") else add a (shl b c)

  let shl_in   r   a b = r := shl a b
  let shr_in   r   a b = r := shr a b
  let split_in r s a b = let x,y = split a b in r := x; s := y
  let join_in  r a b c = r := join a b c

  (* exponentiation *)

  (*let pow        a b =      power_big_int_positive_int a b *)
  (* bogu� : 416577169024505710327332685500 ^ 45 -> ...442939392 *)

  let pow a b =
    if b < 0 then raise (Error "negative exponent");
    let rec loop n =
      if n = 0 then unit_big_int
      else let x = square_big_int(loop (n/2)) in
           if n land 1 = 0 then x else mult_big_int a x
    in loop b

  let pow_in   r a b = r := pow a b
  let pow_1      a b =      power_int_positive_int     a b
  let pow_1_in r a b = r := power_int_positive_int     a b

  let gpowmod m a b c =
    if sign_big_int(b) < 0 then raise (Error "negative exponent");
    let r = ref(unit_big_int) in
    for i=nbits(b)-1 downto 0 do
      r := mod_big_int (square_big_int !r) c;
      if nth_bit b i then r := mod_big_int (mult_big_int !r a) c;
    done;
    gmod m !r c

  let powmod         a b c =      gpowmod Floor a b c
  let powmod_in    r a b c = r := gpowmod Floor a b c
  let gpowmod_in m r a b c = r := gpowmod m     a b c
        
  (* racine carr�e *)
  let sqrt    a = sqrt_big_int a
  let gsqrt m a = match m with

    | Floor -> sqrt_big_int a

    | Ceil  -> let b = sqrt_big_int a in
               let c = sub_big_int a (square_big_int b) in
               if sign_big_int(c) = 0 then b else succ_big_int(b)

    | _     -> let a1 = mult_int_big_int 4 a in
               let b1 = sqrt_big_int a1 in
               let b,r = quomod_big_int b1 two in
               if sign_big_int(r) = 0 then b else succ_big_int b

  let sqrt_in    r a = r := sqrt_big_int a
  let gsqrt_in m r a = r := gsqrt m a

  (* racine p-�me *)
  let root a p =

    let p1 = p-1 in

    (* it�ration de Newton : cas a > 0 *)
    let rec loop_positif a x =
      let y = pow x p1  in
      let z = sub_big_int a (mult_big_int x y) in
      if sign_big_int(z) >= 0 then x
      else loop_positif a (add_big_int x (div_big_int z (mult_int_big_int p y)))

    (* it�ration de Newton : cas a < 0 *)
    and loop_negatif a x =
      let y = pow x p1  in
      let z = sub_big_int a (mult_big_int x y) in
      match sign_big_int(z) with
        | 0  -> x
        | -1 -> pred_big_int x
        | _  -> let y = mult_int_big_int p y
                in loop_negatif a (add_big_int x (quo (add_big_int z (pred_big_int y)) y))
    in

    (* racine r�cursive *)
    let rec root_rec a =

      (* valeur initiale : puissance de 2 si a est petit, *)
      (* sinon racine p-�me de la partie haute            *)
      let init =
        let n = nbits(a)  in
        let q = n/(2*p)   in
        if q < 2 then shl (big_int_of_int (sign_big_int a)) ((n+p1)/p)
                 else shl (add_int_big_int (sign_big_int a) (root_rec (shr a (p*q)))) q
      in
      (if sign_big_int(a) >= 0 then loop_positif else loop_negatif) a init
    in

    (* contr�le de signe *)
    if p <= 0                                   then raise (Error "negative exponent");
    if ((p mod 2) = 0) && (sign_big_int(a) < 0) then raise (Error "negative base");
    if (sign_big_int(a) = 0) then zero_big_int else root_rec a

  let groot m a p = match m with
    | Floor -> root a p
    | Ceil  -> let r = root a p in
               let b = sub_big_int a (pow r p) in
                 if sign_big_int(b) = 0 then r else succ_big_int r
    | _     -> let r = root (shl a p) p in div_big_int (succ_big_int r) two

  let root_in    r a p = r := root    a p
  let groot_in m r a p = r := groot m a p

  (* factorielle *)
  let fact a = 
    if a < 0 then raise (Error "negative base")
    else begin
      let r = ref(unit_big_int) in
      for i=2 to a do r := mul !r (big_int_of_int i) done;
      !r
    end
  let fact_in r a = r := fact a

  (* pgcd *)
  let gcd a b = gcd_big_int a b

  let rec gcd_ex a b = match sign_big_int(a) with

    | 0 -> if sign_big_int(b) >= 0
              then b,zero_big_int,big_int_of_int(-1)
              else minus_big_int(b),zero_big_int,unit_big_int

    | 1 -> let q,r = quomod b a      in
           let d,u1,v1 = gcd_ex r a  in
           let u = minus_big_int(u1) in d,(sub_big_int (mult_big_int q u) v1),u

    |_  -> let d,u,v = gcd_ex (minus_big_int a) b in d,(minus_big_int u),v

  let cfrac a b =
    if (sign_big_int(a) = 0) & (sign_big_int(b) = 0)
    then zero_big_int,unit_big_int,zero_big_int,unit_big_int,zero_big_int
    else let d,u,v = gcd_ex a b in d,u,v,(div_big_int a d),(div_big_int b d)

  let gcd_in    d         a b = d := gcd_big_int a b
  let gcd_ex_in d u v     a b = let     x,y,z = gcd_ex a b in d := x; u := y; v := z
  let cfrac_in  d u v p q a b = let r,s,x,y,z = cfrac  a b in d := r; u := s; v := x; p := y; q := z

  (* comparaisons *)
  let sgn     a   = sign_big_int a
  let cmp     a b = compare_big_int a b
  let eq      a b = eq_big_int      a b
  let neq     a b = not(eq_big_int  a b)
  let inf     a b = lt_big_int      a b
  let infeq   a b = le_big_int      a b
  let sup     a b = gt_big_int      a b
  let supeq   a b = ge_big_int      a b
  let cmp_1   a b = compare_big_int a (big_int_of_int b)
  let eq_1    a b = eq_big_int      a (big_int_of_int b)
  let neq_1   a b = not(eq_big_int  a (big_int_of_int b))
  let inf_1   a b = lt_big_int      a (big_int_of_int b)
  let infeq_1 a b = le_big_int      a (big_int_of_int b)
  let sup_1   a b = gt_big_int      a (big_int_of_int b)
  let supeq_1 a b = ge_big_int      a (big_int_of_int b)

  (* conversions *)
  let of_int      a =      big_int_of_int a
  let of_int_in r a = r := big_int_of_int a

  let of_string ch =

    let l = String.length(ch)
    and i = ref(0) in

    (* d�code le signe *)
    let s =      if (l > 0) & ch.[0] = '-' then (incr i; -1)
            else if (l > 0) & ch.[0] = '+' then (incr i;  1) else 1 in

    (* d�code la base *)
    let b = if (l > !i+1) & (ch.[!i] = '0') then match ch.[!i+1] with
            | 'x' | 'X' -> i := !i + 2; 16
            | 'o' | 'O' -> i := !i + 2;  8
            | 'b' | 'B' -> i := !i + 2;  2
            | _         -> incr i;      10
            else 10 in

    (* s'il n'y a plus de caract�res disponibles, cha�ne invalide *)
    if !i >= l then raise (Error "invalid string");

    (* d�code le reste de la cha�ne (algorithme en n^2) *)
    let r = ref(zero_big_int) in
    while !i < l do
      let c = match ch.[!i] with
      | '0'..'9' as x -> Char.code(x)-48
      | 'a'..'f' as x -> Char.code(x)-87
      | 'A'..'F' as x -> Char.code(x)-55
      | _             -> raise (Error "invalid string")
      in
      if c > b then raise (Error "invalid string");
      r := add_int_big_int c (mult_int_big_int b !r);
      incr i
    done;

    (* r�sultat *)
    if s = 1 then !r else minus_big_int !r

  let of_string_in r ch = r := of_string ch

  let int_of a    = int_of_big_int a
  let string_of a = string_of_big_int a

  let bstring_of a =
    if sign_big_int(a) = 0 then "0"
    else begin
      let n = nbits(a)
      and s = if sign_big_int(a) < 0 then 1 else 0 in
      let r = String.create (n+2+s) in
        if (s=1) then r.[0] <- '-';
        r.[s]   <- '0';
        r.[s+1] <- 'b';
        for i=0 to n-1 do r.[n+1+s-i] <- if nth_bit a i then '1' else '0' done;
        r
    end

  let hstring_of a =
    if sign_big_int(a) = 0 then "0"
    else begin
      let n = (nbits(a)+3)/4
      and s = if sign_big_int(a) < 0 then 1 else 0 in
      let r = String.create (n+2+s) in
        if (s=1) then r.[0] <- '-';
        r.[s]   <- '0';
        r.[s+1] <- 'x';
        for i=0 to n-1 do
          let c = ((nth_word a (i/4)) lsr (4*(i land 3))) land 15 in
          r.[n+1+s-i] <- if c < 10 then Char.chr(48+c) else Char.chr(55+c)
        done;
        r
    end

  let ostring_of a =
    if sign_big_int(a) = 0 then "0"
    else begin
      let n = (nbits(a)+2)/3
      and s = if sign_big_int(a) < 0 then 1 else 0 in
      let r = String.create (n+2+s) in
        if (s=1) then r.[0] <- '-';
        r.[s]   <- '0';
        r.[s+1] <- 'o';
        for i=0 to n-1 do
          let c = (if nth_bit a (3*i)   then 1 else 0)
                + (if nth_bit a (3*i+1) then 1 else 0)*2
                + (if nth_bit a (3*i+2) then 1 else 0)*4 in
          r.[n+1+s-i] <- Char.chr(48+c)
        done;
        r
    end

  (* nombres al�atoires *)
  let nrandom n =
    if n < 0 then raise (Error "negative size")
    else if n = 0 then zero_big_int
    else begin
      let p = if n land 15 = 0 then 16 else n land 15 in
      let mask = (1 lsl p) - 1 in
      let r = ref(big_int_of_int(Random.bits() land mask)) in
      for i=1 to (n-p)/16 do
        r := add_int_big_int (Random.bits() land 0xffff) (mult_int_big_int 0x10000 !r)
      done;
      !r
    end

  let nrandom1 n =
    if n < 0 then raise (Error "negative size")
    else if n = 0 then zero_big_int
    else begin
      let p = if n land 15 = 0 then 16 else n land 15 in
      let mask = 1 lsl (p-1) in
      let r = ref(big_int_of_int((Random.bits() land (mask-1)) lor mask)) in
      for i=1 to (n-p)/16 do
        r := add_int_big_int (Random.bits() land 0xffff) (mult_int_big_int 0x10000 !r)
      done;
      !r
    end

  let zrandom n = match Random.bits() land 1 with
    | 0 -> nrandom n
    | _ -> minus_big_int(nrandom n)

  let zrandom1 n = match Random.bits() land 1 with
    | 0 -> nrandom1 n
    | _ -> minus_big_int(nrandom1 n)

  let nrandom_in  r n = r := nrandom  n
  let zrandom_in  r n = r := zrandom  n
  let nrandom1_in r n = r := nrandom1 n
  let zrandom1_in r n = r := zrandom1 n

  let random_init x = if x = 0 then Random.self_init() else Random.init(x)

  (* affichage tronqu� � 1000 caract�res *)
  let toplevel_print(a) =
    let s = string_of a      in
    let l = String.length(s) in
    if l < 1000 then Format.print_string s
    else begin
      Format.print_string (String.sub s 0 100);
      Format.print_string " ... (";
      Format.print_int    (l-200);
      Format.print_string " digits) ... ";
      Format.print_string (String.sub s (l-100) 100)
    end

  let toplevel_print_tref(a) =
    Format.print_string "tref(";
    toplevel_print(look a);
    Format.print_char ')'

                         (* +---------------------+
                            |  Test de primalit�  |
                            +---------------------+ *)

  (* n >= 0, cherche s'il a un diviseur non trivial <= 2^10 *)
  let trivial_prime n =

    let rec loop d =
      if inf_1 n (d*d)      then True
      else if d > 1024      then Unknown
      else if mod_1 n d = 0 then False
      else loop(d+2)
    in

    match cmp_1 n 2 with
    |  1 when nth_bit n 0 -> loop 3
    |  0 -> True
    |  _ -> False

  (* test de Rabin-Miller pour les bases 2,3,5,7,11,13,17.
     on suppose n >= 0 et trivial_prime(n) = Unknown
     (ie. n >= 2^20 et n n'a pas de facteur premier <= 2^10)

     Ribenboim, The Little Book of Bigger Primes, ch. VII p. 98 :

     Given k >= 1, denote by tk the smallest integer which is a strong pseudoprime
     for the bases p1 = 2, p2 = 3, . . . , pk, simultaneously. Then the
     calculations of Pomerance, Selfridge & Wagstaff (1980), extended by
     Jaeschke (1993), provide the following values:

     t2 =                1 373 653 = 829 � 1657,
     t3 =               25 326 001 = 2251 � 11251,
     t4 =            3 215 031 751 = 151 � 751 � 28351,
     t5 =        2 152 302 898 747 = 6763 � 10627 � 29947,
     t6 =        3 474 749 660 383 = 1303 � 16927 � 157543,
     t7 = t8 = 341 550 071 728 321 = 10670053 � 32010157.

     Compl�ment (http://primes.utm.edu/prove/prove2_3.html) : le premier
     contre-exemple suivant t4 est

     t4' = 118 670 087 467 = 172243 � 688969.

     On utilisera cette borne car t4 est reconnu non premier par trivial_prime.
     Le m�me argument s'applique � t2, avec

     t2' = 2 284 453 = 1069 � 2137.

     Le premier nombre sans facteur trivial spp(2) est

     t1' = 1 194 649 = 1093^2.

     Quelques contre-exemples (th�se de Fran�ois Arnault,
     http://www.unilim.fr/pages_perso/francois.arnault/documents/these.ps):

     1195068768795265792518361315725116351898245581
     = 24444516448431392447461 � 48889032896862784894921
     est spp(2,..,31)

     80383745745363949125707961434194210813883768828755\
     81458374889175222974273765333652186502336163960045\
     45791504202360320876656996676098728404396540823292\
     87387918508691668573282677617710293896977394701670\
     82304286871099974399765441448453411558724506334092\
     79022275296229414984230688168540432645753401832978\
     6111298960644845216191652872597534901

     = 20047910831974980270915322604227342650259408302\
     05662543872531023690016085350598121358111595798609\
     86679108158254267908348457261690695858464376399022\
     2898400226296015918301

     � 400958216639499605418306452084546853005188166041\
     13250877450620473800321707011962427162231915972197\
     33582163165085358166969145233813917169287527980445\
     796800452592031836601

     est spp(2,..,199)

     Ces deux nombres sont reconnus non premiers par small_prime en comparant les
     racines carr�es de -1 obtenues aux cours des tests. De m�me, tous les spp
     donn�s dans la th�se de Fran�ois Arnault page 29 sont reconnus non premiers
     par le m�me crit�re � l'exception de

     n = 340770619658554299579514119954792724101734599955221
     = 13053172404794060245721341 � 26106344809588120491442681

     qui est spp(2,..,31). On aurait trouv� une deuxi�me racine carr�e
     de -1 si on avait pouss� le test jusqu'� la base 23.
  *)

  let miller_rabin =

    let tests = [of_int  2, of_string         "1194649";
                 of_int  3, of_string         "2284453";
		 of_int  5, of_string        "25326001";
		 of_int  7, of_string    "118670087467";
		 of_int 11, of_string   "2152302898747";
		 of_int 13, of_string   "3474749660383";
		 of_int 17, of_string "341550071728321"] in

    fun n ->

      (* d�compose n-1 = q*2^k, q impair *)
      let q,k =
	let rec loop k = if nth_bit n k then (shr n k),k else loop (k+1)
	in loop 1
      in

      (* racine de -1 potentielle *)
      let i = make_ref(zero) in

      (* �l�ve a au carr� au plus k-2 fois jusqu'� trouver une racine carr�e de -1 *)
      let rec sqr_loop a k =
	if k <= 1 then None
	else let b = gmod Nearest_up (sqr a) n in
	     if eq_1 b (-1) then Some(if sgn a > 0 then a else neg a)
	     else sqr_loop b (k-1)
      in

      (* test de Rabin-Miller pour la base a *)
      let mr_test a =
	let b = gpowmod Nearest_up a q n in
	(eq_1 b 1) or (eq_1 b (-1)) or match sqr_loop b k with
	| Some(x) when eq_1 (look i) 0 -> copy_in i x; true
	| Some(x) -> eq (look i) x
	| None    -> false
      in

      (* passe en revue toutes les bases jusqu'� obtenir une certitude *)
      let rec loop l = match l with
      | [] -> Unknown
      | (a,t)::suite -> match cmp t n with
	  | 0 -> False
	  | 1 -> if mr_test a then True else False
	  | _ -> if mr_test a then loop suite else False
      in loop tests

  (* test de primalit� *)
  let isprime n =
    let n = if inf_1 n 0 then neg n else n in
    match trivial_prime n with Unknown -> miller_rabin n | r -> r

  let isprime_1 n = isprime(of_int n)

end
