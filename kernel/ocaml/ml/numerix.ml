(* file kernel/ocaml/ml/numerix.ml: Objective-Caml extensible integer definitions
 +-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                           D�finitions                                 |
 |                                                                       |
 +-----------------------------------------------------------------------*)

(* mode d'arrondi *)
type round_mode = Floor | Nearest_up | Ceil | Nearest_down

(* r�sultat ternaire *)
type tristate = False | Unknown | True

module type Int_type = sig

  type t                     (* entier         *)
  type tref                  (* entier mutable *)

  val name : unit -> string  (* nom du module  *)

  val zero : t               (* the number 0   *)
  val one  : t               (* the number 1   *)

  (* r�f�rence      ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val make_ref    :                               t ->               tref   
  val copy_in     :               tref ->         t ->               unit   
  val copy_out    :               tref ->                            t      
  val look        :               tref ->                            t      
  
  (* addition       ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val add         :                               t -> t   ->        t      
  val add_1       :                               t -> int ->        t      
  val add_in      :               tref ->         t -> t   ->        unit   
  val add_1_in    :               tref ->         t -> int ->        unit   
  
  (* soustraction   ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val sub         :                               t -> t   ->        t      
  val sub_1       :                               t -> int ->        t      
  val sub_in      :               tref ->         t -> t   ->        unit   
  val sub_1_in    :               tref ->         t -> int ->        unit   
  
  (* multiplication ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val mul         :                               t -> t   ->        t      
  val mul_1       :                               t -> int ->        t      
  val mul_in      :               tref ->         t -> t   ->        unit   
  val mul_1_in    :               tref ->         t -> int ->        unit   
  
  (* division       ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val quomod      :                               t -> t   ->        t*t    
  val quo         :                               t -> t   ->        t      
  val modulo      :                               t -> t   ->        t      
  val gquomod     : round_mode ->                 t -> t   ->        t*t    
  val gquo        : round_mode ->                 t -> t   ->        t      
  val gmod        : round_mode ->                 t -> t   ->        t      
  
  val quomod_in   :               tref -> tref -> t -> t   ->        unit   
  val quo_in      :               tref ->         t -> t   ->        unit   
  val mod_in      :                       tref -> t -> t   ->        unit   
  val gquomod_in  : round_mode -> tref -> tref -> t -> t   ->        unit   
  val gquo_in     : round_mode -> tref ->         t -> t   ->        unit   
  val gmod_in     : round_mode ->         tref -> t -> t   ->        unit   
  
  val quomod_1    :                               t -> int ->        t*int  
  val quo_1       :                               t -> int ->        t      
  val mod_1       :                               t -> int ->        int    
  val gquomod_1   : round_mode ->                 t -> int ->        t*int  
  val gquo_1      : round_mode ->                 t -> int ->        t      
  val gmod_1      : round_mode ->                 t -> int ->        int    
  
  val quomod_1_in :               tref ->         t -> int ->        int    
  val quo_1_in    :               tref ->         t -> int ->        unit   
  val gquomod_1_in: round_mode -> tref ->         t -> int ->        int    
  val gquo_1_in   : round_mode -> tref ->         t -> int ->        unit   
  
  (* valeur absolue ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val abs         :                               t ->               t      
  val abs_in      :               tref ->         t ->               unit   
  
  (* oppos�         ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val neg         :                               t ->               t      
  val neg_in      :               tref ->         t ->               unit   
  
  (* puissance p-�me----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val sqr         :                               t ->               t      
  val pow         :                               t -> int ->        t      
  val pow_1       :                             int -> int ->        t      
  val powmod      :                               t -> t   -> t ->   t      
  val gpowmod     : round_mode ->                 t -> t   -> t ->   t      
  val sqr_in      :               tref ->         t ->               unit   
  val pow_in      :               tref ->         t -> int ->        unit   
  val pow_1_in    :               tref ->       int -> int ->        unit   
  val powmod_in   :               tref ->         t -> t   -> t ->   unit   
  val gpowmod_in  : round_mode -> tref ->         t -> t   -> t ->   unit   
  
  (* racine p-�me    ----------------------------------------------------- *)
  (*                 mode          r       s       a    b      c      res  *)
  val sqrt        :                               t ->               t      
  val root        :                               t -> int ->        t      
  val gsqrt       : round_mode ->                 t ->               t      
  val groot       : round_mode ->                 t -> int ->        t      
  val sqrt_in     :               tref ->         t ->               unit   
  val root_in     :               tref ->         t -> int ->        unit   
  val gsqrt_in    : round_mode -> tref ->         t ->               unit   
  val groot_in    : round_mode -> tref ->         t -> int ->        unit   
  
  (* factorielle    ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val fact        :                             int ->               t      
  val fact_in     :               tref ->       int ->               unit   
  
  (* pgcd           ----------------------------------------------------- *)
  (*                  d     u     v     p    q    a    b      c      res  *)
  val gcd         :                               t -> t   ->        t      
  val gcd_ex      :                               t -> t   ->        t*t*t  
  val cfrac       :                               t -> t   ->     t*t*t*t*t 
  val gcd_in      : tref->                        t -> t   ->        unit   
  val gcd_ex_in   : tref->tref->tref->            t -> t   ->        unit   
  val cfrac_in    : tref->tref->tref->tref->tref->t -> t   ->        unit   
  
  (* primalit�      ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val isprime     :                               t ->               tristate
  val isprime_1   :                             int ->               tristate

  (* comparaison    ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val sgn         :                               t ->               int    
  val cmp         :                               t -> t   ->        int    
  val cmp_1       :                               t -> int ->        int    
  val eq          :                               t -> t   ->        bool   
  val eq_1        :                               t -> int ->        bool   
  val neq         :                               t -> t   ->        bool   
  val neq_1       :                               t -> int ->        bool   
  val inf         :                               t -> t   ->        bool   
  val inf_1       :                               t -> int ->        bool   
  val infeq       :                               t -> t   ->        bool   
  val infeq_1     :                               t -> int ->        bool   
  val sup         :                               t -> t   ->        bool   
  val sup_1       :                               t -> int ->        bool   
  val supeq       :                               t -> t   ->        bool   
  val supeq_1     :                               t -> int ->        bool   
  
  (* conversion     ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val of_int      :                             int ->               t      
  val of_string   :                          string ->               t      
  val of_int_in   :               tref ->       int ->               unit   
  val of_string_in:               tref ->    string ->               unit   
  val int_of      :                               t ->               int    
  val string_of   :                               t ->               string 
  val bstring_of  :                               t ->               string 
  val hstring_of  :                               t ->               string 
  val ostring_of  :                               t ->               string 
  
  (* nombre al�atoire --------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val nrandom     :                             int->                t      
  val zrandom     :                             int->                t      
  val nrandom1    :                             int->                t      
  val zrandom1    :                             int->                t      
  val nrandom_in  :               tref ->       int->                unit   
  val zrandom_in  :               tref ->       int->                unit   
  val nrandom1_in :               tref ->       int->                unit   
  val zrandom1_in :               tref ->       int->                unit   
  val random_init :                             int->                unit   
  
  (* repr�sentation binaire --------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val nbits       :                               t ->               int    
  val lowbits     :                               t ->               int    
  val highbits    :                               t ->               int    
  val nth_word    :                               t -> int ->        int    
  val nth_bit     :                               t -> int ->        bool   
  
  (* d�calage       ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val shl         :                               t -> int ->        t      
  val shr         :                               t -> int ->        t      
  val split       :                               t -> int ->        t*t    
  val join        :                               t -> t   -> int -> t      
  val shl_in      :               tref ->         t -> int ->        unit   
  val shr_in      :               tref ->         t -> int ->        unit   
  val split_in    :               tref -> tref -> t -> int ->        unit   
  val join_in     :               tref ->         t -> t   -> int -> unit   
  
  (* affichage      ----------------------------------------------------- *)
  (*                mode          r       s       a    b      c      res  *)
  val toplevel_print   :                               t ->               unit
  val toplevel_print_tref:             tref ->                            unit
  
  (* exceptions *)
  exception Error of string

end

(* notation infixe *)
module Infixes(E : Int_type) = struct
  open E

  let (  ++  ) = add      
  let (  --  ) = sub      
  let (  **  ) = mul      
  let (  //  ) = quo      
  let (  %%  ) = modulo   
  let (  /%  ) = quomod   
  let (  <<  ) = shl      
  let (  >>  ) = shr      
  let (  ^^  ) = pow      

  let (  +=  ) r x = add_in r (look r) x
  let (  -=  ) r x = sub_in r (look r) x
  let (  *=  ) r x = mul_in r (look r) x
  let (  /=  ) r x = quo_in r (look r) x
  let (  %=  ) r x = mod_in r (look r) x
                
  let (  +.  ) = add_1    
  let (  -.  ) = sub_1    
  let (  *.  ) = mul_1    
  let (  /.  ) = quo_1    
  let (  %.  ) = mod_1    
  let (  /%. ) = quomod_1
  let (  ^.  ) = pow_1      
                
  let (  +=. ) r x = add_1_in r (look r) x
  let (  -=. ) r x = sub_1_in r (look r) x
  let (  *=. ) r x = mul_1_in r (look r) x
  let (  /=. ) r x = quo_1_in r (look r) x

  let (   =. ) = eq_1     
  let (  <>. ) = neq_1    
  let (   <. ) = inf_1    
  let (  <=. ) = infeq_1  
  let (   >. ) = sup_1    
  let (  >=. ) = supeq_1  

  let (  ~~  ) = look     

end

(* affiche en secondes le temps du processus, la diff�rence avec le temps *)
(* pr�c�dent et la chaine pass�e en argument.                             *)
external chrono : string -> unit = "chrono"

