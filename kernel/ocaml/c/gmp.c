// file kernel/ocaml/c/gmp.c: Interface with Gmp
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                   Entiers extensibles, interface Gmp                  |
 |                                                                       |
 +-----------------------------------------------------------------------*/

/* headers standard */
#include <string.h>
#include <stdio.h>
#include <time.h>

/* header Gmp */
#include <gmp.h>

/* headers Ocaml */
#include <mlvalues.h>
#include <custom.h>
#include <alloc.h>
#include <fail.h>
#include <callback.h>
#if 0
#include <memory.h>
#else
/* remplacement de <memory.h> pour supprimer le contr�le de type
   sur local_roots (sinon on a des warnings d�sagr�ables avec les
   macros xx_push_roots) */

extern void *local_roots;
extern void  modify(value *, value);
extern value alloc_shr(mlsize_t, tag_t);
extern value caml_check_urgent_gc (value);
#endif
#include <intext.h>

/* mode d'arrondi */
#define Round_val Long_val

/* valeur 3 �tats */
#define Val_tri Val_long

/* Un entier est vu depuis Ocaml comme une donn�e abstraite
   avec customisation */
typedef struct {
  void *custom_ops;
  mpz_t val;
} *gint;

                      /* +--------------------------+
                         |  Op�rations polymorphes  |
                         +--------------------------+ */

void gx_finalize(value a){
  mpz_clear(((gint)a)->val);
}

int gx_compare(value a, value b) {
    return mpz_cmp(((gint)a)->val,((gint)b)->val);
}

extern unsigned long hash(char *k, long length, long initval);
long gx_hash(value v) {
    __mpz_struct *a = (__mpz_struct *)&(((gint)v)->val);
    return hash((char *)(a->_mp_d), abs(a->_mp_size)*sizeof(mp_limb_t), a->_mp_size);
}

/* signe, longueur en chiffres de 16 bits, chiffres poids faible en t�te */
void gx_serialize(value x, unsigned long *w32, unsigned long *w64) {
  __mpz_struct *a = (__mpz_struct *)(((gint)x)->val);
  mp_limb_t *b,c=0;
  long l = abs(a->_mp_size), lw, i;

  /* signe */
  serialize_int_1((a->_mp_size < 0) ? -1 : 0);

  /* longueur en nombre de mots de 16 bits */
  lw = l*(sizeof(mp_limb_t)*8);
  if (lw) for (lw-=sizeof(mp_limb_t)*8, c=a->_mp_d[l-1]; c; c>>=1, lw++);
  lw = (lw + 15)/16;
  if (lw >= 0x10000000) failwith("number too big for serialization");
  serialize_int_4(lw);

  /* liste des chiffres par tranche de 16 bits */
  for (i=0, b=a->_mp_d; i<lw; i++) {
    if ((i%(sizeof(mp_limb_t)/2)) == 0) c = *(b++);
    serialize_int_2(c&0xffff);
    c >>= 16;
  }

  /* nombre d'octets �mis */
  *w32 = *w64 = 2*lw + 5;
}

/* d�s�rialisation */
unsigned long gx_deserialize(void *x) {
  __mpz_struct *a = (__mpz_struct *)x;
  mp_limb_t *b,c=0;
  long i,lw,s;
  int count;

  /* signe */
  s = (deserialize_uint_1()) ? -1 : 1;

  /* longueur en nombre de mots de 16 bits */
  lw = deserialize_uint_4();

  /* constitue l'en-t�te */
  mpz_init2(a,16*lw);
  a->_mp_size = s*((lw + (sizeof(mp_limb_t)/2) - 1)/(sizeof(mp_limb_t)/2));

  /* r�cup�re les chiffres par tranches de 16 bits */
  for (i=0, count=0, b=a->_mp_d; i<lw; i++) {
    c += deserialize_uint_2() << count;
    count += 16;
    if (count == 8*sizeof(mp_limb_t)) {*(b++) = c; c = 0; count = 0;}
  }
  if (count) *b = c;

  /* nombre d'octets lus */
  return(2*lw + 5);
}

struct custom_operations gx_ops = {
  "Numerix Gmp Integer 0.21",/* identifier  */
  gx_finalize,               /* finalize    */
  gx_compare,                /* compare     */
  gx_hash,                   /* hash        */
  gx_serialize,              /* serialise   */
  gx_deserialize             /* deserialise */
};

value gx_register() {
  static int unregistered = 1;
  if (unregistered) {
    register_custom_operations(&gx_ops);
    unregistered = 0;
  }
  return Val_unit;
}

                            /* +--------------+
                               |  allocation  |
                               +--------------+ */

/*
  gx_alloc_1_j(x,y1,..,yj) alloue x (gint) en pr�servant y1,..,yj.

  gx_alloc_n_i_j(r,n,x1,..,xi,y1,..,yj) alloue r (n-uplet) et x1,..,xi (gint)
  en pr�servant y1,..,yj.

  gx_alloc_string_1(s,n,y) alloue s (cha�ne de n caract�res) en pr�servant y

*/

#define gx_alloc_1_0(x) do {                                         \
    x = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);               \
    mpz_init(x->val);                                                \
} while(0)

#define gx_alloc_1_1(x,y1) do {                                      \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&y1) y1;}                                             \
    __lr = {local_roots,1,1,&y1};                                    \
    local_roots = &__lr;                                             \
    x = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);               \
    mpz_init(x->val);                                                \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_1_2(x,y1,y2) do {                                   \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&y1) y1;                                              \
        typeof(&y2) y2;}                                             \
    __lr = {local_roots,2,1,&y1,&y2};                                \
    local_roots = &__lr;                                             \
    x = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);               \
    mpz_init(x->val);                                                \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_1_3(x,y1,y2,y3) do {                                \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&y1) y1;                                              \
        typeof(&y2) y2;                                              \
        typeof(&y3) y3;}                                             \
    __lr = {local_roots,3,1,&y1,&y2,&y3};                            \
    local_roots = &__lr;                                             \
    x = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);               \
    mpz_init(x->val);                                                \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_n_1_2(r,n,x1,y1,y2) do {                            \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&x1) x1;                                              \
        typeof(&y1) y1;                                              \
        typeof(&y2) y2;}                                             \
    __lr = {local_roots,3,1,&x1,&y1,&y2};                            \
    local_roots = &__lr;                                             \
    x1 = (gint)Val_unit;                                             \
    x1 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x1->val);                                               \
    r = alloc_tuple(n);                                              \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_n_1_1(r,n,x1,y1) do {                               \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&x1) x1;                                              \
        typeof(&y1) y1;}                                             \
    __lr = {local_roots,2,1,&x1,&y1};                                \
    local_roots = &__lr;                                             \
    x1 = (gint)Val_unit;                                             \
    x1 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x1->val);                                               \
    r = alloc_tuple(n);                                              \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_n_2_1(r,n,x1,x2,y1) do {                            \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&x1) x1;                                              \
        typeof(&x2) x2;                                              \
        typeof(&y1) y1;}                                             \
    __lr = {local_roots,3,1,&x1,&x2,&y1};                            \
    local_roots = &__lr;                                             \
    x1 = (gint)Val_unit;                                             \
    x2 = (gint)Val_unit;                                             \
    x1 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x1->val);                                               \
    x2 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x2->val);                                               \
    r = alloc_tuple(n);                                              \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_n_2_2(r,n,x1,x2,y1,y2) do {                         \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&x1) x1;                                              \
        typeof(&x2) x2;                                              \
        typeof(&y1) y1;                                              \
        typeof(&y2) y2;}                                             \
    __lr = {local_roots,4,1,&x1,&x2,&y1,&y2};                        \
    local_roots = &__lr;                                             \
    x1 = (gint)Val_unit;                                             \
    x2 = (gint)Val_unit;                                             \
    x1 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x1->val);                                               \
    x2 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x2->val);                                               \
    r = alloc_tuple(n);                                              \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_n_3_2(r,n,x1,x2,x3,y1,y2) do {                      \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&x1) x1;                                              \
        typeof(&x2) x2;                                              \
        typeof(&x3) x3;                                              \
        typeof(&y1) y1;                                              \
        typeof(&y2) y2;}                                             \
    __lr = {local_roots,5,1,&x1,&x2,&x3,&y1,&y2};                    \
    local_roots = &__lr;                                             \
    x1 = (gint)Val_unit;                                             \
    x2 = (gint)Val_unit;                                             \
    x3 = (gint)Val_unit;                                             \
    x1 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x1->val);                                               \
    x2 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x2->val);                                               \
    x3 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x3->val);                                               \
    r = alloc_tuple(n);                                              \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_n_5_2(r,n,x1,x2,x3,x4,x5,y1,y2) do {                \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&x1) x1;                                              \
        typeof(&x2) x2;                                              \
        typeof(&x3) x3;                                              \
        typeof(&x4) x4;                                              \
        typeof(&x5) x5;                                              \
        typeof(&y1) y1;                                              \
        typeof(&y2) y2;}                                             \
    __lr = {local_roots,5,1,&x1,&x2,&x3,&x4,&x5,&y1,&y2};            \
    local_roots = &__lr;                                             \
    x1 = (gint)Val_unit;                                             \
    x2 = (gint)Val_unit;                                             \
    x3 = (gint)Val_unit;                                             \
    x4 = (gint)Val_unit;                                             \
    x5 = (gint)Val_unit;                                             \
    x1 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x1->val);                                               \
    x2 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x2->val);                                               \
    x3 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x3->val);                                               \
    x4 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x4->val);                                               \
    x5 = (gint)alloc_custom(&gx_ops,sizeof(mpz_t),0,1);              \
    mpz_init(x5->val);                                               \
    r = alloc_tuple(n);                                              \
    local_roots = __lr.next;                                         \
} while(0)

#define gx_alloc_string_1(s,n,y) do {                                \
    struct {                                                         \
        void *next;                                                  \
        long __nroots;                                               \
        long __dummy;                                                \
        typeof(&y) y;}                                               \
    __lr = {local_roots,1,1,&y};                                     \
    local_roots = &__lr;                                             \
    s = (char *)alloc_string(n);                                     \
    local_roots = __lr.next;                                         \
} while(0)

                            /* +--------------+
                               |  exceptions  |
                               +--------------+ */

extern inline void gx_failwith(char *msg) __attribute__((noreturn));
extern inline void gx_failwith(char *msg) {
  value *id = caml_named_value("gx_error");
  if (id) raise_with_string(*id, msg+16); /* +16 = saute "Numerix kernel: " */
  else failwith(msg);
}

/* code commun Caml-Ocaml */
#include "../../x/c/gmp.c"
