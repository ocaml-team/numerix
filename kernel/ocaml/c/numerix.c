// file kernel/ocaml/c/numerix.c: Ocaml extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                   Entiers extensibles, interface Ocaml                |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include "../h/numerix.h"

#include "../../x/c/add.c"
#include "../../x/c/cmp.c"
#include "../../x/c/convert.c"
#include "../../x/c/copy.c"
#include "../../x/c/div.c"
#include "../../x/c/dump.c"
#include "../../x/c/fact.c"
#include "../../x/c/gcd.c"
#include "../../x/c/mul.c"
#include "../../x/c/pow.c"
#include "../../x/c/powmod.c"
#include "../../x/c/prime.c"
#include "../../x/c/random.c"
#include "../../x/c/root.c"
#include "../../x/c/shift.c"
#include "../../x/c/sqrt.c"
#include "../../x/c/string.c"


                            /* +---------------+
                               |  Comparaison  |
                               +---------------+ */
/*
  entr�e :
  a,b = entiers extensibles

  sortie :
  1 si a > b, 0 si a = b, -1 si a < b
*/
int xx(custom_cmp)(value v1, value v2) {
#define a ((xint)v1)
#define b ((xint)v2)
  long la = xx_lg(a), lb = xx_lg(b), sa = xx_sgn(a), sb = xx_sgn(b);

  if (sa) {
    if (sb) return(-xn(cmp)(a->val,la,b->val,lb));
    else return(-1);
  }
  else {
    if (sb) return(1);
    else return(xn(cmp)(a->val,la,b->val,lb));
  }
#undef a
#undef b
}


                              /* +-----------+
                                 |  Hachage  |
                                 +-----------+ */
/*
  entr�e :
  a = entier extensible

  sortie :
  cl� de hachage de a
*/
extern unsigned long hash(char *k, long length, long initval);
long xx(hash)(value v1) {
#define a ((xint)v1)
    return hash((char *)(a->val), xx_lg(a)*(HW/8), a->hd);
#undef a
}

                           /* +-----------------+
                              |  S�rialisation  |
                              +-----------------+ */

#include <intext.h>

/* signe, longueur en chiffres de 16 bits, chiffres poids faible en t�te */
void xx(serialize)(value x, unsigned long *w32, unsigned long *w64) {
  xint a = (xint)x;
  chiffre *b,c=0;
  long l = xx_lg(a), lw, i;

  /* signe */
  serialize_int_1((xx_sgn(a)) ? -1 : 0);

  /* longueur en nombre de mots de 16 bits */
  lw = l*HW;
  if (lw) for (lw-=HW, c=a->val[l-1]; c; c>>=1, lw++);
  lw = (lw + 15)/16;
#ifdef bits_64
  if (lw >= 0x10000000) failwith("number too big for serialization");
#endif
  serialize_int_4(lw);

  /* liste des chiffres par tranche de 16 bits */
  for (i=0, b=a->val; i<lw; i++) {
    if ((i%(HW/16)) == 0) c = *(b++);
    serialize_int_2(c&0xffff);
    c >>= 16;
  }

  /* nombre d'octets �mis */
  *w32 = *w64 = 2*lw + 5;
}

                          /* +-------------------+
                             |  D�s�rialisation  |
                             +-------------------+ */

unsigned long xx(deserialize)(void *x) {
  xint a = (xint)((long *)x-1);
  chiffre *b,c=0;
  long i,lw,s;
  int count;

  /* signe */
  s = (deserialize_uint_1()) ? SIGN_m : 0;

  /* longueur en nombre de mots de 16 bits */
  lw = deserialize_uint_4();

  /* constitue l'en-t�te */
  a->hd = ((lw + HW/16 - 1)/(HW/16)) | s;

  /* r�cup�re les chiffres par tranches de 16 bits */
  for (i=0, count=0, b=a->val; i<lw; i++) {
    c += deserialize_uint_2() << count;
    count += 16;
    if (count == HW) {*(b++) = c; c = 0; count = 0;}
  }
  if (count) *b = c;

  /* nombre d'octets lus */
  return(2*lw + 5);
}

                            /* +--------------+
                               |  lib�ration  |
                               +--------------+ */

#ifdef debug_alloc

/* marques pour tester les d�bordements */
#if defined(bits_32)
#define magic_1 3141592653UL
#elif defined(bits_64)
#define magic_1 3141592653589793238UL
#endif
#if HW == 16
#define magic_2 27182
#elif HW == 32
#define magic_2 2718281828UL
#elif HW == 64
#define magic_2 2718281828459045235UL
#endif

/* compteur d'allocation */
static long xalloc = 0;
long xx(get_alloc_count)() {return(xalloc);}

/* d�sallocation d'une valeur */
void xx(finalize)(value v) {
    xint x = (xint)v;

    if (xalloc) xalloc--;
    else xx(internal_error)("unexpected call to xx(finalize)",0);

    if (   (xx_lg(x) <= xx_capacity(x))
           && (x->m1 == magic_1)
           && (x->val[xx_capacity(x)] == magic_2)) {
        return;
    }

    xx(internal_error)("buffer overflow (x layer)",0);
}
#endif

              /* +------------------------------------------+
                 |  descripteur des op�rations polymorphes  |
                 +------------------------------------------+ */

struct custom_operations xx(custom_bloc) = {
#if defined(use_slong)
  "Numerix Slong Integer 0.21",/* identifier  */
#elif defined(use_dlong)
  "Numerix Dlong Integer 0.21",
#elif defined(use_clong)
  "Numerix Clong Integer 0.21",
#endif
#ifdef debug_alloc
  xx(finalize),
#else
  NULL,                        /* finalize    */
#endif
  xx(custom_cmp),              /* compare     */
  xx(hash),                    /* hash        */
  xx(serialize),               /* serialise   */
  xx(deserialize)              /* deserialise */
};

                            /* +--------------+
                               |  Allocation  |
                               +--------------+ */

extern value check_urgent_gc (value);

/* alloue max(2a,b) chiffres */
xint xx(alloc)(long a, long b) {
  long l;
  xint x;

  /* convertit 2a et b en nombre de long + taille header */
#if chiffres_per_long == 1
  a = 2*a + 2; b += 2;
#else
  a += 2; b = (b+5)/2;
#endif
#ifdef debug_alloc
  a += 2; b += 2;
#endif

  if (b > Max_wosize) xx(failwith)(NUMBER_TOO_BIG);
  if (a > Max_wosize) a = Max_wosize;
  l = (a > b) ? a : b;

#ifdef debug_alloc
  x = (xint)check_urgent_gc(alloc_shr(l,Custom_tag));
  x->m1 = magic_1;
  x->val[xx_capacity(x)] = magic_2;
  xalloc++;
#else
  x = (l <= Max_young_wosize) ? (xint)(alloc_small(l,Custom_tag)) :
      (xint)check_urgent_gc(alloc_shr(l,Custom_tag));
#endif
  x->cb = &xx(custom_bloc);
  x->hd = 0;

  return(x);
}

                    /* +-------------------------------+
                       |  enregistrement des m�thodes  |
                       +-------------------------------+ */

value xx(register)() {
  static int unregistered = 1;
  if (unregistered) {
    register_custom_operations(&xx(custom_bloc));
    unregistered = 0;
  }
  return Val_unit;
}

