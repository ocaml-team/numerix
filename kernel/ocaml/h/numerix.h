// file kernel/ocaml/h/numerix.h: Ocaml extensible integers
/*-----------------------------------------------------------------------+
 |  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
 |                                                                       |
 |  This file is part of Numerix. Numerix is free software; you can      |
 |  redistribute it and/or modify it under the terms of the GNU Lesser   |
 |  General Public License as published by the Free Software Foundation; |
 |  either version 2.1 of the License, or (at your option) any later     |
 |  version.                                                             |
 |                                                                       |
 |  The Numerix Library is distributed in the hope that it will be       |
 |  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
 |  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
 |  Lesser General Public License for more details.                      |
 |                                                                       |
 |  You should have received a copy of the GNU Lesser General Public     |
 |  License along with the GNU MP Library; see the file COPYING. If not, |
 |  write to the Free Software Foundation, Inc., 59 Temple Place -       |
 |  Suite 330, Boston, MA 02111-1307, USA.                               |
 +-----------------------------------------------------------------------+
 |                                                                       |
 |                 D�finitions pour l'interface Ocaml                    |
 |                                                                       |
 +-----------------------------------------------------------------------*/

#include "../../n/h/numerix.h"
#include <mlvalues.h>
#include <custom.h>
#include <alloc.h>
#include <fail.h>
#include <callback.h>

#if 0
#include <memory.h>
#else
/* remplacement de <memory.h> pour supprimer le contr�le de type
   sur local_roots (sinon on a des warnings d�sagr�ables avec les
   macros xx_push_roots) */

extern void *local_roots;
extern void  modify(value *, value);
extern value alloc_shr(mlsize_t, tag_t);
extern value caml_check_urgent_gc (value);
#endif

/*
  Un entier extensible est repr�sent� par un pointeur sur le bloc des
  op�rations polymorphes, puis un "long" codant la longueur effective
  en nombre de chiffres et le signe, suivi du tableau des chiffres
  codant la valeur absolue (encadr� par deux marques si debug_alloc
  est d�fini). Le mot d'en-t�te du nombre x est interpr�t� comme suit :

    -- le bit de poids fort vaut 0 si x >= 0, 1 si x < 0
    -- les autres bits codent la longueur l avec x = 0 si l = 0
       et BASE^(l-1) <= |x| < BASE^l sinon.
    -- les chiffres d'indice >= l sont ignor�s
  La longueur maximale d'un entier extensible est cod�e (en nombre de mots)
  dans le mot d'en-t�te propre � OCaml qui pr�c�de l'entier extensible.
*/

typedef struct {       /* entier extensible          */
  struct custom_operations *cb;     /* custom bloc   */
  long    hd;          /* longueur r�elle et signe   */
#ifdef debug_alloc
  long    m1;                     /* marque de d�but */
#endif
  chiffre val[1];      /* 1 pour faire plaisir � gcc */
} * xint;

/* acc�s aux champs */
#ifdef debug_alloc
#define xx_capacity(a) (((a)==Val_null) ? -1 : \
  (Wosize_val((a)) - 4)*chiffres_per_long)
#else
#define xx_capacity(a) (((a)==Val_null) ? -1 : \
  (Wosize_val((a)) - 2)*chiffres_per_long)
#endif
#define xx_sgn(a) ((a)->hd & SIGN_m)
#define xx_lg(a)  ((a)->hd & LONG_m)

/* pointeur nul */
#define xx_null  (xint *)Val_unit
#define Val_null (xint)  Val_unit

/* Interface avec le GC */

#define xx_push_roots_1(a)                      \
  struct {                                      \
      void *next;                               \
      long __nroots;                            \
      long __dummy;                             \
      typeof(&a) a;}                            \
     __lr = {local_roots,1,1,&a};               \
   local_roots = &__lr

#define xx_push_roots_2(a,b)                    \
  struct {                                      \
      void *next;                               \
      long __nroots;                            \
      long __dummy;                             \
      typeof(&a) a;                             \
      typeof(&b) b;}                            \
     __lr = {local_roots,2,1,&a,&b};            \
   local_roots = &__lr

#define xx_push_roots_3(a,b,c)                  \
  struct {                                      \
      void *next;                               \
      long __nroots;                            \
      long __dummy;                             \
      typeof(&a) a;                             \
      typeof(&b) b;                             \
      typeof(&c) c;}                            \
     __lr = {local_roots,3,1,&a,&b,&c};         \
   local_roots = &__lr

#define xx_push_roots_4(a,b,c,d)                \
  struct {                                      \
      void *next;                               \
      long __nroots;                            \
      long __dummy;                             \
      typeof(&a) a;                             \
      typeof(&b) b;                             \
      typeof(&c) c;                             \
      typeof(&d) d;}                            \
     __lr = {local_roots,4,1,&a,&b,&c,&d};      \
   local_roots = &__lr

#define xx_push_roots_5(a,b,c,d,e)              \
  struct {                                      \
      void *next;                               \
      long __nroots;                            \
      long __dummy;                             \
      typeof(&a) a;                             \
      typeof(&b) b;                             \
      typeof(&c) c;                             \
      typeof(&d) d;                             \
      typeof(&e) e;}                            \
     __lr = {local_roots,5,1,&a,&b,&c,&d,&e};   \
   local_roots = &__lr

#define xx_push_roots_6(a,b,c,d,e,f)            \
  struct {                                      \
      void *next;                               \
      long __nroots;                            \
      long __dummy;                             \
      typeof(&a) a;                             \
      typeof(&b) b;                             \
      typeof(&c) c;                             \
      typeof(&d) d;                             \
      typeof(&e) e;                             \
      typeof(&f) f;}                            \
     __lr = {local_roots,6,1,&a,&b,&c,&d,&e,&f};\
   local_roots = &__lr

#define xx_push_roots_11(a,b)                   \
  xint b = Val_null;                            \
  xx_push_roots_2(a,b)

#define xx_push_roots_21(a,b,c)                 \
  xint c = Val_null;                            \
  xx_push_roots_3(a,b,c)

#define xx_push_roots_32(a,b,c,d,e)             \
  xint d = Val_null;                            \
  xint e = Val_null;                            \
  xx_push_roots_5(a,b,c,d,e)

#define xx_push_roots_42(a,b,c,d,e,f)           \
  xint e = Val_null;                            \
  xint f = Val_null;                            \
  xx_push_roots_6(a,b,c,d,e,f)

#define xx_push_roots_75(a,b,c,d,e,f,g,h,i,j,k,l) \
  xint h = Val_null, i = Val_null, j = Val_null,\
       k = Val_null, l = Val_null;              \
  struct {                                      \
      void *next;                               \
      long __nroots;                            \
      long __dummy;                             \
      typeof(&a) a;                             \
      typeof(&b) b;                             \
      typeof(&c) c;                             \
      typeof(&d) d;                             \
      typeof(&e) e;                             \
      typeof(&f) f;                             \
      typeof(&g) g;                             \
      typeof(&h) h;                             \
      typeof(&i) i;                             \
      typeof(&j) j;                             \
      typeof(&k) k;                             \
      typeof(&l) l;}                            \
     __lr = {local_roots,12,1,&a,&b,&c,&d,&e,&f,&g,&h,&i,&j,&k,&l};\
   local_roots = &__lr

#define xx_pop_roots() local_roots = __lr.next

/* mise � jour d'une r�f�rence */
extern inline void xx(update)(xint *_x, xint y) {
    if ((_x != xx_null) && (*_x != y)) modify((value *)_x,(value)y);
}

/* retour d'une valeur */
#define xx_update_and_return(_x,x) do {               \
    if (_x == xx_null) {                              \
        xx_pop_roots();                               \
        return(x);                                    \
    } else {                                          \
        if (*_x != x) modify((value *)_x,(value)x);   \
        xx_pop_roots();                               \
        return(Val_null);                             \
    }                                                 \
} while(0)

/* traitement des erreurs */
extern inline void xx(failwith)(char *msg) __attribute__((noreturn));
extern inline void xx(failwith)(char *msg) {
  value *id = caml_named_value(
#if defined(use_clong)
"cx_error"
#elif defined(use_dlong)
"dx_error"
#elif defined(use_slong)
"sx_error"
#endif
      );
  if (id) raise_with_string(*id, msg+16); /* +16 = saute "Numerix kernel: " */
  else failwith(msg);
}

/* mode d'arrondi */
#define Round_val Long_val

/* valeur 3 �tats */
#define Val_tri Val_long

/* interface commune � toutes les api */
#define ocaml_api
#include "../../x/h/numerix.h"
