#!/bin/sh
# file kernel/ocaml/numerix.sh: output a suitable numerix.ml/mli file
#-----------------------------------------------------------------------+
#  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
#                                                                       |
#  This file is part of Numerix. Numerix is free software; you can      |
#  redistribute it and/or modify it under the terms of the GNU Lesser   |
#  General Public License as published by the Free Software Foundation; |
#  either version 2.1 of the License, or (at your option) any later     |
#  version.                                                             |
#                                                                       |
#  The Numerix Library is distributed in the hope that it will be       |
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
#  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
#  Lesser General Public License for more details.                      |
#                                                                       |
#  You should have received a copy of the GNU Lesser General Public     |
#  License along with the GNU MP Library; see the file COPYING. If not, |
#  write to the Free Software Foundation, Inc., 59 Temple Place -       |
#  Suite 330, Boston, MA 02111-1307, USA.                               |
#-----------------------------------------------------------------------+
#                                                                       |
#                       Cr�ation de numerix.ml/mli                      |
#                                                                       |
#-----------------------------------------------------------------------+

# arguments = liste des modules � d�finir et nom du fichier principal
# positionne les variables associ�es
clong=0
dlong=0
slong=0
big=0
gmp=0
ext=
sedlist=
while : ; do
    case $1 in
    "")    break;;
    clong) clong=1; sedlist="${sedlist} $1";;
    dlong) dlong=1; sedlist="${sedlist} $1";;
    slong) slong=1; sedlist="${sedlist} $1";;
    big)   big=1;   sedlist="${sedlist} $1";;
    gmp)   gmp=1;   sedlist="${sedlist} $1";;
    *.ml)  ext=ml ;;
    *.mli) ext=mli;;
    *)     ;;
    esac
    shift
done

# on y va
cat kernel/ocaml/$ext/numerix.$ext
if test "$clong" = "1"
then sed -e "s/xx(\([^)]*\))/cx_\1/g" \
      -e "s/_Name_/Clong/g" \
      -e "s/_name_/clong/g" \
      -e "1,/\*)/d"         \
      kernel/ocaml/$ext/xlong.$ext
fi
if test "$dlong" = "1"
then sed -e "s/xx(\([^)]*\))/dx_\1/g" \
      -e "s/_Name_/Dlong/g" \
      -e "s/_name_/dlong/g" \
      -e "1,/\*)/d"         \
      kernel/ocaml/$ext/xlong.$ext
fi
if test "$slong" = "1"
then sed -e "s/xx(\([^)]*\))/sx_\1/g" \
      -e "s/_Name_/Slong/g" \
      -e "s/_name_/slong/g" \
      -e "1,/\*)/d"         \
      kernel/ocaml/$ext/xlong.$ext
fi
if test "$big"   = "1"
then sed -e "1,/\*)/d" kernel/ocaml/$ext/big.$ext
fi
if test "$gmp"   = "1"
then sed -e "s/xx(\([^)]*\))/gx_\1/g" \
      -e "s/_Name_/Gmp/g" \
      -e "s/_name_/gmp/g" \
      -e "1,/\*)/d"         \
      kernel/ocaml/$ext/xlong.$ext
fi
sed -e "1,/\*)/d" kernel/ocaml/$ext/cmp.$ext
sed -e "1,/\*)/d" kernel/ocaml/$ext/count.$ext
sed -e "1,/\*)/d" kernel/ocaml/$ext/rfuns.$ext
sed -e "1,/\*)/d" kernel/ocaml/$ext/start.$ext >kernel/ocaml/o/start.tmp
for i in $sedlist; do
  sed -e "s/(\*\(.*$i.*\)\*)/  \1/" kernel/ocaml/o/start.tmp >kernel/ocaml/o/start.tmp1
  mv kernel/ocaml/o/start.tmp1 kernel/ocaml/o/start.tmp
done
cat kernel/ocaml/o/start.tmp
rm  kernel/ocaml/o/start.tmp
