%  file doc/english/c.tex: Numerix documentation
%-----------------------------------------------------------------------+
%  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
%                                                                       |
%  This file is part of Numerix. Numerix is free software; you can      |
%  redistribute it and/or modify it under the terms of the GNU Lesser   |
%  General Public License as published by the Free Software Foundation; |
%  either version 2.1 of the License, or (at your option) any later     |
%  version.                                                             |
%                                                                       |
%  The Numerix Library is distributed in the hope that it will be       |
%  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
%  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
%  Lesser General Public License for more details.                      |
%                                                                       |
%  You should have received a copy of the GNU Lesser General Public     |
%  License along with the GNU MP Library; see the file COPYING. If not, |
%  write to the Free Software Foundation, Inc., 59 Temple Place -       |
%  Suite 330, Boston, MA 02111-1307, USA.                               |
%-----------------------------------------------------------------------+
%                                                                       |
%                       Documentation de Numerix                        |
%                                                                       |
%-----------------------------------------------------------------------+

\chapter{Use with C}
\minitoc

                           %+-------------+
                           %|  Interface  |
                           %+-------------+

\section{Interface}

The \Numerix\ C interface was derived from the Ocaml one by adding
a simple memory manager in order to cope with the lack of
Ocaml GC, and by restricting the interface to the operations implemented
in the C kernel of \Numerix.
The main purpose of this
interface is to allow a fair comparison between \Numerix\
and \GMP\ (designed to be used with C),
and to allow the compilation and the execution of test programs on computers
where Ocaml is not installed.
The \Numerix\ C interface was successfully tested with various {\tt gcc} releases
ranging from {\tt gcc-2.7.2.3} to {\tt gcc-3.4.4}, and the
Linux, OpenBSD, Digital Unix and MacOSX operating systems.
It was also successfully tested with the Microsoft Windows operating system within the Cygwin and the Msys
Unix environments.

%-------------------- Conventions

\subsection{Conventions}

The three \clong, \dlong\ and \slong\ modules
are available, as far as the C compiler and the computer hardware
allow compilation. The choice of the module to be used is done at
compile-time with the help of a
{\tt \#define use\_xxx} directive where {\tt xxx} is the name of the module.
This directive can be included in each source file or it can be
given to the preprocessor with a {\tt -Duse\_xxx} option.
\sv

The {\tt numerix.h} file defines the {\tt xint} datatype representing
a big integer and gives prototypes for the functions operating on these
big integers. The function names are prefixed with a three character string
identifying the module to which they belong:
{\tt cx\_} for the \clong\ module, {\tt dx\_} for \dlong\ and
{\tt sx\_} for \slong. In order to allow the programmer to write big
integer implementation independent code, the
{\tt numerix.h} file defines a {\tt xx} macro which catenates its argument with
the {\tt cx\_}, {\tt dx\_} or {\tt sx\_} prefix depending on which
{\tt use\_clong}, {\tt use\_dlong} or {\tt use\_slong} symbol is defined.
One will write:

\begin{verbatim}
        xx(add)(&x,a,b);
\end{verbatim}

to add {\tt a} and {\tt b} into {\tt x}, this code being transformed
by the preprocessor into:
\sv
\verb?     cx_add(&x,a,b);? or \verb?dx_add(&x,a,b);? or \verb?sx_add(&x,a,b);?
\sv
The user is advised to use systematically the {\tt xx} macro rather than
to use the expanded identifiers. Doing this this way, he can
recompile his program with another big integer implementation
by only modifying the {\tt \#define use\_xxx} directive. Anyway
the functions of one module cannot operate on the data of another module
and there is no mechanism for distinguishing the {\tt xint} datatype
according to a specific module.
\sv

As a general rule, a function computing a result {\tt a} of
type {\tt xint} is available in two versions differing by their calling convention:

\begin{clisting}
\begin{verbatim}
        xint xx(func)(xint *_a,�it args�tt)
        xint xx(f_func)(�it�let�a= args�tt)
\end{verbatim}
\end{clisting}

In both cases, the return value is the computed result {\tt a}.
Moreover, if \hbox{\tt \_a != NULL}, then the result is copied into the memory location
designated by {\tt \_a}. There are two exceptions to this naming convention:
{\tt copy\_int} and {\tt copy\_string}
have as associated functions the {\tt of\_int} and {\tt of\_string} functions
instead of {\tt f\_copy\_int} and {\tt f\_copy\_string} for the sake of compatibility
with previous versions of \Numerix.
A function computing several results {\tt a}, {\tt b},\dots\
of type {\tt xint} is available in only one version:

\begin{clisting}
\begin{verbatim}
        void xx(func)(xint *_a, xint *_b,...,�it args)
\end{verbatim}
\end{clisting}

The results {\tt a}, {\tt b},\dots\ computed are stored in the memory locations
designated by the pointers {\tt \_a}, {\tt \_b},\dots\ If one of these
pointers is {\tt NULL}, the corresponding result is not copied and is not
accessible to the caller.

%-------------------- numerix.h

\subsection{The {\tt numerix.h} file}

Below is a part of {\tt numerix.h} giving the prototypes
of the public functions:

\begin{clisting}
\begin{verbatim}
typedef struct {...} *xint;

/*-------------------- creation/destruction */
xint xx(new)();
void xx(free)(xint *_x);

xint xx(copy)   (xint *_b, xint a);
xint xx(f_copy) (xint a);

/*-------------------- addition/subtraction */
xint xx(add)    (xint *_c, xint a, xint b);
xint xx(sub)    (xint *_c, xint a, xint b);
xint xx(add_1)  (xint *_c, xint a, long b);
xint xx(sub_1)  (xint *_c, xint a, long b);

xint xx(f_add)  (xint a, xint b);
xint xx(f_sub)  (xint a, xint b);
xint xx(f_add_1)(xint a, long b);
xint xx(f_sub_1)(xint a, long b);

/*-------------------- multiplication/square */
xint xx(mul)    (xint *_c, xint a, xint b);
xint xx(mul_1)  (xint *_c, xint a, long b);
xint xx(sqr)    (xint *_b, xint a);

xint xx(f_mul)  (xint a, xint b);
xint xx(f_mul_1)(xint a, long b);
xint xx(f_sqr)  (xint a);

/*-------------------- division */
void xx(quomod)   (xint *_c, xint *_d, xint a, xint b);
xint xx(quo)      (xint *_c,           xint a, xint b);
xint xx(mod)      (xint *_d,           xint a, xint b);
long xx(quomod_1) (xint *_c,           xint a, long b);
xint xx(quo_1)    (xint *_c,           xint a, long b);
long xx(mod_1)    (                    xint a, long b);
void xx(gquomod)  (xint *_c, xint *_d, xint a, xint b, long mode);
xint xx(gquo)     (xint *_c,           xint a, xint b, long mode);
xint xx(gmod)     (xint *_d,           xint a, xint b, long mode);
long xx(gquomod_1)(xint *_c,           xint a, long b, long mode);
xint xx(gquo_1)   (xint *_c,           xint a, long b, long mode);
long xx(gmod_1)   (                    xint a, long b, long mode);

xint xx(f_quo)    (xint a, xint b);
xint xx(f_mod)    (xint a, xint b);
xint xx(f_quo_1)  (xint a, long b);
long xx(f_mod_1)  (xint a, long b);
xint xx(f_gquo)   (xint a, xint b, long mode);
xint xx(f_gmod)   (xint a, xint b, long mode);
xint xx(f_gquo_1) (xint a, long b, long mode);
long xx(f_gmod_1) (xint a, long b, long mode);

/*-------------------- absolute value, opposite */
xint xx(abs)    (xint *_b, xint a);
xint xx(neg)    (xint *_b, xint a);
xint xx(f_abs)  (xint a);
xint xx(f_neg)  (xint a);

/*-------------------- exponentiation */
xint xx(pow)      (xint *_b, xint a, long p);
xint xx(pow_1)    (xint *_b, long a, long p);
xint xx(powmod)   (xint *_d, xint a, xint b, xint c);
xint xx(gpowmod)  (xint *_d, xint a, xint b, xint c, long mode);

xint xx(f_pow)    (xint a, long p);
xint xx(f_pow_1)  (long a, long p);
xint xx(f_powmod) (xint a, xint b, xint c);
xint xx(f_gpowmod)(xint a, xint b, xint c, long mode);

/*-------------------- roots */
xint xx(sqrt)   (xint *_b, xint a);
xint xx(root)   (xint *_b, xint a, long p);
xint xx(gsqrt)  (xint *_b, xint a,         long mode);
xint xx(groot)  (xint *_b, xint a, long p, long mode);

xint xx(f_sqrt) (xint a);
xint xx(f_root) (xint a, long p);
xint xx(f_gsqrt)(xint a,         long mode);
xint xx(f_groot)(xint a, long p, long mode);

/*-------------------- factorial */
xint xx(fact)  (xint *_a, long n);
xint xx(f_fact)(long n);

/*-------------------- greatest common divisor */
xint xx(gcd)   (xint *_d, xint a, xint b);
void xx(gcd_ex)(xint *_d, xint *_u, xint *_v, xint a, xint b);
void xx(cfrac) (xint *_d, xint *_u, xint *_v, xint *_p, xint *_q, xint a, xint b);
xint xx(f_gcd) (xint a, xint b);

/*-------------------- primality */
long xx(isprime)  (xint a);
long xx(isprime_1)(long a);

/*-------------------- comparison */
long xx(sgn)    (xint a);
long xx(cmp)    (xint a, xint b);
long xx(cmp_1)  (xint a, long b);

long xx(eq)     (xint a,xint b);
long xx(neq)    (xint a,xint b);
long xx(inf)    (xint a,xint b);
long xx(infeq)  (xint a,xint b);
long xx(sup)    (xint a,xint b);
long xx(supeq)  (xint a,xint b);

long xx(eq_1)   (xint a,long b);
long xx(neq_1)  (xint a,long b);
long xx(inf_1)  (xint a,long b);
long xx(infeq_1)(xint a,long b);
long xx(sup_1)  (xint a,long b);
long xx(supeq_1)(xint a,long b);

/*-------------------- conversion */
xint xx(copy_int)   (xint *_b, long a);
xint xx(of_int)     (long a);
long xx(int_of)     (xint a);
xint xx(copy_string)(xint *_a, char *s);
xint xx(of_string)  (char *s);

char *xx(string_of) (xint a);
char *xx(hstring_of)(xint a);
char *xx(ostring_of)(xint a);
char *xx(bstring_of)(xint a);

/*-------------------- random integers */
void xx(random_init)(long n);
xint xx(nrandom) (xint *_a, long n);
xint xx(zrandom) (xint *_a, long n);
xint xx(nrandom1)(xint *_a, long n);
xint xx(zrandom1)(xint *_a, long n);

xint xx(f_nrandom) (long n);
xint xx(f_zrandom) (long n);
xint xx(f_nrandom1)(long n);
xint xx(f_zrandom1)(long n);

/*-------------------- binary representation */
long xx(nbits)   (xint a);
long xx(lowbits) (xint a);
long xx(highbits)(xint a);
long xx(nth_word)(xint a, long n);
long xx(nth_bit) (xint a, long n);

/*-------------------- shifts */
xint xx(shl)  (xint *_b,           xint a, long n);
xint xx(shr)  (xint *_b,           xint a, long n);
void xx(split)(xint *_b, xint *_c, xint a, long n);
xint xx(join) (xint *_c, xint a,   xint b, long n);

xint xx(f_shl) (xint a,         long n);
xint xx(f_shr) (xint a,         long n);
xint xx(f_join)(xint a, xint b, long n);

/*-------------------- timing facility */
void chrono(char *msg);
\end{verbatim}
\end{clisting}

%-------------------- Gestion de la memoire

\subsection{Memory management}

A {\tt a} variable of type {\tt xint} is a pointer to a data structure
managed by the memory manager included in the C version of
\Numerix. The initialization of {\tt a} is normally done in two steps:
\sv
\begin{itemize}
\item
initialization of the {\tt a} pointer;
\item
assignment of a value by giving the {\tt \&a} address
as a result parameter of a computation.
\end{itemize}
\sv
It is possible to merge these two steps into a single one by assigning to
{\tt a} the result of type {\tt xint} returned by a computation.
Therefore, the following sequences where {\tt a} denotes a variable
of type {\tt xint} not initialized and {\tt b},{\tt c} denote
variables of type {\tt xint} initialized having been assigned the values
$b$ and $c$ are equivalent: their common effect is to allocate a memory block,
to copy into this block the internal representation of the number $b+c$, and
to copy the address of the block into {\tt a}.

\begin{verbatim}
        a = xx(new)(); xx(add)(&a,b,c);
        a = xx(f_add)(b,c);
        a = xx(add)(NULL,b,c);
\end{verbatim}

Once the {\tt a} pointer is initialized, the {\tt \&a} address
can be given as a result parameter to a computation. For instance:

\begin{verbatim}
        xx(mul)(&a,b,c);
\end{verbatim}

has for effect to compute the product $bc$ and to copy into {\tt a} the address
of the memory block where this product has been stored.
It is not necessary for {\tt a} to have been assigned a value prior to this operation.
If it is the case,  then the memory block containing this value
is overwritten with the internal representation of $bc$ if the block is large enough,
otherwise a new memory block is allocated to store the result,
{\tt a} is modified in order to point to the new block and the old block is reclaimed.
The read-modify-write operations where the same variable is given both as an
operand and as a result are handled correctly.
On the other way, concerning the operations computing several results
({\tt quomod}, {\tt gquomod}, {\tt gcd\_ex}, {\tt cfrac} and {\tt split})
one variable cannot be given more than one time as a result.
Therefore the following instruction is illegal:

\begin{verbatim}
        xx(quomod)(&a,&a,b,c); /* illegal */
\end{verbatim}

The {\tt xx(free)} function enables one to return a memory block
to the memory manager when the value stored in this memory block
is no longer useful. The instruction:

\begin{verbatim}
        xx(free)(&a);
\end{verbatim}

has for effect to free the memory block designated by {\tt a} if there is one
and to reinitialize the {\tt a} pointer. After this instruction,
the {\tt a} variable is still operational and can be assigned a new value.

%-------------------- Mode d'arrondi

\subsection{Rounding mode}

The operations computing an integer approximation of a real number $a$
(division, square root and $p$-th root) are available in two versions:

\begin{clisting}
\begin{verbatim}
        xx(func) (�it�let�a= args�tt)
        xx(gfunc)(�it�let�a= args�tt, long mode)
\end{verbatim}
\end{clisting}

The {\tt mode} parameter of {\tt xx(gfunc)} specifies in which way
the number $a$ is to be rounded:
\sv
\begin{tabular}{lll}
\verb?  ?
&if {\tt mode} \& $3 = 0$ : &compute $\lfloor a    \rfloor$ ;\\
&if {\tt mode} \& $3 = 1$ : &compute $\lfloor a+\hbox{\footnotesize 1/2}\rfloor$ ;\\
&if {\tt mode} \& $3 = 2$ : &compute $\lceil  a    \rceil $ ;\\
&if {\tt mode} \& $3 = 3$ : &compute $\lceil  a-\hbox{\footnotesize 1/2}\rceil $. \\
\end{tabular}
\sv
{\tt xx(func)} is equivalent to {\tt xx(gfunc)} with ${\tt mode}=0$.

%-------------------- Description des fonctions

\subsection{Description of the functions}

The operations implemented in the C interface of \Numerix\ are identical
to the ones implemented in the Ocaml interface and described in sections
{\bf \ref{operations-ocaml-begin} Arithmetic operations} to
{\bf \ref{operations-ocaml-end} Access to the binary representation},
pages \pageref{operations-ocaml-begin} and following,
and in section {\bf \ref{chrono-ocaml} Timing}, page~\pageref{chrono-ocaml}.
Below are mentioned the particularities of the C interface.

\begin{itemize}

\sv\item
When an Ocaml function returns a boolean result, the equivalent C function
returns an integer of type {\tt long} the value of which is $0$ for {\tt false}
and $1$ for {\tt true}.

\sv\item
When an Ocaml function returns a three-valued logical result, the equivalent C function
returns an integer of type {\tt long} the value of which is $0$ for {\tt False},
$1$ for {\tt Unknown} and $2$ for {\tt True}.

\sv\item
The C functions converting a big integer into a character string
return a pointer to a string allocated on the heap. This string must be
released after use by calling the {\tt free} function.

\sv\item
The {\tt xx(lowbits)} and {\tt xx(highbits)} functions return respectively
the $31$ least significant bits and the $31$ most significant bits
of their argument, regardless of the machine word size.
Also, the {\tt xx(int\_of)} function raises systematically an error
when the absolute value of its argument is greater than~$2^{30}$.
\end{itemize}


\section{Use}

                          %+---------------+
                          %|  Compilation  |
                          %+---------------+

\subsection{Compilation}
The C programs using \Numerix\ must be compiled with the following command:
\sv
\begin{shelllisting}
\begin{verbatim}
        gcc�it options�tt -Duse_xxx�it source files�tt -lnumerix-c
\end{verbatim}
\end{shelllisting}
\sv

{\tt -Duse\_xxx} specifies which module to use, \clong\ or \dlong\
or \slong.
\sv
{\tt -lnumerix-c} asks the linker to search in the
{\tt libnumerix-c} library the required compiled functions.
It may be necessary to tell the linker where to find this library
with a {\tt -L\it path} option.
Similarly it may be necessary to tell the preprocessor
where to find the {\tt numerix.h} header file with a {\tt -I\it path} option.




                            %+-----------+
                            %|  Exemple  |
                            %+-----------+

\subsection{Example}

\begin{clisting}
\begin{verbatim}
/* file simple.c: simple demo of Numerix
   compute  (sqrt(3) + sqrt(2))/(sqrt(3)-sqrt(2)) with n digits */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "numerix.h"

int main(int argc, char **argv) {

   xint a,b,d,d2,x,y;
   char *s;
   long n;

   /* number of digits */
   if ((argc > 2) && (strcmp(argv[1],"-n") == 0)) n = atol(argv[2]);
   else n = 30;

   /* d <- 10^n, d2 <- 10^(2n) */
   d  = xx(f_pow_1)(5,n); xx(shl)(&d,d,n);
   d2 = xx(f_sqr)(d);

   /* a <- round(sqrt(2*10^(2n+2))), b <- round(sqrt(3*10^(2n+2)))  */
   a = xx(f_mul_1)(d2,200); xx(gsqrt)(&a,a,1);
   b = xx(f_mul_1)(d2,300); xx(gsqrt)(&b,b,1);

   /* x <- round(10^n*(b+a)/(b-a)) */
   x = xx(f_add)(b,a); xx(mul)(&x,x,d);
   y = xx(f_sub)(b,a);
   xx(gquo)(&x,x,y,1);

   /* print x */
   s = xx(string_of)(x); printf("x=%s\n",s); free(s);

   /* free temporary memory */
   xx(free)(&d); xx(free)(&d2);
   xx(free)(&a); xx(free)(&b);
   xx(free)(&x); xx(free)(&y);

   return(0);
}
\end{verbatim}
\end{clisting}

Compilation and execution:
\sv
\begin{verbatim}
> gcc -O2 -Wall -I/home/quercia/include -Duse_slong \
      -o simple simple.c -lnumerix-c -L/home/quercia/lib
> ./simple -n 20
x=989897948556635619642
>
\end{verbatim}

