%  file doc/english/intro.tex: Numerix documentation
%-----------------------------------------------------------------------+
%  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
%                                                                       |
%  This file is part of Numerix. Numerix is free software; you can      |
%  redistribute it and/or modify it under the terms of the GNU Lesser   |
%  General Public License as published by the Free Software Foundation; |
%  either version 2.1 of the License, or (at your option) any later     |
%  version.                                                             |
%                                                                       |
%  The Numerix Library is distributed in the hope that it will be       |
%  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
%  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
%  Lesser General Public License for more details.                      |
%                                                                       |
%  You should have received a copy of the GNU Lesser General Public     |
%  License along with the GNU MP Library; see the file COPYING. If not, |
%  write to the Free Software Foundation, Inc., 59 Temple Place -       |
%  Suite 330, Boston, MA 02111-1307, USA.                               |
%-----------------------------------------------------------------------+
%                                                                       |
%                       Documentation de Numerix                        |
%                                                                       |
%-----------------------------------------------------------------------+

\chapter{Presentation}
\minitoc

                          %+---------------+
                          %|  Description  |
                          %+---------------+

\section{Description}

\Numerix\ is a library implementing arbitrary long signed integers
and the usual arithmetic operations between those numbers.
Designed for a use with the Ob\-jec\-tive-Caml language, it is also available
with reduced functionalities for the
Camllight, C and Pascal  languages on 32 or 64 bit Unix-type computers.
It is shipped in three different versions:

\begin{description}
\item\Clong:\par
written in standard C. The basic object is a ``digit'',
the length of which is half of a machine word.
So, elementary operations giving a two digit result
are implemented with ordinary C operations on {\tt long} variables,
and this library should be portable to every computer with a binary
arithmetic and words of even bit-length not fewer than 32 bits.

\item\Dlong:\par
also written in C, but a digit is a whole machine word.
Operations between digits are handled with the
{\tt long long} datatype when the computer and the C compiler allow
it; otherwise those operations are emulated.

\item\Slong~:\par
written partly in assembly language and partly in C, a digit is a whole machine word.
Five different implementations of the \Slong\ module are available~:
\begin{center}
\begin{tabular}{ll}
\tt x86 :      &Pentium-like 32 bit processors;\\
\tt x86-sse2 : &Pentium-{\tt SSE2} like 32 bit processors;\\
\tt x86-64 :   &Pentium or Athlon-64 64 bit processors;\\
\tt alpha :    &Alpha-21264 64 bit processors;\\
\tt ppc32 :    &{\tt PowerPC} 32 bit processors.\\
\end{tabular}
\end{center}
\end{description}

Concerning the speed, \Numerix\ compares well to the other
multi-precision libraries commonly available, especially
\Bigint\ (the adaptation of {\tt BigNum} for Camllight/Ocaml) and \GMP.
Below are the computing times for the $n$ first decimal digits
of~$\pi$ on a Linux PC with a Pentium-4-3.0Ghz processor and a
1Gb random access memory:
\sv
\begin{center}
\begin{tabular}{r|r|r|r|r|r|r|r|}
\cline{2-8}
&         &\Slong    &\Slong    &\Dlong     &\Clong     &\multicolumn{1}{|c|}{\GMP}&\Bigint \\
&$n$      &\multicolumn{1}{|c|}{\tt SSE2}
          &\multicolumn{1}{|c|}{\tt x86}
          &&&\multicolumn{1}{|c|}{\tt 4.2.1}&\\
\cline{2-8}
&$10^4$   &  0.01s   &  0.01s   &    0.02s  &    0.03s  &    0.01s &   0.27s\vphantom{$2^{2^{2}}$}\\
&$10^5$   &  0.19s   &  0.35s   &    0.74s  &    0.76s  &    0.31s &  32.51s\\
&$10^6$   &  4.23s   &  7.74s   &   15.45s  &   15.45s  &    7.42s &  2642s \\
\cline{2-8}
\end{tabular}
\end{center}
\sv
The same algorithm is used in the five cases, derived from a series expansion
from Ramanujan, and only the big integer implementation differs.
The \Slong, \Clong, \Dlong\ and \GMP\ libraries were used with a main
program in C, whereas \Bigint\ was with a main program in Ocaml.
However, the influence of the main language on the running time is 
negligible for this kind of program for which the main part of the computing
time is spent with the operations on several-million-bit-long numbers;
the running times are similar when all the libraries are used with an Ocaml
main program.

                          %+---------------+
                          %|  Portabilité  |
                          %+---------------+

\section{Portability}

The \Clong\ and \Dlong\ modules should be portable on any 32 bit or 64 bit computer
with an Unix-like operating system (Linux, OpenBSD, Windows-XP$+$Cygwin Windows-XP$+$Msys and
MacOSX were successfully tested).
\sv
The \Slong\ module is portable only on computers with a {\tt x86}, a {\tt x86\_64}, an {\tt alpha}
or a {\tt PowerPC-32} processor.

                            %+-----------+
                            %|  Licence  |
                            %+-----------+

\section{License}
\Numerix\ is distributed under the terms of the GNU Library General Public License version 2
with the right to link statically or dynamically the \Numerix\ library
with a program without having to transfer the license on the executable.
Please refer to the
{\tt COPYING} and {\tt LICENSE} files shipped within the {\tt numerix.tar.gz}
archive in order to learn about the terms of this license.

                              %+-----------------+
                              %|  Modifications  |
                              %+-----------------+

\section{Differences with Numerix-0.21}

\begin{itemize}

\item
The \Dlong\ module is now available for any hardware and software configuration.
When the C compiler does not support the double-precision integer arithmetic,
this arithmetic is emulated.

\sv\item
The \Slong\ module is available for new processors:
{\tt x86-64}, {\tt alpha} and {\tt PowerPC-32}.

\sv\item
\Numerix\ can now be compiled on Windows computers, with the Cygwin or
the Msys environments.

\sv\item
\Numerix\ can now be compiled into a set of shared libraries (for now,
this works only with the Linux and Digital Unix operating systems).

\sv\item
Modification of the Pascal interface in order to be compatible
with both the Free-Pascal and the GNU-Pascal compilers.
The {\tt pow} function has been renamed {\tt power}.
The string conversion functions now return a string of type {\tt pchar}
instead of {\tt ansistring} ;
this string must be deallocated explicitly.
Add the {\tt of\_pstring} and {\tt copy\_pstring} functions, similar to
{\tt of\_string} and {\tt copy\_string}, but taking a string argument
of type {\tt string} instead of {\tt pchar}.

\sv\item
Add the {\tt isprime} and {\tt isprime\_1} functions, enabling one to test
the primality of an integer. The algorithm implemented in the
\Clong, \Dlong\ and \Slong\ modules, derived from the BPSW algorithm
(Baillie, Pomerance, Selfridge, Wagstaff), is accurate up to $2^{50}$.

\sv\item
The functions from the {\tt Rfuns} module for the Ocaml language come with a
new interface {\tt e\_f} which receives as second argument a bit number
$p$ designating the denominator $2^p$.

\end{itemize}
