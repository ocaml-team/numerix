\select@language {english}
\contentsline {chapter}{\numberline {1}Presentation}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Description}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Portability}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}License}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Differences with Numerix-0.21}{3}{section.1.4}
\contentsline {chapter}{\numberline {2}Use with Ocaml}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Interface}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}The Int\_type signature}{14}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Integers and references}{14}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Various versions of an operation}{15}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Rounding mode}{16}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Arithmetic operations}{16}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Primality}{17}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Comparisons}{18}{subsection.2.2.6}
\contentsline {subsection}{\numberline {2.2.7}Conversions}{18}{subsection.2.2.7}
\contentsline {subsection}{\numberline {2.2.8}Pseudo-random numbers}{19}{subsection.2.2.8}
\contentsline {subsection}{\numberline {2.2.9}Access to the binary representation}{19}{subsection.2.2.9}
\contentsline {subsection}{\numberline {2.2.10}Hashing, serialization and de-serialization}{20}{subsection.2.2.10}
\contentsline {subsection}{\numberline {2.2.11}Errors}{20}{subsection.2.2.11}
\contentsline {section}{\numberline {2.3}The functors using the Int\_type signature}{21}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Infix symbols}{21}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Comparison between two modules}{21}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Statistics}{22}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Approximation of the usual functions}{23}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Run-time selection of a module}{26}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Timing}{26}{subsection.2.3.6}
\contentsline {section}{\numberline {2.4}Use}{26}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Compilation}{26}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Example}{27}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Toplevel}{28}{subsection.2.4.3}
\contentsline {chapter}{\numberline {3}Use with Camllight}{29}{chapter.3}
\contentsline {section}{\numberline {3.1}Interface}{29}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Modules}{29}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Functions}{30}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Use}{30}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Compilation}{30}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Example}{30}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Toplevel}{31}{subsection.3.2.3}
\contentsline {chapter}{\numberline {4}Use with C}{33}{chapter.4}
\contentsline {section}{\numberline {4.1}Interface}{33}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Conventions}{33}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}The {\tt numerix.h} file}{34}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Memory management}{38}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Rounding mode}{39}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Description of the functions}{39}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}Use}{39}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Compilation}{39}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Example}{40}{subsection.4.2.2}
\contentsline {chapter}{\numberline {5}Use with Pascal}{42}{chapter.5}
\contentsline {section}{\numberline {5.1}Interface}{42}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Units}{42}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Memory management}{46}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Rounding mode}{47}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Description of the functions}{48}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Strings}{48}{subsection.5.1.5}
\contentsline {section}{\numberline {5.2}Use}{49}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Compilation}{49}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Exemple}{49}{subsection.5.2.2}
\contentsline {chapter}{\numberline {6}Installation}{51}{chapter.6}
\contentsline {section}{\numberline {6.1}Downloading}{51}{section.6.1}
\contentsline {section}{\numberline {6.2}Configuration}{52}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Automatic configuration}{52}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Manual configuration}{54}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Editing the {\tt Makefile}}{54}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Editing the {\tt kernel/config.h}}{56}{subsection.6.2.4}
\contentsline {section}{\numberline {6.3}Compilation}{57}{section.6.3}
\contentsline {section}{\numberline {6.4}Description of the examples}{58}{section.6.4}
\contentsline {subsection}{\numberline {6.4.1}chrono}{58}{subsection.6.4.1}
\contentsline {subsection}{\numberline {6.4.2}digits}{60}{subsection.6.4.2}
\contentsline {subsection}{\numberline {6.4.3}pi}{61}{subsection.6.4.3}
\contentsline {subsection}{\numberline {6.4.4}shanks}{62}{subsection.6.4.4}
\contentsline {subsection}{\numberline {6.4.5}simple}{62}{subsection.6.4.5}
\contentsline {subsection}{\numberline {6.4.6}sqrt-163}{62}{subsection.6.4.6}
\contentsline {subsection}{\numberline {6.4.7}prime-test}{62}{subsection.6.4.7}
\contentsline {subsection}{\numberline {6.4.8}cmp, rcheck}{63}{subsection.6.4.8}
