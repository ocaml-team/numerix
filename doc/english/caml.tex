%  file doc/english/caml.tex: Numerix documentation
%-----------------------------------------------------------------------+
%  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
%                                                                       |
%  This file is part of Numerix. Numerix is free software; you can      |
%  redistribute it and/or modify it under the terms of the GNU Lesser   |
%  General Public License as published by the Free Software Foundation; |
%  either version 2.1 of the License, or (at your option) any later     |
%  version.                                                             |
%                                                                       |
%  The Numerix Library is distributed in the hope that it will be       |
%  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
%  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
%  Lesser General Public License for more details.                      |
%                                                                       |
%  You should have received a copy of the GNU Lesser General Public     |
%  License along with the GNU MP Library; see the file COPYING. If not, |
%  write to the Free Software Foundation, Inc., 59 Temple Place -       |
%  Suite 330, Boston, MA 02111-1307, USA.                               |
%-----------------------------------------------------------------------+
%                                                                       |
%                       Documentation de Numerix                        |
%                                                                       |
%-----------------------------------------------------------------------+


\chapter{Use with Camllight}
\minitoc

The \Numerix\ Camllight interface was derived from the
Ocaml one by removing or adapting the functionalities specific
to the Ocaml language. Please refer to the previous chapter to see the
list of available functions, only the differences with the Ocaml version
are mentioned here. This interface was successfully tested with
Camllight-0.74 and Camllight-0.75.


\section{Interface}
                            %+-----------+
                            %|  Modules  |
                            %+-----------+

\subsection{Modules}

Camllight has a limited module system and provides neither
sub-modules nor functors. However, it is
possible to write implementation independent code
by using the short functions names,
the long ones are inferred by the compiler with the help of
{\tt\#open}  directives in the source file. One only needs to modify these
directives (possibly in an automatic way with a preprocessor)
and to recompile the source code in order to change the big integer
implementation used.
\sv

The available modules have the same names as those of Ocaml in lowercase:
\clong, \dlong, \slong, \gmp\ and \big.
There is no equivalent to the modules built with the Ocaml functors
{\tt Cmp}, {\tt Count} and {\tt Rfuns}. The infix notations are available
by opening the {\tt infxxx} module where {\tt xxx} is the name of the module
implementing big integers.


                           %+-------------+
                           %|  Fonctions  |
                           %+-------------+

\subsection{Functions}

The functions described in the {\tt Int\_type} Ocaml signature 
are available with Camllight with only three differences:

\begin{itemize}
\sv\item
The division without remainder is named {\tt quo} in Ocaml and {\tt div}
in Camllight. The reason for this difference is that the
{\tt quo}  identifier has an infix status in Camllight. The other names
derived from {\tt quo}: {\tt quomod}, {\tt quo\_1}, {\tt gquo}, etc.
are the same as those in Ocaml.

\sv\item
Accessing the value held by a reference is written {\tt look} or {\tt \~{}\~{}}
in Ocaml, whereas it is written {\tt look} or {\tt ?} in Camllight.
There are two reasons for this difference: the {\tt ?} identifier is reserved
in Ocaml and the {\tt\~{}\~{}} identifier has a prefix status in Ocaml
and an infix one in Camllight.

\sv\item
The run-time errors raise an {\tt Error }{\it msg} exception in Ocaml and
a {\tt Failure "Numerix kernel: {\it msg}"} exception in Camllight.
This is a result of the impossibility in Camllight to raise any
exception except {\tt Failure} and {\tt Invalid\_argument}
from within a C function.

\end{itemize}

\section{Use}

                          %+---------------+
                          %|  Compilation  |
                          %+---------------+

\subsection{Compilation}
The Caml programs using \Numerix\ must be compiled with the following command:
\sv
\begin{shelllisting}
\begin{verbatim}
      camlc -custom�it options�tt nums.zo numerix.zo�it source files�tt \
            -lnumerix-caml -lnums -lgmp
\end{verbatim}
\end{shelllisting}
\sv
The {\tt nums.zo} and {\tt numerix.zo} files contain in a compiled form
the Caml part of the \Bigint\ and \Numerix\ libraries.
It may be necessary to tell the compiler where to find the
{\tt numerix.zo} file with a {\tt -I} {\it path} option.
\sv

The {\tt -lnumerix-caml},  {\tt -lnums}
and {\tt -lgmp} options ask the linker to look for the required C primitives
in the {\tt libnumerix-caml},  {\tt libnums} and {\tt libgmp} libraries.
It may be necessary to tell the linker where to find these libraries
with {\tt -ccopt -L\it path} options.
If {\tt GMP} is not installed or if its Camllight interface is not
included in \Numerix, then the  {\tt -lgmp} option must be omitted.
Similarly, the {\tt nums.zo} and {\tt -lnums} parameters must be omitted
if the {\tt big} module is not included in \Numerix.


                            %+-----------+
                            %|  Exemple  |
                            %+-----------+

\subsection{Example}

\begin{camllisting}
\begin{verbatim}
(* file simple.ml: simple demo of Numerix
   compute (sqrt(3) + sqrt(2))/(sqrt(3)-sqrt(2)) with n digits *)

#open "clong";;
#open "infclong";;
  
  let main arglist =

    let n = match arglist with  
    | _::"-n"::x::_ -> int_of_string x
    | _             -> 30
    in
  
    (* d <- 10^n, d2 <- 10^(2n) *)
    let d  = (5 ^. n) << n in
    let d2 = sqr d         in
  
    (* a <- round(sqrt(2*10^(2n+2))), b <- round(sqrt(3*10^(2n+2))) *)
    let a = gsqrt Nearest_up (d2 *. 200) in
    let b = gsqrt Nearest_up (d2 *. 300) in
  
    (* r <- round(10^n*(b+a)/(b-a)) *)
    let r = gquo Nearest_up (d**(b++a)) (b--a) in
    printf__printf "r=%s\n" (string_of r);
    flush stdout

  in
  main (list_of_vect sys__command_line);;

\end{verbatim}
\end{camllisting}
\noindent
Compilation and execution:
\sv
\begin{verbatim}
> camlc -custom -I ~/lib -o simple nums.zo numerix.zo simple.ml \
        -lnumerix-caml -lnums -lgmp -ccopt -L/home/quercia/lib
> ./simple
r=9898979485566356196394568149411
>
\end{verbatim}

Note that the three {\tt libnumerix-caml}, {\tt libnums}
and {\tt libgmp} libraries must be given to the linker even if 
the \clong\ module is the only one used, because the other modules are
included in {\tt numerix.zo} and contain references to functions from
these three libraries.

                      %+----------------------+
                      %|  Systeme interactif  |
                      %+----------------------+

\subsection{Toplevel}

A customized toplevel is available in Camllight for doing \Numerix\ computations:
\sv

\begin{verbatim}
> camllight ~/lib/camlnumx
>       Caml Light version 0.75

camlnumx : Caml toplevel with big integer libraries
Numerix submodules : clong dlong slong big gmp
Numerix version    : 0.22

##open "slong";;
##open "infslong";;
#fact 30;;
- : t = 265252859812191058636308480000000
#one << 100;;
- : t = 1267650600228229401496703205376
#quit();;
>
\end{verbatim}

