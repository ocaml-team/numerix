%  file doc/english/install.tex: Numerix documentation
%-----------------------------------------------------------------------+
%  Copyright 2005-2006, Michel Quercia (michel.quercia@prepas.org)      |
%                                                                       |
%  This file is part of Numerix. Numerix is free software; you can      |
%  redistribute it and/or modify it under the terms of the GNU Lesser   |
%  General Public License as published by the Free Software Foundation; |
%  either version 2.1 of the License, or (at your option) any later     |
%  version.                                                             |
%                                                                       |
%  The Numerix Library is distributed in the hope that it will be       |
%  useful, but WITHOUT ANY WARRANTY; without even the implied warranty  |
%  of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU  |
%  Lesser General Public License for more details.                      |
%                                                                       |
%  You should have received a copy of the GNU Lesser General Public     |
%  License along with the GNU MP Library; see the file COPYING. If not, |
%  write to the Free Software Foundation, Inc., 59 Temple Place -       |
%  Suite 330, Boston, MA 02111-1307, USA.                               |
%-----------------------------------------------------------------------+
%                                                                       |
%                       Documentation de Numerix                        |
%                                                                       |
%-----------------------------------------------------------------------+

\chapter{Installation}
\minitoc


                        %+------------------+
                        %|  Telechargement  |
                        %+------------------+

\section{Downloading}

\Numerix\ is available at the following URL:
\sv
\verb?http://pauillac.inria.fr/~quercia/cdrom/bibs/numerix.tar.gz?
\sv\noindent
You will need the {\tt gcc} C compiler
to compile the C and assembly parts of the library, any
recent version of {\tt gcc} should fit for that. The library was
successfully compiled with various {\tt gcc} releases
ranging from {\tt gcc-2.7.2.3} to {\tt gcc-3.4.4}, and the
Linux, OpenBSD, Digital Unix and MacOSX operating systems.
It was also successfully compiled with the Microsoft Windows operating system within the Cygwin and the Msys
Unix environments.
\sv
For Ocaml you need a not less than~{\tt 3.06} version
and for Camllight a not less than~{\tt 0.74} version.
Ocaml and Camllight are available at the URL:
\sv
\verb?http://caml.inria.fr/index.en.html?
\sv\noindent
If you want to include the {\tt Gmp} module
in the interfaces for Ocaml and Camllight then you
need \GMP\ installed on your computer. \Numerix-0.22 was successfully compiled
with \GMP\ versions 4.1.4 and 4.2.1.
\GMP\ is available at the URL:
\sv
\verb?http://www.swox.com/gmp/?
\sv
For the Pascal interface you need one of the Free-Pascal or GNU-Pascal
Pascal compilers. \Numerix-0.22 was successfully compiled
with the Free-Pascal version 2.0.2 and the GNU-Pascal version 20050217
compilers, and the Linux and Windows operating systems.
Free-Pascal and GNU-Pascal are available at the URLs:
\sv
\verb?http://www.freepascal.org/?

\verb?http://www.gnu-pascal.de/?
\sv
Concerning the installation of \Numerix\ on a Windows computer,
you need one of the Cygwin or Msys Unix environments.
\Numerix-0.22 was successfully compiled with {\tt CYGWIN-1.5.19}
for the C, Ocaml, Free-Pascal and GNU-Pascal interfaces,
and with {\tt Msys-1.0.10} for the C and Free-Pascal interfaces.
Cygwin and Msys are available at the URLs:
\sv
\verb?http://www.cygwin.com/?

\verb?http://www.mingw.org/msys.shtml?


                         %+-----------------+
                         %|  Configuration  |
                         %+-----------------+

\section{Configuration}

%-------------------- Configuration automatique

\subsection{Automatic configuration}
\label{autoconf}

Extract the {\tt numerix.tar.gz} archive in a temporary directory
and run the configuration script at the root. The commands shown below
are conforming to the {\tt bash} shell syntax. If you use the {\tt csh}
shell, then replace the ``\verb?2>&1 |?'' redirection operator with ``\verb?|&?''.

\begin{verbatim}
        ./configure 2>&1 | tee conflog
\end{verbatim}

This  script checks which  parts of  \Numerix\ can  be compiled  on your
computer and  creates a {\tt Makefile} file 
suited  for your configuration.
The {\tt configure} script accepts the following options:

\begin{description}
\item{\tt-{}-prefix=\it dir}

Set the common root for installation directories:

\indent\verb?INSTALL_LIB     = ?{\it dir}\verb?/lib?,\par
\indent\verb?INSTALL_BIN     = ?{\it dir}\verb?/bin?,\par
\indent\verb?INSTALL_INCLUDE = ?{\it dir}\verb?/include?.\par
{\it dir} must be an absolute path. The default prefix is \verb?$HOME?.%$

\item{\tt-{}-libdir=\it dir, \tt-{}-bindir=\it dir, \tt-{}-includedir=\it dir}

Set  one  of \verb?INSTALL_LIB?, \verb?INSTALL_BIN?
and \verb?INSTALL_INCLUDE? directory regardless of the others.
{\it dir} must be an absolute path. 

\item{\tt-{}-enable-c\rm,\tt\ \ \ \ \ \ -{}-disable-c}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-caml\rm,\tt\ \ \    -{}-disable-caml}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-ocaml\rm,\tt\ \     -{}-disable-ocaml}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-pascal\rm,\tt\      -{}-disable-pascal}\par

Select or un-select the corresponding interfaces.
The default is to always select the C interface, and to select the other
ones when the {\tt configure} script finds in the computer a compiler
for this language.
Concerning the Pascal interface, when both the {\tt fpc} and the {\tt gpc}
Pascal compilers are installed in the computer, you can specify
in the option which compiler you want to use:
{\tt-{}-enable-pascal=fpc} or {\tt-{}-enable-pascal=gpc}.
When the Pascal compiler is not specified, the {\tt configure} script
will give priority to {\tt gpc}.
If you want to compile the interfaces for the two compilers, then
you must build \Numerix\ twice, and select a different \verb?INSTALL_LIB?
directory for each compiler.
Also, concerning the Free-Pascal interface under the Windows-Cygwin environment,
you must use the {\tt mingw} configuration for {\tt gcc}, so as to build
object files that are compatible with Free-Pascal. In order to select this
{\tt mingw} configuration, enter the {\tt-{}-enable-mingw} option described below.

\item{\tt-{}-enable-clong\rm,\tt\   \ \ \ \ \ \ \ -{}-disable-clong}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-dlong\rm,\tt\   \ \ \ \ \ \ \ -{}-disable-dlong}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-slong\rm,\tt\   \ \ \ \ \ \ \ -{}-disable-slong}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-gmp\rm,\tt\ \ \ \ \ \ \ \ \ \ -{}-disable-gmp}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-caml\_bignum\rm,\tt\ \        -{}-disable-caml\_bignum}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-ocaml\_bignum\rm,\tt\         -{}-disable-ocaml\_bignum}

Select or un-select the corresponding modules.
The default is to always select the \Clong\ module,
to select the \Dlong\ module when {\tt gcc} supports the double-long
arithmetic (with the {\tt longlong} datatype),
to select the \Slong\ module when the {\tt configure} script detects
a processor for which an assembly version is available,
to select the {\tt Gmp} module if the \GMP\ library is present,
and to select the {\tt Big} modules for Caml and Ocaml if the
associated {\tt libnums} libraries are present.

\item{\tt-{}-disable-lang}\par\vskip-10pt\vskip0pt
\item{\tt-{}-disable-modules}\par\vskip-10pt\vskip0pt
\item{\tt-{}-disable-all}

Un-select all the languages, all the modules, or all the language,module
pairs not explicitly selected with a {\tt-{}-enable-xxx} option.

\item{\tt-{}-enable-processor=\it proc}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-sse2}\par\vskip-5pt\vskip0pt
\item{\tt-{}-disable-sse2}

Tell which processor is in the computer.
Valid choices are {\tt x86}, {\tt x86-64},
{\tt alpha}, {\tt ppc32}, {\tt generic} and {\tt unknown}.
When the processor is not specified or when it is declared as {\tt unknown},
the {\tt configure} script tries and guess which processor is actually
present by looking at the canonical name of the operating system and if
possible by browsing the {\tt /proc/cpuinfo} file.
When the processor is declared as {\tt generic}, the {\tt configure}
script makes no attempt to determine which processor is actually present,
and un-selects the \Slong\ module.
When compiling \Numerix\ for an {\tt x86} processor, you can enable or disable
the use of the {\tt SSE2} instruction set. The default is to disable it
for AMD processors because it has been observed that the {\tt SSE2} code
is slower than the regular one on the {\it sole} AMD processor that was tested
(AMD Athlon-XP-3000).

\item{\tt-{}-enable-mingw\rm,\tt\ \ \ -{}-disable-mingw}

Select or un-select the {\tt mingw} configuration of the Cygwin environment.
If this configuration is not selected (this is the default), the object files
compiled by {\tt gcc} will be linked with the {\tt cygwin1.dll} dynamic
library. If the {\tt mingw} configuration is selected, the object files
compiled by {\tt gcc} will be linked with the {\tt libgcc.a} and {\tt libmsvcrt.a}
static libraries. When to select or to un-select the {\tt mingw} configuration
depends on the compilers for the languages other than C, that is to say
Caml, Ocaml and Pascal, that are installed in the computer and for which
you want to compile a \Numerix\ interface. For instance, the Free-Pascal
compiler requires the selection of the {\tt mingw} configuration.

\item{\tt-{}-enable-shared\rm,\tt\ \ \ -{}-disable-shared}

Select or un-select the compilation of \Numerix\ into a set
of shared libraries. The default is to build static libraries.
The compilation of shared libraries was successfully tested with the
Linux and Digital Unix operating systems; it {\it does not work\/} with
the Windows and MacOSX ones.

\item{\tt-{}-enable-longlong\rm,\tt\   -{}-disable-longlong}\par\vskip-10pt\vskip0pt
\item{\tt-{}-enable-alloca\rm,\tt\ \ \ -{}-disable-alloca}

Enable or disable the use of the {\tt longlong} arithmetic for the \Dlong\ and \Slong\ modules,
and the use of the {\tt alloca} temporary memory allocation.
The default is to enable these facilities when they are available.

\end{description}


%-------------------- Configuration manuelle

\subsection{Manual configuration}

Normally the {\tt configure} script described in the previous section
should create suitable {\tt Makefile}, {\tt kernel/*/makefile}
and {\tt kernel/config.h} files. In case of trouble,
edit the {\tt Makefile} and {\tt kernel/config.h} files in order
to fix the values written by {\tt configure} when they are wrong.
After correction, you must re-create the
{\tt kernel/*/makefile} auxiliary files in order to take into account
the modifications and you must delete the files created during a
preceding compilation. To do this, launch:

\begin{verbatim}
        make makefiles
        make clean
\end{verbatim}

%-------------------- Makefile

\subsection{Editing the {\tt Makefile}}

Use the values $0$ or $1$ for boolean parameters ($1=$ true).
\begin{verbatim}
PROCESSOR = x86-sse2
\end{verbatim}
Specify the processor type: {\tt x86}, {\tt x86-sse2}, {\tt x86-64},
{\tt alpha}, {\tt ppc32} or {\tt generic}.
\sv

\begin{verbatim}
MAKE_C_LIB      = 1
MAKE_OCAML_LIB  = 1
MAKE_CAML_LIB   = 1
MAKE_PASCAL_LIB = 1
\end{verbatim}
Specify which interfaces you want.
\sv

\begin{verbatim}
USE_CLONG        = 1
USE_DLONG        = 1
USE_SLONG        = 1
USE_GMP          = 1
USE_CAML_BIGNUM  = 1
USE_OCAML_BIGNUM = 1
\end{verbatim}
Specify the modules to be compiled: severals modules
can be specified. The \Slong\ module cannot be compiled
on computers with a {\tt generic} processor.
The \Gmp\ and \Big\ modules can be compiled only if you have \GMP\ and \Bigint.
\sv

\begin{verbatim}
GCC = gcc -O2 -Wall
AR  = ar -rc
RANLIB = ranlib
\end{verbatim}
Specify the commands to launch to call the C compiler and the librarian.
You can add {\tt -Ixxx} and {\tt -Lxxx} directives if the compiler or
the linker fail to find some header files or libraries.
\sv

\begin{verbatim}
SHARED = 0
PIC    =
\end{verbatim}
Enter 1 for {\tt SHARED} if you want to build shared libraries and
specify in the {\tt PIC} variable which {\tt gcc} switch to use in order to
make position independent code: {\tt -fpic} is recommended but may not
work on some architectures, {\tt -fPIC} should work on all architectures
but may produce slower code. If you want static libraries and if the
processor is not of type {\tt x86-64}, enter {\tt SHARED = 0} and
leave the {\tt PIC} variable blank. With {\tt x86-64} processors,
{\tt PIC} should be set to one of {\tt -fpic} or {\tt -fPIC} regardless
of the value of {\tt SHARED}.
\sv

\begin{verbatim}
CAML_LIBDIR = /usr/local/lib/caml-light
CAMLC       = camlc
CAMLLIBR    = camllibr
CAMLMKTOP   = camlmktop
\end{verbatim}
Specify the Camllight directory and the commands to launch
to call the Camllight compiler,
the Camllight archiver and the Camllight toplevel compiler.
\sv

\begin{verbatim}
OCAML_LIBDIR = /usr/local/lib/ocaml
OCAMLC       = ocamlc
OCAMLOPT     = ocamlopt
OCAMLMKTOP   = ocamlmktop
OCAMLMKLIB   = ocamlmklib
\end{verbatim}
Specify the Ocaml directory and the commands to launch
to call the Ocaml compiler, the Ocaml optimizing compiler,
the Ocaml toplevel compiler and the Ocaml library generator.
\sv

\begin{verbatim}
PASCAL = gpc
PC     = gpc
\end{verbatim}
Specify which Pascal compiler to use ({\tt fpc} or {\tt gpc})
and the command to launch for this compiler.
\sv

\begin{verbatim}
INSTALL_LIB     = $(HOME)/lib
INSTALL_INCLUDE = $(HOME)/include
INSTALL_BIN     = $(HOME)/bin
\end{verbatim} %$
Specify in which directories the compiled libraries,
the header files and the binaries
should be installed.

\begin{verbatim}
C_INSTALL_BIN          = $(INSTALL_BIN)
C_INSTALL_LIB          = $(INSTALL_LIB)
C_INSTALL_INCLUDE      = $(INSTALL_INCLUDE)

CAML_INSTALL_BIN       = $(INSTALL_BIN)
CAML_INSTALL_LIB       = $(INSTALL_LIB)
CAML_INSTALL_INCLUDE   = $(INSTALL_INCLUDE)

OCAML_INSTALL_BIN      = $(INSTALL_BIN)
OCAML_INSTALL_LIB      = $(INSTALL_LIB)
OCAML_INSTALL_INCLUDE  = $(INSTALL_INCLUDE)

PASCAL_INSTALL_BIN     = $(INSTALL_BIN)
PASCAL_INSTALL_LIB     = $(INSTALL_LIB)
PASCAL_INSTALL_INCLUDE = $(INSTALL_INCLUDE)
\end{verbatim}
By default the {\tt INSTALL\_BIN},
{\tt INSTALL\_LIB} and {\tt INSTALL\_INCLUDE} directories are used for all languages.
You can define a different directory set for each language
by modifying the corresponding parameters.
Note that the values of {\tt OCAML\_INSTALL\_LIB} and {\tt CAML\_INSTALL\_LIB}
are hard-coded into the {\tt ocamlnumx} and
{\tt camlnumx} toplevels so that these toplevels can find
the {\tt numerix.cmi} and {\tt numerix.zi} compiled interfaces by themselves.
Therefore, if you want to move these directories, you will need to
recompile {\tt camlnumx} and {\tt ocamlnumx}.

%-------------------- kernel/config.h

\subsection{Editing the {\tt kernel/config.h}}

This file contains internal settings for the C/assembly kernel of \Numerix.
Normally it is created by the {\tt configure} script with the help of the
informations given or found on the processor and the possibility
to use the {\tt alloca} function and the {\tt long long} arithmetic.
When {\tt configure} detects wrong informations, use the
{\tt -{}-enable\_xxx} and {\tt -{}-disable\_xxx} options described in
section~\ref{autoconf} in order to force correct values.
If {\tt configure} fails to write a {\tt kernel/config.h} file,
then copy one of the
{\tt generic.h}, {\tt x86.h}, {\tt x86-sse2.h}, {\tt x86-64.h}, {\tt ppc32.h} or {\tt alpha.h} file
in the {\tt config} directory
onto the {\tt kernel/config.h} file, and edit this last file in order to
specify the bit length of a machine word
and if the {\tt alloca} function and the {\tt long long} arithmetic can be used:

\begin{verbatim}
/* Machine word size */
#define bits_@machine_word_size@

/* Memory allocation strategy  */
@use_alloca@

/* Double-long available */
@have_long_long@
\end{verbatim}

\noindent
Replace the \verb?@machine_word_size@? string with $32$ or $64$,
replace \verb?@use_alloca@? with \verb?#define use_alloca? or
\verb?#undef use_alloca?,
replace \verb?@have_long_long@? with \verb?#define have_long_long? or
\verb?#undef have_long_long?.

                          %+---------------+
                          %|  Compilation  |
                          %+---------------+

\section{Compilation}

After the automatic or manual configuration step
you can launch the compilation. The targets are:
\begin{description}
\item{\tt lib}~:\par
compile the libraries and the interface files;

\item{\tt examples}~:\par
compile the examples;

\item{\tt test}~:\par
execute each example program with the {\tt -test} option;

\item{\tt install}~:\par
copy the libraries, the header files and the binaries
in the directories specified by the
{\tt INSTALL\_xxx} variables;

\item{\tt makefiles}~:\par
rewrite the {\tt kernel/*/makefile} files in order to take into account
the modifications made into the {\tt Makefile} file;

\item{\tt clean}~:\par
delete all compiled files.
\end{description}
\sv

Successively launch:

\begin{verbatim}
        make lib      2>&1 | tee liblog
        make exemples 2>&1 | tee exlog
        make test     2>&1 | tee testlog
\end{verbatim}

There should be neither compile error nor warning.
If there are some and if you cannot solve the problem on your own,
please send the
{\tt conflog}, {\tt liblog}, {\tt exlog} and {\tt testlog} log files
to \verb?michel.quercia@prepas.org? for diagnosis.
If you have faced some
problems that you have been able to fix alone, please let me know so
that I may modify the faulty files.
\sv

If the compilation and the tests have been successful, you can
install the \Numerix\ library with the command:


\begin{verbatim}
        make install 2>&1 | tee inslog
\end{verbatim}

\noindent
Refer to page~\pageref{filelist} for the list of the files to be installed.
The files actually installed depend on the modules and languages selected.

\begin{figure}[p]
\label{filelist}
\caption{list of the \Numerix\ files to install}
\sv
\leftskip=-2cm\multiply\hsize by 2\small\tt\begin{tabular}{|l|l|l|l|}
\hline
\$(C\_INSTALL\_LIB)    &\$(CAML\_INSTALL\_LIB)    &\$(OCAML\_INSTALL\_LIB)    &\$(PASCAL\_INSTALL\_LIB) \\
\hline
libnumerix-c.a/so      &libnumerix-caml.a/so      &libnumerix-ocaml.a/so      &                 \\ 
                       &                          &dllnumerix-ocaml.so        &                 \\
\hline
                       &numerix.zo                &numerix.a                  &clong.o          \\
                       &camlnumx                  &numerix.cma                &clong.ppu/gpi    \\
                       &big.zi                    &numerix.cmi                &dlong.o          \\
                       &clong.zi                  &numerix.cmxa               &dlong.ppu/gpi    \\
                       &dlong.zi                  &                           &slong.o          \\
                       &gmp.zi                    &                           &slong.ppu/gpi    \\
                       &slong.zi                  &                           &                 \\
                       &infbig.zi                 &                           &                 \\
                       &infclong.zi               &                           &                 \\
                       &infdlong.zi               &                           &                 \\
                       &infgmp.zi                 &                           &                 \\
                       &infslong.zi               &                           &                 \\
\hline
\noalign{\sv}
\hline
\$(C\_INSTALL\_INCLUDE)    &\$(CAML\_INSTALL\_INCLUDE)    &\$(OCAML\_INSTALL\_INCLUDE)    &\$(PASCAL\_INSTALL\_INCLUDE) \\
\hline
numerix.h              &big.ml                    &numerix.ml                 &clong.p          \\
		       &big.mli     		  &numerix.mli		      &dlong.p		\\
		       &clong.ml    		  &			      &slong.p		\\
		       &clong.mli   		  &			      &			\\
		       &dlong.ml    		  &			      &			\\
		       &dlong.mli   		  &			      &			\\
		       &gmp.ml      		  &			      &			\\
		       &gmp.mli     		  &			      &			\\
		       &slong.ml    		  &			      &			\\
		       &slong.mli   		  &			      &			\\
		       &infbig.ml   		  &			      &			\\
		       &infbig.mli  		  &			      &			\\
		       &infclong.ml 		  &			      &			\\
		       &infclong.mli		  &			      &			\\
		       &infdlong.ml 		  &			      &			\\
		       &infdlong.mli		  &			      &			\\
		       &infgmp.ml   		  &			      &			\\
		       &infgmp.mli  		  &			      &			\\
		       &infslong.ml 		  &			      &			\\
		       &infslong.mli              &                           &                 \\                 

\hline
\noalign{\sv}
\hline
\$(C\_INSTALL\_BIN)    &\$(CAML\_INSTALL\_BIN)    &\$(OCAML\_INSTALL\_BIN)    &\$(PASCAL\_INSTALL\_BIN) \\
\hline
                       &                          &ocamlnumx                  &                 \\
\hline
\end{tabular}
\end{figure}
\sv

Now the installation is finished and you can enjoy the
multi-precision programming.
The user guide that you are presently reading
is available in the {\tt doc/english} subdirectory in PDF and \LaTeX\  formats
(files {\tt numerix.pdf} and {\tt numerix.tex}).


                   %+----------------------------+
                   %|  Description des exemples  |
                   %+----------------------------+

\section{Description of the examples}

The {\tt c},{\tt caml},{\tt ocaml} and {\tt pascal} sub-directories
of the {\tt exemples} directory 
contain various programs using \Numerix.
To compile these programs launch the command:

\begin{verbatim}
        make examples
\end{verbatim}

Concerning the examples in C, Caml and Pascal, a
{\tt example.}{\it ext} source file is compiled in as many executables as there
are available big integer modules for this language.
Each executable is named {\tt example-x} where {\tt x} is the initial letter
of the big integer module used. 
Concerning the examples in Ocaml, a {\tt example.ml} source file
is compiled in two executables: {\tt example} with the {\tt ocamlc} compiler
and {\tt example-opt} with the {\tt ocamlopt} compiler. The choice
of a big integer module is done at run-time with a
{\tt -e xxx} option as described in section
{\bf \ref{selmodule} Run-time selection of a module},
page~\pageref{selmodule}.

%-------------------- chrono

\subsection{chrono}

Speed measurement of the different libraries (C interface only).
This program chooses random big integers of sizes $n$ and $2n$ bits and
measures the time of various operations between these integers:
\sv

\verb?mul    ? multiplication $n$ bits by $n$ bits;\par
\verb?sqr    ? square of a $n$ bit integer;\par
\verb?quomod ? division with remainder $2n$ bits by $n$ bits;\par
\verb?quo    ? division without remainder $2n$ bits by $n$ bits;\par
\verb?sqrt   ? square root of a $2n$ bit integer;\par
\verb?gcd    ? gcd of two $n$ bit integers;\par
\verb?gcd_ex ? gcd and B\'ezout coefficients of two $n$ bit integers;\par
\verb?all    ? all the operations above.

\sv\noindent
Specify on the command line a value for $n$ and which operations
to do among {\tt-mul}, {\tt-sqr}, {\tt-quomod}, {\tt-quo}, {\tt-sqrt}, {\tt-gcd} and
{\tt-gcd\_ex}. You can specify a repetition count with the 
{\tt -r \it r} option, in this case each operation is repeated $r$ times.

\begin{verbatim}
> exemples/c/chrono-s -all 1000000 -r 10
    0.01     0.01 d�but
    0.30     0.29 mul
    0.51     0.21 sqr
    1.30     0.79 quomod
    1.95     0.65 quo
    2.61     0.66 sqrt
    9.82     7.21 gcd
   20.79    10.97 gcd_ex
> exemples/c/chrono-g -all 1000000 -r 10
    0.00     0.00 d�but
    0.42     0.42 mul
    0.75     0.33 sqr
    2.61     1.86 quomod
    4.47     1.86 quo
    5.89     1.42 sqrt
   67.05    61.16 gcd
  191.65   124.60 gcd_ex
>
\end{verbatim}

So on the test computer (PC-Linux, Pentium-4, 3Ghz) with the \Slong\ module,
the time for multiplying two one million bit numbers
is $29$ milliseconds, the time for squaring
a one million bit number is $21$ milliseconds, and so on.
The second test shows the corresponding times for the
\GMP-4.2.1 library on the same computer.

%-------------------- digits

\subsection{digits}

Search the smallest power of a number $a$ for which the decimal expansion
begins with a given digit sequence (Ocaml interface only).
Formally, the program searches a minimal $(x,y)$ pair of natural integers
such that ${c < a^x/10^y < c+1}$ where $c$ is the number designated by the
digit sequence. The search is done with $n$ bit
approximations of $\ln(a)$, $\ln(10)$, $\ln(c)$ and $\ln(c+1)$
where $n$ is determined from $a$ and $c$.
If the search is unsuccessful or if the solution found
cannot be granted minimal then $n$ is doubled and
the computation is restarted.
The command line parameters are in this order:
the base $a$, the digit sequence $c$, and the maximum number of trials.

\begin{verbatim}
> exemples/ocaml/digits 3 1234567890 1
5399108054 2576029200
> exemples/ocaml/digits 3 1234567890 2
2440080224 1164214129 (minimal)
>
\end{verbatim}

So $3^{5399108054} \approx 1234567890\times 10^{2576029200}$, solution found
in the first trial, and $3^{2440080224} \approx 1234567890\times 10^{1164214129}$, solution found
in the second trial. The second solution is minimal.

%-------------------- pi

\subsection{pi}

Compute the $n$ first digits of $\pi$ (C, Caml, Ocaml and Pascal interfaces).
This program implements the approximate computation of $\pi$ described in
the {\tt BigNum} reference manual
({\it The Caml Numbers Reference Manual}, Inria, RT-0141) with a
binary summation algorithm.
Specify on the command line the number $n$ and the computation options:
\sv
\halign{\indent\tt# &#\cr
-d      &print the steps and the computing time for each step.\hfil\cr
-noprint&do not convert the number into a decimal string.\hfil\cr
-skip   &convert the number into a decimal string, but display only the\cr
        &beginning and the end of the string.\hfil\cr
-gcd    &reduce the fraction returned by the summation step before com-\cr
        &puting the quotient (one is advised against this reduction step\cr
        &because it takes longer than the time saved by doing a shorter\cr
        &division).\hfil\cr
}\sv

\begin{verbatim}
> exemples/c/pi-s 1000000 -d -skip
    0.00     0.00 start
    0.04     0.04 puiss-5
    0.38     0.34 sqrt
    3.01     2.63 series lb=6875847
    3.46     0.45 quotient
    4.21     0.75 conversion
3.
14159 26535  89793 23846  26433 83279  50288 41971  69399 37510
... (19998 lines omitted)
56787 96130  33116 46283  99634 64604  22090 10610  57794 58151
> exemples/c/pi-g 1000000 -d -skip
    0.00     0.00 start
    0.06     0.06 puiss-5
    0.82     0.76 sqrt
    4.58     3.76 series lb=6875847
    5.97     1.39 quotient
    7.38     1.41 conversion
3.
14159 26535  89793 23846  26433 83279  50288 41971  69399 37510
... (19998 lines omitted)
56787 96130  33116 46283  99634 64604  22090 10610  57794 58151
>
\end{verbatim}

\noindent Please note the the $\pi$-computation program given as a \GMP\ example at the URL

\sv\verb?http://www.swox.com/gmp/pi-with-gmp.html?

\sv\noindent uses a faster algorithm. With \GMP, it calculates on the same
computer the first million digits of $\pi$ in 5.2 seconds.

%-------------------- shanks

\subsection{shanks}

Compute the modular square root $b$ of a number $a$ modulo an odd prime $p$.
(C, Caml, Ocaml and Pascal interfaces).
Specify on the command line the values for $a$ and $p$ with
{\tt -p }{\it value} and {\tt -a }{\it value} options. If either value
is not specified then the corresponding number is chosen at random.
In this case, the \hbox{\tt -bits \it bits} option specifies the bit size
for the random numbers.

\begin{verbatim}
> exemples/pascal/shanks-s -bits 200
p = 1176779509942443506598255665583537849578666974235481551059793
a = 437108932652457493069203833813802572416560291357556696448749
b = 454514942632629769505137250184398999851276948222103958331459
>
\end{verbatim}


%-------------------- simple

\subsection{simple}
Simple demonstration program (C, Caml, Ocaml and Pascal interfaces).
This program shows how to use the various \Numerix\ interfaces. It computes the $n$ first digits
of ${(\sqrt3 + \sqrt2)/(\sqrt3 - \sqrt2)}$.

%-------------------- sqrt-163

\subsection{sqrt-163}

Compute $\lfloor 10^ne^{\pi\sqrt{163}}\rfloor$
where $n$ is given on the command line
(Ocaml interface only).

\begin{verbatim}
> exemples/ocaml/sqrt-163-opt 10
262537412640768743.9999999999
\end{verbatim}

Note that the result displayed proves that
$e^{\pi\sqrt{163}}$ is not an integer: if there was an infinity of {\tt 9}
after those displayed then the program could not have determined
the floor part it was asked for.
 

%-------------------- prime-test
\subsection{prime-test}

\label{prime-test}
This program is available with the C interface only. Its purpose is to
check that no composite number $n$ such that $|n|\le 4^x$ passes the
primality test implemented in the \Clong, \Dlong, \Slong\ modules
(refer to section {\bf \ref{internal-prime-test} Primality} page~\pageref{internal-prime-test} for the description of this test).
In order to achieve that purpose, the program calculates the list of primes $p$
in the $[\![2^s,2^x]\!]$ range, determines for each $p$ which
discriminants $d$ may be used when testing an
integer $n$ divisible by $p$ (with $|n| \le 4^x$), then searches for
each $(p,d)$ pair the possible integers $q$ such that $q$ has no divisor
not greater that $2^s$ and $(1\pm\sqrt d)^{pq+1} \equiv 1-d\bmod {pq}$.
The most useful options of {\tt prime-test} are the following:
\sv
\halign{\indent\tt# &$#$&: #\hfil\cr
-s&s& small primes are primes not greater than $2^s$;\cr
-x&x& search for composites not greater than $4^x$;\cr
-k&k& use only the $k$ first Selfridge discriminants;\cr
-c&c& use a sieve of $2^c$ bits to search the candidates $q$;\cr
-h& & display all options and all default values.\cr
}
\sv

\begin{shelllisting}
\begin{verbatim}
> exemples/c/prime-test-s -x 20
    0.18     0.18 171 primes <= 2^10, 81853 primes between 2^10 and 2^20
    0.22     0.04 index plist on 17 first Jacobi symbols
   75.36    75.14 87185116 numbers <= 2^30 without small divisors
   75.40     0.04 unsort prime list
  166.21    90.81 245575 composites tested
>
\end{verbatim}
\end{shelllisting}

The first phase calculates the list of all primes not greater than $2^s$
and the list of all primes in the $[\![2^s,2^x]\!]$ range.
The second phase sorts the second list according to the first Jacobi symbols
in the Selfridge discriminant sequence.
The third phase scans all the integers $q$ between $2^s$ and
$4^x/2^s$ with no divisor not greater than $2^s$ and looks for the primes
$p\le 2^x$ having the same first Jacobi symbols as $q$.
If all the Jacobi symbols of $p$ and $q$ are equal, it is checked that
$pq$ is a square, otherwise $p$ and $q$ are displayed and the program is stopped.
When they are not all equal, the discriminant that will be used
for testing the primality of $pq$ is recorded (the test is not done at this point,
it will be done if necessary during the fifth phase).
The number of integers $q$ examined is displayed with the timing of this phase.
The fourth phase reorders the $p$-list in ascending order; this is not useful
for the search, but it facilitates the control of the results.
The fifth phase examines each $(p,d)$ pair determined during the third
phase and searches for which integers $q$ one has
$(1\pm\sqrt d)^{pq+1} \equiv 1-d\bmod p$ (when there are solutions, they form
an arithmetic sequence and the parameters of this sequence can be computed
knowing  $p$ and $d$). Then, all integers $q$
belonging to the arithmetic sequence are tested, as long as $pq \le 4^x$,
and if $(1\pm\sqrt d)^{pq+1} \equiv 1-d\bmod {pq}$ the values of $p,d,q$
are displayed.
The total count of integers $q$ examined is displayed with the timing of the
phase.
\sv

The above example shows the unsuccessful search for composites
not greater than $4^{20}$ passing the primality test. The same program,
with the {\tt -x} $25$ option, runs in approximately $94\,000$ seconds,
that is to say 26 hours and 6 minutes on the same computer (PC-Linux, Pentium-4, 3Ghz) and
finds no composite not greater than $4^{25}$ passing the primality test,
so this test is accurate up to this limit at least. By extrapolation, a search
with {\tt -x} $30$ should take approximately 4 years in order to validate the
test up to $4^{30}$; this was not attempted. Please note that one can
easily find composites passing the test because of a lack of discriminant:
with $s=10$ the number
$n = 4 + \prod_{p\le2^{10}}p\approx 1.4\times2^{1418}$ is not a square, it is a composite,
and {\tt isprime}$(n)$ returns the {\tt Unknown} value.


%-------------------- cmp,rcheck

\subsection{cmp, rcheck}

These programs are available with the Ocaml interface only.
{\tt cmp} makes a sequence of random operations with
random integer operands, so as to detect \Numerix\ internal bugs.
Two big integer modules must be specified on the command line
so as to compare the results returned by each module.
The other command line options are the following:
\sv
\halign{\indent\tt# &\it#&: #\hfil\cr
-n &bits      &specify the bit size for the operands;\cr
-op&operation &specify one operation to check;\cr
-r &count     &specify the number of trials to do;\cr
-s &seed      &seed for the pseudo-random generator;\cr
-h            &&display the list of operations.\cr
}\sv

\begin{verbatim}
> exemples/ocaml/cmp -n 1000 -r 10000 -e clong -e gmp
Cmp(Clong,Gmp)
i=10000
>
\end{verbatim}

$10000$ operations done without detecting any error.
\sv

{\tt rcheck} is is a test program for the real-valued functions of
the {\tt Rfuns} functor. The program makes a sequence
of computations for each of these functions and prints on the standard output stream
{\tt MuPAD} instructions to check the results.
The command line options are:
\sv

\halign{\indent\tt# &$#$&: #\hfil\cr
-bits &p&specifies the bit sizes for the $a$ and~$b$ operands;\cr
-n &n&specifies the precision for the {\tt xxx} functions of {\tt Rfuns};\cr
-c &c&specifies the $c$ coefficient for the {\tt r\_xxx} functions;\cr
-niter &i&specifies the number of trials to do for each function;\cr
-seed &s&seed for the pseudo-random generator.\cr
}\sv

\begin{shelllisting}
\begin{verbatim}
> exemples/ocaml/rcheck -niter 100 -bits 200 -c 1000000000000 | mupad -P pe

   *----*    MuPAD 3.1.1 -- The Open Computer Algebra System
  /|   /|
 *----* |    Copyright (c)  1997 - 2005  by SciFace Software
 | *--|-*                   All rights reserved.
 |/   |/
 *----*      Licensed to:   7SPE175


                             c = 1000000000000

                                   x = 1

    u = -1301784500998197302497576591391465824801563379443885192200172

      v = 21215339524597729401701905709530780600629394731248058072755

                                  f = exp

                                 r = ceil

>
\end{verbatim}
\end{shelllisting}

Only one error was detected: \Numerix\ returns the result $x=1$
for the value of $\lceil c\times\exp(u/v) \rceil$ whereas MuPAD
finds another result (not displayed).
After verification it turns out that MuPAD was wrong and
\Numerix\ was right.
